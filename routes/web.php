<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\LanguageSwitcherController;
use App\Http\Controllers\LicenseController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\QuestionnaireController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\UniversityController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['language'])->group(function () {
    Route::get('/', [CompanyController::class, 'index']);
    Route::resource('categories', CategoryController::class)->except('edit');
    Route::resource('certificates', CertificateController::class)->except('edit', 'show');
    Route::resource('comments', CommentController::class)->only('destroy');
    Route::resource('companies', CompanyController::class)->only('index');;
    Route::resource('licenses', LicenseController::class)->only('update', 'store', 'create');
    Route::resource('partners', PartnerController::class)->only('index');
    Route::resource('questionnaires', QuestionnaireController::class)->only('update', 'store', 'create');
    Route::resource('tests', TestController::class)->only('index', 'store');
    Route::resource('trainings', TrainingController::class)->only('index');
    Route::resource('universities', UniversityController::class)->except('index', 'show');
    Route::resource('users', UserController::class);
    Route::resource('works', WorkController::class)->except('index', 'show');
    Route::resource('types', \App\Http\Controllers\TypeController::class)->only('show');
    Route::resource('answers', \App\Http\Controllers\AnswerController::class)->only('index');
    Route::resource('meetings', \App\Http\Controllers\MeetingController::class)->only('index', 'create');
    Route::resource('books', \App\Http\Controllers\BookController::class)->only('index');
    Route::resource('suggestions', \App\Http\Controllers\SuggestionController::class)->only('index');
    Route::resource('likes', \App\Http\Controllers\LikeController::class)->only('store');
    Route::resource('votes', \App\Http\Controllers\VoteController::class)->only('store', 'destroy');
    Route::resource('subcomments', \App\Http\Controllers\SubcommentController::class)->only('destroy', 'store');
    Route::get('/trainings/{category}', [TrainingController::class, 'show'])->name('trainings.show');
    Route::post('/trainings/next', [TrainingController::class, 'showNext'])->name('trainings.next');
    Route::post('/trainings/previous', [TrainingController::class, 'showPrevious'])->name('trainings.previous');
    Route::get('/tests/category/{category}', [TestController::class, 'create'])->name('tests.create');
    Route::post('/callback/{user?}', [\App\Http\Controllers\CallbackController::class, 'store'])->name('callbacks.store');
    Route::delete('likes/destroy', [\App\Http\Controllers\LikeController::class, 'destroy'])->name('likes.destroy');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::post('users/{user}/comments', [CommentController::class, 'store'])->name('comments.store');
    Route::post('/zoom/store', [\App\Http\Controllers\ZoomController::class, 'store'])->name('zoom.store');
    Route::delete('/zoom/{meeting}', [\App\Http\Controllers\ZoomController::class, 'destroy'])->name('zoom.destroy');
    Route::get('/user/mediators', [UserController::class, 'mediators'])->name('users.mediators');
    Auth::routes(['verify' => true]);
});

Route::get('language/{locale}', [LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')
    ->where('locale', 'en|ru');

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::get('/sms', function () {
    \Illuminate\Support\Facades\Notification::send(auth()->user(), new \App\Notifications\WelcomeNotification());
});
Route::get('/auth/facebook', [\App\Http\Controllers\SocialController::class, 'facebookRedirect'])->name('auth.facebook');
Route::get('/auth/facebook/callback', [\App\Http\Controllers\SocialController::class, 'loginWithFacebook'])->name('loginWithFacebook');

