<?php

use App\Http\Controllers\Api\AnswerController;
use App\Http\Controllers\Api\BookController;
use App\Http\Controllers\Api\CallbackController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CertificateController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\LicenseController;
use App\Http\Controllers\Api\LikeController;
use App\Http\Controllers\Api\MeetingController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\PartnerController;
use App\Http\Controllers\Api\QuestionnaireController;
use App\Http\Controllers\Api\SubcommentController;
use App\Http\Controllers\Api\SuggestionController;
use App\Http\Controllers\Api\TestController;
use App\Http\Controllers\Api\TrainingController;
use App\Http\Controllers\Api\TypeController;
use App\Http\Controllers\Api\UniversityController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\VoteController;
use App\Http\Controllers\Api\WorkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::middleware('language')->group(function (){
//    Route::apiResource('users', UserController::class)->only(['index', 'show']);
//    Route::get('/international/users/', [UserController::class, 'international']);
//    Route::get('/top_users', [UserController::class, 'topUser'])->name('users.top');
//    Route::apiResource('books', BookController::class)->only('index');
//    Route::apiResource('categories', CategoryController::class)->only('index', 'show');
//    Route::apiResource('types', TypeController::class)->only('show');
//    Route::apiResource('news', NewsController::class)->only(['index', 'show']);
//    Route::apiResource('suggestions', SuggestionController::class)->only('index');
//    Route::apiResource('partners', PartnerController::class)->only('index', 'show');
//    Route::apiResource('votes', VoteController::class);
//
//    Route::middleware('auth:api')->group(function () {
//        Route::apiresource('licenses', LicenseController::class);
//        Route::apiResource('answers', AnswerController::class)->only( 'index');
//        Route::apiResource('tests', TestController::class)->only( 'store');
//        Route::apiResource('works', WorkController::class)->only('index', 'show', 'store', 'destroy', 'update');
//        Route::apiResource('universities', UniversityController::class)->only('index', 'store', 'destroy', 'update');
//        Route::apiResource('certificates', CertificateController::class)->only('index', 'store', 'destroy','update');
//        Route::apiResource('certificates', CertificateController::class)->only('index', 'store', 'destroy','update');
//        Route::apiResource('questionnaires', QuestionnaireController::class)->only('index', 'store', 'update');
//        Route::apiResource('countries', CountryController::class)->only('index');
//        Route::apiResource('subcomments', SubcommentController::class)->only('store', 'destroy');
//        Route::apiResource('comments', CommentController::class)->only('store', 'destroy');
//        Route::post('/trainings/next', [TrainingController::class, 'next'])->name('trainings.next');
//        Route::post('/trainings/previous', [TrainingController::class, 'previous'])->name('trainings.previous');
//        Route::post('/trainings/', [TrainingController::class, 'index'])->name('trainings.index');
//        Route::apiResource('callbacks', CountryController::class)->only('index');
//        Route::get('/auth/categories', [CategoryController::class, 'getAuthCategories'])->name('auth.categories');
//        Route::delete('/categories/{category}', [CategoryController::class, 'destroy'])->name('destroy.categories');
//        Route::post('/categories', [CategoryController::class, 'store'])->name('post.categories');
//        Route::post('/first/question', [TestController::class, 'first']);
//        Route::get('/subcomments/{comment?}', [SubcommentController::class, 'index'])->name('subcomments.index');
//        Route::apiResource('likes', LikeController::class)->only('store');
//        Route::apiResource('meetings', MeetingController::class)->only('index', 'store', 'destroy');
//        Route::apiResource('suggestions', SuggestionController::class);
//        Route::get('/user/votes', [SuggestionController::class, 'getUser']);
//    });
//
//    Route::post('/callback/{user?}', [CallbackController::class, 'store'])->name('callbacks.store');
//    Route::post('register', [App\Http\Controllers\Api\PassportAuthController::class, 'register'])->name('register');
//    Route::post('login', [App\Http\Controllers\Api\PassportAuthController::class, 'login'])->name('login');
//});

