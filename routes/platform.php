<?php

declare(strict_types=1);


use App\Orchid\Screens\Category\CategoryEditScreen;
use App\Orchid\Screens\Category\CategoryListScreen;
use App\Orchid\Screens\Comment\CommentEditScreen;
use App\Orchid\Screens\Comment\CommentListScreen;
use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\Phone\PhoneEditScreen;
use App\Orchid\Screens\Phone\PhoneListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Questionnaire\QuestionnaireEditScreen;
use App\Orchid\Screens\Questionnaire\QuestionnaireListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\Subcomment\SubcommentEditScreen;
use App\Orchid\Screens\Subcomment\SubcommentListScreen;
use App\Orchid\Screens\University\UniversityListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use App\Orchid\Screens\Work\WorkEditScreen;
use App\Orchid\Screens\Work\WorkListScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Users'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > Users
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Example...
Route::screen('example', ExampleScreen::class)
    ->name('platform.example')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Example screen');
    });

Route::screen('example-fields', ExampleFieldsScreen::class)->name('platform.example.fields');
Route::screen('example-layouts', ExampleLayoutsScreen::class)->name('platform.example.layouts');
Route::screen('example-charts', ExampleChartsScreen::class)->name('platform.example.charts');
Route::screen('example-editors', ExampleTextEditorsScreen::class)->name('platform.example.editors');
Route::screen('example-cards', ExampleCardsScreen::class)->name('platform.example.cards');
Route::screen('example-advanced', ExampleFieldsAdvancedScreen::class)->name('platform.example.advanced');

Route::screen('work/{work?}', WorkEditScreen::class)->name('platform.works.edit');

Route::screen('works', WorkListScreen::class)
    ->name('platform.works.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.index')->push('Works');
    });


Route::screen('phone/{phone?}', PhoneEditScreen::class)->name('platform.phones.edit');

Route::screen('phones', PhoneListScreen::class)
    ->name('platform.phones.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.index')->push('Phones');
    });

Route::screen('links', \App\Orchid\Screens\Links\LinksListScreen::class)->name('platform.links.list');
Route::screen('link/{link?}', \App\Orchid\Screens\Links\LinksEditScreen::class)->name('platform.links.edit');

Route::screen('licenses', \App\Orchid\Screens\License\LicenseListScreen::class)->name('platform.licenses.list');
Route::screen('license/{license?}', \App\Orchid\Screens\License\LicenseEditScreen::class)->name('platform.licenses.edit');


Route::screen('books', \App\Orchid\Screens\Book\BookListScreen::class)->name('platform.books.list');
Route::screen('book/{book?}', \App\Orchid\Screens\Book\BookEditScreen::class)->name('platform.books.edit');


Route::screen('trainings', \App\Orchid\Screens\Training\TrainingListScreen::class)->name('platform.trainings.list');
Route::screen('training/{training?}', \App\Orchid\Screens\Training\TrainingEditScreen::class)->name('platform.trainings.edit');

Route::screen('partners', \App\Orchid\Screens\Partner\PartnerListScreen::class)->name('platform.partners.list');
Route::screen('partner/{partner?}', \App\Orchid\Screens\Partner\PartnerEditScreen::class)->name('platform.partners.edit');

Route::screen('newses', \App\Orchid\Screens\News\NewsListScreen::class)->name('platform.newses.list');
Route::screen('news/{news?}', \App\Orchid\Screens\News\NewsEditScreen::class)->name('platform.newses.edit');

Route::screen('universities', \App\Orchid\Screens\University\UniversityListScreen::class)->name('platform.universities.list');
Route::screen('university/{university?}', \App\Orchid\Screens\University\UniversityEditScreen::class)->name('platform.universities.edit');

Route::screen('categories', CategoryListScreen::class)->name('platform.categories.list');
Route::screen('category/{category?}', CategoryEditScreen::class)->name('platform.categories.edit');

Route::screen('suggestions', \App\Orchid\Screens\Suggestion\SuggestionListScreen::class)->name('platform.suggestions.list');
Route::screen('suggestion/{suggestion?}', \App\Orchid\Screens\Suggestion\SuggestionEditScreen::class)->name('platform.suggestions.edit');

Route::screen('callbacks', \App\Orchid\Screens\Callback\CallbackListScreen::class)->name('platform.callbacks.list');
Route::screen('callback/{callback?}', \App\Orchid\Screens\Callback\CallbackEditScreen::class)->name('platform.callbacks.edit');

Route::screen('trainings', \App\Orchid\Screens\Training\TrainingListScreen::class)->name('platform.trainings.list');
Route::screen('training/{training?}', \App\Orchid\Screens\Training\TrainingEditScreen::class)->name('platform.trainings.edit');


Route::screen('test', \App\Orchid\Screens\Test\TestListScreen::class)
    ->name('platform.tests.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.index')->push('Test');
    });

Route::screen('test/{test}', \App\Orchid\Screens\Test\TestShowScreen::class)->name('platform.tests.show');

Route::screen('questionnaire/{questionnaire?}', QuestionnaireEditScreen::class)
    ->name('platform.questionnaires.edit');

Route::screen('questionnaires', QuestionnaireListScreen::class)
    ->name('platform.questionnaires.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.index')->push('Questionnaires');
    });



Route::screen('comment/{comment?}', CommentEditScreen::class)
    ->name('platform.comments.edit');

Route::screen('comments', CommentListScreen::class)
    ->name('platform.comments.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.index')->push('Comments');
    });

Route::screen('question/{question?}', \App\Orchid\Screens\Question\QuestionEditScreen::class)
    ->name('platform.questions.edit');

Route::screen('questions', \App\Orchid\Screens\Question\QuestionListScreen::class)
    ->name('platform.questions.list')
    ->breadcrumbs(function (Trail $trail) {
        return $trail->parent('platform.index')->push('Questions');
    });

Route::screen('works', \App\Orchid\Screens\Work\WorkListScreen::class)->name('platform.works.list');
Route::screen('work/{work?}', \App\Orchid\Screens\Work\WorkEditScreen::class)->name('platform.works.edit');

Route::screen('types', \App\Orchid\Screens\Type\TypeListScreen::class)->name('platform.types.list');
Route::screen('type/{type?}', \App\Orchid\Screens\Type\TypeEditScreen::class)->name('platform.types.edit');

Route::screen('/companies/list', \App\Orchid\Screens\Company\CompanyListScreen::class)->name('platform.companies');
Route::screen('/companies/{company?}', \App\Orchid\Screens\Company\CompanyScreen::class)->name('platform.company');

Route::screen('/countries/list', \App\Orchid\Screens\Country\CountryListScreen::class)->name('platform.countries.list');
Route::screen('/countries/{country?}', \App\Orchid\Screens\Country\CountryEditScreen::class)->name('platform.countries.edit');

Route::screen('/certificates/list', \App\Orchid\Screens\Certificate\CertificateListScreen::class)->name('platform.certificates');
Route::screen('/certificates/{certificate?}', \App\Orchid\Screens\Certificate\CertificateScreen::class)->name('platform.certificate');

Route::screen('comment/{comment?}', CommentEditScreen::class)->name('platform.comments.edit');
Route::screen('comments', CommentListScreen::class)->name('platform.comments.list');

Route::screen('subcomment/{subcomment?}', SubcommentEditScreen::class)->name('platform.subcomments.edit');
Route::screen('subcomments', SubcommentListScreen::class)->name('platform.subcomments.list');

