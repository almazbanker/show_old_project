<p align="center">
<a href="https://reactjs.org" target="_blank">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png" width="200" alt="React">
</a>
<a href="https://laravel.com" target="_blank">
<img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="500" alt="Laravel">
</a>
<a href="https://www.postgresql.org" target="_blank">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/540px-Postgresql_elephant.svg.png" width="200" alt="Postgres">
</a>
</p>

##### Склонировать проект себе с помощью https://gitlab.com/almazbanker/show_old_project.git
##### В проект используется PHP, JavaScript, PostgreSQL поэтому у вас в системе должна быть установлены следующее: PHP, NodeJS, PostgreSQL

##### Для установки и запуска приложения нужно написать на терминал в корневом папке

```
composer update
php artisan storage:link
php artisan passport:install
php artisan migrate --seed
cp .env.example .env
php artisan serve
npm i && npm run dev
cd frontend
npm i && npm start
```
