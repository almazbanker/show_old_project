<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;


class LicenseTest extends DuskTestCase
{


    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testAuthUserCanSeeLicenseCreatePage()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
            ;
        });
    }



    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNotAuthUserCanNotSeeCreateLicensesPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertDontSee('Edit Licenses')
               ;
        });
    }


    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testStartDateIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('start_date', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }


    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testFinishDateIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }



    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testDescriptionIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('description', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }



    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNumberIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }





    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNumberAndFinishDateAndDescriptionStartDateIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->type('description', '')
                ->waitForText('This field is required');
        });
    }



    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testDescriptionHasMinCharactersCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('description', 'a')
                ->type('start_date', '')
                ->waitForText('This field must be at least 5 characters');
        });
    }







    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testAtLeast5CharactersNumberAndStartDateIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '11')
                ->type('start_date', '')
                ->waitForText('This field must be at least 5 characters');
        });
    }




    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNoMoreThan255CharactersCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('description', 'qweqsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssswewqeqweqeqwewqeqewqeqeqeqweewqeqwqeqwweqwewqeqweqweqeqweqweqwewqeqweqweqweqweqweqweqweqweqweqweqwewqeqweqweqweqweqweqweqweqweqweqweqweqweqweqweqweqeqweqweqwewqewqeqweqwewqeqweqweqwwqewqeqwewqeqweqweqweqeqwewqeqeqweqweqwewq')
                ->type('start_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }





    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNoMoreThan255CharactersNumberCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '1')
                ->type('start_date', '')
                ->waitForText('This field must be at least 5 characters');
        });
    }




    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testWrongFormatAndFinishDateIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('start_date', '2012.12.015')
                ->type('finish_date', '')
                ->waitForText('This field does not match the format');
        });
    }






    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testWrongFormat2AndDescriptionIsRequiredCreate()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('finish_date', '2012.12.015')
                ->type('description', '')
                ->waitForText('This field does not match the format');
        });
    }







    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testAuthUserCanSeeLicenseEditPage()
    {

        $this->artisan('passport:install');

        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses');
        });
    }


    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNotAuthUserCanNotSeeLicensesPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertDontSee('Edit Licenses')
                ->assertDontSee('Edit Licenses');
        });
    }


    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testStartDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('start_date', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }


    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testFinishDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }



    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testDescriptionIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('start_date', '')
                ->type('description', '')
                ->waitForText('This field is required');
        });
    }



    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNumberIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }





    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNumberAndFinishDateAndDescriptionStartDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->type('description', '')
                ->waitForText('This field is required');
        });
    }




    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testDescriptionHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('description', 'a')
                ->type('start_date', '')
                ->waitForText('This field must be at least 5 characters');
        });
    }







    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testAtLeast5CharactersNumberAndStartDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '11')
                ->type('start_date', '')
                ->waitForText('This field must be at least 5 characters');
        });
    }




    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNoMoreThan255Characters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('description', 'qweqsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssswewqeqweqeqwewqeqewqeqeqeqweewqeqwqeqwweqwewqeqweqweqeqweqweqwewqeqweqweqweqweqweqweqweqweqweqweqwewqeqweqweqweqweqweqweqweqweqweqweqweqweqweqweqweqeqweqweqwewqewqeqweqwewqeqweqweqwwqewqeqwewqeqweqweqweqeqwewqeqeqweqweqwewq')
                ->type('start_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }




    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testNoMoreThan255CharactersNumber()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('number', '1')
                ->type('start_date', '')
                ->waitForText('This field must be at least 5 characters');
        });
    }




    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testWrongFormatAndFinishDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('start_date', '2012.12.015')
                ->type('finish_date', '')
                ->waitForText('This field does not match the format');
        });
    }






    /**
     * A Dusk test example.
     * @group license
     * @return void
     */
    public function testWrongFormat2AndDescriptionIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Licenses')
                ->waitForLocation('/licenses')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->type('finish_date', '2012.12.015')
                ->type('description', '')
                ->waitForText('This field does not match the format');
        });
    }

}
