<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FreeConsultingCategoriesTest extends DuskTestCase
{
    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testNotAuthUserCanNotSeeFreeConsultingPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact');
        });
    }


    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testNameIsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', '')
                        ->type('phone_number', '12312231')
                        ->type('description', 'sdadadadsaldkasldkasasdasdas')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testPhoneIsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '')
                        ->type('description', 'sdadadadsaldkasldkasasdasdas')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testDescriptionIsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->type('description', '')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testDescriptionMin5IsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->type('description', 'we')
                        ->press('Save')
                        ->waitForText('This field must be at least 5 characters');
                })
            ;
        });
    }




    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testDescriptionMaxIsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->type('description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaawe')
                        ->press('Save')
                        ->waitForText('This field must not be greater than 255 characters');
                })
            ;
        });
    }




    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testNameMaxIsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaawe')
                        ->type('description', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->press('Save')
                        ->waitForText('This field must not be greater than 255 characters');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', '')
                        ->type('description', '')
                        ->type('phone_number', '')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingCategories
     */
    public function testPhoneMaxIsRequired()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asdsadadas')
                        ->type('description', 'asadadas')
                        ->type('phone_number', '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111')
                        ->press('Save')
                        ->waitForText('This field must not be greater than 255 characters');
                })
            ;
        });
    }
}
