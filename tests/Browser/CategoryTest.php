<?php

namespace Tests\Browser;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CategoryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group category
     * @return void
     */
    public function testNotAuthUserCanNotSeeCategoriesPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/categories')
                ->assertDontSee('Questionnaires')
                ->assertDontSee('UPLOAD FILE')
                ->assertDontSee('SUBMIT')
                ->assertDontSee('Works')
                ->assertDontSee('Certificates')
                ->assertDontSee('Universities')
                ->assertDontSee('Licenses')
            ;
        });
    }




    /**
     * A Dusk test example.
     * @group category
     * @return void
     */
    public function testAuthUserCanNotSeeCategoriesPage()
    {
        $user = User::first();
        $category = Category::all()->random();
        $this->browse(function (Browser $browser) use ($user, $category) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->visit('user/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee($category->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses');
        });
    }





    /**
     * A Dusk test example.
     * @group category
     * @return void
     */
    public function testAddCategory()
    {
        $user = User::first();
        $category = Category::all()->random();
        $this->browse(function (Browser $browser) use ($user, $category) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->visit('user/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->check($category->id)
                ->clickLink("Save")
                ->assertSee($category->name)
            ;
        });
    }




    /**
     * A Dusk test example.
     * @group category
     * @return void
     */
    public function testAddCategories()
    {
        $user = User::first();
        $category1 = Category::all()->find(1);
        $category2 = Category::all()->find(2);
        $category3 = Category::all()->find(3);
        $this->browse(function (Browser $browser) use ($user, $category1, $category2, $category3) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->visit('user/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->check($category1->id)
                ->check($category2->id)
                ->check($category3->id)
                ->clickLink("Save")
                ->assertSee($category1->name)
                ->assertSee($category2->name)
                ->assertSee($category3->name)
            ;
        });
    }




    /**
     * A Dusk test example.
     * @group category
     * @return void
     */
    public function testDeleteCategories()
    {
        $user = User::first();
        $category = Category::all()->random();
        $this->browse(function (Browser $browser) use ($user, $category) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->visit('user/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses')
                ->check($category->id)
                ->clickLink("Save")
                ->waitForText('Delete')
                ->clickLink("Delete")
            ;
        });
    }

}
