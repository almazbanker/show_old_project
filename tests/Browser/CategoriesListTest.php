<?php

namespace Tests\Browser;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CategoriesListTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group categories
     * @return void
     */
    public function testCategoriesList()
    {
        $category = Category::all()->random();
        $this->browse(function (Browser $browser) use ($category) {
            $browser->visit('/categories')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertSee('Free Consulting')
                ->assertSee($category->name);
        });
    }
}
