<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class QuestionnaireTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group questionnaire
     * @return void
     */
    public function testAuthUserCanSeeQuestionnairesCreatePage()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->visit('/questionnaires')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
                ->assertSee($user->name)
                ->assertSee('Questionnaires')
                ->assertSee('Works')
                ->assertSee('Certificates')
                ->assertSee('Universities')
                ->assertSee('Licenses');
        });
    }



    /**
     * A Dusk test example.
     * @group questionnaire
     * @return void
     */
    public function testNotAuthUserCanNotSeeQuestionnairesCreatePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/questionnaires')
                ->assertDontSee('Create')
                ->assertDontSee('Edit Questionnaire');
        });
    }



    /**
     * A Dusk test example.
     * @group questionnaireq
     * @return void
     * @throws Throwable
     */
    public function testBornDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->visit('/questionnaires')
                ->type('born_date', '')
                ->type('gender', '')
                ->waitForText('This field is required');
        });
    }



}
