<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Throwable;

class WorkTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanSeeWorksCreatePage()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create');
        });
    }


    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testNotAuthUserCanNotSeeWorksCreatePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/works')
                ->assertDontSee('Create');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testStartDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('start_date', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testFinishDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testTitleIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('name', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testDescriptionIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('description', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testDescriptionHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testTitleHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testTitleHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('name', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testDescriptionHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('description', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testStartDateHasFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('start_date', '2021-01-01')
                ->type('description', 'aa')
                ->waitForText('This field does not match the format');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testFinishDateHasFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('finish_date', '2021-01-01')
                ->type('description', 'aa')
                ->waitForText('This field does not match the format');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testFinishDateHasValidFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('finish_date', '01-01-2021')
                ->type('description', 'aa')
                ->assertDontSee('This field does not match the format');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testStartDateHasValidFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('start_date', '01-01-2021')
                ->type('description', 'aa')
                ->assertDontSee('This field does not match the format');
        });
    }

    /**
     * A Dusk test example.
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanCreateAWork()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->assertSee('Create')
                ->type('start_date', '01-01-2021')
                ->type('finish_date', '01-02-2021')
                ->type('description', 'about work')
                ->type('name', 'new work')
                ->press('CREATE')
                ->waitForText('new work');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanSeeHisWorks()
    {
        $user = User::first();
        $work = $user->works->random();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->assertSee('Create');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanDeleteHisWorks()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('DELETE')
                ->waitUntilMissingText($work->name)
                ->waitUntilMissingText($work->start_date->format('Y-m-d'))
                ->waitUntilMissingText($work->finish_date->format('Y-m-d'));
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanEditHisWorks()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->typeSlowly('#work-edit-name', 'sdsdsd');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditNameFieldIsRequired()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->clear('#work-edit-name')
                ->assertSee('This field is required');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditDescriptionFieldIsRequired()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->clear('#work-edit-description')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditStartDateFieldIsRequired()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->clear('#work-edit-start-date')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditFinishDateFieldIsRequired()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->clear('#work-edit-finish-date')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditFinishDateFieldHasFormat()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Work')
                ->type('#work-edit-finish-date', '2022-11-01')
                ->type('#work-edit-start-date', '2022-11-01')
                ->assertSee('This field does not match the format');

        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditStartDateFieldHasFormat()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Work')
                ->type('#work-edit-start-date', '2022-11-01')
                ->type('#work-edit-finish-date', '2022-11-01')
                ->assertSee('This field does not match the format');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditDescriptionFieldHasMaxCharacters()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->type('#work-edit-description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#work-edit-name', 'new name')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditNameFieldHasMaxCharacters()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->type('#work-edit-name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#work-edit-description', 'new description')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditNameFieldHasMinCharacters()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->clear('#work-edit-name')
                ->type('#work-edit-name', 'aa')
                ->type('#work-edit-description', 'new description')
                ->assertSee('This field must be at least 3 characters');
        });
    }

    /**
     * @group work
     * @return void
     * @throws Throwable
     */
    public function testWorksEditDescriptionFieldHasMinCharacters()
    {
        $user = User::first();
        $work = $user->works->first();
        $this->browse(function (Browser $browser) use ($user, $work) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Works')
                ->waitForLocation('/works')
                ->waitForText($work->name)
                ->waitForText($work->start_date->format('Y-m-d'))
                ->waitForText($work->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Work')
                ->clear('#work-edit-description')
                ->type('#work-edit-description', 'aa')
                ->type('#work-edit-name', 'new name')
                ->assertSee('This field must be at least 3 characters');
        });
    }
}
