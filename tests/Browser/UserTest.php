<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UserTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group user
     * @return void
     */
    public function testAnUnauthorizedUserCanSeeOnTheUsersPage()
    {
        $users = [];
        $data = User::all();
        foreach ($data as $d){
            if ($d->questionnaire){
                if ($d->questionnaire->country['code'] === "996"){
                    $users[] = $d;
                }
            }
        }
        $user = $users[0];
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/users')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($user->name)
                ->assertSee('Likes:')
            ;
        });
    }



    /**
     * A Dusk test example.
     * @group user
     * @return void
     */
    public function testAuthorizedUserCanSeeOnTheUsersPage()
    {
        $user = User::first();
        $users = [];
        $data = User::all();
        foreach ($data as $d){
            if ($d->questionnaire){
                if ($d->questionnaire->country['code'] === "996"){
                    $users[] = $d;
                }
            }
        }
        $kyrgyz_user = $users[0];
        $this->browse(function (Browser $browser) use ($kyrgyz_user, $user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Users')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee($kyrgyz_user->name)
                ->assertSee('Likes:');
        });
    }
}
