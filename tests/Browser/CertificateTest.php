<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Throwable;

class CertificateTest extends DuskTestCase
{
    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanSeeWorksCreatePage()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create');
        });
    }


    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testNotAuthUserCanNotSeeWorksCreatePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/certificates')
                ->assertDontSee('Create');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testStartDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('start_date', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testFinishDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testTitleIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('name', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testDescriptionIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('description', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testDescriptionHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testTitleHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testTitleHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('name', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testDescriptionHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('description', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testStartDateHasFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('start_date', '2021-01-01')
                ->type('description', 'aa')
                ->waitForText('This field does not match the format');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testFinishDateHasFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('finish_date', '2021-01-01')
                ->type('description', 'aa')
                ->assertSee('This field does not match the format');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testFinishDateHasValidFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('finish_date', '01-01-2021')
                ->type('description', 'aa')
                ->assertDontSee('This field does not match the format');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testStartDateHasValidFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('start_date', '01-01-2021')
                ->type('description', 'aa')
                ->assertDontSee('This field does not match the format');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanCreateAWork()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->assertSee('Create')
                ->type('start_date', '01-01-2021')
                ->type('finish_date', '01-02-2021')
                ->type('description', 'about certificate')
                ->type('name', 'new certificate')
                ->press('CREATE')
                ->waitForText('new certificate');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanSeeHisWorks()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->assertSee('Create');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanDeleteHisWorks()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('DELETE')
                ->waitUntilMissingText($certificate->name)
                ->waitUntilMissingText($certificate->start_date->format('Y-m-d'))
                ->waitUntilMissingText($certificate->finish_date->format('Y-m-d'))
                ->assertDontSee($certificate->name);
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanEditHisWorks()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Certificate');
        });
    }



    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditNameFieldIsRequired()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->clear('#certificate-edit-name')
                ->assertSee('This field is required');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditDescriptionFieldIsRequired()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->clear('#certificate-edit-description')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditStartDateFieldIsRequired()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit Certificate')
                ->clear('#certificate-edit-start-date')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditFinishDateFieldIsRequired()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->clear('#certificate-edit-finish-date')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditFinishDateFieldHasFormat()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->type('#certificate-edit-finish-date', '2022-11-01')
                ->type('#certificate-edit-start-date', '2022-11-01')
                ->assertSee('This field does not match the format');

        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditStartDateFieldHasFormat()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->type('#certificate-edit-start-date', '2022-11-01')
                ->type('#certificate-edit-finish-date', '2022-11-01')
                ->assertSee('This field does not match the format');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditDescriptionFieldHasMaxCharacters()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->type('#certificate-edit-description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#certificate-edit-name', 'new name')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditNameFieldHasMaxCharacters()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->type('#certificate-edit-name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#certificate-edit-description', 'new description')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditNameFieldHasMinCharacters()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->clear('#certificate-edit-name')
                ->type('#certificate-edit-name', 'aa')
                ->type('#certificate-edit-description', 'new description')
                ->assertSee('This field must be at least 3 characters');
        });
    }

    /**
     * @group certificate
     * @return void
     * @throws Throwable
     */
    public function testCertificatesEditDescriptionFieldHasMinCharacters()
    {
        $user = User::first();
        $certificate = $user->certificates->first();
        $this->browse(function (Browser $browser) use ($user, $certificate) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Certificates')
                ->waitForLocation('/certificates')
                ->waitForText($certificate->name)
                ->waitForText($certificate->start_date->format('Y-m-d'))
                ->waitForText($certificate->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit Certificate')
                ->clear('#certificate-edit-description')
                ->type('#certificate-edit-description', 'aa')
                ->type('#certificate-edit-name', 'new name')
                ->assertSee('This field must be at least 3 characters');
        });
    }
}
