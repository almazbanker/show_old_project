<?php

namespace Tests\Browser;

use App\Models\Book;
use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class BookIndexTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group books
     * @return void
     */
    public function testBookIndex()
    {
        $book = Book::all()->random();
        $user = User::first();
        $this->browse(function (Browser $browser) use ($book, $user) {
            $browser->loginAs($user)
                ->visit('/books')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('Categories')
                ->assertSeeLink('Questionnaire')
                ->assertSeeLink('For Mediator')
                ->assertSeeLink('News')
                ->assertSee($book->name)
                ->assertSee($book->author)
                ->assertSee($book->link);
        });
    }
}
