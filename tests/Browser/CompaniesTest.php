<?php

namespace Tests\Browser;

use App\Models\Category;
use App\Models\Partner;
use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CompaniesTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group companiesq
     * @return void
     */
    public function testNotAuthSeeCompaniesPage()
    {
        $partner = Partner::all()->random();
        $category = Category::all()->random();
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user, $category, $partner) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('Categories')
                ->assertSeeLink('Questionnaire')
                ->assertSeeLink('For Mediator')
                ->assertSeeLink('News')
                ->assertSee('Free Consulting')
                ->assertSee($category->name)
                ->assertSee($partner->name);
        });

    }




    /**
     * A Dusk test example.
     * @group companies
     * @return void
     */
    public function testAuthSeeCompaniesPage()
    {
        $partner = Partner::all()->random();
        $category = Category::all()->random();
        $this->browse(function (Browser $browser) use ($partner, $category) {
            $browser->visit('/')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('Categories')
                ->assertDontSee('Questionnaire')
                ->assertDontSee('For Mediator')
                ->assertSeeLink('News')
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertSee('Free Consulting')
                ->assertSee($category->name)
                ->assertSee($partner->name);
        });

    }

}
