<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FreeConsultingFormMediatorTest extends DuskTestCase
{
    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testNotAuthUserCanNotSeeFreeConsultingPage()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact');
        });
    }


    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testNameIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', '')
                        ->type('phone_number', '12312231')
                        ->type('description', 'sdadadadsaldkasldkasasdasdas')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testPhoneIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '')
                        ->type('description', 'sdadadadsaldkasldkasasdasdas')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testDescriptionIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->type('description', '')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testDescriptionMin5IsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->type('description', 'we')
                        ->press('Save')
                        ->waitForText('This field must be at least 5 characters');
                })
            ;
        });
    }




    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testDescriptionMaxIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->type('description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaawe')
                        ->press('Save')
                        ->waitForText('This field must not be greater than 255 characters');
                })
            ;
        });
    }




    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testNameMaxIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaawe')
                        ->type('description', 'asadadas')
                        ->type('phone_number', '13113231')
                        ->press('Save')
                        ->waitForText('This field must not be greater than 255 characters');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', '')
                        ->type('description', '')
                        ->type('phone_number', '')
                        ->press('Save')
                        ->waitForText('This field is required');
                })
            ;
        });
    }



    /**
     * @throws \Throwable
     * @group consultingMediators
     */
    public function testPhoneMaxIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/mediators')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Free Consulting')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->press('Free Consulting')
                ->whenAvailable('.modal',function ($modal){
                    $modal->assertSee('Close')
                        ->assertSee('Save')
                        ->type('name', 'asdsadadas')
                        ->type('description', 'asadadas')
                        ->type('phone_number', '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111')
                        ->press('Save')
                        ->waitForText('This field must not be greater than 255 characters');
                })
            ;
        });
    }
}
