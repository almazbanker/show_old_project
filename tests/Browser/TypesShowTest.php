<?php

namespace Tests\Browser;

use App\Models\Type;
use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Throwable;

class TypesShowTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group type
     * @return void
     * @throws Throwable
     */
    public function testTypeShow()
    {
        $type = Type::first();
        $user = User::first();


        $this->browse(function (Browser $browser) use ($type, $user) {
            $browser->loginAs($user)
                ->visit(route('types.show', ['type' => $type]))
                ->assertSee($type->name)
                ->assertSee($type->description);
        });
    }
}
