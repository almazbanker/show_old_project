<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Throwable;

class UniversityTest extends DuskTestCase
{
    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanSeeUniversitiesCreatePage()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create');
        });
    }


    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testNotAuthUserCanNotSeeUniversitiesCreatePage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/universities')
                ->assertDontSee('Create');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testStartDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('start_date', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testFinishDateIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('finish_date', '')
                ->type('start_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testTitleIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('name', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testDescriptionIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('description', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testFacultyIsRequired()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('faculty', '')
                ->type('finish_date', '')
                ->waitForText('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testDescriptionHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testTitleHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testTitleHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('name', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testDescriptionHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('description', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testFacultyHasMinCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('faculty', 'aa')
                ->type('finish_date', '')
                ->waitForText('This field must be at least 3 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testFacultyHasMaxCharacters()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('faculty', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('finish_date', '')
                ->waitForText('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testStartDateHasFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('start_date', '2021-01-01')
                ->type('description', 'aa')
                ->waitForText('This field does not match the format');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testFinishDateHasFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('finish_date', '2021-01-01')
                ->type('description', 'aa')
                ->waitForText('This field does not match the format');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testFinishDateHasValidFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('finish_date', '01-01-2021')
                ->type('description', 'aa')
                ->assertDontSee('This field does not match the format');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testStartDateHasValidFormat()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('start_date', '01-01-2021')
                ->type('description', 'aa')
                ->assertDontSee('This field does not match the format');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanCreateAUniversity()
    {
        $user = User::first();
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->assertSee('Create')
                ->type('start_date', '01-01-2021')
                ->type('finish_date', '01-02-2021')
                ->type('description', 'about university')
                ->type('name', 'new university')
                ->type('faculty', 'new faculty')
                ->press('CREATE')
                ->waitForText('new faculty')
                ->assertSee('new university');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanSeeHisUniversities()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->assertSee('Create');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanDeleteHisUniversities()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('DELETE')
                ->waitUntilMissingText($university->name)
                ->waitUntilMissingText($university->start_date->format('Y-m-d'))
                ->waitUntilMissingText($university->finish_date->format('Y-m-d'))
                ->assertDontSee($university->name);
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testAuthUserCanEditHisUniversities()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->assertSee('Edit University');
        });
    }


    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditNameFieldIsRequired()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-name')
                ->assertSee('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditFacultyFieldIsRequired()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-faculty')
                ->assertSee('This field is required');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditDescriptionFieldIsRequired()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-description')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditStartDateFieldIsRequired()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-start-date')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditFinishDateFieldIsRequired()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-finish-date')
                ->assertSee('This field is required');

        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditFinishDateFieldHasFormat()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->type('#university-edit-finish-date', '2022-11-01')
                ->type('#university-edit-start-date', '2022-11-01')
                ->assertSee('This field does not match the format');

        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditStartDateFieldHasFormat()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->type('#university-edit-start-date', '2022-11-01')
                ->type('#university-edit-finish-date', '2022-11-01')
                ->assertSee('This field does not match the format');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditDescriptionFieldHasMaxCharacters()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->type('#university-edit-description', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#university-edit-name', 'new name')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditFacultyFieldHasMaxCharacters()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->type('#university-edit-faculty', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#university-edit-name', 'new name')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditNameFieldHasMaxCharacters()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->type('#university-edit-name', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
                ->type('#university-edit-description', 'new description')
                ->assertSee('This field must not be greater than 255 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditNameFieldHasMinCharacters()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-name')
                ->type('#university-edit-name', 'aa')
                ->type('#university-edit-description', 'new description')
                ->assertSee('This field must be at least 3 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditFacultyFieldHasMinCharacters()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#certificate-edit-name')
                ->type('#university-edit-faculty', 'aa')
                ->type('#university-edit-description', 'new description')
                ->assertSee('This field must be at least 3 characters');
        });
    }

    /**
     * @group university
     * @return void
     * @throws Throwable
     */
    public function testUniversitiesEditDescriptionFieldHasMinCharacters()
    {
        $user = User::first();
        $university = $user->universities->first();
        $this->browse(function (Browser $browser) use ($user, $university) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('Questionnaires')
                ->waitForLocation('/questionnaires/')
                ->clickLink('Universities')
                ->waitForLocation('/universities')
                ->waitForText($university->name)
                ->waitForText($university->start_date->format('Y-m-d'))
                ->waitForText($university->finish_date->format('Y-m-d'))
                ->press('EDIT')
                ->waitForText('Edit University')
                ->clear('#university-edit-description')
                ->type('#university-edit-description', 'aa')
                ->type('#university-edit-name', 'new name')
                ->assertSee('This field must be at least 3 characters');
        });
    }
}
