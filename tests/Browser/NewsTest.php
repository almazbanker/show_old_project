<?php

namespace Tests\Browser;

use App\Models\News;
use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class NewsTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group news
     * @return void
     */
    public function testAnUnauthorizedUserCanSeeOnTheNewsPage()
    {
        $news = News::all()->random();
        $this->browse(function (Browser $browser) use ($news) {
            $browser->visit('/news')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
                ->assertSee($news->title);
        });
    }


    /**
     * @throws \Throwable
     * @return void
     * @group news
     */
    public function testAnUnauthorizedUserCanSeeTheNewsShowOnThePage()
    {
        $news = News::first();
        $this->browse(function (Browser $browser) use ($news) {
            $browser->visit('/news/1')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertSee($news->title)
                ->assertSee($news->description)
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
            ;
        });
    }


    /**
     * @throws \Throwable
     * @return void
     * @group news
     */
    public function testAuthorizedUserCanSeeOnTheNewsPage()
    {
        $user = User::first();
        $news = News::all()->random();
        $this->browse(function (Browser $browser) use ($user, $news) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('News')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
                ->assertSee($news->title)
                ->assertSee($user->name)
            ;
        });
    }


    /**
     * @throws \Throwable
     * @return void
     * @group news
     */
    public function testAuthorizedUserCanSeeTheNewsShowOnThePage()
    {
        $user = User::first();
        $news = News::first();
        $this->browse(function (Browser $browser) use ($news, $user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->visit('/news/1')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee($news->title)
                ->assertSee($news->description)
                ->assertSee($user->name)
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
            ;
        });
    }
}
