<?php

namespace Tests\Browser;

use App\Models\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class InternationalTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group international
     * @return void
     */
    public function testAnUnauthorizedUserCanSeeOnTheInternationalPage()
    {
        $users = [];
        $data = User::all();
        foreach ($data as $d){
            if ($d->questionnaire){
                if ($d->questionnaire->country['code'] !== "996"){
                    $users[] = $d;
                }
            }
        }
        $international = $users[0];
        $this->browse(function (Browser $browser) use ($international) {
            $browser->visit('/users/international/')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('Login')
                ->assertSeeLink('Register')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
                ->assertSee($international->name)
                ->assertSee('Likes:')
            ;
        });
    }



    /**
     * A Dusk test example.
     * @group international
     * @return void
     */
    public function testAuthorizedUserCanSeeOnTheInternationalPage()
    {
        $user = User::first();
        $users = [];
        $data = User::all();
        foreach ($data as $d){
            if ($d->questionnaire){
                if ($d->questionnaire->country['code'] !== "996"){
                    $users[] = $d;
                }
            }
        }
        $international = $users[0];
        $this->browse(function (Browser $browser) use ($international, $user) {
            $browser->visit('/auth/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('LOGIN')
                ->waitForLocation('/')
                ->clickLink('International')
                ->assertSeeLink('Home')
                ->assertSeeLink('Users')
                ->assertSeeLink('International')
                ->assertSeeLink('Categories')
                ->assertSeeLink('News')
                ->assertSeeLink('For Mediators')
                ->assertSeeLink('Questionnaires')
                ->assertSee('Practice Area')
                ->assertSee('200, A-block, Green road, USA')
                ->assertSee('+10 367 267 2678')
                ->assertSee('lawyer@contact.com')
                ->assertSee('Useful Links')
                ->assertSee('Subscribe')
                ->assertSee('Business law')
                ->assertSee('Finance law')
                ->assertSee('Education law')
                ->assertSee('Family law')
                ->assertSee('About')
                ->assertSee('Blog')
                ->assertSee('Contact')
                ->assertSee('Subscribe newsletter to get updates')
                ->assertSee($international->name)
                ->assertSee('Likes:');
        });
    }
}
