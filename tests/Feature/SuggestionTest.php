<?php

namespace Tests\Feature;

use App\Models\Suggestion;
use App\Models\User;
use App\Models\Vote;
use Laravel\Passport\Passport;
use Tests\TestCase;

class SuggestionTest extends TestCase
{
    public function test_auth_user_get_all_suggestions()
    {
        Suggestion::factory()->count(10)->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('suggestions.index')]);
        $res = $this->getJson(route('suggestions.index'));
        $res->assertStatus(403);
    }

    public function test_not_auth_user_get_all_suggestions()
    {
        Suggestion::factory()->count(10)->create();
        $res = $this->getJson(route('suggestions.index'));
        $res->assertStatus(401);
    }

    public function test_auth_user_votes_on_suggestion_agree()
    {
        $suggestion = Suggestion::factory()->create();
        $data = [
            'suggestion_id' => $suggestion->id,
            'agree' => true
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('suggestions.store')]);
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(201);
        $res->assertJsonStructure([
            'id'
        ]);
    }

    public function test_not_auth_user_votes_on_suggestion_agree()
    {
        $suggestion = Suggestion::factory()->create();
        $data = [
            'suggestion_id' => $suggestion->id,
            'agree' => true
        ];
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(401);
    }

    public function test_auth_user_votes_on_suggestion_disagree()
    {
        $suggestion = Suggestion::factory()->create();
        $data = [
            'suggestion_id' => $suggestion->id,
            'agree' => false
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('suggestions.store')]);
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(201);
        $res->assertJsonStructure([
            'id'
        ]);
    }

    public function test_not_auth_user_votes_on_suggestion_disagree()
    {
        $suggestion = Suggestion::factory()->create();
        $data = [
            'suggestion_id' => $suggestion->id,
            'agree' => false
        ];
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(401);
    }

    public function test_auth_user_votes_on_suggestion_validation_required_suggestion_id()
    {
        $data = [
            'agree' => true
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('suggestions.store')]);
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_not_auth_user_votes_on_suggestion_validation_required_agree()
    {
        $suggestion = Suggestion::factory()->create();
        $data = [
            'suggestion_id' => $suggestion->id,
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('suggestions.store')]);
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_auth_user_votes_on_suggestion_disagree_validation_suggestion_id()
    {
        Suggestion::factory()->create();
        $data = [];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('suggestions.store')]);
        $res = $this->postJson(route('suggestions.store'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_auth_user_vote_delete()
    {
        $user = User::factory()->create();
        $suggestion = Suggestion::factory()->create();
        $vote = new Vote([
            'suggestion_id' => $suggestion->id,
            'agree' => true,
            'user_id' => $user->id
        ]);
        $vote->save();

        Passport::actingAs($user, [route('suggestions.destroy', $suggestion->id)]);
        $res = $this->deleteJson(route('suggestions.destroy', $suggestion->id));
        $res->assertStatus(403);
    }

    public function test_not_auth_user_vote_delete()
    {
        $user = User::factory()->create();
        $suggestion = Suggestion::factory()->create();
        $vote = new Vote([
            'suggestion_id' => $suggestion->id,
            'agree' => true,
            'user_id' => $user->id
        ]);
        $vote->save();

        $res = $this->deleteJson(route('suggestions.destroy', $suggestion->id));
        $res->assertStatus(401);
    }
}
