<?php

namespace Tests\Feature;

use App\Models\Suggestion;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class VoteTest extends TestCase
{
    public function test_create_vote()
    {
        $sug = Suggestion::factory()->create();
        $data = [
            'suggestion_id' => $sug->id,
            'agree' => true
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('votes.store')]);
        $res = $this->postJson(route('votes.store'), $data);
        $res->assertStatus(201);
    }
}
