<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Type;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    public function test_auth_user_get_all_categories()
    {
        Category::factory()->count(10)->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('categories.index')]);
        $res = $this->getJson(route('categories.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_all_categories()
    {
        Category::factory()->count(10)->create();
        $res = $this->getJson(route('categories.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_auth_user_get_user_all_categories()
    {
        $categories = Category::factory()->count(10)->create();
        $user = User::factory()->create();
        $data = [$categories[0]->id];
        $user->categories()->sync($data);
        Passport::actingAs($user, [route('auth.categories')]);
        $res = $this->getJson(route('auth.categories'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_user_all_categories()
    {
        Category::factory()->count(10)->create();
        $res = $this->getJson(route('auth.categories'));
        $res->assertStatus(401);
    }

    public function test_auth_user_add_categories_user()
    {
        $categories = Category::factory()->count(10)->create();
        $user = User::factory()->create();
        $data = [$categories[0]->id];
        Passport::actingAs($user, [route('post.categories')]);
        $res = $this->postJson(route('post.categories'), $data);
        $res->assertStatus(201);
    }

    public function test_not_auth_user_add_categories_user()
    {
        Category::factory()->count(10)->create();
        $data = [1, 2, 3, 4];
        $res = $this->postJson(route('post.categories'), $data);
        $res->assertStatus(401);
    }

    public function test_auth_user_get_category()
    {
        $category = Category::factory()->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('categories.show', $category->id)]);
        $res = $this->getJson(route('categories.show', $category->id));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_category()
    {
        $category = Category::factory()->create();
        $res = $this->getJson(route('categories.show', $category->id));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_auth_user_destroy_in_user_categories()
    {
        $categories = Category::factory()->count(10)->create();
        $category = Category::factory()->create();
        $user = User::factory()->create();
        $data = [$categories[0]->id, $category->id];
        $user->categories()->sync($data);
        Passport::actingAs($user, [route('destroy.categories', $category->id)]);
        $res = $this->deleteJson(route('destroy.categories', $category->id));
        $res->assertStatus(204);
    }

    public function test_not_auth_user_destroy_in_user_categories()
    {
        $categories = Category::factory()->count(10)->create();
        $category = Category::factory()->create();
        $user = User::factory()->create();
        $data = [$categories[0]->id, $category->id];
        $user->categories()->sync($data);
        $res = $this->deleteJson(route('destroy.categories', $category->id));
        $res->assertStatus(401);
    }

    public function test_auth_user_can_get_all_categories()
    {
        Category::factory()->count(6)->create();
        $response = $this->getJson(route('categories.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'total',
                    'per_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'to'
                ],
                'links'
            ]
        );
        $this->assertCount(6, $response->json('data'));
    }


    /**
     * A basic feature test example.
     *  group hello
     * @return void
     */
    public function test_user_success_get_one_category()
    {
        $categories = Category::factory()->count(6)->has(
            Type::factory()->count(5)
        )->create();
        $response = $this->getJson(route('categories.show', ['category' => $categories->random()]));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'description',
                    'picture',
                    'types' => [
                        0 => [
                            'id',
                            'description',
                        ]
                    ],
                ]
            ]
        );
    }
}
