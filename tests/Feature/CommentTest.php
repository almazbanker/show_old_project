<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{
    public function test_auth_user_create_comment()
    {
        $user = User::factory()->create();
        $data = [
            'content' => 'Comment content',
            'user_id' => $user->id
        ];
        Passport::actingAs($user, [route('comments.store')]);
        $res = $this->postJson(route('comments.store'), $data);
        $res->assertStatus(201);
        $this->assertDatabaseHas('comments', [
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'author_id' => $user->id
        ]);
    }

    public function test_auth_user_create_comment_validate_required_content()
    {
        $user = User::factory()->create();
        $data = [
            'content' => '',
            'user_id' => $user->id
        ];
        Passport::actingAs($user, [route('comments.store')]);
        $res = $this->postJson(route('comments.store'), $data);
        $res->assertStatus(422);
        $this->assertDatabaseMissing('comments', [
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'author_id' => $user->id
        ]);
    }

    public function test_auth_user_create_comment_validate_string_content()
    {
        $user = User::factory()->create();
        $data = [
            'content' => 12345678,
            'user_id' => $user->id
        ];
        Passport::actingAs($user, [route('comments.store')]);
        $res = $this->postJson(route('comments.store'), $data);
        $res->assertStatus(422);
        $this->assertDatabaseMissing('comments', [
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'author_id' => $user->id
        ]);
    }

    public function test_auth_user_create_comment_validate_required_user_id()
    {
        $user = User::factory()->create();
        $data = [
            'content' => 'Content'
        ];
        Passport::actingAs($user, [route('comments.store')]);
        $res = $this->postJson(route('comments.store'), $data);
        $res->assertStatus(422);
        $this->assertDatabaseMissing('comments', [
            'content' => $data['content'],
            'author_id' => $user->id
        ]);
    }

    public function test_auth_user_create_comment_validate_numeric_user_id()
    {
        $user = User::factory()->create();
        $data = [
            'content' => 'Content',
            'user_id' => 'qwe'
        ];
        Passport::actingAs($user, [route('comments.store')]);
        $res = $this->postJson(route('comments.store'), $data);
        $res->assertStatus(422);
        $this->assertDatabaseMissing('comments', [
            'content' => $data['content'],
            'author_id' => $user->id
        ]);
    }

    public function test_not_auth_user_create_comment()
    {
        $data = [
            'content' => 'Comment content',
            'user_id' => 1
        ];
        $user = User::factory()->create();
        $res = $this->postJson(route('comments.store'), $data);
        $res->assertStatus(401);
        $this->assertDatabaseMissing('comments', [
            'content' => $data['content'],
            'user_id' => $data['user_id'],
            'author_id' => $user->id
        ]);
    }

    public function test_auth_user_destroy_comment()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        Passport::actingAs($user, [route('comments.destroy', $comment->id)]);
        $res = $this->deleteJson(route('comments.destroy', $comment->id));
        $res->assertStatus(204);
    }

    public function test_not_auth_user_destroy_comment()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $res = $this->deleteJson(route('comments.destroy', $comment->id));
        $res->assertStatus(401);
    }
}
