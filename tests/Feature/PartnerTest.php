<?php

namespace Tests\Feature;

use App\Models\Partner;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class PartnerTest extends TestCase
{
    public function test_auth_user_get_all_news()
    {
        Partner::factory()->count(10)->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('partners.index')]);
        $res = $this->getJson(route('partners.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'picture',
                    'name',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_all_news()
    {
        Partner::factory()->count(10)->create();
        $res = $this->getJson(route('partners.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'picture',
                    'name',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_auth_user_get_news()
    {
        $partner = Partner::factory()->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('partners.show', $partner->id)]);
        $res = $this->getJson(route('partners.show', $partner->id));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'picture',
                'name',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_news()
    {
        $partner = Partner::factory()->create();
        $res = $this->getJson(route('partners.show', $partner->id));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'picture',
                'name',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_get_all_partners()
    {
        Partner::factory()->count(6)->create();
        $response = $this->getJson(route('partners.index'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    0 => [
                        'id',
                        'picture',
                        'name',
                        'description',
                    ]
                ]
            ]
        );
        $this->assertCount(6, $response->json('data'));
    }



    /**
     * A basic feature test example.
     *  group hello
     * @return void
     */
    public function test_user_success_get_one_category()
    {
        $partners = Partner::factory()->count(6)->create();
        $response = $this->getJson(route('partners.show', ['partner' => $partners->random()]));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                'data' => [
                    'id',
                    'name',
                    'description',
                    'picture',
                ]
            ]
        );
    }
}
