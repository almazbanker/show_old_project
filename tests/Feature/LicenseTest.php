<?php

namespace Tests\Feature;

use App\Models\License;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class LicenseTest extends TestCase
{
    public function test_success_get_users_license()
    {
        $user = User::factory()->create();
        License::factory()->for($user)->create();
        Passport::actingAs($user, [route('licenses.index')]);
        $response = $this->getJson(route('licenses.index'));
        $response->assertOk();
    }

    public function test_not_auth_user_trying_get_users_license()
    {
        $user = User::factory()->create();
        License::factory()->for($user)->count(4)->create();
        $response = $this->getJson(route('licenses.index'));
        $response->assertStatus(401);
    }

    public function test_create_license()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => '1234567890'
        ];
        Passport::actingAs($user, [route('licenses.store', $data)]);
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(201);
        $this->assertDatabaseHas('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['finish_date'],
            'number' => $data['number'],
            'description' => $data['description']
        ]);
    }

    public function test_create_license_validate_required_start_date()
    {
        $user = User::factory()->create();
        $data = [
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => '1234567890'
        ];
        Passport::actingAs($user, [route('licenses.store', $data)]);
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('licenses', [
            'finish_date' => $data['finish_date'],
            'number' => $data['number'],
            'description' => $data['description']
        ]);
    }

    public function test_create_license_validate_required_finish_date()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => '1234567890'
        ];
        Passport::actingAs($user, [route('licenses.store', $data)]);
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('licenses', [
            'start_date' => $data['start_date'],
            'number' => $data['number'],
            'description' => $data['description']
        ]);
    }

    public function test_create_license_validate_required_description()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'number' => '1234567890'
        ];
        Passport::actingAs($user, [route('licenses.store', $data)]);
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['start_date'],
            'number' => $data['number'],
        ]);
    }

    public function test_create_license_validate_required_number()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
        ];
        Passport::actingAs($user, [route('licenses.store', $data)]);
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['start_date'],
            'description' => $data['description']
        ]);
    }

    public function test_create_license_validate_numeric_number()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => 'qwe'
        ];
        Passport::actingAs($user, [route('licenses.store', $data)]);
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['start_date'],
            'description' => $data['description']
        ]);
    }

    public function test_not_auth_user_create_license()
    {
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => '1234567890'
        ];
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(401);
        $this->assertDatabaseMissing('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['finish_date'],
            'number' => $data['number'],
            'description' => $data['description']
        ]);
    }

    public function test_update_license()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => '1234567890'
        ];
        $license = license::factory()->for($user)->create();
        Passport::actingAs($user, [route('licenses.update', $license)]);
        $this->putJson(route('licenses.update', $license), $data);
        $this->assertDatabaseHas('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['finish_date'],
            'number' => $data['number'],
            'description' => $data['description']
        ]);
    }

    public function test_not_auth_user_update_license()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date("Y-m-d"),
            'finish_date' => date("Y-m-d"),
            'picture' => '',
            'description' => 'description description description description',
            'number' => '1234567890'
        ];
        $license = license::factory()->for($user)->create();
        $response = $this->putJson(route('licenses.update', $license), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('licenses', [
            'start_date' => $data['start_date'],
            'finish_date' => $data['finish_date'],
            'number' => $data['number'],
            'description' => $data['description']
        ]);
    }

    public function test_success_get_users_licenses()
    {
        $user = User::factory()->create();
        $licenses = License::factory()->for($user)->count(6)->create();
        Passport::actingAs($user, [route('licenses.index')]);
        $response = $this->getJson(route('licenses.index'));
        $response->assertOk();
        $this->assertCount($licenses->count(), $response->json()['data']);
    }


    public function test_not_auth_user_trying_get_users_licenses()
    {
        $user = User::factory()->create();
        License::factory()->for($user)->count(4)->create();
        $response = $this->getJson(route('licenses.index'));
        $response->assertStatus(401);
    }

    public function test_not_auth_user_create_licenses()
    {
        $data = ['description' => 'License description, License description, License description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $response = $this->postJson(route('licenses.store', $data));
        $response->assertStatus(401);
        $this->assertDatabaseMissing('licenses', ['description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_licenses_number()
    {
        $user = User::factory()->create();
        $License = License::factory()->for($user)->create();
        $data = ['number' => 123456];
        Passport::actingAs($user, [route('licenses.update', $License)]);
        $res = $this->putJson(route('licenses.update', $License), $data);
        $res->assertStatus(422);
    }

    public function test_update_licenses_description()
    {
        $user = User::factory()->create();
        $License = License::factory()->for($user)->create();
        $data = ['description' => 'qwe'];
        Passport::actingAs($user, [route('licenses.update', $License)]);
        $res = $this->putJson(route('licenses.update', $License), $data);
        $res->assertStatus(422);
    }

    public function test_update_License_start_date()
    {
        $user = User::factory()->create();
        $License = License::factory()->for($user)->create();
        $data = ['start_date' => date("Y-m-d")];
        Passport::actingAs($user, [route('licenses.update', $License)]);
        $res = $this->putJson(route('licenses.update', $License), $data);
        $res->assertStatus(422);
    }

    public function test_update_License_finish_date()
    {
        $user = User::factory()->create();
        $License = License::factory()->for($user)->create();
        $data = ['finish_date' => date("Y-m-d")];
        Passport::actingAs($user, [route('licenses.update', $License)]);
        $res = $this->putJson(route('licenses.update', $License), $data);
        $res->assertStatus(422);
    }

    public function test_update_licenses()
    {
        $user = User::factory()->create();
        $License = License::factory()->for($user)->create();
        $data = ['description' => 'License description, License description, License description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('licenses.update', $License)]);
        $res = $this->putJson(route('licenses.update', $License), $data);
        $res->assertStatus(422);
    }

    public function test_not_auth_user_update_licenses()
    {
        $user = User::factory()->create();
        $license = License::factory()->for($user)->create();
        $data = ['description' => 'University description, university description, university description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $response = $this->postJson(route('licenses.store', $license), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('licenses', ['description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }


    public function test_not_auth_user_delete_licenses()
    {
        $user = User::factory()->create();
        $license = License::factory()->for($user)->create();
        $this->assertDatabaseHas('licenses', [
            'description' => $license->description,
            'user_id' => $license->user->id,
            'number' => $license->number,
            'id' => $license->id,
            'start_date' => $license->start_date,
            'finish_date' => $license->finish_date
        ]);
        $response = $this->deleteJson(route('licenses.destroy', compact('license')));
        $response->assertStatus(401);
        $this->assertDatabaseHas('licenses', [
            'description' => $license->description,
            'user_id' => $license->user->id,
            'id' => $license->id,
            'number' => $license->number,
            'start_date' => $license->start_date,
            'finish_date' => $license->finish_date
        ]);
    }
}
