<?php

namespace Tests\Feature;

use App\Models\Certificate;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CertificateTest extends TestCase
{
    public function test_auth_user_get_certificates()
    {
        $user = User::factory()->has(Certificate::factory())->create();
        Passport::actingAs($user, [route('certificates.index')]);
        $res = $this->getJson(route('certificates.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'start_date',
                    'finish_date',
                    'picture',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_certificates()
    {
        User::factory()->has(Certificate::factory())->create();
        $res = $this->getJson(route('certificates.index'));
        $res->assertStatus(401);
    }

    public function test_auth_user_create_certificate()
    {
        $user = User::factory()->create();
        $data = [
            'name' => 'Certificate name',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        Passport::actingAs($user, [route('certificates.store')]);
        $res = $this->postJson(route('certificates.store'), $data);
        $res->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'start_date',
                'finish_date',
                'picture',
                'description'
            ]
        ]);
        $res->assertStatus(201);
    }

    public function test_auth_user_create_certificate_validate_required_name()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        Passport::actingAs($user, [route('certificates.store')]);
        $res = $this->postJson(route('certificates.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_certificate_validate_required_start_date()
    {
        $user = User::factory()->create();
        $data = [
            'name' => 'Name',
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        Passport::actingAs($user, [route('certificates.store')]);
        $res = $this->postJson(route('certificates.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_certificate_validate_required_finish_date()
    {
        $user = User::factory()->create();
        $data = [
            'start_date' => date('Y-m-d'),
            'name' => 'Name',
            'picture' => '',
            'description' => 'Certificate description'
        ];
        Passport::actingAs($user, [route('certificates.store')]);
        $res = $this->postJson(route('certificates.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_certificate_validate_string_description()
    {
        $user = User::factory()->create();
        $data = [
            'name' => 'Name',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 123456789
        ];
        Passport::actingAs($user, [route('certificates.store')]);
        $res = $this->postJson(route('certificates.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_not_auth_user_create_certificate()
    {
        $data = [
            'name' => 'Certificate name',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        $res = $this->postJson(route('certificates.store'), $data);
        $res->assertStatus(401);
    }

    public function test_auth_user_update_certificates()
    {
        $data = [
            'name' => 'Certificate name',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        Passport::actingAs($user, [route('certificates.update', $certificate->id)]);
        $res = $this->putJson(route('certificates.update', $certificate->id), $data);
        $res->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'start_date',
                'finish_date',
                'picture',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_auth_user_update_certificates_validate_required_name()
    {
        $data = [
            'name' => '',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        Passport::actingAs($user, [route('certificates.update', $certificate->id)]);
        $res = $this->putJson(route('certificates.update', $certificate->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_certificates_validate_required_start_date()
    {
        $data = [
            'name' => 'Name',
            'start_date' => '',
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        Passport::actingAs($user, [route('certificates.update', $certificate->id)]);
        $res = $this->putJson(route('certificates.update', $certificate->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_certificates_validate_required_finish_date()
    {
        $data = [
            'name' => 'Name',
            'start_date' => date('Y-m-d'),
            'finish_date' => '',
            'picture' => '',
            'description' => 'Certificate description'
        ];
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        Passport::actingAs($user, [route('certificates.update', $certificate->id)]);
        $res = $this->putJson(route('certificates.update', $certificate->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_certificates_validate_string_description()
    {
        $data = [
            'name' => 'Name',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 12345676543
        ];
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        Passport::actingAs($user, [route('certificates.update', $certificate->id)]);
        $res = $this->putJson(route('certificates.update', $certificate->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_not_auth_user_update_certificates()
    {
        $data = [
            'name' => 'Certificate name',
            'start_date' => date('Y-m-d'),
            'finish_date' => date('Y-m-d'),
            'picture' => '',
            'description' => 'Certificate description'
        ];
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        $res = $this->putJson(route('certificates.update', $certificate->id), $data);
        $res->assertStatus(401);
    }

    public function test_auth_user_delete_certificates()
    {
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        Passport::actingAs($user, [route('certificates.destroy', $certificate->id)]);
        $res = $this->deleteJson(route('certificates.destroy', $certificate->id));
        $res->assertStatus(204);
        $this->assertDatabaseMissing('certificates', ['id' => $certificate->id]);
    }

    public function test_not_auth_user_delete_certificates()
    {
        $user = User::factory()->has(Certificate::factory())->create();
        $certificate = $user->certificates->first();
        $res = $this->deleteJson(route('certificates.destroy', $certificate->id));
        $res->assertStatus(401);
    }

    public function test_success_get_users_certificates()
    {
        $user = User::factory()->create();
        $certificates = Certificate::factory()->for($user)->count(4)->create();
        Passport::actingAs($user, [route('certificates.index')]);
        $response = $this->getJson(route('certificates.index'));
        $response->assertOk();
        $this->assertCount($certificates->count(), $response->json()['data']);
    }

    public function test_not_auth_user_trying_get_users_certificates()
    {
        $user = User::factory()->create();
        Certificate::factory()->for($user)->count(4)->create();
        $response = $this->getJson(route('certificates.index'));
        $response->assertStatus(401);
    }
}
