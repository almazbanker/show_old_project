<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('passport:install');
    }

    public function test_success_register()
    {
        $data = [
            'name' => 'Kanimet',
            'email' => 'admin@admin.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'token',
            'name',
            'approved',
            'id',
            'moderator',
            'admin'
        ]);
    }

    public function test_failed_register_required_name()
    {
        $data = [
            'email' => 'admin@admin.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_required_email()
    {
        $data = [
            'name' => 'Kanimet',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_required_password()
    {
        $data = [
            'name' => 'Kanimet',
            'email' => 'admin@admin.com',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_required_password_confirmation()
    {
        $data = [
            'name' => 'Kanimet',
            'email' => 'admin@admin.com',
            'password' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_min_name()
    {
        $data = [
            'Name' => 'K',
            'email' => 'admin@admin.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_email_format()
    {
        $data = [
            'name' => 'Kanimet',
            'email' => 'admin.admin',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_min_password()
    {
        $data = [
            'name' => 'Kanimet',
            'email' => 'admin@admin.com',
            'password' => 'pass',
            'password_confirmation' => 'pass'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_string_name()
    {
        $data = [
            'name' => 12345678,
            'email' => 'admin@admin.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_register_unique_email()
    {
        $user = User::factory()->create();
        $data = [
            'name' => 'Kanimet',
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_success_login()
    {
        $user = User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'email' => $user->email,
            'password' => 'password'
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'token',
            'name',
            'approved',
            'id',
            'moderator',
            'admin'
        ]);
    }

    public function test_failed_login_required_email()
    {
        User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'password' => 'password'
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_login_required_password()
    {
        $user = User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'email' => $user->email
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_login_min_password()
    {
        $user = User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'email' => $user->email,
            'password' => 'pass'
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_login_email_format()
    {
        User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'email' => 'admin.admin',
            'password' => 'password'
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
    }

    public function test_failed_login_email_unauthorized()
    {
        $user = User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'email' => $user->email . 'q',
            'password' => 'password'
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(401);
    }

    public function test_failed_login_password_unauthorized()
    {
        $user = User::factory()->state(['password' => Hash::make('password')])->create();
        $data = [
            'email' => $user->email,
            'password' => 'passwordq'
        ];
        $res = $this->postJson(route('login'), $data);
        $res->assertStatus(401);
    }
}
