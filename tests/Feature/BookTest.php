<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class BookTest extends TestCase
{
    public function test_auth_user_get_all_books()
    {
        Book::factory()->count(10)->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('books.index')]);
        $res = $this->getJson(route('books.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'description',
                    'link',
                    'author'
                ]
            ]
        ]);
        $res->assertOk();
    }

    public function test_auth_user_get_all_books_name()
    {
        Book::factory()->count(10)->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('books.index')]);
        $res = $this->getJson(route('books.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'name'
                ]
            ]
        ]);
        $res->assertOk();
    }

    public function test_not_auth_user_get_all_books()
    {
        Book::factory()->count(10)->create();
        $res = $this->getJson(route('books.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'description',
                    'link',
                    'author'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_get_null_books()
    {
        $res = $this->getJson(route('books.index'));
        $res->assertJsonStructure([
            'data'
        ]);
        $res->assertStatus(200);
    }
}
