<?php

namespace Tests\Feature;

use App\Models\Country;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CountryTest extends TestCase
{
    public function test_success_get_users_countries()
    {
        $user = User::factory()->create();
        Country::factory()->count(4)->create();
        Passport::actingAs($user, [route('countries.index')]);
        $response = $this->getJson(route('countries.index'));
        $response->assertStatus(200);
    }

    public function test_not_auth_user_trying_get_users_countries()
    {
        Country::factory()->count(4)->create();
        $response = $this->getJson(route('countries.index'));
        $response->assertStatus(401);
    }
}
