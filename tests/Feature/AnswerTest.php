<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AnswerTest extends TestCase
{
    public function test_auth_user_get_all_answer()
    {
        $user = User::factory()->create();
        Passport::actingAs($user, [route('answers.index')]);
        $res = $this->getJson(route('answers.index'));
        $res->assertStatus(403);
    }

    public function test_not_auth_user_get_all_answer()
    {
        $res = $this->getJson(route('answers.index'));
        $res->assertStatus(401);
    }

    /**
     * @return void
     * @group qwe
     */
    public function test_success_register()
    {
        Artisan::call('passport:install');
        $data = [
            'name' => 'Kanimet',
            'email' => 'admin@admin.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $res = $this->postJson(route('register'), $data);
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'token',
            'name',
            'approved',
            'id',
            'moderator',
            'admin'
        ]);
    }
}
