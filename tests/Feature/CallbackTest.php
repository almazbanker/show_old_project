<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CallbackTest extends TestCase
{
    public function test_auth_user_get_store()
    {
        $data = [
            'name' => 'Name',
            'phone_number' => 12345678,
            'description' => 'Description'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('callbacks.store')]);
        $res = $this->postJson(route('callbacks.store'), $data);
        $res->assertOk();
    }

    public function test_auth_user_get_store_validate_required_name()
    {
        $data = [
            'phone_number' => 12345678,
            'description' => 'Description'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('callbacks.store')]);
        $res = $this->postJson(route('callbacks.store'), $data);
        $res->assertStatus(422);
    }

    public function test_auth_user_get_store_validate_required_phone()
    {
        $data = [
            'name' => 'Name',
            'description' => 'Description'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('callbacks.store')]);
        $res = $this->postJson(route('callbacks.store'), $data);
        $res->assertStatus(422);
    }

    public function test_auth_user_get_store_validate_min_description()
    {
        $data = [
            'name' => 'Name',
            'phone_number' => 12345678,
            'description' => 'D'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('callbacks.store')]);
        $res = $this->postJson(route('callbacks.store'), $data);
        $res->assertStatus(422);
    }

    public function test_auth_user_get_store_validate_min_name()
    {
        $data = [
            'name' => 'Na',
            'phone_number' => 12345678,
            'description' => 'Description'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('callbacks.store')]);
        $res = $this->postJson(route('callbacks.store'), $data);
        $res->assertStatus(422);
    }

    public function test_not_auth_user_get_store()
    {
        $data = [
            'name' => 'Name',
            'phone_number' => 12345678,
            'description' => 'Description'
        ];
        $res = $this->postJson(route('callbacks.store'), $data);
        $res->assertOk();
    }
}
