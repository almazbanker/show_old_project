<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Work;
use Laravel\Passport\Passport;
use Tests\TestCase;
use function route;

class WorksTest extends TestCase
{
    public function test_success_get_users_works()
    {
        $users = User::factory()->count(4)->create();
        foreach ($users as $user){
            $works = Work::factory()->for($user)->count(4)->create();
        }
        Passport::actingAs($user, [route('works.index')]);
        $response = $this->getJson(route('works.index'));
        $response->assertOk();
        $this->assertCount($works->count(), $response->json()['data']);
    }

    public function test_success_get_one_user_works()
    {
        $user = User::factory()->create();
        $works = Work::factory()->for($user)->count(4)->create();
        Passport::actingAs($user, [route('works.index')]);
        $response = $this->getJson(route('works.index'));
        $response->assertOk();
        $this->assertCount($works->count(), $response->json()['data']);
    }

    public function test_not_auth_user_trying_get_users_works()
    {
        $user = User::factory()->create();
        Work::factory()->for($user)->count(4)->create();
        $response = $this->getJson(route('works.index'));
        $response->assertStatus(401);
    }

    public function test_create_work()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('works.store', $data)]);
        $response = $this->postJson(route('works.store', $data));
        $response->assertStatus(201);
        $this->assertDatabaseHas('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_not_auth_user_create_work()
    {
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $response = $this->postJson(route('works.store', $data));
        $response->assertStatus(401);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_work()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $work = Work::factory()->for($user)->create();
        Passport::actingAs($user, [route('works.update', $work)]);
        $this->putJson(route('works.update', $work), $data);
        $this->assertDatabaseHas('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_work_with_wrong_data()
    {
        $user = User::factory()->create();
        $work = Work::factory()->for($user)->create();
        $data = ['name' => 'name', 'description' => 'work description, work description, work description.', 'start_date' => 'qwe', 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('works.update', $work)]);
        $this->putJson(route('works.update', $work), $data);
        $response = $this->postJson(route('works.update', $work), $data);
        $response->assertStatus(405);
    }

    public function test_not_auth_user_update_work()
    {
        $user = User::factory()->create();
        $work = Work::factory()->for($user)->create();
        $data = ['name' => 'Work name.', 'description' => 'University description, university description, university description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $response = $this->putJson(route('works.update', $work), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_not_auth_users_update_work()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'University description, university description, university description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $work = Work::factory()->for($user)->create();
        $response = $this->putJson(route('works.update', $work), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_user_success_delete_work()
    {
        $user = User::factory()->create();
        $work = Work::factory()->for($user)->create();
        Passport::actingAs($user, [route('works.destroy', compact('work'))]);
        $this->assertDatabaseHas('works', [
            'name' => $work->name,
            'description' => $work->description,
            'user_id' => $work->user->id,
            'id' => $work->id,
            'start_date' => $work->start_date,
            'finish_date' => $work->finish_date
        ]);
        $response = $this->deleteJson(route('works.destroy', compact('work')));
        $response->assertNoContent();
        $this->assertDatabaseMissing('works', [
            'name' => $work->name,
            'description' => $work->description,
            'user_id' => $work->user->id,
            'id' => $work->id,
            'start_date' => $work->start_date,
            'finish_date' => $work->finish_date
        ]);
    }

    public function test_not_auth_user_delete_work()
    {
        $user = User::factory()->create();
        $work = Work::factory()->for($user)->create();
        $this->assertDatabaseHas('works', [
            'name' => $work->name,
            'description' => $work->description,
            'user_id' => $work->user->id,
            'id' => $work->id,
            'start_date' => $work->start_date,
            'finish_date' => $work->finish_date
        ]);
        $response = $this->deleteJson(route('works.destroy', compact('work')));
        $response->assertStatus(401);
        $this->assertDatabaseHas('works', [
            'name' => $work->name,
            'description' => $work->description,
            'user_id' => $work->user->id,
            'id' => $work->id,
            'start_date' => $work->start_date,
            'finish_date' => $work->finish_date
        ]);
    }

    public function test_failed_create_work_validate_required_name()
    {
        $user = User::factory()->create();
        $data = ['description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('works.store', $data)]);
        $response = $this->postJson(route('works.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_failed_create_work_validate_required_start_date()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('works.store', $data)]);
        $response = $this->postJson(route('works.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'finish_date' => $data['finish_date']]);
    }

    public function test_failed_create_work_validate_required_finish_date()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d")];
        Passport::actingAs($user, [route('works.store', $data)]);
        $response = $this->postJson(route('works.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date']]);
    }

    public function test_failed_create_work_validate_required_description()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('works.store', $data)]);
        $response = $this->postJson(route('works.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_failed_update_work_validate_required_name()
    {
        $user = User::factory()->create();
        $data = ['description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $work = Work::factory()->for($user)->create();
        Passport::actingAs($user, [route('works.update', $work)]);
        $response = $this->putJson(route('works.update', $work), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_failed_update_work_validate_required_start_date()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'finish_date' => date('Y-m-d')];
        $work = Work::factory()->for($user)->create();
        Passport::actingAs($user, [route('works.update', $work)]);
        $response = $this->putJson(route('works.update', $work), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'finish_date' => $data['finish_date']]);
    }

    public function test_failed_update_work_validate_required_finish_date()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'description' => 'work description, work description, work description.', 'start_date' => date("Y-m-d")];
        $work = Work::factory()->for($user)->create();
        Passport::actingAs($user, [route('works.update', $work)]);
        $response = $this->putJson(route('works.update', $work), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date']]);
    }

    public function test_failed_update_work_validate_required_description()
    {
        $user = User::factory()->create();
        $data = ['name' => 'Work name.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $work = Work::factory()->for($user)->create();
        Passport::actingAs($user, [route('works.update', $work)]);
        $response = $this->putJson(route('works.update', $work), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('works', ['name' => $data['name'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }
}
