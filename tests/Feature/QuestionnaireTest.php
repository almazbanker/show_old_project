<?php

namespace Tests\Feature;

use App\Models\Country;
use App\Models\Questionnaire;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class QuestionnaireTest extends TestCase
{
    public function test_auth_user_get_questionnaire()
    {
        $countries = Country::factory()->count(10)->create();
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.index')]);
        $res = $this->getJson(route('questionnaires.index'));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'born_date',
                'country_id',
                'gender'
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_questionnaire()
    {
        $countries = Country::factory()->count(10)->create();
        User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        $res = $this->getJson(route('questionnaires.index'));
        $res->assertStatus(401);
    }

    public function test_auth_user_create_questionnaire()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[0]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'data' => [
                'id',
                'born_date',
                'country_id',
                'gender'
            ]
        ]);
        $this->assertDatabaseHas('questionnaires', [
            'user_id' => $user->id,
            'born_date' => $data['born_date'],
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(201);
    }

    public function test_not_auth_user_create_questionnaire()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[0]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        $res = $this->postJson(route('questionnaires.store'), $data);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'born_date' => $data['born_date'],
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(401);
    }

    public function test_auth_user_update_questionnaire()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[1]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'data' => [
                'id',
                'born_date',
                'country_id',
                'gender'
            ]
        ]);
        $this->assertDatabaseHas('questionnaires', [
            'user_id' => $user->id,
            'born_date' => $data['born_date'],
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_update_questionnaire()
    {
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => rand(1, 10),
            'gender' => 'female'
        ];
        $countries = Country::factory()->count(10)->create();
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'born_date' => $data['born_date'],
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(401);
    }

    public function test_auth_user_create_questionnaire_validate_required_born_date()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'country_id' => $countries[0]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_questionnaire_validate_date_born_date()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => 'date',
            'country_id' => $countries[0]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_questionnaire_validate_required_country_id()
    {
        $data = [
            'born_date' => date('Y-m-d'),
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'born_date' => $data['born_date'],
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_questionnaire_validate_string_gender()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[0]->id,
            'gender' => 1234
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_questionnaire_validate_required_gender()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[0]->id,
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_questionnaire_validate_country_id_not_exists()
    {
        Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => 11,
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'born_date' => $data['born_date'],
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_questionnaire_validate_numeric_country_id()
    {
        Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => '$countries[0]->id',
            'gender' => 'female'
        ];
        $user = User::factory()->create();
        Passport::actingAs($user, [route('questionnaires.store')]);
        $res = $this->postJson(route('questionnaires.store'), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'born_date' => $data['born_date'],
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_string_gender()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[0]->id,
            'gender' => 1234
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'country_id' => $data['country_id']
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_required_gender()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => $countries[0]->id,
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_required_born_date()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'country_id' => $countries[0]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_date_born_date()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => 'date',
            'country_id' => $countries[0]->id,
            'gender' => 'female'
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_required_country_id()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'gender' => 'female'
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'born_date' => $data['born_date'],
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_country_id_not_exists()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => 11,
            'gender' => 'female'
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'born_date' => $data['born_date'],
        ]);
        $res->assertStatus(422);
    }

    public function test_auth_user_update_questionnaire_validate_numeric_country_id()
    {
        $countries = Country::factory()->count(10)->create();
        $data = [
            'born_date' => date('Y-m-d'),
            'country_id' => '$countries[0]->id',
            'gender' => 'female'
        ];
        $user = User::factory()->has(Questionnaire::factory()->state(['country_id' => $countries[0]->id]))->create();
        Passport::actingAs($user, [route('questionnaires.update', $user->questionnaire->id)]);
        $res = $this->putJson(route('questionnaires.update', $user->questionnaire->id), $data);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('questionnaires', [
            'user_id' => $user->id,
            'gender' => $data['gender'],
            'born_date' => $data['born_date'],
        ]);
        $res->assertStatus(422);
    }
}
