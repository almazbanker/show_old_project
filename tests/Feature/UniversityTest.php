<?php

namespace Tests\Feature;

use App\Models\University;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use function route;

class UniversityTest extends TestCase
{
    public function test_success_get_users_universities()
    {
        $user = User::factory()->create();
        $universities = University::factory()->for($user)->count(4)->create();
        Passport::actingAs($user, [route('universities.index')]);
        $response = $this->getJson(route('universities.index'));
        $response->assertOk();
        $this->assertCount($universities->count(), $response->json()['data']);
    }

    public function test_not_auth_user_trying_get_users_universities()
    {
        $user = User::factory()->create();
        University::factory()->for($user)->count(4)->create();
        $response = $this->getJson(route('universities.index'));
        $response->assertStatus(401);
    }

    public function test_create_university()
    {
        $user = User::factory()->create();
        $data = ['name' => 'University name.', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('universities.store', $data)]);
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(201);
        $this->assertDatabaseHas('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_not_auth_user_create_university()
    {
        $data = ['name' => 'University name.', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(401);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_university()
    {
        $user = User::factory()->create();
        $data = ['name' => 'University name.', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.update', $university)]);
        $this->putJson(route('universities.update', $university), $data);
        $this->assertDatabaseHas('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_not_auth_user_update_university()
    {
        $user = User::factory()->create();
        $university = University::factory()->for($user)->create();
        $data = ['name' => 'University name.', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $response = $this->putJson(route('universities.update', $university), $data);
        $response->assertStatus(401);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_user_success_delete_university()
    {
        $user = User::factory()->create();
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.destroy', compact('university'))]);
        $this->assertDatabaseHas('universities', [
            'name' => $university->name,
            'description' => $university->description,
            'user_id' => $university->user->id,
            'id' => $university->id,
            'faculty' => $university->faculty,
            'start_date' => $university->start_date,
            'finish_date' => $university->finish_date
        ]);
        $response = $this->deleteJson(route('universities.destroy', compact('university')));
        $response->assertNoContent();
        $this->assertDatabaseMissing('universities', [
            'name' => $university->name,
            'description' => $university->description,
            'user_id' => $university->user->id,
            'id' => $university->id,
            'faculty' => $university->faculty,
            'start_date' => $university->start_date,
            'finish_date' => $university->finish_date
        ]);
    }

    public function test_not_auth_user_delete_university()
    {
        $user = User::factory()->create();
        $university = University::factory()->for($user)->create();
        $this->assertDatabaseHas('universities', [
            'name' => $university->name,
            'description' => $university->description,
            'user_id' => $university->user->id,
            'id' => $university->id,
            'faculty' => $university->faculty,
            'start_date' => $university->start_date,
            'finish_date' => $university->finish_date
        ]);
        $response = $this->deleteJson(route('universities.destroy', compact('university')));
        $response->assertStatus(401);
        $this->assertDatabaseHas('universities', [
            'name' => $university->name,
            'description' => $university->description,
            'user_id' => $university->user->id,
            'id' => $university->id,
            'faculty' => $university->faculty,
            'start_date' => $university->start_date,
            'finish_date' => $university->finish_date
        ]);
    }

    public function test_auth_user_can_not_delete_university()
    {
        $user = User::factory()->create();
        $another_user = User::factory()->create();
        $university = University::factory()->for($user)->create();
        Passport::actingAs($another_user, [route('universities.destroy', compact('university'))]);
        $this->assertDatabaseHas('universities', [
            'name' => $university->name,
            'description' => $university->description,
            'user_id' => $university->user->id,
            'id' => $university->id,
            'faculty' => $university->faculty,
            'start_date' => $university->start_date,
            'finish_date' => $university->finish_date
        ]);
        $response = $this->deleteJson(route('universities.destroy', compact('university')));
        $response->assertForbidden();
        $this->assertDatabaseHas('universities', [
            'name' => $university->name,
            'description' => $university->description,
            'user_id' => $university->user->id,
            'id' => $university->id,
            'faculty' => $university->faculty,
            'start_date' => $university->start_date,
            'finish_date' => $university->finish_date
        ]);
    }

    public function test_create_university_validate_required_name()
    {
        $user = User::factory()->create();
        $data = ['description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('universities.store', $data)]);
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_create_university_validate_required_faculty()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'description' => 'University description, university description, university description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('universities.store', $data)]);
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_create_university_validate_required_start_date()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('universities.store', $data)]);
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'finish_date' => $data['finish_date']]);
    }

    public function test_create_university_validate_required_finish_date()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d")];
        Passport::actingAs($user, [route('universities.store', $data)]);
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date']]);
    }

    public function test_success_create_university_validate_nullable_description()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        Passport::actingAs($user, [route('universities.store', $data)]);
        $response = $this->postJson(route('universities.store', $data));
        $response->assertStatus(201);
        $this->assertDatabaseHas('universities', ['name' => $data['name'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_university_validate_required_name()
    {
        $user = User::factory()->create();
        $data = ['description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.update', $university)]);
        $response = $this->putJson(route('universities.update', $university), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_university_validate_required_faculty()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'description' => 'University description, university description, university description.', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.update', $university)]);
        $response = $this->putJson(route('universities.update', $university), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_university_validate_required_start_date()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'finish_date' => date('Y-m-d')];
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.update', $university)]);
        $response = $this->putJson(route('universities.update', $university), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'finish_date' => $data['finish_date']]);
    }

    public function test_update_university_validate_required_finish_date()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'description' => 'University description, university description, university description.', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d")];
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.update', $university)]);
        $response = $this->putJson(route('universities.update', $university), $data);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('universities', ['name' => $data['name'], 'description' => $data['description'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date']]);
    }

    public function test_success_update_university_validate_nullable_description()
    {
        $user = User::factory()->create();
        $data = ['name' => '123456', 'faculty' => 'University faculty', 'start_date' => date("Y-m-d"), 'finish_date' => date('Y-m-d')];
        $university = University::factory()->for($user)->create();
        Passport::actingAs($user, [route('universities.update', $university)]);
        $response = $this->putJson(route('universities.update', $university), $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('universities', ['name' => $data['name'], 'faculty' => $data['faculty'], 'start_date' => $data['start_date'], 'finish_date' => $data['finish_date']]);
    }
}
