<?php

namespace Tests\Feature;

use App\Models\Like;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class LikeTest extends TestCase
{
    public function test_auth_user_create_like()
    {
        $user = User::factory()->create();
        $data = [
            'user_id' => $user->id
        ];
        $current_user = User::factory()->create();
        Passport::actingAs($current_user, [route('likes.store')]);
        $res = $this->postJson(route('likes.store'), $data);
        $res->assertStatus(201);
        $res->assertJsonStructure([
            'data'
        ]);
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id
        ]);
    }

    public function test_auth_user_delete_like()
    {
        $user = User::factory()->create();
        $current_user = User::factory()->create();
        Passport::actingAs($current_user, [route('likes.store')]);

        $like = new Like(['user_id' => $user->id, 'author_id' => $current_user->id]);
        $like->save();
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id
        ]);
        $res3 = $this->postJson(route('likes.store'), ['user_id' => $like->user_id]);
        $res3->assertStatus(200);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id
        ]);
    }

    public function test_not_auth_user_create_like()
    {
        $user = User::factory()->create();
        $data = [
            'user_id' => $user->id
        ];
        $res = $this->postJson(route('likes.store'), $data);
        $res->assertStatus(401);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id
        ]);
    }

    public function test_not_auth_user_delete_like()
    {
        $user = User::factory()->create();
        $current_user = User::factory()->create();

        $like = new Like(['user_id' => $user->id, 'author_id' => $current_user->id]);
        $like->save();
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id
        ]);
        $res3 = $this->postJson(route('likes.store'), ['user_id' => $like->user_id]);
        $res3->assertStatus(401);
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id
        ]);
    }

    public function test_auth_user_create_like_validate_required()
    {
        $user = User::factory()->create();
        $data = [];
        $current_user = User::factory()->create();
        Passport::actingAs($current_user, [route('likes.store')]);
        $res = $this->postJson(route('likes.store'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id
        ]);
    }

    public function test_auth_user_delete_like_validate_required()
    {
        $user = User::factory()->create();
        $current_user = User::factory()->create();
        Passport::actingAs($current_user, [route('likes.store')]);

        $like = new Like(['user_id' => $user->id, 'author_id' => $current_user->id]);
        $like->save();
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id
        ]);
        $res3 = $this->postJson(route('likes.store'), []);
        $res3->assertStatus(422);
    }

    public function test_auth_user_create_like_validate_numeric()
    {
        $user = User::factory()->create();
        $data = [
            'user_id' => 'qwe'
        ];
        $current_user = User::factory()->create();
        Passport::actingAs($current_user, [route('likes.store')]);
        $res = $this->postJson(route('likes.store'), $data);
        $res->assertStatus(422);
        $res->assertJsonStructure([
            'errors'
        ]);
        $this->assertDatabaseMissing('likes', [
            'user_id' => $user->id
        ]);
    }

    public function test_auth_user_delete_like_validate_numeric()
    {
        $user = User::factory()->create();
        $current_user = User::factory()->create();
        Passport::actingAs($current_user, [route('likes.store')]);

        $like = new Like(['user_id' => $user->id, 'author_id' => $current_user->id]);
        $like->save();
        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id
        ]);
        $res3 = $this->postJson(route('likes.store'), ['user_id' => 'qwe']);
        $res3->assertStatus(422);
    }
}
