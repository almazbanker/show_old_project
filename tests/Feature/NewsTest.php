<?php

namespace Tests\Feature;

use App\Models\News;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class NewsTest extends TestCase
{
    public function test_auth_user_get_all_news()
    {
        News::factory()->count(10)->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('news.index')]);
        $res = $this->getJson(route('news.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'picture',
                    'title',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_all_news()
    {
        News::factory()->count(10)->create();
        $res = $this->getJson(route('news.index'));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'picture',
                    'title',
                    'description'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_auth_user_get_news()
    {
        $news = News::factory()->create();
        $user = User::factory()->create();
        Passport::actingAs($user, [route('news.show', $news->id)]);
        $res = $this->getJson(route('news.show', $news->id));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_not_auth_user_get_news()
    {
        $news = News::factory()->create();
        $res = $this->getJson(route('news.show', $news->id));
        $res->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'description'
            ]
        ]);
        $res->assertStatus(200);
    }
}
