<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Subcomment;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class SubcommentTest extends TestCase
{
    public function test_auth_user_get_all_subcomments()
    {
        $user = User::factory()->create();
        $comments = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->count(10)->create();
        foreach ($comments as $comment) {
            Subcomment::factory()->state(['user_id' => $user->id, 'comment_id' => $comment->id])->count(rand(3, 5))->create();
        }
        Passport::actingAs($user, [route('subcomments.index', $comments[0]->id)]);
        $res = $this->getJson(route('subcomments.index', $comments[0]->id));
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'content',
                    'created_at',
                    'user'
                ]
            ]
        ]);
        $res->assertStatus(200);
    }

    public function test_auth_user_create_subcomment()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $data = [
            'content' => 'Comment content',
            'comment_id' => $comment->id,
        ];
        Passport::actingAs($user, [route('subcomments.store', $comment->id)]);
        $res = $this->postJson(route('subcomments.store', $comment->id), $data);
        $res->assertStatus(201);
        $this->assertDatabaseHas('subcomments', [
            'content' => $data['content'],
            'comment_id' => $data['comment_id']
        ]);
    }

    public function test_auth_user_create_subcomment_validate_required_content()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $data = [
            'content' => 'Comment content'
        ];
        Passport::actingAs($user, [route('subcomments.store', $comment->id)]);
        $res = $this->postJson(route('subcomments.store', $comment->id), $data);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_subcomment_validate_string_content()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $data = [
            'content' => 123456789,
            'comment_id' => $comment->id
        ];
        Passport::actingAs($user, [route('subcomments.store', $comment->id)]);
        $res = $this->postJson(route('subcomments.store', $comment->id), $data);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_subcomment_validate_required_comment_id()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $data = [
            'content' => '123456789',
        ];
        Passport::actingAs($user, [route('subcomments.store', $comment->id)]);
        $res = $this->postJson(route('subcomments.store', $comment->id), $data);
        $res->assertStatus(422);
    }

    public function test_auth_user_create_subcomment_validate_numeric_comment_id()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $data = [
            'content' => '123456789',
            'comment_id' => '$comment->id'
        ];
        Passport::actingAs($user, [route('subcomments.store', $comment->id)]);
        $res = $this->postJson(route('subcomments.store', $comment->id), $data);
        $res->assertStatus(422);
    }

    public function test_not_auth_user_create_comment()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $data = [
            'content' => 'Comment content',
            'comment_id' => $comment->id
        ];
        $res = $this->postJson(route('subcomments.store'), $data);
        $res->assertStatus(401);
        $this->assertDatabaseMissing('subcomments', [
            'content' => $data['content'],
            'comment_id' => $data['comment_id'],
            'user_id' => $user->id
        ]);
    }

    public function test_auth_user_destroy_comment()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $subcomment = Subcomment::factory()->for($user)->for($comment)->create();
        Passport::actingAs($user, [route('subcomments.destroy', $subcomment->id)]);
        $res = $this->deleteJson(route('subcomments.destroy', $subcomment->id));
        $res->assertStatus(204);
        $this->assertDatabaseMissing('subcomments', [
            'id' => $subcomment->id,
            'content' => $subcomment->content,
            'user_id' => $subcomment->user_id,
            'comment_id' => $subcomment->comment_id
        ]);
    }

    public function test_not_auth_user_destroy_comment()
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->state(['user_id' => $user->id, 'author_id' => $user->id])->create();
        $subcomment = Subcomment::factory()->for($user)->for($comment)->create();
        $res = $this->deleteJson(route('subcomments.destroy', $subcomment->id));
        $res->assertStatus(401);
        $this->assertDatabaseHas('subcomments', [
            'id' => $subcomment->id,
            'content' => $subcomment->content,
            'user_id' => $subcomment->user_id,
            'comment_id' => $subcomment->comment_id
        ]);
    }
}
