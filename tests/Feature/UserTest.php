<?php

namespace Tests\Feature;

use App\Models\Country;
use App\Models\Questionnaire;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_get_kg_mediators()
    {
        $country = Country::factory()->state(['code' => '996'])->create();
        $user = User::factory()->create();
        User::factory()->count(10)->state(['approved' => true])->has(Questionnaire::factory()->state(['country_id' => $country->id]))->create();
        Passport::actingAs($user, [route('users.index')]);
        $res = $this->getJson(route('users.index'));
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'email',
                    'likes',
                    'link'
                ]
            ]
        ]);
    }

    public function test_not_auth_get_kg_mediators()
    {
        $country = Country::factory()->state(['code' => '996'])->create();
        User::factory()->count(10)->state(['approved' => true])->has(Questionnaire::factory()->state(['country_id' => $country->id]))->create();
        $res = $this->getJson(route('users.index'));
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'email',
                    'likes',
                    'link'
                ]
            ]
        ]);
    }

    public function test_get_user()
    {
        $users = User::factory()->count(2)->create();
        Passport::actingAs($users[0], [route('users.show', $users[1]->id)]);
        $res = $this->getJson(route('users.show', $users[1]->id));
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'likes',
                'link'
            ]
        ]);
    }

    public function test_not_auth_get_user()
    {
        $user = User::factory()->create();
        $res = $this->getJson(route('users.show', $user->id));
        $res->assertStatus(200);
        $res->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'likes',
                'link'
            ]
        ]);
    }

    public function test_get_international_mediators()
    {
        $country = Country::factory()->state(['code' => '200'])->create();
        $user = User::factory()->create();
        User::factory()->count(10)->state(['approved' => true])->has(Questionnaire::factory()->state(['country_id' => $country->id]))->create();
        Passport::actingAs($user, [route('users.index')]);
        $res = $this->getJson(route('users.index'));
        $res->assertStatus(200);
    }

    public function test_not_auth_get_international_mediators()
    {
        $country = Country::factory()->state(['code' => '200'])->create();
        User::factory()->count(10)->state(['approved' => true])->has(Questionnaire::factory()->state(['country_id' => $country->id]))->create();
        $res = $this->getJson(route('users.index'));
        $res->assertStatus(200);
    }
}
