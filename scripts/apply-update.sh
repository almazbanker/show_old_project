#!/usr/bin/env bash
# shellcheck disable=SC2086
set -e
INSTANCE_NAME=${1}
LATEST_ARTIFACT_NAME=${2}
APPLICATION_ROOT_PATH=${3}
VERSION=$(cat ${APPLICATION_ROOT_PATH}/VERSION)


PROJECT_ROOT=${APPLICATION_ROOT_PATH}/application
BACKUP_DIR=/application_backups

# Create backup directory if not exist
mkdir -p ${BACKUP_DIR}
BACKUP_DATE=$(date +"%Y_%m_%d__%H_%M")
BACKUP_DIR_PATH=${BACKUP_DIR}/${PROJECT_ROOT}_backup_${BACKUP_DATE}_v${VERSION}

STASH_DB_DIR=${APPLICATION_ROOT_PATH}/db_backups
STASH_DB_PATH=${STASH_DB_DIR}/${BACKUP_DATE}

mkdir -p ${STASH_DB_DIR}

mkdir -p ${STASH_DB_PATH}

pg_dump mediator | gzip > mediator.gz
mv mediator.gz ${STASH_DB_PATH}

info() {
echo -e "\e[33m[Info]     \e[33m$1 \e[39m $(for i in {12..21} ; do echo -en "\e[33;1;${i}m.\e[0m" ; done ; echo)"
}

success() {
echo -e "\e[32m[Success] \e[32m $1 \e[39m $(for i in {12..21} ; do echo -en "\e[32;1;${i}m.\e[0m" ; done ; echo)"
}

if [[ "$(id -u)" != "0" ]]; then
 echo "This script must be run as root" 1>&2
 exit 1
fi

cd ${APPLICATION_ROOT_PATH}

mkdir -p ${STASH_DB_DIR}

info "# Creating backup"
if [[ -f ${DB_PATH} ]]; then
   info "# Put db to temporary stash"
   cp ${DB_PATH} ${STASH_DB_PATH}
fi

mkdir -p ${BACKUP_DIR_PATH}
mv ${PROJECT_ROOT} ${BACKUP_DIR_PATH}

info "# Extracting app package"
mkdir -p ${PROJECT_ROOT}
tar -xzf ${LATEST_ARTIFACT_NAME} -C ${PROJECT_ROOT}
if [[ -f ${STASH_DB_PATH} ]]; then
    info "# Restore DB from stash"
    mv ${STASH_DB_PATH} ${DB_PATH}
fi

info "# Setup permission for cache dir and storage"
chmod -R 777 ${PROJECT_ROOT}/storage/
chmod -R 777 ${PROJECT_ROOT}/bootstrap/cache

info "# Set user to .env file"
chown root: ${PROJECT_ROOT}/.env

info "# Set application key"
cd ${PROJECT_ROOT}
php artisan key:generate

info "# Optimizing Configuration Loading..."
php artisan config:cache

info "# Optimizing Route Loading"
php artisan route:cache

info "# Optimizing View Loading"
php artisan view:cache

info "# Database folder permission"
chmod -R 775 database
chown -R $(whoami) database

#info "# Restart nginx server"
#service nginx restart

cd ${APPLICATION_ROOT_PATH}

info "# Destroy archive ${LATEST_ARTIFACT_NAME}"
rm ${LATEST_ARTIFACT_NAME}
info "# Destroy VERSION file"
rm VERSION
info "# Destroy script"
rm apply-update.sh

success "Update successfully!"
