#!/usr/bin/env bash

set -e

BRANCH=${1}
VERSION=${2}
SECRET_SERVER_ENVIRONMENT_FILE_PATH=${3}

info() {
echo -e "\e[33m[Info]     \e[33m$1 \e[39m $(for i in {12..21} ; do echo -en "\e[33;1;${i}m>\e[0m" ; done ; echo)"
}

success() {
echo -e "\e[32m[Success] \e[32m $1 \e[39m $(for i in {12..21} ; do echo -en "\e[32;1;${i}m>\e[0m" ; done ; echo)"
}

info "build started under user $(whoami)"

info "Clear all cache"
php artisan cache:clear
php artisan view:clear
php artisan config:clear
php artisan route:cache

info "Install environment for server ${BRANCH}"

cat "$SECRET_SERVER_ENVIRONMENT_FILE_PATH" > .env

info "Install requirements"
composer install

info "Generate application key"
php artisan key:generate

info "Create directory 'artifacts' if not exists"
mkdir -p ${HOME}/artifacts

info "Create application artifact for version: ${VERSION}"
tar -czf ${HOME}/artifacts/application-${VERSION}.tar.gz \
--exclude=README.md \
--exclude=tests \
.

ls ${HOME}/artifacts/

success "Create package completed!"
