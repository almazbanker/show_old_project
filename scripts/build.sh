#!/usr/bin/env bash

set -e

info() {
echo -e "\e[33m[Info]     \e[33m$1 \e[39m $(for i in {12..21} ; do echo -en "\e[33;1;${i}m>\e[0m" ; done ; echo)"
}

success() {
echo -e "\e[32m[Success] \e[32m $1 \e[39m $(for i in {12..21} ; do echo -en "\e[32;1;${i}m>\e[0m" ; done ; echo)"
}

info "./build.sh starts here..."
info "build started under user $(whoami)"

info "Install environment for unit tests"
cp .env.unit .env

info "Install requirements   "
composer install

info "Generate application key   "
php artisan key:generate

#info "migrate database and seed"
#php artisan migrate --seed

info "Start unit test   "
#php artisan test --group=license_user

info "Setup environment for dusk test"
cp .env.dusk .env
info "Generate application key for dusk test   "
php artisan key:generate
sleep 5
info "Install requirements for frontend, start dev server frontend   "
cd frontend && npm install && export NODE_OPTIONS="--max-old-space-size=8192" && npm install node-sass --save && CI=false npm run build
#cd ..
info "Install chrome-driver version  "
#php artisan dusk:chrome-driver
info "Run server for dusk test in background mode   "
#nohup bash -c "export DUSK_SERVER=true && php artisan serve --env=dusk --port=4343 2>&1 &" && sleep 5

info "Start dusk tests  "
#cd frontend
#npm install
#sleep 5

#cd ..
#php artisan dusk --group=login

success "Completed testing!"
