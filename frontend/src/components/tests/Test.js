import React, {useEffect, useState} from 'react';
import {post} from "../../request";
import {isAuth} from "../../utils";
import Button from "@mui/material/Button";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardActions from "@mui/material/CardActions";
import Card from "@mui/material/Card";
import {useParams} from "react-router-dom";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Container from "@mui/material/Container";
import Timer from "../timer/Timer";

const Test = () => {

    const {id} = useParams();
    const token = isAuth();
    const [isLoading, setIsLoading] = useState(false);
    const [question, setQuestion] = useState([]);
    const [value, setValue] = React.useState();

    useEffect(() => {
        const response = post("/first/question", {level: 1, category_id: id}, token);
        response.then(data => {
            if(data.data === 'error') {
                window.location.replace("/home");
            }
            setQuestion(data);
        });
        setIsLoading(true);
    }, [isLoading === false]);


    const handleChange = (event) => {
        setValue(event.target.value);
    };


    const saveAnswer = () => {
        const response = post("/tests",
            {
                category_id: id,
                answer: value,
                test_id: question.test_id,
                level: 1,
                question_id: question.question_id,
            },
            token);

        response.then(payload => {
            if(payload.data === 'finish') {
                localStorage.removeItem("time");
                window.location.replace("/answers");
            } else {
                setQuestion(payload);
            }
        });
    }

    return (
        <Container>
            <Timer category_id={id} value={value} question={question} level={1} />
            {question.variants &&
                <Card sx={{marginBottom: 20, marginTop: 20, marginX: 'auto'}}>
                    <CardContent>
                        <Typography sx={{fontSize: 14}} color="text.secondary"
                                    gutterBottom>
                            {question.question}
                        </Typography>
                        <FormControl>
                            <FormLabel id="variants">Variants</FormLabel>
                            <RadioGroup
                                aria-labelledby="variants"
                                name="answer"
                                row
                                onChange={handleChange}
                            >
                                <FormControlLabel value={question.variants[0]} control={<Radio/>}
                                                  label={question.variants[0]}/>
                                <FormControlLabel value={question.variants[1]} control={<Radio/>}
                                                  label={question.variants[1]}/>
                                <FormControlLabel value={question.variants[2]} control={<Radio/>}
                                                  label={question.variants[2]}/>
                                <FormControlLabel value={question.variants[3]} control={<Radio/>}
                                                  label={question.variants[3]}/>
                            </RadioGroup>
                        </FormControl>
                    </CardContent>
                    <CardActions>
                        <Button variant="outlined" color="secondary"
                                onClick={saveAnswer}>
                            Save
                        </Button>
                    </CardActions>
                </Card>
            }
        </Container>
    );
}

export default Test;
