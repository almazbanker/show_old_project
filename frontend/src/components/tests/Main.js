import React, {useEffect, useState} from 'react';
import {get} from "../../request";
import {isAuth} from "../../utils";
import {Link as RouterLink, useParams} from "react-router-dom";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ListItemText from "@mui/material/ListItemText";
import Box from "@mui/material/Box";
import {CircularProgress} from "@mui/material";

const TestStart = () => {
    const {id} = useParams();
    const token = isAuth();
    const [isLoading, setIsLoading] = useState(false);
    const [category, setCategory] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        const category = get("/categories/" + id, token);
        category.then(data => {
            setCategory(data.data)
            setIsLoading(false);
        })
    }, [])

    return (
        <>
            {isLoading &&
                <Box sx={{width: '100%'}}>
                    <CircularProgress/>
                </Box>
            }
            {category &&
                <List  dense sx={{maxWidth: 600, marginBottom: 40, marginTop: 40, marginX: 'auto'}}>
                    <ListItem key={category.id}>
                        <ListItemButton>
                            <ListItemAvatar>
                                <Avatar
                                    alt={category.name}
                                    src={category.picture}
                                />
                            </ListItemAvatar>
                            <ListItemText primary={category.name}/>
                            <Button component={RouterLink} sx={{maxWidth: 60}} to={`/test/start/${id}`}
                                    className="card__btn">Start</Button>
                        </ListItemButton>
                    </ListItem>
                </List>
            }
        </>
    );
};

export default TestStart;
