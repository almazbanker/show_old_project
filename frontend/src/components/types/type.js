import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {isAuth} from "../../utils";
import {get} from "../../request";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import Grid from "@mui/material/Grid";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import {useTranslation} from "react-i18next";

const Type = () => {
    const {id} = useParams();
    const [isLoading, setIsLoading] = useState(false);
    const [type, setType] = useState(null);
    const {t} = useTranslation();
    const token = isAuth();

    useEffect(() => {
        setIsLoading(true);
        const response = get("/types/" + id, token);
        response.then(data => {
            setType(data.data);
            setIsLoading(false);
        })
    }, [])
    return (
        <>
            {isLoading &&
            <Box sx={{width: '100%'}}>
                <LinearProgress/>
            </Box>
            }
            {type &&
            <div>
                <div className="container">
                <h2 className="h22 text-color">{t('Type')}: {type.name}</h2>
                    <div className="picture picClass">
                        <div className="content-wrapper">
                        <img className="img-fluid" width={400} height={250}
                             src={type.picture}
                             alt="#"/>
                        </div>
                    </div>
                    <p className="news-card__excerpt_show">{type.description}</p>
                </div>
                <h2 className="h22 text-color">{t('Mediators')}</h2>
                <div className="content-wrapper">
                    <Grid sx={{mt: 3}} container spacing={{xs: 3, md: 4}} columns={{xs: 4, sm: 8, md: 12}}>
                        {type.category.users.map(user =>
                            <Grid item xs={2} sm={4} md={4} key={user.id}>
                                <div className="our-team">
                                    <div className="picture">
                                        <img className="img-fluid"
                                             src="https://static.tildacdn.com/tild3931-6635-4265-b232-663830313530/12.jpg"
                                             alt="#"/>
                                    </div>
                                    <div className="team-content">
                                        <h3 className="name">{user.name}</h3>
                                        <h4 className="title">{user.email}</h4>
                                    </div>
                                    <ul className="social">
                                        <li><a href="#" aria-hidden="true"><TwitterIcon/></a></li>
                                        <li><a href="#" aria-hidden="true"><InstagramIcon/></a></li>
                                        <li><a href="#" aria-hidden="true"><FacebookRoundedIcon/></a></li>
                                    </ul>
                                </div>
                            </Grid>
                        )}
                    </Grid>
                </div>
            </div>
            }
        </>
    );
};

export default Type;
