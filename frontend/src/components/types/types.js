import {Link} from "react-router-dom";
import '../categories/Categories.css';

const Types = (props) => {

    return (
            <div key={props.type.id} className="news-card">
                <Link to={`/types/${props.type.id}`} className="news-card__card-link"> </Link>
                <img
                    src={props.type.picture}
                    alt="" className="news-card__image" />
                <div className="news-card__text-wrapper">
                    <h2 className="news-card__title"> {props.type.name}</h2>
                    <div className="news-card__details-wrapper">
                        <p className="news-card__excerpt">{props.type.description}</p>
                    </div>
                </div>
            </div>
    );
};

export default Types;
