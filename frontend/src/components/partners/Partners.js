import React, {Component} from 'react';
import {API_BASE_URL} from "../../config";
import Grid from '@mui/material/Grid';
import {Message} from "@mui/icons-material";
import "./Partners.css";


export default class Partners extends Component {
    constructor(props) {
        super(props);
        this.state = {
            partners: null,
            isLoading: false
        };
    }
    async getPartners() {
        if (!this.state.partners) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/partners', {
                    headers: {
                        Accept: 'application/json',
                        'X-localization': localStorage.getItem('localization') ? localStorage.getItem('localization') : 'en',
                    }
                });
                const partnerList = await response.json();
                this.setState({partners: partnerList.data, isLoading: false})
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }
    componentDidMount() {
        this.getPartners().then()
    }
    render() {
        return (
            <div className="container">
                {this.state.isLoading && <Message info header="Loading Partners..." />}
                {this.state.partners &&
                    <>
                        <Grid sx={{mt: 3}} container spacing={{xs: 2, md: 3}} columns={{xs: 2, sm: 4, md: 8}}>
                            <div className="cards-list">
                                {this.state.partners.map( partner =>
                                    <div className="card 1" key={partner.id}>
                                        <div className="card_image"><img src={partner.picture} alt=""/></div>
                                        <div className="card_title title-white">
                                            <p>{partner.name}</p>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </Grid>
                    </>
                }
            </div>
        )
    }

}





