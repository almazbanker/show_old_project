import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import './BasicModal.scss'
import {post} from "../../request";
import {isRecaptcha} from "../../utils";
import {useRef, useState} from "react";
import TextField from "@mui/material/TextField";
import {useTranslation} from "react-i18next";
import ReCAPTCHA from "react-google-recaptcha";
import {useForm} from "react-hook-form";
import Alert from "../alert/Alert";
function onChange(value) {
    console.log("Captcha value:", value);
}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BasicModal() {
    const [showAlert, setShowAlert] = useState(false);
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const {t} = useTranslation();

    const recaptchaRef = useRef(null)


    const onSubmit = async (data) => {
        const recaptchaValue = await recaptchaRef.current.getValue();
        console.log(data, recaptchaValue)
        const payload = {
            ...data,
            'g-recaptcha-response': recaptchaValue
        }
        const response = await post("/callback", payload );
        if(Object.keys(response).includes('success')){
            window.grecaptcha.reset();
            reset();
            setOpen(false);
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
        }
    }

    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onSubmit"
    });

    const sitekey = isRecaptcha();
    return (
        <div>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(121,253,2,0.3)'}/>
            <button className="home__btn" onClick={handleOpen}>{t('Free Consulting')}</button>

            <Modal
                className="modal"
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}
                     component="form"
                     autoComplete="off"
                     onSubmit={handleSubmit(onSubmit)}
                >
                    <TextField
                        label="Name:"
                        id="outlined-size-small"
                        size="small"
                        fullWidth
                        name="name"
                        {...register('name', {
                            required: t('This field is required'),
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 10, color: 'red'}}>
                        {errors?.name && <p>{errors?.name?.message || "Error!"}</p>}
                    </div>

                    <TextField
                        sx={{mt: 4}}
                        label="Phone"
                        id="outlined-size-small"
                        size="small"
                        fullWidth
                        name="phone_number"
                        {...register('phone_number', {
                            required: t('This field is required'),
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /^[\+\d+$]/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 10, color: 'red'}}>
                        {errors?.phone_number && <p>{errors?.phone_number?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        sx={{mt: 4}}
                        label="Description:"
                        id="outlined-size-small"
                        size="small"
                        fullWidth
                        name="description"
                        {...register('description',{
                            required: t('This field is required'),
                            minLength: {
                                value: 5,
                                message: t("This field must be at least 5 characters"),
                            },
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 10, color: 'red'}}>
                        {errors?.description && <p>{errors?.description?.message || "Error!"}</p>}
                    </div>

                    <ReCAPTCHA
                        sitekey={sitekey}
                        onChange={onChange}
                        ref={recaptchaRef}
                    />


                    <Typography>
                        <div className="modal-footer">
                            <button onClick={handleClose} type="button" className="btn-close mt-5 bg-dark" data-bs-dismiss="modal">{t('Close')}
                            </button>
                            <button type="submit" className="btn-save mt-5">{t('Save')}</button>
                        </div>
                    </Typography>
                </Box>
            </Modal>
        </div>
    );
}
