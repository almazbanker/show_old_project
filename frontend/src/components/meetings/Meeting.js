import * as React from 'react';
import {useEffect, useState} from "react";
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Button from '@mui/material/Button';
import {get, post, remove} from '../../request';
import {isAdmin, isAuth, isModerator} from "../../utils";
import Container from "@mui/material/Container";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardActions from "@mui/material/CardActions";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {useTranslation} from "react-i18next";
import {Link as RouterLink} from "react-router-dom"
import {useForm} from "react-hook-form";


const Meetings = () => {
        const [isLoading, setIsLoading] = useState(false);
        const [meetings, setMeetings] = useState(null);
        const [categories, setCategories] = useState(null);

        const token = isAuth();
        const moderator = isModerator();
        const admin = isAdmin();
        const {t} = useTranslation();


        const MeetingDelete = (meetingForDelete) => {
            const response = remove(`/meetings/${meetingForDelete.id}`, token);
            response.then(() => {
                const newA = meetings.filter(meeting => meetingForDelete.id !== meeting.id);
                setMeetings(newA);
            })
        }

        const [password, setPassword] = useState('');
        const [meeting, setDataMeeting] = useState({
            category_id: '',
            topic: '',
            start_time: '',
            duration: '',
        });

        const savePassword = (e) => {
            setPassword(e.target.value);
        }

        const handleMeetingForm = (event) => {
            setDataMeeting({
                ...meeting,
                [event.target.name]: event.target.value
            });
        }

        const CreateMeeting = (e) => {
            e.preventDefault();
            const response = post("/meetings", meeting, token);
            response.then(payload => {
                setMeetings([...meetings, payload.data]);
            });
            setDataMeeting({
                category_id: '',
                topic: '',
                start_time: '',
                duration: '',
            });
        }

        const {
            register,
            formState: {
                errors,
                isValid
            },
            handleSubmit,
            reset,
        } = useForm({
            mode: "onBlur"
        });


        useEffect(() => {
            setIsLoading(true);
            const response = get(`/meetings`, token);
            const categories = get(`/categories`, token);
            response.then(data => {
                setMeetings(data.data);
                setIsLoading(false);
            });
            categories.then(data => {
                setCategories(data.data);
                setIsLoading(false);
            });
        }, []);

        return (
            <Container>
                {moderator === 'Moderator' || admin === 'Admin'  ?
                    <Box
                        component="form"
                        sx={{
                            '& .MuiTextField-root': {m: 1, width: '100%'},
                        }}
                        autoComplete="off"
                        style={{maxWidth: "600px", margin: "80px auto 80px"}}
                    >
                        <h1 style={{textAlign: "center"}}>{t('Create')} {t('New')} {t('Meeting')}</h1>
                        <TextField
                            label="Topic"
                            required
                            id="outlined-size-small"
                            value={meeting.topic}
                            size="small"
                            fullWidth
                            name="topic"
                            onChange={handleMeetingForm}
                        />
                        {categories &&
                            <div>
                                <Box sx={{marginLeft: 1, width: 600}}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">{t('Category')}</InputLabel>
                                        <Select
                                            label="Category"
                                            id="demo-simple-select"
                                            name="category_id"
                                            value={meeting.category_id}
                                            onChange={handleMeetingForm}
                                        >
                                            {categories.map(category =>
                                                <MenuItem key={category.id}
                                                          value={category.id}>{category.name}</MenuItem>
                                            )}
                                        </Select>
                                    </FormControl>
                                </Box>

                            </div>
                        }
                        <TextField
                            id="outlined-multiline-static"
                            label="Duration"
                            required
                            fullWidth
                            type="number"
                            name="duration"
                            value={meeting.duration}
                            onChange={handleMeetingForm}
                        />
                        <TextField
                            id="datetime-local"
                            label="Next appointment"
                            type="datetime-local"
                            name="start_time"
                            value={meeting.start_time}
                            sx={{width: 250}}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            onChange={handleMeetingForm}
                        />
                        <Button
                            style={{marginLeft: "10px", background: "#F2C64D", color: "white", border: "none"}}
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{mt: 3, mb: 2}}
                            onClick={CreateMeeting}
                        >
                            {t('Create')}
                        </Button>
                    </Box>
                    : null
                }
                {isLoading &&
                    <Box sx={{width: '100%'}}>
                        <LinearProgress/>
                    </Box>
                }
                {meetings &&
                    <div>
                        <h1 style={{textAlign: "center"}}>{t('Meetings')}</h1>
                        {meetings.map(meeting =>
                            <div key={meeting.id}>
                                <Card sx={{minWidth: 275, marginTop: 5}}>
                                    { moderator === 'Moderator' || admin === 'Admin' ?
                                        <CardActions>
                                            <Button variant="outlined" color="error"
                                                    onClick={() => MeetingDelete(meeting)}>
                                                {t('Delete')}
                                            </Button>
                                        </CardActions>
                                        : null
                                    }
                                    <CardContent>
                                        { moderator === 'Moderator' ||  admin === 'Admin'  ?
                                            <div>
                                                <Typography sx={{fontSize: 14}} color="text.secondary"
                                                            gutterBottom>
                                                    {meeting.password}
                                                </Typography>
                                                <Typography variant="body2">
                                                    <Link href={meeting.start_url} variant="body2">
                                                        {t('Start')}
                                                    </Link>
                                                </Typography>
                                            </div>
                                            : null
                                        }


                                        <Typography sx={{fontSize: 14}} color="text.secondary"
                                                    gutterBottom>
                                            {meeting.user}
                                        </Typography>
                                        <Typography sx={{fontSize: 14}} color="text.secondary"
                                                    gutterBottom>
                                            {meeting.topic}
                                        </Typography>

                                        <Typography sx={{fontSize: 14}} color="text.secondary"
                                                    gutterBottom>
                                            {meeting.category.name}, Start time {meeting.start_time}
                                        </Typography>

                                        <Typography variant="body2">
                                            <Link href={meeting.join_url} variant="body2">
                                                {t('Join')}
                                            </Link>
                                        </Typography>
                                    </CardContent>

                                    <CardActions>
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="Password"
                                            required
                                            type="text"
                                            {...register("password", {
                                                required: t('This field is required'),
                                                pattern: {
                                                    value: new RegExp(meeting.password),
                                                    message: "Неправильный пароль"
                                                },
                                            })}
                                            onChange={savePassword}
                                        />
                                        <Button sx={{padding: 2, marginLeft: 2}}
                                                variant="outlined"
                                                color="secondary"
                                                disabled={!isValid}
                                                component={RouterLink}
                                                to={`/mainTest/start/${meeting.category.id}`}>
                                            {t('Start Test')}
                                        </Button>
                                        <div style={{height: 20, color: 'red'}}>
                                            {errors?.password && <p>{errors?.password?.message || "Error!"}</p>}
                                        </div>
                                    </CardActions>
                                </Card>
                            </div>
                        )}
                    </div>
                }
            </Container>
        );
    }
;

export default Meetings;
