import React from 'react';
import style from './alerts.module.css';

const Alert = (props) => {
    return (
        <div className={props.show ? [style.alert, style.active].join(' ') : style.alert} style={{'backgroundColor': props.bgColor}}>
            {props.content}
        </div>
    );
};

export default Alert;
