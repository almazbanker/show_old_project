import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import LoadingButton from '@mui/lab/LoadingButton';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Link as RouterLink } from "react-router-dom";
import { post } from '../../request';
import {useRef, useState} from "react";
import {useForm} from "react-hook-form";
import {useTranslation} from "react-i18next";
import ReCAPTCHA from "react-google-recaptcha";
import {isRecaptcha} from "../../utils";

const theme = createTheme();

export default function Login() {
    function onChange(value) {
        console.log("Captcha value:", value);
    }
    const [isLoading, setIsLoading] = useState(false);
    const {t} = useTranslation();

    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onBlur"
    });

    const [errorsValid, setErrorsValid] = useState(null);
    const recaptchaRef = useRef(null)

    const onSubmit = async (data) => {
        const recaptchaValue = await recaptchaRef.current.getValue();
        console.log(data, recaptchaValue)
        const payload = {
            ...data,
            'g-recaptcha-response': recaptchaValue
        }

        const response = await post("/login/", payload);
        if (Object.keys(response).includes('token')) {
            localStorage.setItem('token', response.token);
            localStorage.setItem('name', response.name);
            localStorage.setItem('approved', response.approved);
            localStorage.setItem('id', response.id);
            localStorage.setItem('moderator', response.moderator);
            localStorage.setItem('admin', response.admin);
            window.location.href = "/";
        } else {
            if (response.errors){
                setErrorsValid(response.errors);
            }else {
                setErrorsValid(response);
            }

        }
        setIsLoading(false);
        reset();
    }
    const sitekey = isRecaptcha();
    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 16,
                        marginBottom: 23,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >

                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        {t('Login')}
                    </Typography>
                    <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
                        <TextField
                            fullWidth
                            margin="normal"
                            label={t("Email")}
                            {...register("email", {
                                required: t('This field is required'),
                                pattern: {
                                    value: /([^(<)(>)])$/,
                                    message: t("This field does not match the format")
                                }
                            })}
                        />
                        <div style={{height: 10, color: 'red'}}>
                            {errors?.email && <p>{errors?.email?.message || "Error!"}</p>}
                        </div>

                        { errorsValid ?
                            <>
                                {errorsValid.email ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.email.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }

                        <TextField
                            margin="normal"
                            label={t('Password')}
                            type="password"
                            fullWidth
                            {...register("password", {
                                required: t('This field is required'),
                                pattern: {
                                    value: /([^(<)(>)])$/,
                                    message: t("This field does not match the format")
                                }
                            })}
                        />
                        <div style={{height: 10, color: 'red'}}>
                            {errors?.password && <p>{errors?.password?.message || "Error!"}</p>}
                        </div>

                        { errorsValid ?
                            <>
                                {errorsValid.password ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.password.map(pass => {
                                            return (
                                                <>
                                                    <p>
                                                        {pass}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }

                        <ReCAPTCHA
                            sitekey={sitekey}
                            onChange={onChange}
                            ref={recaptchaRef}
                        />

                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label={t('Remember me')}
                        />
                        {errorsValid ?
                            <>
                                <p style={{color: 'red'}}>{errorsValid.error}</p>

                            </>
                            : null
                        }
                        <LoadingButton
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                            loading={isLoading}
                        >
                            {t('Login')}
                        </LoadingButton>
                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2">
                                    {t('Forgot password?')}
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link component={RouterLink} to="/auth/register" variant="body2">
                                    {t("Don't have an account? Sign Up")}
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
}
