import React, {useState, useEffect, useRef} from 'react';
import Avatar from '@mui/material/Avatar';
import LoadingButton from '@mui/lab/LoadingButton';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {Link as RouterLink} from "react-router-dom";
import {post} from '../../request';
import {isAuth, isRecaptcha} from "../../utils";
import {useForm} from "react-hook-form";
import {useTranslation} from "react-i18next";
import ReCAPTCHA from "react-google-recaptcha";

const theme = createTheme();

export default function Register() {

    function onChange(value) {
        console.log("Captcha value:", value);
    }

    const [isLoading, setIsLoading] = useState(false);
    const {t} = useTranslation();

    useEffect(() => {
        if (isAuth()) {
            window.location.href = '/';
        }
    }, []);

    const {
        register,
        formState: {
            errors
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onSubmit"
    });

    const [errorsValid, setErrorsValid] = useState(null);
    const recaptchaRef = useRef(null)


    const onSubmit = async (data) => {
        setIsLoading(true);
        const recaptchaValue = await recaptchaRef.current.getValue();
        console.log(data, recaptchaValue)
        const payload = {
            ...data,
            'g-recaptcha-response': recaptchaValue
        }
        const response = await post("/register", payload);
        if (Object.keys(response).includes('token')) {
            localStorage.setItem('token', response.token);
            localStorage.setItem('name', response.name);
            localStorage.setItem('approved', response.approved);
            localStorage.setItem('id', response.id);
            localStorage.setItem('moderator', response.moderator);
            localStorage.setItem('admin', response.admin);
            window.location.href = "/";
        } else {
            if (response.errors) {
                setErrorsValid(response.errors);
            } else {
                setErrorsValid(response);
            }

        }
        setIsLoading(false);
        reset();
    }

    const sitekey = isRecaptcha();
    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <Box
                    sx={{
                        marginTop: 16,
                        marginBottom: 13,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        {t('Registration')}
                    </Typography>
                    <Box component="form" noValidate onSubmit={handleSubmit(onSubmit)} sx={{mt: 3}}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id="firstName"
                                    label={t('First Name')}
                                    autoFocus
                                    {...register("name", {
                                        required: t('This field is required'),
                                        minLength: {
                                            value: 5,
                                            message: t("This field must be at least 5 characters")
                                        },
                                        maxLength: {
                                            value: 255,
                                            message: t("This field must not be greater than 255 characters"),
                                        },
                                        pattern: {
                                            value: /([^(<)(>)])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.name && <p>{errors?.name?.message || "Error!"}</p>}
                                </div>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    label={t('Email')}
                                    autoComplete="email"
                                    {...register("email", {
                                        required: t('This field is required'),
                                        pattern: {
                                            value: /([^(<)(>)])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.email && <p>{errors?.email?.message || "Error!"}</p>}
                                </div>

                                {errorsValid ?
                                    <>
                                        {errorsValid.email ?
                                            <div style={{height: 20, color: 'red'}}>
                                                {errorsValid.email.map(item => {
                                                    return (
                                                        <>
                                                            <p>
                                                                {item}
                                                            </p>
                                                        </>
                                                    );
                                                })}
                                            </div>
                                            : null}
                                    </>
                                    : null
                                }

                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label={t('Password')}
                                    type="password"
                                    fullWidth
                                    {...register("password", {
                                        required: t('This field is required'),
                                        pattern: {
                                            value: /([^(<)(>)])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.password && <p>{errors?.password?.message || "Error!"}</p>}
                                </div>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    label={t("Password confirmation")}
                                    type="password"
                                    {...register("password_confirmation", {
                                        required: t('This field is required'),
                                        pattern: {
                                            value: /([^(<)(>)])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.password_confirmation &&
                                    <p>{errors?.password_confirmation?.message || "Error!"}</p>}
                                </div>

                                {errorsValid ?
                                    <>
                                        {errorsValid.password ?
                                            <div style={{height: 20, color: 'red'}}>
                                                {errorsValid.password.map(pass => {
                                                    return (
                                                        <>
                                                            <p>
                                                                {pass}
                                                            </p>
                                                        </>
                                                    );
                                                })}
                                            </div>
                                            : null}
                                    </>
                                    : null
                                }


                                <ReCAPTCHA
                                    sitekey={sitekey}
                                    onChange={onChange}
                                    ref={recaptchaRef}
                                />

                            </Grid>
                        </Grid>
                        <LoadingButton
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{mt: 3, mb: 2}}
                            loading={isLoading}
                        >
                            {t('Registration')}
                        </LoadingButton>
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Link component={RouterLink} to="/auth/login" href="#" variant="body2">
                                    {t('Already have an account? Sign in')}
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
}
