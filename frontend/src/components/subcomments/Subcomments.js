import React, {useEffect, useState} from 'react';
import {get, post, remove} from "../../request";
import {AuthId, isAdmin, isAuth, isModerator} from "../../utils";
import Button from "@mui/material/Button";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardActions from "@mui/material/CardActions";
import Card from "@mui/material/Card";
import {TextField} from "@mui/material";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";
import LoadingButton from "@mui/lab/LoadingButton";
import {useForm} from "react-hook-form";
import Box from "@mui/material/Box";

const Subcomments = (props) => {

    const token = isAuth();
    const [showAlert, setShowAlert] = useState(false);
    const [showAlertDelete, setShowAlertDelete] = useState(false);
    const {t} = useTranslation();
    const [isLoading, setIsLoading] = useState(false);
    const [subcomments, setSubcomments] = useState([]);


    useEffect(() => {
        setSubcomments(props.comment.subcomments);
    }, []);


    const subcommentDelete = (id) => {
        console.log(id)
        const response = remove(`/subcomments/${id}`, token);
        response.then(() => {
            const newSubcomments = subcomments.filter(subcomment => id !== subcomment.id);
            setSubcomments(newSubcomments);
            setShowAlertDelete(true);
            setTimeout(() => {setShowAlertDelete(false)}, 2500);
        });
    }



    const subcommentPost = (data) => {
        const payload = {
            ...data,
            'comment_id': props.comment.id,
        }
        const response = post("/subcomments", payload, token);
        response.then(payload => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
            setSubcomments([...subcomments, payload.data]);
            reset();
        });
    }

    const admin = isAdmin();
    const moderator = isModerator();
    const authId = AuthId();

    const {
        register,
        formState: {
            errors
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onSubmit"
    });

    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <Alert show={showAlertDelete} content={t('Successfully deleted!!')} bgColor={'rgba(72,157,0,0.5)'}/>
            {isAuth() ?
                <div>
                    <Box
                        component="form"
                        fullWidth sx={{marginLeft: 11}}
                        onSubmit={handleSubmit(subcommentPost)}
                    >
                        <TextField
                            label="Add subcomment"  className="subcomment" variant="outlined"
                            {...register('content', {
                                required: t('This field is required'),
                                pattern: {
                                    value: /([^(<)(>)])$/,
                                    message: t("This field does not match the format")
                                }
                            })}
                        />
                        <div style={{height: 20, color: 'red'}}>
                            {errors?.content && <p>{errors?.content?.message || "Error!"}</p>}
                        </div>
                        <LoadingButton
                            style={{marginLeft: 7}}
                            type="submit"
                            fullWidth
                            variant="contained"
                            className="submit-comment"
                        >
                            {t('Save')}
                        </LoadingButton>
                    </Box>
                </div>
                : null
            }

            {subcomments &&
                <>

                    {subcomments.map(comment =>
                        <Card key={comment.content} sx={{minWidth: 275, marginBottom: 3, marginLeft: 11}}>
                            <CardContent>
                                <Typography sx={{fontSize: 14}} color="text.secondary"
                                            gutterBottom>
                                    {comment.user}
                                </Typography>
                                <Typography sx={{mb: 1.5}} color="text.secondary">
                                    {comment.created_at}
                                </Typography>
                                <Typography variant="body2">
                                    {comment.content}
                                </Typography>
                            </CardContent>
                            {admin === 'Admin' || moderator === 'Moderator' || authId === comment.user_id ?
                                <CardActions>
                                    <Button variant="outlined" color="error"
                                            onClick={() => subcommentDelete(comment.id)}>
                                        {t('Delete')}
                                    </Button>
                                </CardActions>
                                : null
                            }
                        </Card>
                    )}
                </>
            }
        </>
    );
};

export default Subcomments;

