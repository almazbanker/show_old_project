import React, {useEffect, useState} from 'react';
import {post} from "../../request";
import {isAuth} from "../../utils";
import {useParams} from "react-router-dom";
import Button from "@mui/material/Button";
import Youtube from "../player/Youtube";
import "./Training.css";
import Container from "@mui/material/Container";
import {useTranslation} from "react-i18next";

const Training = () => {
    const {id} = useParams();
    const token = isAuth();
    const [isLoading, setIsLoading] = useState(false);
    const [training, setTraining] = useState([]);
    const {t} = useTranslation();

    useEffect(() => {
        const response = post("/trainings", {category_id: id}, token);
        response.then(data => {
            setTraining(data.data);
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const nextPost = (e) => {
        e.preventDefault();
        const response = post("/trainings/next", {training_id: training.id, category_id: id}, token);
        response.then(payload => {
            if (payload.data === 'last') {
                window.location.replace("/test/category/" + id);
            }
            setTraining(payload.data);
        });
    }

    const previousPost = (e) => {
        e.preventDefault();
        const response = post("/trainings/previous", {training_id: training.id, category_id: id}, token);
        response.then(payload => {
            setTraining(payload.data);
        });
    }

    return (
        <Container>
        <div className="training">
            <div>
                <h1 className="buttons">{training.name}</h1>
                <p>  {training.description}</p>
                {training.videos &&
                    <div className="youtube">
                        {training.videos.map(video =>
                            <div key={video.id}>
                                <Youtube />
                            </div>
                        )
                        }
                    </div>
                }
            </div>
            <div className="buttons">
                <Button sx={{marginRight: 5}} variant="contained" onClick={previousPost}>{t('Previous')}</Button>
                <Button variant="contained" onClick={nextPost}>{t('Next')}</Button>
            </div>

        </div>
        </Container>
    );
};

export default Training;
