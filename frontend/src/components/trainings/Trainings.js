import * as React from 'react';
import {Link as RouterLink} from "react-router-dom";
import {useEffect, useState} from "react";
import {isAuth} from "../../utils";
import {get} from "../../request";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import {CircularProgress} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {useTranslation} from "react-i18next";

const Trainings = () => {

    const [isLoading, setIsLoading] = useState(false);
    const [categories, setCategories] = useState(null);
    const token = isAuth();
    const {t} = useTranslation();


    useEffect(() => {
        setIsLoading(true);
        const response = get("/categories/", token);
        response.then(data => {
            setCategories(data.data);
            setIsLoading(false);
        })
    }, [])

    return (
        <div>
            {isLoading &&
                <Box sx={{width: '100%'}}>
                    <CircularProgress/>
                </Box>
            }
            {categories &&
                <List dense sx={{maxWidth: 900, marginBottom: 20, marginTop: 20, marginX: 'auto'}}>
                    {categories.map(category => {
                        const labelId = `checkbox-list-secondary-label-${category}`;
                        return (
                            <ListItem key={category.id}>
                                <ListItemButton key={category.id}>
                                    <ListItemAvatar>
                                        <Avatar
                                            alt={category.name}
                                            src={category.picture}
                                        />
                                    </ListItemAvatar>
                                    <ListItemText id={labelId} primary={category.name}/>
                                    <Button component={RouterLink} sx={{maxWidth: 80}} to={`/training/${category.id}`} className="card__btn">{t('Start')}</Button>
                                </ListItemButton>
                            </ListItem>
                        );
                    })}
                </List>
            }
        </div>
    )
}

export default Trainings;
