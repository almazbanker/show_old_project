import React, {useEffect, useState} from 'react';
import { styled } from '@mui/material/styles';
import {Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, TextField} from "@mui/material";
import {get, post, put, remove} from "../../request";
import {isAuth} from "../../utils";
import Paper from '@mui/material/Paper';
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import './certificate.css';
import NavbarLinks from "../questionnaires/NavbarLinks";
import {useForm} from "react-hook-form";
import LoadingButton from "@mui/lab/LoadingButton";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";


const Certificates = () => {

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    const token = isAuth();
    const [showAlert, setShowAlert] = useState(false);
    const [showAlertDelete, setShowAlertDelete] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [certificates, setCertificates] = useState([]);
    const [certificateEdit, setCertificateEdit] = useState({id: '', name: '', description: '', start_date: '', finish_date: ''});
    const [countCertificates, setCountCertificates] = useState(0);
    const [active, setActive] = useState(false);
    const { t } = useTranslation();


    useEffect(() => {
        const response = get("/certificates", token);
        response.then(data => {
            setCertificates(data.data);
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const certificatePut = (data) => {
        put("/certificates/" + certificateEdit.id, data, token).then(() => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
        });
        modalClose();
        setIsLoading(false);
    }

    const handleDelete = (certificateDELETE) => {
        remove(`/certificates/${certificateDELETE.id}`, token).then(() => {
            const newCertificates = certificates.filter(certificate => certificateDELETE.id !== certificate.id);
            setCertificates(newCertificates);
            setCountCertificates(countCertificates - 1);
            setShowAlertDelete(true);
            setTimeout(() => {setShowAlertDelete(false)}, 2500);
        });
    }

    const modalClose = () => {
        setCertificateEdit({id: '', name: '', description: '', start_date: '', finish_date: ''});
        setActive(!active);
    }

    const modalEdit = (certificate) => {
        setCertificateEdit(certificate);
        setActive(!active);
    }


    const {
        register,
        formState: {
            errors
        },
        handleSubmit,
        reset
    } = useForm({
        mode: "onBlur"
    });


    const {
        register: register2,
        formState: {
            errors: errors2
        },
        setValue: setValue,
        handleSubmit: handleSubmit2
    } = useForm({
        mode: "onBlur",
    });


    const onPost = (data) => {
        post("/certificates", data, token).then(payload => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
            setCertificates([...certificates, payload.data]);
        });
       reset();
    }

    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <Alert show={showAlertDelete} content={t('Successfully deleted!!')} bgColor={'rgba(72,157,0,0.5)'}/>
            <NavbarLinks/>
            <div className="uni_bg">
                <div className="container">
                    <div>
                        <form style={{maxWidth: "600px", margin: "30px auto 50px"}} onSubmit={handleSubmit(onPost)}>
                            <Box   sx={{
                                '& .MuiTextField-root': { m: 1, width: '100%' },
                            }}>
                                <h3  style={{margin: "60px auto 20px"}} className="headerUni">{t('Create')}</h3>
                                <TextField
                                    label={t("Start date")}
                                    type="date"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    {...register("start_date", {
                                        required: t('This field is required'),
                                        pattern: {
                                            value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                            message: t("This field does not match the format")
                                        },
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.start_date && <p>{errors?.start_date?.message || "Error!"}</p>}
                                </div>
                                <TextField
                                    label={t("Finish date")}
                                    type="date"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}

                                    {...register("finish_date", {
                                        required: t('This field is required'),
                                        pattern: {
                                            value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.finish_date && <p>{errors?.finish_date?.message || "Error!"}</p>}
                                </div>
                                <TextField
                                    fullWidth
                                    label={t("Title")}
                                    {...register("name", {
                                        required: t('This field is required'),
                                        minLength: {
                                            value: 3,
                                            message: t("This field must be at least 3 characters"),
                                        },
                                        maxLength: {
                                            value: 255,
                                            message: t("This field must not be greater than 255 characters"),
                                        },
                                        pattern: {
                                            value: /([^(<)(>)])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.name && <p>{errors?.name?.message || "Error!"}</p>}
                                </div>
                                <TextField
                                    fullWidth
                                    multiline
                                    rows={4}
                                    label={t("Content")}
                                    {...register("description", {
                                        required: t('This field is required'),
                                        minLength: {
                                            value: 3,
                                            message: t("This field must be at least 3 characters"),
                                        },
                                        maxLength: {
                                            value: 255,
                                            message: t("This field must not be greater than 255 characters"),
                                        },
                                        pattern: {
                                            value: /([^(<)(>)])$/,
                                            message: t("This field does not match the format")
                                        }
                                    })}
                                />
                                <div style={{height: 20, color: 'red'}}>
                                    {errors?.description && <p>{errors?.description?.message || "Error!"}</p>}
                                </div>

                            </Box>
                            <LoadingButton
                                type="submit"
                                style={{marginLeft: 7}}
                                fullWidth
                                variant="contained"
                            >
                                {t('Create')}
                            </LoadingButton>
                        </form>
                    </div>
                    <TableContainer   component={Paper}>
                        <Table sx={{ minWidth: 700 }} aria-label="customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell sx={{width: '25%'}}>{t('Name')}</StyledTableCell>
                                    <StyledTableCell>{t('Start date')}</StyledTableCell>
                                    <StyledTableCell>{t('Finish date')}</StyledTableCell>
                                    <StyledTableCell>{t('Actions')}</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {certificates.map((certificate) => (
                                    <StyledTableRow key={certificate.id}>
                                        <StyledTableCell>{certificate.name}</StyledTableCell>
                                        <StyledTableCell>{certificate.start_date}</StyledTableCell>
                                        <StyledTableCell>{certificate.finish_date}</StyledTableCell>
                                        <StyledTableCell>
                                            <Button variant="outlined" sx={{marginRight: 1}} onClick={() => modalEdit(certificate)}>{t('Edit')}</Button>
                                            <Button variant="outlined" color="error" onClick={() => handleDelete(certificate)}>{t('Delete')}</Button>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
            <div className={active ? "certificate_modal active" : "certificate_modal notActive"} onClick={modalClose}>
                <div className="certificate_modal_body" onClick={(e) => e.stopPropagation()}>
                    <div>
                        <h3 className="headerUni2">{t('Edit')} {t('Certificate')}</h3>
                        <Box
                            component="form"
                            sx={{
                                '& .MuiTextField-root': { mt: 2, width: '100%' },
                               }}
                            style={{maxWidth: "600px", margin: "30px auto 50px"}}
                            autoComplete="off"
                            onSubmit={handleSubmit2(certificatePut)}
                        >
                            <TextField
                                label={t("Title")}
                                name="name"
                                focused
                                id="certificate-edit-name"
                                fullWidth
                                {...register2("name", {
                                    value:  setValue('name', certificateEdit.name),
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}

                            />
                            <span style={{height: 20, color: 'red'}}>
                                {errors2?.name && <span>{errors2?.name?.message || "Error!"}</span>}
                            </span>

                            <TextField
                                label={t("Content")}
                                multiline
                                fullWidth
                                focused
                                id="certificate-edit-description"
                                rows={4}
                                {...register2("description", {
                                    value:  setValue('description', certificateEdit.description),
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <span style={{height: 20, color: 'red'}}>
                                {errors2?.description && <span>{errors2?.description?.message || "Error!"}</span>}
                            </span>

                            <TextField
                                label={t("Start date")}
                                type="date"
                                focused
                                id="certificate-edit-start-date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                {...register2("start_date", {
                                    value: setValue('start_date', certificateEdit.start_date),
                                    required: t('This field is required'),
                                    pattern: {
                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />

                            <span style={{height: 20,color: 'red'}}>
                                {errors2?.start_date && <span>{errors2?.start_date?.message || "Error!"}</span>}
                            </span>

                            <TextField
                                label={t("Finish date")}
                                type="date"
                                focused
                                id="certificate-edit-finish-date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                {...register2("finish_date", {
                                    value: setValue('finish_date', certificateEdit.finish_date),
                                    required: t('This field is required'),
                                    pattern: {
                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />

                            <span style={{height: 20,color: 'red'}}>
                                {errors2?.finish_date && <span>{errors2?.finish_date?.message || "Error!"}</span>}
                            </span>

                            <Button
                                style={{marginLeft: 'auto', marginRight: 'auto',display: 'block', width: '9rem', textAlign: 'center',marginTop: '2rem'}}
                                variant="contained"
                                type="submit"
                            >
                                {t('Update')}
                            </Button>
                        </Box>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Certificates;
