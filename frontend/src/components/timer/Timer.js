import React, {useEffect, useState} from 'react';
import './Timer.css';
import {post} from "../../request";
import {isAuth} from "../../utils";
import {TIMER_TIME} from "../../config";
import {useTranslation} from "react-i18next";


export default function Timer(props) {

    const token = isAuth();
    const padTime = time => {
        return String(time).length === 1 ? `0${time}` : `${time}`;
    };

    const format = time => {
        const minutes = Math.floor(time / 60);
        const seconds = time % 60;
        return `${minutes}:${padTime(seconds)}`;
    };

    if (!localStorage.getItem('time')) {
        localStorage.setItem('time', TIMER_TIME);
    }

    const saveAnswer = () => {
        const response = post("/tests",
            {
                category_id: props.category_id,
                answer: props.value,
                test_id: props.question.test_id,
                level: props.level,
                question_id: props.question.question_id,
            },
            token);
        localStorage.removeItem("time");
        window.location.replace("/answers");
    }


    const [counter, setCounter] = useState(localStorage.getItem('time'));
    const {t} = useTranslation();


    useEffect(() => {
        let timer;
        let minus;
        if (counter > 0) {
            timer = setTimeout(() =>
                setCounter(c => c - 1), 1000);
            minus = setTimeout(() =>
                localStorage.setItem('time', (localStorage.getItem('time') - 1)), 1000);
        }

        if (counter === 0) {
            saveAnswer();
        }

        return () => {
            if (timer) {
                clearTimeout(timer);
            }
        };
    }, [counter]);

    return (
        <div id="countdown">
            <div id='tiles' className="color-full">{format(counter)}</div>
            <div className="countdown-label">{t("Time Remaining")}</div>
        </div>
    );
}


