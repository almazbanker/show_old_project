import React, {useEffect, useState} from 'react';
import './style.css';
import {isAuth} from "../../utils";
import {get, post, postFormData, put, putFormData} from "../../request";
import NavbarLinks from "../questionnaires/NavbarLinks";
import {useTranslation} from "react-i18next";
import Box from "@mui/material/Box";
import {TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useForm} from "react-hook-form";
import Alert from "../alert/Alert";

const License = () => {

    const token = isAuth();
    const [showAlert, setShowAlert] = useState(false);
    const [license, setLicense] = useState(null);
    const {t} = useTranslation();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        get('/licenses', token).then(payload => {
            if (payload.data){
                setLicense(payload.data);
            }
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const {
        register,
        formState: {
            errors,
            isValid
        },
        handleSubmit,
    } = useForm({
        mode: "onBlur"
    });

    const {
        register: register2,
        formState: {
            errors: errors2,
            isValid: isValid2
        },
        handleSubmit: handleSubmit2,
    } = useForm({
        mode: "onBlur"
    });

    const handleSubmitPUT = (data) => {
        if (data.picture === null) {
            const response = put("/licenses/" + license.id, data, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setIsLoading(false);
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                } else {
                    console.log(payload);
                }
            });


        } else {
            if (data.picture.length === 1){
                console.log(data)
                const formData = new FormData();
                formData.append("picture", data.picture[0])
                formData.append('start_date', data.start_date);
                formData.append('finish_date', data.finish_date);
                formData.append('description', data.description);
                formData.append('number', data.number);
                formData.append('_method', 'PUT');
                const response = putFormData("/licenses/" + license.id, formData, token);
                response.then(payload => {
                    if (Object.keys(payload).includes('data')) {
                        setIsLoading(false);
                        setShowAlert(true);
                        setTimeout(() => {setShowAlert(false)}, 2500);
                    } else {
                        console.log(payload);
                    }
                });
            }else {
                const response = put("/licenses/" + license.id, data, token);
                response.then(payload => {
                    if (Object.keys(payload).includes('data')) {
                        setIsLoading(false);
                        setShowAlert(true);
                        setTimeout(() => {setShowAlert(false)}, 2500);
                    } else {
                        console.log(payload);
                    }
                });
            }

        }
    };

    const handleSubmitCreate = (data) => {
        if (data.picture.length) {
            const formData = new FormData();
            formData.append("picture", data.picture[0])
            formData.append('start_date', data.start_date);
            formData.append('finish_date', data.finish_date);
            formData.append('description', data.description);
            formData.append('number', data.number);
            const res = postFormData("/licenses", formData, token)
            res.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setIsLoading(false);
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                } else {
                    console.log(payload);
                }
            });

        } else {
            delete data.picture;
            const response = post("/licenses", data, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setIsLoading(false);
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                } else {
                    console.log(payload);
                }
            });
        }
    };

    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
      <NavbarLinks/>
        <div className="container">
            {license === null  ?
                <Box
                    component="form"
                    sx={{
                        '& .MuiTextField-root': { m: 1, width: '100%' },
                    }}
                    style={{maxWidth: "600px", margin: "30px auto 50px"}}
                    autoComplete="off"
                    onSubmit={handleSubmit2(handleSubmitCreate)}
                >
                    <h3  style={{margin: "60px auto 20px"}} className="headerUni">{t('Create')} {t('Licenses')}</h3>
                    <TextField
                        label={t("Start date")}
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        {...register2("start_date", {
                            required: t('This field is required'),
                            pattern: {
                                value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors2?.start_date && <p>{errors2?.start_date?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        label={t("Finish date")}
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        {...register2("finish_date", {
                            required: t('This field is required'),
                            pattern: {
                                value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors2?.finish_date && <p>{errors2?.finish_date?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        label={t("Content")}
                        multiline
                        fullWidth
                        focused
                        rows={4}
                        {...register2("description", {
                            required: t('This field is required'),
                            minLength: {
                                value: 5,
                                message: t("This field must be at least 5 characters"),
                            },
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors2?.description && <p>{errors2?.description?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        fullWidth
                        label={t("Number")}
                        type="number"
                        focused
                        {...register2("number", {
                            required: t('This field is required'),
                            minLength: {
                                value: 5,
                                message: t("This field must be at least 5 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors2?.number && <p>{errors2?.number?.message || "Error!"}</p>}
                    </div>
                    <input
                        type="file"
                        {...register2("picture", {
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <Button
                        style={{marginLeft: 'auto', marginRight: 'auto',display: 'block', width: '9rem', textAlign: 'center',marginTop: '2rem'}}
                        variant="contained"
                        disabled={!isValid2}
                        type="submit"
                    >
                        {t('Submit')}
                    </Button>
                </Box>
                :
                <Box
                    component="form"
                    sx={{
                        '& .MuiTextField-root': { m: 1, width: '100%' },
                    }}
                    style={{maxWidth: "600px", margin: "30px auto 50px"}}
                    autoComplete="off"
                    onSubmit={handleSubmit(handleSubmitPUT)}
                >
                    <h3  style={{margin: "60px auto 20px"}} className="headerUni">{t('Edit')} {t('Licenses')}</h3>
                    <TextField
                        label={t("Start date")}
                        type="date"
                        focused
                        InputLabelProps={{
                            shrink: true,
                        }}
                        {...register("start_date", {
                            value: license.start_date,
                            required: t('This field is required'),
                            pattern: {
                                value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.start_date && <p>{errors?.start_date?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        label={t("Finish date")}
                        type="date"
                        focused
                        InputLabelProps={{
                            shrink: true,
                        }}
                        {...register("finish_date", {
                            value: license.finish_date,
                            required: t('This field is required'),
                            pattern: {
                                value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.finish_date && <p>{errors?.finish_date?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        label={t("Content")}
                        multiline
                        fullWidth
                        rows={4}
                        focused
                        {...register("description", {
                            value: license.description,
                            required: t('This field is required'),
                            minLength: {
                                value: 5,
                                message: t("This field must be at least 5 characters"),
                            },
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.description && <p>{errors?.description?.message || "Error!"}</p>}
                    </div>
                    <TextField
                        fullWidth
                        label={t("Number")}
                        type="number"
                        focused
                        {...register("number", {
                            value: license.number,
                            required: t('This field is required'),
                            minLength: {
                                value: 5,
                                message: t("This field must be at least 5 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.number && <p>{errors?.number?.message || "Error!"}</p>}
                    </div>

                    {license.picture &&
                        <img
                            src={license.picture}
                            alt="" className="news-card__image"
                        />
                    }
                    <input type="file"
                           {...register('picture', {
                               value: license.picture
                           })}
                    />
                    <Button
                        style={{marginTop: '2rem'}}
                        variant="contained"
                        fullWidth
                        disabled={!isValid}
                        type="submit"
                    >
                        {t('Update')}
                    </Button>
                </Box>
            }
        </div>
        </>
    );
};

export default License;
