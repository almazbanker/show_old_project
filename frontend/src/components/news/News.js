import {Link} from "react-router-dom";
import React, {useState, useEffect} from 'react';
import {get} from '../../request';
import {isAuth} from "../../utils";
import "../categories/Categories.css";
import {CircularProgress} from "@mui/material";
import "../common/Null.css";


const News = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [news, setNews] = useState(null);
    const token = isAuth();
    const [error, setError] = useState(null);

    useEffect(() => {
        const response = get("/news/", token);
        response.then(data => {
            if (data.data) {
                setNews(data.data);
            }else {
                setError('ПОКА ПУСТО');
            }
        })
        setIsLoading(false);
    }, [isLoading === true]);


    return (
        <>
            {isLoading && <CircularProgress />}
            {error &&
            <h3 className="null">{error}</h3>
            }
            {news &&
            <div className="content-wrapper">
                {news.map( neww =>
                    <div key={neww.id} className="news-card">
                        <Link to={`/news/${neww.id}`} className="news-card__card-link"> </Link>
                        <img
                            src={neww.picture}
                            alt="" className="news-card__image" />
                        <div className="news-card__text-wrapper">
                            <h2 className="news-card__title"> {neww.title}</h2>
                            <div className="news-card__details-wrapper">
                                <p className="news-card__excerpt" dangerouslySetInnerHTML={{__html: neww.description}}></p>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            }
        </>
    );
};

export default News;
