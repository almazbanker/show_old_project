import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {isAuth} from "../../utils";
import {get} from "../../request";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import "./News.scss";

const New = () => {
    const {id} = useParams();
    const [isLoading, setIsLoading] = useState(false);
    const [news, setNews] = useState(null);
    const token = isAuth();
    const [error, setError] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        const response = get("/news/" + id, token);
        response.then(data => {
            if (data.data) {
                setNews(data.data);
            }else {
                setError('ПОКА ПУСТО');
            }
        })
        setIsLoading(false);
    }, [])
    return (
        <>
            {isLoading &&
            <Box sx={{width: '100%'}}>
                <LinearProgress/>
            </Box>
            }
            {error &&
            <h1>{error}</h1>
            }
            {news &&
            <div className="news-main container">
                <h1 className="h22 text-color">{news.title}</h1>
               <p className=" news-text news-card__excerpt_show">{news.description}</p>
            </div>
            }
        </>
    );
};

export default New;
