import React, {useEffect, useState} from 'react';
import './style.css';
import {get, post, remove} from "../../request";
import {isAuth} from "../../utils";
import NavbarLinks from "../questionnaires/NavbarLinks";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";

const AuthCategories = () => {

    const [isLoading, setIsLoading] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [showAlertDelete, setShowAlertDelete] = useState(false);
    const token = isAuth();
    const [categories, setCategories] = useState([]);
    const [formCategories, setFormCategories] = useState([]);
    const [authCategories, setAuthCategories] = useState([]);
    const { t } = useTranslation();

    useEffect(() => {
        get('/categories', token).then(payload => {
            setCategories(payload.data);
        });
        get('/auth/categories', token).then(payload => {
            setAuthCategories(payload.data);
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const onInput = (e) => {
        const index = formCategories.indexOf(e.target.value);
        if (index !== -1) {
            formCategories.splice(index, 1);
        } else {
            setFormCategories([...formCategories, e.target.value]);
        }
    }

    const submit = (e) => {
        e.preventDefault();
        post('/categories', formCategories, token).then(() => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
        });
        setIsLoading(false);
    }

    const deleteQC = (e, id) => {
        e.preventDefault();
        remove('/categories/' + id, token).then(() => {
            setShowAlertDelete(true);
            setTimeout(() => {setShowAlertDelete(false)}, 2500);
        });
        setIsLoading(false);
    }

    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <Alert show={showAlertDelete} content={t('Successfully deleted!!')} bgColor={'rgba(72,157,0,0.5)'}/>
            <NavbarLinks />
            <div className="auth_categories">
            <div className="container">
                <ul className="ks-tags">
                    {categories.map(category =>
                        <li key={'category_' + category.id}>
                            <input value={category.id} type="checkbox" id={'category' + category.id} name={category.id} onChange={onInput}/>
                            <label htmlFor={'category' + category.id}>{category.name}</label>
                        </li>
                    )}
                </ul>
                <a href="#" onClick={submit} className="submit_qc">{t('Save')}</a>

                <h2 className="h23">{t('Categories')}</h2>
                <ul className="ks-tags">
                    {authCategories.map(category =>
                        <div id={'category_card_' + category.id} key={"card_category_" + category.id} className="auth_category">
                            <div className="qbody"><p>{category.name}</p></div>
                            <div><a className="qdelete" href="#" onClick={(e) => {deleteQC(e, category.id)}}>{t('Delete')}</a></div>
                        </div>
                    )}
                </ul>
            </div>
        </div>
        </>
    );
};

export default AuthCategories;
