import React, {useEffect, useState} from 'react';
import {get, postFormData, post, putFormData, put} from "../../request";
import {isAuth} from "../../utils";
import NavbarLinks from "./NavbarLinks";
import {useForm} from "react-hook-form";
import {TextField} from "@mui/material";
import Select from '@mui/material/Select';
import MenuItem from "@mui/material/MenuItem";
import LoadingButton from "@mui/lab/LoadingButton";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";


const Questionnaire = () => {

    const token = isAuth();
    const [showAlert, setShowAlert] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [questionnaire, setQuestionnaire] = useState({id: '', born_date: '', gender: '', country_id: ''});
    const [countries, setCountries] = useState([]);
    const [file, handleFile] = useState(null);
    const {t} = useTranslation();

    useEffect(() => {
        get('/questionnaires', token).then(payload => {
            if (payload.data) {
                setQuestionnaire(payload.data);
            }
        });
        get('/countries', token).then(payload => {
            setCountries(payload.data);
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const handleForm = (event) => {
        setQuestionnaire({...questionnaire, [event.target.name]: event.target.value});
    }

    const handleAddFile = (event) => {
        console.log(event.target.files[0])
        handleFile(event.target.files[0]);

    }

    const [errorsValid, setErrorsValid] = useState(null);

    const handleSubmitPUT = (event) => {
        event.preventDefault();
        if (file) {
            const formData = new FormData();
            formData.append('picture', file, file.name);
            formData.append('born_date', questionnaire.born_date);
            formData.append('country_id', questionnaire.country_id);
            formData.append('gender', questionnaire.gender);
            formData.append('_method', 'PUT');
            const response = putFormData("/questionnaires/" + questionnaire.id, formData, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                    setIsLoading(false);
                    setErrorsValid(null);

                } else {
                    console.log(payload);
                    if (payload.errors){
                        setErrorsValid(payload.errors);
                    }else {
                        setErrorsValid(payload);
                    }
                }
            });

        } else {
            const response = put("/questionnaires/" + questionnaire.id, questionnaire, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                    setIsLoading(false);
                    setErrorsValid(null);

                } else {
                    console.log(payload);
                    if (payload.errors){
                        setErrorsValid(payload.errors);
                    }else {
                        setErrorsValid(payload);
                    }
                }
            });
        }
    };

    const handleSubmitForm = (event) => {
        setIsLoading(true);
        event.preventDefault();
        if (file) {
            const formData = new FormData();
            formData.append('picture', file, file.name);
            formData.append('born_date', questionnaire.born_date);
            formData.append('country_id', questionnaire.country_id);
            formData.append('gender', questionnaire.gender);
            const response = postFormData("/questionnaires", formData, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setIsLoading(false);
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                    setIsLoading(false);
                    setErrorsValid(null);

                } else {
                    console.log(payload);
                    if (payload.errors){
                        setErrorsValid(payload.errors);
                    }else {
                        setErrorsValid(payload);
                    }
                }
            });

        } else {
            const response = post("/questionnaires", questionnaire, token);
            response.then(payload => {
                if (Object.keys(payload).includes('data')) {
                    setIsLoading(false);
                    setShowAlert(true);
                    setTimeout(() => {setShowAlert(false)}, 2500);
                    setIsLoading(false);
                    setErrorsValid(null);
                } else {
                    console.log(payload);
                    if (payload.errors){
                        setErrorsValid(payload.errors);
                    }else {
                        setErrorsValid(payload);
                    }
                }
            });
        }
    };



    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <NavbarLinks/>
            <div className="questionnaire">
                {questionnaire.id === '' ?

                    <form style={{maxWidth: "600px", margin: "30px auto 50px"}}>
                        <h2 className="h2">{t('Create')} {t('Questionnaire')}</h2>
                        <label htmlFor="born_date">{t('Born')} {t('Date')}</label>
                        <input onChange={handleForm} type="date" id="born_date" name="born_date"/>
                        { errorsValid ?
                            <>
                                {errorsValid.born_date ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.born_date.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <label htmlFor="gender">Gender</label>
                        <select onChange={handleForm} name="gender" id="gender">
                            <option value="female">{t('Female')}</option>
                            <option value="male">{t('Male')}</option>
                        </select>
                        { errorsValid ?
                            <>
                                {errorsValid.gender ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.gender.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <label htmlFor="country_id">{t('Country')}</label>
                        <select onChange={handleForm} name="country_id" id="country_id">
                            {countries.map(country =>
                                <option key={country.id} value={country.id}>{country.name}</option>
                            )}
                        </select>
                        { errorsValid ?
                            <>
                                {errorsValid.country_id ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.country_id.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <input type="file" name="picture" onChange={handleAddFile}/>
                        { errorsValid ?
                            <>
                                {errorsValid.picture ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.picture.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <button onClick={handleSubmitForm} className="ques_btn">{t('Submit')}</button>
                    </form>
                    :
                    <form style={{maxWidth: "600px", margin: "30px auto 50px"}}>
                        <h2 className="h2">{t('Edit')} {t('Questionnaire')}</h2>
                        <TextField fullWidth label={t('Born date')} onChange={handleForm}
                                   value={questionnaire.born_date} type="date" id="born_date"
                                   name="born_date"/>
                        { errorsValid ?
                            <>
                                {errorsValid.born_date ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.born_date.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <label htmlFor="gender">{t('Gender')}</label>
                        <Select value={questionnaire.gender}
                                fullWidth label="Gender" name="gender" id="gender" onChange={handleForm}>
                            <MenuItem selected={'female' === questionnaire.gender}
                                      value="female">{t('Female')}</MenuItem>
                            <MenuItem selected={'male' === questionnaire.gender} value="male">{t('Male')}</MenuItem>
                        </Select>
                        { errorsValid ?
                            <>
                                {errorsValid.gender ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.gender.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <label htmlFor="country_id">{t('Country')}</label>
                        <Select label="Gender" fullWidth value={questionnaire.country_id} onChange={handleForm}
                                name="country_id">
                            {countries.map(country =>
                                <MenuItem key={country.id}
                                          value={country.id}>{country.name}</MenuItem>
                            )}
                        </Select>
                        { errorsValid ?
                            <>
                                {errorsValid.country_id ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.country_id.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        {questionnaire.picture_url &&
                            <img
                                src={questionnaire.picture_url}
                                alt="" className="news-card__image"
                            />
                        }
                        <input type="file" name="picture" onChange={handleAddFile}/>
                        { errorsValid ?
                            <>
                                {errorsValid.picture ?
                                    <div style={{height: 20, color: 'red'}}>
                                        {errorsValid.picture.map(item => {
                                            return (
                                                <>
                                                    <p>
                                                        {item}
                                                    </p>
                                                </>
                                            );
                                        })}
                                    </div>
                                    : null}
                            </>
                            : null
                        }
                        <LoadingButton
                            sx={{marginTop: 5}}
                            type="submit"
                            fullWidth
                            variant="contained"
                            onClick={handleSubmitPUT}
                        >
                            {t('Edit')}
                        </LoadingButton>
                    </form>
                }
            </div>
        </>
    );
};

export default Questionnaire;
