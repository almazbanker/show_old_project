import React from 'react';
import {Link} from "react-router-dom";
import "./button-link.css";
import {useTranslation} from "react-i18next";

const NavbarLinks = () => {
    const { t } = useTranslation();

    return (
        <div className="links">
            <Link key={'button_link1'} className="button-link" to="/questionnaires">
                {t('Questionnaires')}
            </Link>
            <Link key={'button_link2'} className="button-link" to="/user/categories">
                 {t('Categories')}
            </Link>
            <Link key={'button_link3'} className="button-link" to="/works">
                 {t('Works')}
            </Link>
            <Link key={'button_link4'} className="button-link" to="/certificates">
                 {t('Certificates')}
            </Link>
            <Link key={'button_link5'} className="button-link" to="/universities">
                 {t('Universities')}
            </Link>
            <Link key={'button_link6'} className="button-link" to="/licenses">
                 {t('Licenses')}
            </Link>
        </div>
    );
}

export default NavbarLinks;
