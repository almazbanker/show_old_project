import React from "react";
import Container from "@mui/material/Container";
import logo from './logo.png'
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import './Footer.scss'
import {Link} from "react-router-dom";

export default function Footer() {
    return (
        <div className="footer">
            <Container>
                <Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}}>
                    <Grid item xs={2} sm={3} md={4}>

                        <img src={logo} alt="Logo"/>

                        <p className="footer_text">200, A-block, Green road, USA <br/>
                            +10 367 267 2678 <br />
                            <a className="domain" href="#">lawyer@contact.com</a>
                        </p>

                        <div className="social-icon--list">
                            <Button
                                url="https://www.facebook.com/"
                                className="social-icon"
                                size="small"><FacebookIcon/></Button>
                            <Button
                                url="https://twitter.com/?lang=ru"
                                className="social-icon"
                                size="small"><TwitterIcon/></Button>
                            <Button
                                url="https://www.instagram.com/"
                                className="social-icon"
                                size="small"><InstagramIcon/></Button>
                        </div>
                    </Grid>
                    <Grid sx={{marginX: 15}} item xs={2} sm={2} md={2}>
                        <h3 className="footer_title">
                            Links
                        </h3>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/users">Users</Link></li>
                            <li><Link to="/users/international/">International</Link></li>
                            <li><Link to="/categories">Categories</Link></li>
                            <li><Link to="/news">News</Link></li>
                        </ul>
                    </Grid>
                    <Grid item xs={2} sm={2} md={2}>
                        <h3 className="footer_title">
                            Useful Links
                        </h3>
                        <ul>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#"> Contact</a></li>
                        </ul>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}
