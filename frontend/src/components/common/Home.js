import React, {useEffect, useState} from 'react';
import "./Home.css";
import "../suggestions/Suggestions";
import Partners from "../partners/Partners";
import BasicModal from "../modal/BasicModal";
import {useTranslation} from "react-i18next";
import {isAuth} from "../../utils";
import {get} from "../../request";
import {CircularProgress} from "@mui/material";
import {Link} from "react-router-dom";
import TopUsers from "../users/Top-3-users";
import "../common/Null.css";

const Home = () => {
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState(true);
    const [categories, setCategories] = useState(null);
    const token = isAuth();
    const [error, setError] = useState(null);

    useEffect(() => {
        const response = get("/categories/", token);
        response.then(data => {
            if (data.data) {
                setCategories(data.data);
            }else {
                setError('ПОКА ПУСТО');
            }
        })
        setIsLoading(false);
    }, [isLoading === true]);

    return (
        <>
            <div className="home">
                <div className="home__content">
                    <div className="container">
                        <div className="home__intro">
                            <h3 className="home__title">{t('High Quality Law Advice And Support')}</h3>
                            <BasicModal />
                        </div>
                    </div>
                </div>
                <div className="home__container">
                    <h1>{t('categories')}</h1>
                    {isLoading && <CircularProgress />}
                    {error &&
                    <h3 className="null">{error}</h3>
                    }
                    {categories &&
                        <div className="content-wrapper">
                            {categories.map( category =>
                                <div key={category.id} className="news-card">
                                    <Link to={`/category/${category.id}`} className="news-card__card-link"> </Link>
                                    <img
                                        src={category.picture}
                                        alt="" className="news-card__image" />
                                    <div className="news-card__text-wrapper">
                                        <h2 className="news-card__title"> {category.name}</h2>
                                        <div className="news-card__details-wrapper">
                                            <p className="news-card__excerpt">{category.description}</p>
                                            <Link to={`/category/${category.id}`} className="news-card__read-more">{t('Read more')}</Link>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    }
                    <h1>{t('Partners')}</h1>
                    <Partners/>
                    <h1 className="topUsers">{t('Top users')}</h1>
                    <TopUsers/>
                </div>
            </div>

        </>
    );
};

export default Home;
