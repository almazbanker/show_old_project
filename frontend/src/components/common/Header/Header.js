import React from 'react';
import './style.css';
import {Link} from "react-router-dom";
import Button from "@mui/material/Button";


const MediatorNavbar = () => {

    return (
        <>
            <div className="container">
                <Button variant="contained" color="error" href="#text-buttons">Link</Button>
            </div>
            <nav className="header-nav">

                <ul className="header-nav__list">
                    <li className="header-nav__item" key={1}><Link className="header-nav__link"
                                                           to="/suggestions">Suggestions</Link></li>
                    <li className="header-nav__item" key={2}><Link className="header-nav__link"
                                                           to="/categories">Categories</Link></li>
                    <li className="header-nav__item" key={3}><Link className="header-nav__link"
                                                           to="/partners">Partners</Link></li>
                    <li className="header-nav__item" key={4}><Link className="header-nav__link"
                                                           to="/users">Users</Link></li>
                    <li className="header-nav__item" key={5}><Link className="header-nav__link"
                                                           to="/navbar">To Mediator</Link></li>
                </ul>
            </nav>

        </>
    );
}

export default MediatorNavbar;
