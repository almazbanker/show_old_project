import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import {ThemeProvider, createTheme} from '@mui/material/styles';
import {Link} from "react-router-dom";
import {isAuth, isName} from "../../utils";
import { useTranslation } from "react-i18next";
import "../common/Header/style.css";
import i18next from "i18next";
import {FormControl, InputLabel, Select} from "@mui/material";

const languages = [
    {
        code: 'en',
        name: 'EN',
        country_code: 'gb',
    },
    {
        code: 'ru',
        name: 'RU',
        country_code: 'ru',
    }
];

const ResponsiveAppBar = ({routes}) => {
    const { t } = useTranslation();
    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleLogout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('admin');
        localStorage.removeItem('moderator');
        localStorage.removeItem('id');
        localStorage.removeItem('name');
        localStorage.removeItem('approved');
        handleCloseUserMenu();
        window.location.href = "/";

    }

    const navLinks = () => {

        let buttons;
        buttons = [];
        routes.map((route, key) => {
            if (route.includeNav) {
                if (route.auth) {
                    if (isAuth()) {
                        buttons.push(
                            <MenuItem key={key + 'btn_menu'} onClick={handleCloseNavMenu}>
                                <Typography textAlign="center">
                                    <button className="btn__menu">
                                        <Link className="link__menu" to={route.path}>{t(route.name)}</Link>
                                    </button>
                                </Typography>
                            </MenuItem>
                        )
                    }
                } else {
                    buttons.push(
                        <MenuItem key={key + 'btn_menu'} onClick={handleCloseNavMenu}>
                            <Typography textAlign="center">
                                <button className="btn__menu">
                                    <Link className="link__menu" to={route.path}>{t(route.name)}</Link>
                                </button>
                            </Typography>
                        </MenuItem>
                    )
                }
            }
        })
        return buttons;
    }

    const darkTheme = createTheme({
        palette: {
            mode: 'dark',
            primary: {
                main: '#1976d2',
            },
        },
    });

    const navButtons = () => {
        let buttons;
        buttons = [];
        routes.map((route, key) => {
            if (route.includeNav) {
                if (route.auth) {
                    if (isAuth()) {
                        buttons.push(
                            <li key={key + 'btn_nav'} className="header-nav__item"><Link className="header-nav__link" to={route.path}>{t(route.name)}</Link></li>
                        )
                    }
                } else {
                    buttons.push(
                        <li  key={key + 'btn_nav'} className="header-nav__item"><Link className="header-nav__link" to={route.path}>{t(route.name)}</Link></li>
                    )
                }
            }
        })
        return buttons;
    };




    const authButtons = () => {
        const token = isAuth();
        const username = isName();
        if (token) {
            return <Box sx={{flexGrow: 0}}>
                <Tooltip title="Open settings">
                    <IconButton onClick={handleOpenUserMenu} sx={{p: 0}}>
                        <h6>{username}</h6>
                    </IconButton>
                </Tooltip>
                <Menu
                    sx={{mt: '45px'}}
                    id="menu-appbar"
                    anchorEl={anchorElUser}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={Boolean(anchorElUser)}
                    onClose={handleCloseUserMenu}
                >

                    <Container>
                        <MenuItem onClick={handleLogout}>
                            <Typography textAlign="center">{t('Logout')}</Typography>
                        </MenuItem>
                    </Container>
                </Menu>
            </Box>
        } else {
            return <Box sx={{flexGrow: 0}}>
                <button className="btn__auth">
                    <Link className="link__auth" to="/auth/login">{t('Login')}</Link>
                </button>
                <button className="btn__auth">
                    <Link className="link__auth" to="/auth/register">{t('Register')}</Link>
                </button>
            </Box>;
        }
    }


    const [lang, setLang] = React.useState('');

    const handleChange = (event) => {
        setLang(event.target.value);
        localStorage.setItem('localization', event.target.value);
    };

    return (
        <ThemeProvider theme={darkTheme}>
            <AppBar position="static">
                <Container maxWidth="xl">
                    <Toolbar disableGutters sx={{padding: 2}} >
                        <Box sx={{flexGrow: 1, display: {xs: 'flex', md: 'none'}}}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="inherit"
                            >
                                <MenuIcon/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: {xs: 'block', md: 'none'},
                                }}
                            >
                                {navLinks()}
                            </Menu>
                        </Box>
                        <Box sx={{flexGrow: 1, display: {xs: 'none', md: 'flex'}}}>
                            {navButtons()}
                        </Box>
                        {authButtons()}
                        <FormControl sx={{ m: 1, minWidth: 80 }}>
                            <InputLabel id="demo-simple-select-autowidth-label">{t('Lang')}</InputLabel>
                            <Select
                                labelId="demo-simple-select-autowidth-label"
                                id="demo-simple-select-autowidth"
                                value={localStorage.getItem('localization') ? localStorage.getItem('localization') : 'en'}
                                onChange={handleChange}
                                autoWidth
                                label="Language"
                            >
                                {languages.map(language =>
                                    <MenuItem key={language}  onClick={() => i18next.changeLanguage(language.code)} value={language.code}>{language.name}</MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Toolbar>
                </Container>
            </AppBar>
        </ThemeProvider>
    );
};
export default ResponsiveAppBar;
