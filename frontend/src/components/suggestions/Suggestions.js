import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import {get, post, remove} from '../../request';
import {isAuth} from "../../utils";
import {CircularProgress} from "@mui/material";
import './Suggestions.css';
import Button from "@mui/material/Button";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";

const Suggestions = () => {
    const [showAlert, setShowAlert] = useState(false);
    const [showAlertDelete, setShowAlertDelete] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [suggestions, setSuggestions] = useState(null);
    const token = isAuth();
    const {t} = useTranslation();
    const [votes, setVotes] = useState([]);
    useEffect(() => {
        setIsLoading(true);
        const response = get(`/suggestions`, token);
        const votes = get(`/user/votes`, token);
        response.then(data => {
            setSuggestions(data.data);
            setIsLoading(false);
        });
        votes.then(data => {
            setVotes(data.votes);
            setIsLoading(false);
        });
    }, []);
    const agree = (id) => {
        const response = post("/suggestions", {agree: true, suggestion_id: id}, token);
        response.then((data) => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
            setVotes([...votes, data.id]);
        })
    }
    const disagree = (id) => {
        const response = post("/suggestions", {agree: false, suggestion_id: id}, token);
        response.then((data) => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
            setVotes([...votes, data.id]);
        })
    }
    const handleDeleteVote = (id) => {
        const response = remove(`/suggestions/${id}`, token);
        response.then(() => {
            setShowAlertDelete(true);
            setTimeout(() => {setShowAlertDelete(false)}, 2500);
            const newVotes = votes.filter(vote => vote !== id);
            setVotes(newVotes);
        })
    }
    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <Alert show={showAlertDelete} content={t('Successfully deleted!!')} bgColor={'rgba(72,157,0,0.5)'}/>
            {isLoading &&
                <Box sx={{width: '100%'}}
                >
                    <CircularProgress/>
                </Box>
            }
            {suggestions &&
                <div className="container">
                    <div className="wrapper">
                        {suggestions.map(suggestion =>
                            <div className="card" key={suggestion.id}>
                                <div className="card__body">
                                    <h4 className="card__title">Expiration
                                        date: </h4>
                                    <h4 className="card__title">{suggestion.start_date} - {suggestion.finish_date}</h4>
                                    <p className="card__description">{suggestion.suggestion}</p>
                                    <br/>
                                    <div>
                                        {votes.includes(suggestion.id) ?
                                            <Button onClick={() => handleDeleteVote(suggestion.id)}>
                                                {t('Change my mind')}
                                            </Button>
                                            :
                                            <div>
                                                <Button loading={isLoading}
                                                        onClick={() => agree(suggestion.id)}
                                                        variant="outlined" color="success"
                                                        className="card__btn"
                                                        name="answer"
                                                        type="submit"
                                                        value="1">
                                                    {t("For")}
                                                </Button>
                                                <Button loading={isLoading}
                                                        onClick={() => disagree(suggestion.id)}
                                                        variant="outlined" color="error"
                                                        name="answer"
                                                        type="submit"
                                                        className="card__btn"
                                                        value="0">
                                                    {t("Against")}
                                                </Button>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            }
        </>

    );
};
export default Suggestions;
