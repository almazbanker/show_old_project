import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import {useState, useEffect} from "react";
import {get, post, put, remove} from '../../request';
import {isAuth} from "../../utils";
import LoadingButton from "@mui/lab/LoadingButton";
import {styled} from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, {tableCellClasses} from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from "@mui/material/Button";
import "../universities/university.css";
import NavbarLinks from "../questionnaires/NavbarLinks";
import {useForm} from "react-hook-form";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";

const Work = () => {

    const {t} = useTranslation();

    const StyledTableCell = styled(TableCell)(({theme}) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({theme}) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    const [showAlert, setShowAlert] = useState(false);
    const [showAlertDelete, setShowAlertDelete] = useState(false);
    const token = isAuth();
    const [isLoading, setIsLoading] = useState(false);
    const [works, setWorks] = useState([]);
    const [countWorks, setCountWorks] = useState(0);


    useEffect(() => {
        const response = get("/works", token);
        response.then(data => {
            setWorks(data.data);
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const [workEdit, setWorkEdit] = useState({id: '', name: '', description: '', start_date: '', finish_date: ''});
    const [active, setActive] = useState(false);


    const workPut = (data) => {
        put("/works/" + workEdit.id, data, token).then(() => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
            setIsLoading(false);
        });
        modalClose();
    }

    const modalClose = () => {
        setWorkEdit({id: '', name: '', description: '', start_date: '', finish_date: ''});
        setActive(!active);
    }

    const modalEdit = (work) => {
        setWorkEdit(work);
        setActive(!active);
    }


    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onBlur"
    });

    const {
        register: register2,
        formState: {
            errors: errors2,
        },
        setValue: setValue,
        handleSubmit: handleSubmit2,
    } = useForm({
        mode: "onBlur",
    });

    const [errorsValid, setErrorsValid] = useState(null);


    const handleDeleteWork = (workForDelete) => {
        const response = remove(`/works/${workForDelete.id}`, token);
        response.then(() => {
            const newWorks = works.filter(work => workForDelete.id !== work.id);
            setWorks(newWorks);
            setCountWorks(countWorks - 1);
            setShowAlertDelete(true);
            setTimeout(() => {setShowAlertDelete(false)}, 2500);
        })
    }

    const onSubmit = (data) => {
        post("/works", data, isAuth()).then(payload => {

            if (Object.keys(payload).includes('data')) {
                setWorks([
                    ...works,
                    payload.data
                ]);
                setShowAlert(true);
                setTimeout(() => {setShowAlert(false)}, 2500);
                setIsLoading(false);
            }else {
                if (payload.errors){
                    setErrorsValid(payload.errors);
                }else {
                    setErrorsValid(payload);
                }
            }


        })
        reset();
    }

    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <Alert show={showAlertDelete} content={t('Successfully deleted!!')} bgColor={'rgba(72,157,0,0.5)'}/>
            <NavbarLinks/>
            <div className="container">
                <Box
                    component="form"
                    sx={{
                        '& .MuiTextField-root': {m: 1, width: '100%'},
                    }}
                    style={{maxWidth: "600px", margin: "30px auto 50px"}}
                    autoComplete="off"
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <h3 style={{margin: "60px auto 20px"}} className="headerUni">{t('Create')}</h3>
                    <TextField
                        label={t("Start date")}
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        {...register("start_date", {
                            required: t('This field is required'),
                            pattern: {
                                value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.start_date && <p>{errors?.start_date?.message || "Error!"}</p>}
                    </div>
                    { errorsValid ?
                        <>
                            {errorsValid.start_date ?
                                <div style={{height: 20, color: 'red'}}>
                                    {errorsValid.start_date.map(item => {
                                        return (
                                            <>
                                                <p>
                                                    {item}
                                                </p>
                                            </>
                                        );
                                    })}
                                </div>
                                : null}
                        </>
                        : null
                    }

                    <TextField
                        label={t("Finish date")}
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        {...register("finish_date", {
                            required: t('This field is required'),
                            pattern: {
                                value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.finish_date && <p>{errors?.finish_date?.message || "Error!"}</p>}
                    </div>
                    { errorsValid ?
                        <>
                            {errorsValid.finish_date ?
                                <div style={{height: 20, color: 'red'}}>
                                    {errorsValid.finish_date.map(item => {
                                        return (
                                            <>
                                                <p>
                                                    {item}
                                                </p>
                                            </>
                                        );
                                    })}
                                </div>
                                : null}
                        </>
                        : null
                    }
                    <TextField
                        label={t('Title')}
                        fullWidth
                        {...register("name", {
                            required: t('This field is required'),
                            minLength: {
                                value: 3,
                                message: t("This field must be at least 3 characters"),
                            },
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.name && <p>{errors?.name?.message || "Error!"}</p>}
                    </div>
                    { errorsValid ?
                        <>
                            {errorsValid.name ?
                                <div style={{height: 20, color: 'red'}}>
                                    {errorsValid.name.map(item => {
                                        return (
                                            <>
                                                <p>
                                                    {item}
                                                </p>
                                            </>
                                        );
                                    })}
                                </div>
                                : null}
                        </>
                        : null
                    }

                    <TextField
                        label={t('Content')}
                        multiline
                        fullWidth
                        rows={4}
                        {...register("description", {
                            required: t('This field is required'),
                            minLength: {
                                value: 3,
                                message: t("This field must be at least 3 characters"),
                            },
                            maxLength: {
                                value: 255,
                                message: t("This field must not be greater than 255 characters"),
                            },
                            pattern: {
                                value: /([^(<)(>)])$/,
                                message: t("This field does not match the format")
                            }
                        })}
                    />
                    <div style={{height: 20, color: 'red'}}>
                        {errors?.description && <p>{errors?.description?.message || "Error!"}</p>}
                    </div>

                    { errorsValid ?
                        <>
                            {errorsValid.description ?
                                <div style={{height: 20, color: 'red'}}>
                                    {errorsValid.description.map(item => {
                                        return (
                                            <>
                                                <p>
                                                    {item}
                                                </p>
                                            </>
                                        );
                                    })}
                                </div>
                                : null}
                        </>
                        : null
                    }

                    <LoadingButton
                        style={{marginLeft: 7}}
                        type="submit"
                        fullWidth
                        variant="contained"
                    >
                        {t('Create')}
                    </LoadingButton>
                </Box>
                <TableContainer component={Paper}>
                    <Table sx={{minWidth: 700}} aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>{t('Name')} </StyledTableCell>
                                <StyledTableCell>{t('Start date')}&nbsp;</StyledTableCell>
                                <StyledTableCell>{t('Finish date')}&nbsp;</StyledTableCell>
                                <StyledTableCell sx={{textAlign: 'right'}}>{t('Actions')}</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {works.map((work) => (
                                <StyledTableRow key={work.name}>
                                    <StyledTableCell sx={{width: '25%'}}>
                                        {work.name}
                                    </StyledTableCell>
                                    <StyledTableCell>{work.start_date}</StyledTableCell>
                                    <StyledTableCell>{work.finish_date}</StyledTableCell>
                                    <StyledTableCell sx={{textAlign: 'right'}}>
                                        <Button variant="outlined" sx={{marginRight: 1}}
                                                onClick={() => modalEdit(work)}>{t('Edit')}</Button>
                                        <Button variant="outlined" color="error"
                                                onClick={() => handleDeleteWork(work)}>{t('Delete')}</Button>
                                    </StyledTableCell>
                                </StyledTableRow>
                            ))}


                            <div className={active ? "university_modal active" : "university_modal notActive"}
                                 onClick={modalClose}>
                                <div className="university_modal_body" onClick={(e) => e.stopPropagation()}>
                                    <div>
                                        <h3 className="headerUni2">{t('Edit Work')}</h3>
                                        <Box
                                            component="form"
                                            sx={{
                                                '& .MuiTextField-root': {mt: 2, width: '100%'},
                                            }}
                                            style={{maxWidth: "600px", margin: "30px auto 50px"}}
                                            autoComplete="off"
                                            onSubmit={handleSubmit2(workPut)}
                                        >
                                            <TextField
                                                label={t("Title")}
                                                name="name"
                                                fullWidth
                                                id="work-edit-name"
                                                focused
                                                {...register2("name", {
                                                    value:  setValue('name', workEdit.name),
                                                    required: t('This field is required'),
                                                    minLength: {
                                                        value: 3,
                                                        message: t("This field must be at least 3 characters"),
                                                    },
                                                    maxLength: {
                                                        value: 255,
                                                        message: t("This field must not be greater than 255 characters"),
                                                    },
                                                    pattern: {
                                                        value: /([^(<)(>)])$/,
                                                        message: t("This field does not match the format")
                                                    }
                                                })}

                                            />
                                            <span style={{height: 20, color: 'red'}}>
                                                {errors2?.name && <span>{errors2?.name?.message || "Error!"}</span>}
                                            </span>

                                            <TextField
                                                label={t("Content")}
                                                multiline
                                                id="work-edit-description"
                                                fullWidth
                                                focused
                                                rows={4}
                                                {...register2("description", {
                                                    value:  setValue('description', workEdit.description),
                                                    required: t('This field is required'),
                                                    minLength: {
                                                        value: 3,
                                                        message: t("This field must be at least 3 characters"),
                                                    },
                                                    maxLength: {
                                                        value: 255,
                                                        message: t("This field must not be greater than 255 characters"),
                                                    },
                                                    pattern: {
                                                        value: /([^(<)(>)])$/,
                                                        message: t("This field does not match the format")
                                                    }
                                                })}
                                            />
                                            <span style={{height: 20, color: 'red'}}>
                                                {errors2?.description &&
                                                    <span>{errors2?.description?.message || "Error!"}</span>}
                                            </span>
                                            <TextField
                                                label={t("Start date")}
                                                type="date"
                                                focused
                                                id="work-edit-start-date"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                {...register2("start_date", {
                                                    value: setValue('start_date', workEdit.start_date),
                                                    required: t('This field is required'),
                                                    pattern: {
                                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                                        message: t("This field does not match the format")
                                                    }
                                                })}
                                            />
                                            <span style={{height: 20,color: 'red'}}>
                                                {errors2?.start_date && <span>{errors2?.start_date?.message || "Error!"}</span>}
                                            </span>
                                            <TextField
                                                label={t("Finish date")}
                                                type="date"
                                                id="work-edit-finish-date"
                                                focused
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                {...register2("finish_date", {
                                                    value: setValue('finish_date', workEdit.finish_date),
                                                    required: t('This field is required'),
                                                    pattern: {
                                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                                        message: t("This field does not match the format")
                                                    }
                                                })}
                                            />
                                            <span style={{height: 20,color: 'red'}}>
                                                {errors2?.finish_date && <span>{errors2?.finish_date?.message || "Error!"}</span>}
                                            </span>
                                            <Button
                                                style={{marginLeft: 'auto', marginRight: 'auto',display: 'block', width: '9rem', textAlign: 'center',marginTop: '2rem'}}
                                                variant="contained"
                                                type="submit"
                                            >
                                                {t('Update')}
                                            </Button>
                                        </Box>
                                    </div>
                                </div>
                            </div>
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
            </>

    )
}

export default Work;
