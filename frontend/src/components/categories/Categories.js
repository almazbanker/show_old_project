import {Link} from "react-router-dom";
import './Categories.css';
import React, {useState, useEffect} from 'react';
import {get} from '../../request';
import {isAuth} from "../../utils";
import {CircularProgress} from "@mui/material";
import CategoriesHeader from "./CategoryHeader";
import {useTranslation} from "react-i18next";
import "../common/Null.css";



const Categories = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [categories, setCategories] = useState(null);
    const { t } = useTranslation();
    const token = isAuth();
    const [error, setError] = useState(null);

    useEffect(() => {
        const response = get("/categories/", token);
        response.then(data => {
            if (data.data) {
                setCategories(data.data);
            }else {
                setError('ПОКА ПУСТО');
            }
        })
        setIsLoading(false);
    }, [isLoading === true]);


    return (
        <>
            <CategoriesHeader />
            {isLoading && <CircularProgress />}
            {error &&
            <h3 className="null">{error}</h3>
            }
            {categories &&
            <div className="content-wrapper">
                {categories.map( category =>
                    <div key={category.id} className="news-card">
                        <Link to={`/category/${category.id}`} className="news-card__card-link"> </Link>
                        <img
                            src={category.picture}
                            alt="" className="news-card__image" />
                        <div className="news-card__text-wrapper">
                            <h2 className="news-card__title"> {category.name}</h2>
                            <div className="news-card__details-wrapper">
                                <p className="news-card__excerpt" dangerouslySetInnerHTML={{__html: category.description}}></p>
                                <Link to={`/category/${category.id}`} className="news-card__read-more">{t('Read more')}</Link>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            }
        </>
    );
};

export default Categories;
