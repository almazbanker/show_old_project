import './Categories.css';
import React from 'react';
import {useTranslation} from "react-i18next";
import BasicModal from "../modal/BasicModal";


const CategoriesHeader = () => {
    const {t} = useTranslation();

    return (
        <div className="categories__content">
            <div className="container">
                <div className="home__intro">
                    <h3 className="home__title">{t('High Quality Law Advice And Support')}</h3>
                    <BasicModal />
                </div>
            </div>
        </div>
    );
};

export default CategoriesHeader;
