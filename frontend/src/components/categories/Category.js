import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {isAuth} from "../../utils";
import {get, post} from "../../request";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import Grid from "@mui/material/Grid";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import {useTranslation} from "react-i18next";
import Types from "../types/types";
import FavoriteIcon from "@mui/icons-material/Favorite";
import Alert from "../alert/Alert";

const Category = () => {
    const [showAlert, setShowAlert] = useState(false);
    const {id} = useParams();
    const [isLoading, setIsLoading] = useState(false);
    const [category, setCategory] = useState(null);
    const {t} = useTranslation();
    const token = isAuth();
    const [error, setError] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        const response = get("/categories/" + id, token);
        response.then(data => {
            if (data.data) {
                setCategory(data.data);
            } else {
                setError('ПОКА ПУСТО');
            }
        })
        setIsLoading(false);
    }, []);

    const like = (e, user) => {
        const data = {"user_id": user.id};
        e.preventDefault();
        post('/likes', data, token).then(payload => {
            if (payload.message === 'Unauthenticated.') {
                setShowAlert(true);
                setTimeout(() => {setShowAlert(false)}, 2500);
            } else {
                setIsLoading(false);
            }
        });
    }

    return (
        <>
            <Alert show={showAlert} content={t('Нou are not authorized!!')} bgColor={'rgba(246,8,0,0.5)'}/>
            {isLoading &&
                <Box sx={{width: '100%'}}>
                    <LinearProgress/>
                </Box>
            }
            {error &&
                <h1>{error}</h1>
            }
            {category &&
                <div>
                    <h2 className="h22 text-color">{t('Category')}: {category.name}</h2>
                    <div className="container">
                        <h2 className="news-card__title"> {category.name}</h2>
                        <p className="news-card__excerpt_show">{category.description}</p>
                    </div>
                    <h2 className="h22 text-color">{t('Subcategory')}</h2>
                    <div className="content-wrapper">
                        {category.types.map(type =>
                            <Types type={type}/>
                        )}
                    </div>


                    <h2 className="h22 text-color">{t('Mediators')}</h2>
                    <div className="content-wrapper">
                        <Grid sx={{mt: 3}} container spacing={{xs: 3, md: 4}} columns={{xs: 4, sm: 8, md: 12}}>
                            {category.users.map(user =>
                                <Grid item xs={2} sm={4} md={4} key={user.id}>
                                    <div className="our-team">
                                        <div className="picture">
                                            <img className="img-fluid" src="https://static.tildacdn.com/tild3931-6635-4265-b232-663830313530/12.jpg" alt="#"/>
                                        </div>
                                        <div className="team-content">
                                            <h3 className="name">{user.name}</h3>
                                            <h4 className="title">{user.email}</h4>
                                            <p>{t('Likes')}: {user.likes.length}</p>
                                        </div>
                                        <ul className="social">
                                            <li><a href="#" onClick={(e) => {
                                                like(e, user)
                                            }}><FavoriteIcon/></a></li>
                                            <li><a href="#" aria-hidden="true"><TwitterIcon/></a></li>
                                            <li><a href="#" aria-hidden="true"><InstagramIcon/></a></li>
                                            <li><a href="#" aria-hidden="true"><FacebookRoundedIcon/></a></li>
                                        </ul>
                                    </div>
                                </Grid>
                            )}
                        </Grid>
                    </div>
                </div>
            }
        </>
    );
};

export default Category;
