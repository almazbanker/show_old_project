import React from 'react';
import YouTube from 'react-youtube';

const Youtube = (props) => {
    const opts = {
        height: '480',
        width: '854',
        playerVars: {
            autoplay: 0,
        },
    };

    return (
        <YouTube videoId="BOv-xVPyVx0" opts={opts} />
    )
}

export default Youtube;
