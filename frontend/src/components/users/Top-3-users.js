import React, {useState, useEffect} from 'react';
import {get, post} from '../../request';
import {isAuth} from "../../utils";
import Grid from "@mui/material/Grid";
import './User.css';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import Link from "@mui/material/Link";
import {Link as RouterLink} from "react-router-dom";
import FavoriteIcon from "@mui/icons-material/Favorite";
import {useTranslation} from "react-i18next";
import "../common/Null.css";
import Alert from "../alert/Alert";

const TopUsers = () => {
    const [showAlert, setShowAlert] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [top_users, setTopUsers] = useState(null);
    const token = isAuth();
    const { t } = useTranslation();
    const [error, setError] = useState(null);

    useEffect(() => {

        const response = get("/top_users/", token);
        response.then(data => {
            if (data.data) {
                setTopUsers(data.data);
            }else {
                setError('ПОКА ПУСТО');
            }
            setIsLoading(true);
        })
    }, [isLoading === false]);

    const like = (e, user) => {
        const data = {user_id: user.id};
        e.preventDefault();
        post('/likes', data, token).then(payload => {
            if (payload.message === 'Unauthenticated.') {
                setShowAlert(true);
                setTimeout(() => {setShowAlert(false)}, 2500);
            } else {
                setIsLoading(false);
            }
        });
    }


    return (
        <>
            <Alert show={showAlert} content={t('Нou are not authorized!!')} bgColor={'rgba(246,8,0,0.5)'}/>
            {error &&
            <h3 className="null">{error}</h3>
            }
            {top_users &&
                <Grid sx={{mt: 5}} container spacing={{xs: 3, md: 4}} columns={{xs: 4, sm: 8, md: 12}}>
                    {top_users.map(user =>
                        <Grid item xs={2} sm={4} md={4} key={user.id}>
                            <div className="our-team">
                                <div className="picture">
                                    <img className="img-fluid"
                                         src="https://static.tildacdn.com/tild3931-6635-4265-b232-663830313530/12.jpg"
                                         alt="#"/>
                                </div>
                                <div className="team-content">
                                    <Link component={RouterLink} to={`/users/${user.id}`}>{user.name}</Link>
                                    <h4 className="title">{user.email}</h4>
                                    <p>{t('Likes')}: {user.likes.length}</p>
                                </div>
                                <br/>
                                <ul className="social">
                                    <li><a href="/" onClick={(e) => {
                                        like(e, user)
                                    }}><FavoriteIcon/></a></li>
                                    <li><a href={user.link.twitter} aria-hidden="true"><TwitterIcon/></a></li>
                                    <li><a href={user.link.instagram} aria-hidden="true"><InstagramIcon/></a></li>
                                    <li><a href={user.link.facebook} aria-hidden="true"><FacebookRoundedIcon/></a></li>
                                </ul>
                            </div>
                        </Grid>
                    )}
                </Grid>
            }
        </>
    );
};

export default TopUsers;
