import * as React from 'react';
import {createMuiTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Button from "@mui/material/Button";
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import {lightBlue, purple} from "@mui/material/colors";

export default function UserCard(props) {
    const instagram = createMuiTheme({
        palette: {
            primary: purple,
        },
    });

    const twitter = createMuiTheme({
        palette: {
            primary: lightBlue,
        },
    });

    return (
        <Card sx={{ display: 'flex' }}>
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <CardContent sx={{ flex: '1 0 auto' }}>
                    <Typography component="div" variant="h5">
                        {props.user.name}
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary" component="div">
                        {props.user.email}
                    </Typography>
                </CardContent>
                <Box sx={{ display: 'flex', alignItems: 'center', pl: 1, pb: 1 }}>
                    <Button  size="small"><FacebookRoundedIcon /></Button>
                    <Button theme={twitter} size="small"><TwitterIcon /></Button>
                    <Button theme={instagram} size="small"><InstagramIcon /></Button>
                </Box>
            </Box>
            <CardMedia
                component="img"
                sx={{ width: 150 }}
                image="https://static.tildacdn.com/tild3931-6635-4265-b232-663830313530/12.jpg"
                alt="Live from space album cover"
            />
        </Card>
    );
}
