import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import {get, post} from '../../request';
import {isAuth} from "../../utils";
import Grid from "@mui/material/Grid";
import {CircularProgress} from "@mui/material";
import './User.css';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import FavoriteIcon from '@mui/icons-material/Favorite';
import {Link as RouterLink} from "react-router-dom";
import Link from "@mui/material/Link";
import Pagination from "../Pagination/Pagination";
import {useTranslation} from "react-i18next";
import "../common/Null.css";
import BasicModal from "../modal/BasicModal";
import Alert from "../alert/Alert";


const Users = () => {
    const [showAlert, setShowAlert] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoading2, setIsLoading2] = useState(false);
    const [users, setUsers] = useState([]);
    const token = isAuth();
    const {t} = useTranslation();
    const [error, setError] = useState(null);

    const [currentPage, setCurrentPage] = useState(1);
    const [usersPerPage] = useState(3);

    const lastUserIndex = currentPage * usersPerPage;
    const firstUserIndex = lastUserIndex - usersPerPage;
    const currentUsers = users.slice(firstUserIndex, lastUserIndex);

    const paginate = pageNumber => setCurrentPage(pageNumber)


    useEffect(() => {
        setIsLoading(true);
        const response = get("/users/", token);
        response.then(data => {
            if (data.data) {
                setUsers(data.data);
            }else {
                setError('ПОКА ПУСТО');
            }
            setIsLoading(false);
        })
        setIsLoading2(true);
    }, [isLoading2 === false]);

    const like = (e, user) => {
        const data = {"user_id": user.id};
        e.preventDefault();
        post('/likes', data, token).then(payload => {
            if (payload.message === 'Unauthenticated.') {
                setShowAlert(true);
                setTimeout(() => {setShowAlert(false)}, 2500);
            } else {
                setIsLoading2(false);
            }
        });
    }

    return (
        <>
            <Alert show={showAlert} content={t('Нou are not authorized!!')} bgColor={'rgba(246,8,0,0.5)'}/>
            <div className="users__content">
                <div className="container">
                    <div className="home__intro">
                        <h3 className="home__title">{t('High Quality Law Advice And Support')}</h3>
                        <BasicModal />
                    </div>
                </div>
            </div>
            <div className="container users">
                {isLoading &&
                <Box sx={{width: '100%'}}>
                    <CircularProgress/>
                </Box>
                }
                {error &&
                <h3 className="null">{error}</h3>
                }
                {users &&
                <Grid container spacing={{xs: 4, md: 10}} columns={{xs: 4, sm: 8, md: 12}}>
                    {currentUsers.map(user =>
                        <Grid item xs={2} sm={4} md={4} key={user.id}>
                            <div className="our-team">
                                <div className="picture">
                                    <img className="img-fluid"
                                         src="https:///static.tildacdn.com/tild3931-6635-4265-b232-663830313530/12.jpg"
                                         alt="#"/>
                                </div>
                                <div className="team-content">
                                    <Link component={RouterLink} to={`/users/${user.id}`}>{user.name}</Link>
                                    <h4 className="title">{user.email}</h4>
                                    <p>{t('Likes')}: {user.likes.length}</p>
                                </div>
                                <br/>
                                <ul className="social">
                                    <li><a href="/" onClick={(e) => {
                                        like(e, user)
                                    }}><FavoriteIcon/></a></li>
                                    {user.link ?
                                        <>
                                    <li><a href={user.link.twitter} aria-hidden="true"><TwitterIcon/></a></li>
                                    <li><a href={user.link.instagram} aria-hidden="true"><InstagramIcon/></a></li>
                                    <li><a href={user.link.facebook} aria-hidden="true"><FacebookRoundedIcon/></a></li>
                                        </>

                                        : null
                                    }
                                </ul>
                            </div>
                        </Grid>
                    )}
                </Grid>
                }
                <Pagination
                    usersPerPage={usersPerPage}
                    totalUsers={users.length}
                    paginate={paginate}
                    currentPage={currentPage}
                />
            </div>
        </>
    );
};



export default Users;
