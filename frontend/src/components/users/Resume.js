import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {AuthId, isAdmin, isAuth, isModerator} from "../../utils";
import {get, post, remove} from "../../request";
import './Resume.css';
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import '../modal/BasicModal.scss';
import Container from "@mui/material/Container";
import {Divider, TextField} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import Subcomments from "../subcomments/Subcomments";
import {useTranslation} from "react-i18next";
import BasicModal from "../modal/BasicModal";
import {useForm} from "react-hook-form";
import LoadingButton from "@mui/lab/LoadingButton";

const Resume = () => {
    const {id} = useParams();
    const [isLoading, setIsLoading] = useState(false);
    const [resume, setResume] = useState(null);
    const [comments, setComments] = useState(null);
    const token = isAuth();
    const {t} = useTranslation();

    useEffect(() => {
        setIsLoading(true);
        const response = get("/users/" + id, token);
        response.then(data => {
            setResume(data.data);
            setComments(data.data.comments);
            setIsLoading(false);
        })
    }, [])

    const commentPost = (data) => {
        const payload = {
            ...data,
            'user_id': resume.id
        }
        const response = post("/comments", payload, token);
        response.then(payload => {
            setComments([...comments, payload.data]);
            reset();
        });
    }


    const commentDelete = (id) => {
        const response = remove(`/comments/${id}`, token);
        response.then(() => {
            const newComments = comments.filter(comment => id !== comment.id);
            setComments(newComments);
        });
    }

    const admin = isAdmin();
    const moderator = isModerator();
    const authId = AuthId();

    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onSubmit"
    });


    return (
        <>
            {isLoading &&
                <Box sx={{width: '100%'}}>
                    <LinearProgress/>
                </Box>
            }

            {resume &&
                <>
                    <Grid container>
                        <div className="resume_wrapper">
                            <div className="resume_left">
                                <div className="resume_image">
                                    <img src='https:///static.tildacdn.com/tild3931-6635-4265-b232-663830313530/12.jpg'
                                         alt="Resume_image"/>
                                </div>
                                <div className="resume_bottom">
                                    <div className="resume_item resume_namerole">
                                        <div className="name">{resume.name}</div>
                                        <div className="role">{t('Mediator')}</div>
                                    </div>
                                    <div className="resume_item resume_profile">
                                        <div className="container">
                                            <BasicModal />
                                        </div>
                                    </div>
                                    <div className="resume_item resume_contact">
                                        <div className="resume_title">{t('Contacts')}</div>
                                        <div className="resume_info">
                                            <div className="resume_subtitle">{t('Phone')}</div>
                                            <div className="resume_subinfo">{resume.number}</div>
                                        </div>
                                        <div className="resume_info">
                                            <div className="resume_subtitle">{t('Email')}</div>
                                            <div className="resume_subinfo">{resume.email}</div>
                                        </div>
                                    </div>
                                    <div className="resume_item resume_skills">
                                        <div className="resume_title">{t('Certificates')}</div>
                                        <div className="resume_info">
                                            <div className="resume_subinfo">{resume.questionnaire.born_date}</div>
                                        </div>
                                    </div>
                                    <div className="resume_item resmue_interests">
                                        <div className="resume_title">{t('Links')}</div>
                                        <div className="resume_info row">
                                            <a style={{color: '#F2C64D'}} href="#"><TwitterIcon/></a>
                                            <a style={{color: '#F2C64D', marginLeft: 20, marginRight: 20}}
                                               href="#"><InstagramIcon/></a>
                                            <a style={{color: '#F2C64D'}} href="#"><FacebookRoundedIcon/></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="resume_right">
                                <div className="resume_item resume_namerole">
                                    <div className="name">{resume.name}</div>
                                    <div className="role">{t('Mediator')}</div>
                                </div>
                                <div className="resume_item resume_education">
                                    <div className="resume_title">{t('Education')}</div>
                                    <div className="resume_info">
                                        {resume.universities.map(university =>
                                            <div key={university.id} className="resume_data">
                                                <div
                                                    className="year">{university.start_date} - {university.finish_date}</div>
                                                <div className="content">
                                                    <p>{university.name}</p>
                                                    <p>{university.description}</p>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className="resume_item resume_experience">
                                    <div className="resume_title">{t('Experience')}</div>
                                    <div className="resume_info">
                                        {resume.works.map(work =>
                                            <div key={work.id} className="resume_data">
                                                <div className="year">{work.start_date} - {work.finish_date}</div>
                                                <div className="content">
                                                    <p>{work.name}</p>
                                                    <p dangerouslySetInnerHTML={{__html: work.description}}/>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className="resume_item resume_experience">
                                    <div className="resume_title">{t('Certificates')}</div>
                                    <div className="resume_info">
                                        {resume.certificates.map(certificate =>
                                            <div key={certificate.id} className="resume_data">
                                                <div
                                                    className="year">{certificate.start_date} - {certificate.finish_date}</div>
                                                <div className="content">
                                                    <p>{certificate.name}</p>
                                                    <p>{certificate.description}</p>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Container>
                            <div>
                                {isAuth() ?
                                    <div>
                                        <h1>{t('Comment')}</h1>
                                        <Box
                                            component="form"
                                            fullWidth sx={{m: 2}}
                                            onSubmit={handleSubmit(commentPost)}
                                        >
                                            <TextField
                                                label="Add comment"
                                                variant="outlined"
                                                {...register('content', {
                                                    required: t('This field is required'),
                                                })}
                                            />
                                            <div style={{height: 20, color: 'red'}}>
                                                {errors?.content && <p>{errors?.content?.message || "Error!"}</p>}
                                            </div>
                                            <LoadingButton
                                                style={{marginLeft: 7}}
                                                type="submit"
                                                fullWidth
                                                variant="contained"
                                            >
                                                {t('Save')}
                                            </LoadingButton>
                                            <Divider/>
                                        </Box>
                                    </div>
                                    : null
                                }

                                {comments &&
                                    <>
                                        {comments.map(comment =>
                                            <div key={comment.content}>
                                                <Card sx={{minWidth: 275, marginBottom: 3}}>
                                                    <CardContent>
                                                        <Typography sx={{fontSize: 14}} color="text.secondary"
                                                                    gutterBottom>
                                                            {comment.user}
                                                        </Typography>
                                                        <Typography sx={{mb: 1.5}} color="text.secondary">
                                                            {comment.created_at}
                                                        </Typography>
                                                        <Typography variant="body2">
                                                            {comment.content}
                                                        </Typography>
                                                    </CardContent>
                                                    { admin === 'Admin' || moderator === 'Moderator' || authId === comment.author_id ?
                                                        <CardActions>
                                                            <Button variant="outlined" color="error"
                                                                    onClick={() => commentDelete(comment.id)}>
                                                                {t('Delete')}
                                                            </Button>
                                                        </CardActions>
                                                        : null
                                                    }
                                                </Card>
                                                <Subcomments comment={comment}/>
                                            </div>
                                        )}
                                    </>
                                }
                            </div>
                        </Container>
                    </Grid>
                </>
            }
        </>
    );
};

export default Resume;
