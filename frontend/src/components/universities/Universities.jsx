import React, {useEffect, useState} from 'react';
import {styled} from '@mui/material/styles';
import './university.css';
import {
    Table,
    TableBody,
    TableCell,
    tableCellClasses,
    TableContainer,
    TableHead,
    TableRow,
    TextField
} from "@mui/material";
import {get, post, put, remove} from "../../request";
import {isAuth} from "../../utils";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import NavbarLinks from "../questionnaires/NavbarLinks";
import {useForm} from "react-hook-form";
import LoadingButton from "@mui/lab/LoadingButton";
import {useTranslation} from "react-i18next";
import Alert from "../alert/Alert";


const Universities = () => {

    const StyledTableCell = styled(TableCell)(({theme}) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        }, [`&.${tableCellClasses.body}`]: {fontSize: 14,}
    }));
    const StyledTableRow = styled(TableRow)(({theme}) => ({
        '&:nth-of-type(odd)': {backgroundColor: theme.palette.action.hover,},
        '&:last-child td, &:last-child th': {border: 0,}
    }));
    const [showAlert, setShowAlert] = useState(false);
    const [showAlertDelete, setShowAlertDelete] = useState(false);
    const token = isAuth();
    const [isLoading, setIsLoading] = useState(false);
    const [universities, setUniversities] = useState([]);
    const [universityEdit, setUniversityEdit] = useState({
        id: '',
        name: '',
        faculty: '',
        description: '',
        start_date: '',
        finish_date: ''
    });
    const [countUniversities, setCountUniversities] = useState(0);
    const [active, setActive] = useState(false);
    const {t} = useTranslation();


    useEffect(() => {
        const response = get("/universities", token);
        response.then(data => {
            setUniversities(data.data);
        });
        setIsLoading(true);
    }, [isLoading === false]);

    const universityPut = (data) => {
        put("/universities/" + universityEdit.id, data, token).then(() => {
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
            setIsLoading(false);
        });
        modalClose();
    }

    const handleDelete = (universityDELETE) => {
        const response = remove(`/universities/${universityDELETE.id}`, token);
        response.then(() => {
            const newUniversities = universities.filter(university => universityDELETE.id !== university.id);
            setUniversities(newUniversities);
            setCountUniversities(countUniversities - 1);
            setShowAlertDelete(true);
            setTimeout(() => {setShowAlertDelete(false)}, 2500);
        });
    }

    const modalClose = () => {
        setUniversityEdit({id: '', name: '', faculty: '', description: '', start_date: '', finish_date: ''});
        setActive(!active);
    }

    const modalEdit = (university) => {
        setUniversityEdit(university);
        setActive(!active);
    }


    const {
        register,
        formState: {
            errors,
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: "onBlur"
    });


    const {
        register: register2,
        formState: {
            errors: errors2,
        },
        setValue: setValue,
        handleSubmit: handleSubmit2,
    } = useForm({
        mode: "onBlur",
    });

    const onSubmit = (data) => {
        const response = post("/universities", data, token);
        response.then(payload => {
            setUniversities([...universities, payload.data]);
            setShowAlert(true);
            setTimeout(() => {setShowAlert(false)}, 2500);
        });
        reset();
    }


    return (
        <>
            <Alert show={showAlert} content={t('Successfully saved!!')} bgColor={'rgba(82,171,2,0.5)'}/>
            <Alert show={showAlertDelete} content={t('Successfully deleted!!')} bgColor={'rgba(72,157,0,0.5)'}/>
            <NavbarLinks/>
            <div className="uni_bg">
                <div className="container">
                    <div>

                        <Box
                            component="form"
                            sx={{
                                '& .MuiTextField-root': {m: 1, width: '100%'},
                            }}
                            style={{maxWidth: "600px", margin: "30px auto 50px"}}
                            autoComplete="off"
                            onSubmit={handleSubmit(onSubmit)}>
                            <h3 style={{margin: "60px auto 20px"}} className="headerUni">{t('Create')}</h3>

                            <TextField
                                label={t('Start date')}
                                type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                {...register("start_date", {
                                    required: t('This field is required'),
                                    pattern: {
                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <div style={{height: 20, color: 'red'}}>
                                {errors?.start_date && <p>{errors?.start_date?.message || "Error!"}</p>}
                            </div>
                            <TextField
                                label={t('Finish date')}
                                type="date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                {...register("finish_date", {
                                    required: t('This field is required'),
                                    pattern: {
                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <div style={{height: 20, color: 'red'}}>
                                {errors?.finish_date && <p>{errors?.finish_date?.message || "Error!"}</p>}
                            </div>
                            <TextField
                                fullWidth
                                label={t("Title")}
                                {...register("name", {
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <div style={{height: 20, color: 'red'}}>
                                {errors?.name && <p>{errors?.name?.message || "Error!"}</p>}
                            </div>

                            <TextField
                                fullWidth label={t("Faculty")}
                                name="faculty"
                                {...register("faculty", {
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <div style={{height: 20, color: 'red'}}>
                                {errors?.faculty && <p>{errors?.faculty?.message || "Error!"}</p>}
                            </div>

                            <TextField
                                label={t("Content")}
                                multiline
                                fullWidth
                                rows={4}
                                {...register("description", {
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <div style={{height: 20, color: 'red'}}>
                                {errors?.description && <p>{errors?.description?.message || "Error!"}</p>}
                            </div>

                            <LoadingButton
                                style={{marginLeft: 7}}
                                type="submit"
                                fullWidth
                                variant="contained"
                            >
                                {t('Create')}
                            </LoadingButton>
                        </Box>
                    </div>
                    <TableContainer className="tableUniversity">
                        <Table sx={{minWidth: 700}} aria-label="customized table">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>{t('Name')}</StyledTableCell>
                                    <StyledTableCell>{t('Start date')}</StyledTableCell>
                                    <StyledTableCell>{t('Finish date')}</StyledTableCell>
                                    <StyledTableCell>{t('Faculty')}</StyledTableCell>
                                    <StyledTableCell sx={{textAlign: 'right'}}>{t('Actions')}</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {universities.map((university) => (
                                    <StyledTableRow key={university.id}>
                                        <StyledTableCell sx={{width: '25%'}}>
                                            {university.name}
                                        </StyledTableCell>
                                        <StyledTableCell>{university.start_date}</StyledTableCell>
                                        <StyledTableCell>{university.finish_date}</StyledTableCell>
                                        <StyledTableCell>{university.faculty}</StyledTableCell>
                                        <StyledTableCell sx={{textAlign: 'right'}}>
                                            <Button variant="outlined" sx={{marginRight: 1}}
                                                    onClick={() => modalEdit(university)}>{t('Edit')}</Button>
                                            <Button variant="outlined" color="error"
                                                    onClick={() => handleDelete(university)}>{t('Delete')}</Button>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
            <div className={active ? "university_modal active" : "university_modal notActive"} onClick={modalClose}>
                <div className="university_modal_body" onClick={(e) => e.stopPropagation()}>
                    <div>
                        <h3 className="headerUni2">{t('Edit')} {'University'}</h3>

                        <Box
                            component="form"
                            sx={{
                                '& .MuiTextField-root': {mt: 2, width: '100%'},
                            }}
                            style={{maxWidth: "600px", margin: "30px auto 50px"}}
                            autoComplete="off"
                            onSubmit={handleSubmit2(universityPut)}
                        >

                            <TextField
                                label={t('Title')}
                                name="name"
                                id="university-edit-name"
                                fullWidth
                                focused
                                {...register2("name", {
                                    value: setValue('name', universityEdit.name),
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}

                            />
                            <span style={{height: 20, color: 'red'}}>
                                                {errors2?.name && <span>{errors2?.name?.message || "Error!"}</span>}
                                            </span>

                            <TextField
                                label={t('Content')}
                                name="description"
                                multiline
                                id="university-edit-description"
                                focused
                                fullWidth
                                rows={4}
                                {...register2("description", {
                                    value: setValue('description', universityEdit.description),
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <span style={{height: 20, color: 'red'}}>
                                {errors2?.description &&
                                    <span>{errors2?.description?.message || "Error!"}</span>}
                            </span>


                            <TextField
                                label={t('Faculty')}
                                name="faculty"
                                id="university-edit-faculty"
                                fullWidth
                                focused
                                {...register2("faculty", {
                                    value: setValue('faculty', universityEdit.faculty),
                                    required: t('This field is required'),
                                    minLength: {
                                        value: 3,
                                        message: t("This field must be at least 3 characters"),
                                    },
                                    maxLength: {
                                        value: 255,
                                        message: t("This field must not be greater than 255 characters"),
                                    },
                                    pattern: {
                                        value: /([^(<)(>)])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />
                            <span style={{height: 20, color: 'red'}}>
                                {errors2?.faculty && <span>{errors2?.faculty?.message || "Error!"}</span>}
                            </span>

                            <TextField
                                label={t("Start date")}
                                type="date"
                                focused
                                id="university-edit-start-date"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                {...register2("start_date", {
                                    value: setValue('start_date', universityEdit.start_date),
                                    required: t('This field is required'),
                                    pattern: {
                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />

                            <span style={{height: 20,color: 'red'}}>
                                {errors2?.start_date && <span>{errors2?.start_date?.message || "Error!"}</span>}
                            </span>

                            <TextField
                                label={t("Finish date")}
                                id="university-edit-finish-date"
                                type="date"
                                focused
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                {...register2("finish_date", {
                                    value: setValue('finish_date', universityEdit.finish_date),
                                    required: t('This field is required'),
                                    pattern: {
                                        value: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
                                        message: t("This field does not match the format")
                                    }
                                })}
                            />

                            <span style={{height: 20,color: 'red'}}>
                                {errors2?.finish_date && <span>{errors2?.finish_date?.message || "Error!"}</span>}
                            </span>
                            <Button
                                style={{marginLeft: 'auto', marginRight: 'auto',display: 'block', width: '9rem', textAlign: 'center',marginTop: '2rem'}}
                                variant="contained"
                                type="submit"
                            >
                                {t('Update')}
                            </Button>

                        </Box>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Universities;
