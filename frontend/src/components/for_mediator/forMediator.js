import React from "react";
import {Link, Link as RouterLink} from "react-router-dom";
import './forMediator.css';
import Button from "@mui/material/Button";
import {useTranslation} from "react-i18next";
import {AuthId} from "../../utils";
import BasicModal from "../modal/BasicModal";

const Navbar = () => {
    const {t} = useTranslation();

    return (
        <>
            <div className="mediators__content">
                <div className="container">
                    <div className="home__intro">
                        <h3 className="home__title">{t('High Quality Law Advice And Support')}</h3>
                        <BasicModal/>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="wrapper">
                    <Link to="/trainings/">
                    <div className="card">
                        <img src="https://www.edgepointlearning.com/assets/images/generated/blog/2017-04-27-top-10-types-of-employee-training-1170-36e7538ee.jpg" className="card__img"  alt="#"/>
                        <div className="card__body">
                            <Button component={RouterLink} to="/trainings/" className="card__btn">{t('Trainings')}</Button>
                        </div>
                    </div>
                    </Link>
                    <Link to="/suggestions/">
                        <div className="card">
                            <img src="https://st.depositphotos.com/1010613/5034/i/600/depositphotos_50345779-stock-photo-businessman-placing-suggestion-slip-into.jpg" className="card__img"  alt="#"/>
                            <div className="card__body">
                                <Button component={RouterLink} to="/suggestions/" className="card__btn">{t('Suggestions')}</Button>
                            </div>
                        </div>
                    </Link>
                    <Link to="/answers">
                        <div className="card">
                            <img src="https://investors.st.com/sites/st-micro/files/styles/desktop/public/st-micro/quarterly-results/2.1.1.jpg?itok=0IgdXqBW" className="card__img"  alt="#"/>
                            <div className="card__body">
                                <Button component={RouterLink} to="/answers" className="card__btn">{t('My answers')}</Button>
                            </div>
                        </div>
                    </Link>

                    <Link to="/books/">
                        <div className="card">
                            <img src="https://www.bangor.ac.uk/sites/default/files/styles/16x9_1920w/public/2020-07/cym%20shutterstock_794015686.jpg?h=dd1e4d4b&itok=YVSQ0gZx" className="card__img"  alt="#" />
                            <div className="card__body">
                                <Button component={RouterLink} to="/books/" className="card__btn">{t('Books')}</Button>
                            </div>
                        </div>
                    </Link>

                    <Link to="/meetings/">
                        <div className="card">
                            <img src="https://learnenglish.britishcouncil.org/sites/podcasts/files/RS6243_175211709-hig.jpg" className="card__img" alt="#" />
                            <div className="card__body">
                                <Button component={RouterLink} to="/meetings/" className="card__btn">{t('Meetings')}</Button>
                            </div>
                        </div>
                    </Link>

                    <Link to={"/users/" + AuthId()}>
                        <div className="card">
                            <img src="https://buffer.com/cdn-cgi/image/w=1000,fit=contain,q=90,f=auto/library/content/images/size/w1200/library/wp-content/uploads/2015/03/adjust-tie.jpeg" className="card__img"  alt="#" />
                            <div className="card__body">
                                <Button component={RouterLink} to={"/users/" + AuthId()} className="card__btn">{t('My profile')}</Button>
                            </div>
                        </div>
                    </Link>

                </div>
            </div>
        </>
    );
}

export default Navbar;
