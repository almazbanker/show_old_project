import React, {useEffect, useState} from "react";
import {isAuth} from "../../utils";
import {get} from "../../request";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import '../modal/BasicModal.scss';
import Container from "@mui/material/Container";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";
import {styled} from "@mui/material/styles";
import TableCell, {tableCellClasses} from "@mui/material/TableCell";

const Answers = () => {
    const token = isAuth();
    const [isLoading, setIsLoading] = useState(false);
    const [tests, setTests] = useState(null);


    useEffect(() => {
        setIsLoading(true);
        const response = get("/answers", token);
        response.then(data => {
            setTests(data.data);
            setIsLoading(false);
        });
    }, [])

    const StyledTableCell = styled(TableCell)(({theme}) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({theme}) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    return (
        <>
            {isLoading &&
                <Box sx={{width: '100%'}}>
                    <LinearProgress/>
                </Box>
            }

            <Container sx={{marginTop: 5, marginBottom: 30}}>
                {tests &&
                    <div>
                        {tests.map(test =>
                            <div key={test.id}>
                                <Card sx={{minWidth: 275, marginBottom: 3}}>
                                    <CardContent>
                                        <h3>Grade: {test.grade} %</h3>
                                        <br/>
                                        <Typography sx={{mb: 1.5}} color="text.secondary">
                                            Start time: {test.start_time}
                                        </Typography>
                                        <Typography sx={{mb: 1.5}} color="text.secondary">
                                            Finish time: {test.finish_time}
                                        </Typography>
                                        <Typography variant="body2">
                                            Level: {test.level}
                                        </Typography>
                                    </CardContent>
                                </Card>
                                <TableContainer  sx={{marginBottom: 3}} component={Paper}>
                                    <Table aria-label="customized table">
                                        <TableHead>
                                            <TableRow>
                                                <StyledTableCell>Question </StyledTableCell>
                                                <StyledTableCell>Correct Answer</StyledTableCell>
                                                <StyledTableCell>Your Answer</StyledTableCell>
                                                <StyledTableCell>Correct</StyledTableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {test.answers.map((answer) => (
                                                <StyledTableRow key={answer.id}>
                                                    <StyledTableCell>
                                                        {answer.question}
                                                    </StyledTableCell>
                                                    <StyledTableCell> {answer.correct_answer}</StyledTableCell>
                                                    <StyledTableCell>{answer.answer}</StyledTableCell>
                                                    <StyledTableCell>
                                                      {answer.correct ? 'Yes' : 'No'}
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        )}
                    </div>
                }
            </Container>

        </>
    );
};

export default Answers;
