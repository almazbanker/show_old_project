import React, {Component} from 'react';
import {API_BASE_URL} from "../../config";
import {Message} from "@mui/icons-material";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import "./Book.css";


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));


export default class Books extends Component {
    constructor(props) {
        super(props);
        this.state = {
            books: null,
            isLoading: false
        };
    }
    async getBooks() {
        if (!this.state.books) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/books', {
                    headers: {
                        Accept: 'application/json'
                    }
                });
                const booksList = await response.json();
                this.setState({books: booksList.data, isLoading: false})
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }


    componentDidMount() {
        this.getBooks().then()
    }
    render() {
        return (

            <div className="main-book">
                 <h1 className="book">Helpful information</h1>
                {this.state.isLoading && <Message info header="Loading Books..." />}
                {this.state.books &&

                <div className="content-wrapper">
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell>TITLE</StyledTableCell>
                                        <StyledTableCell>AUTHOR</StyledTableCell>
                                        <StyledTableCell>LINK</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.books.map( book =>
                                        <StyledTableRow key={book.name}>
                                            <StyledTableCell component="th" scope="row">
                                                {book.name}
                                            </StyledTableCell>
                                            <StyledTableCell>{book.author}</StyledTableCell>
                                            <StyledTableCell><a href={book.link}>{book.link}</a></StyledTableCell>
                                        </StyledTableRow>
                                    )}
                                </TableBody>
                            </Table>
                        </TableContainer>
                </div>
                }
            </div>
        )
    }
}
