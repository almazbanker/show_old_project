import React, {Suspense} from 'react';
import Navbar from "./components/common/Navbar";
import {Route, Routes} from "react-router-dom";
import routes from "./routes";
import Home from "./components/common/Home";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Footer from "./components/common/Footer/Footer";
import {useTranslation} from "react-i18next";
function App() {
    const { t } = useTranslation();
    const getRoutes = (routes) => {
        return routes.map((prop, key) => {
            return (
                <Route
                    path={prop.path}
                    key={key}
                    element={prop.element}
                />
            );
        });
    };

    return (
        <div>
            <Suspense fallback={<div>{t('Loading')}...</div>}>
                <Navbar routes={routes}/>

                <Routes>
                    {getRoutes(routes)}
                    <Route path="*" element={<Home/>}/>
                    <Route path="/auth/login" element={<Login/>}/>
                    <Route path="/auth/register" element={<Register/>}/>
                </Routes>
                <Footer/>
            </Suspense>
        </div>
    );
}

export default App;
