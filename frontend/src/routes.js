import Home from "./components/common/Home";
import Users from "./components/users/Users";
import Categories from "./components/categories/Categories";
import Suggestions from "./components/suggestions/Suggestions";
import Books from "./components/books/Books";
import Category from "./components/categories/Category";
import TopUsers from "./components/users/Top-3-users";
import Work from "./components/work/Work";
import Certificates from "./components/certificates/Certificates";
import Resume from "./components/users/Resume";
import Navbar from './components/for_mediator/forMediator'
import Meeting from "./components/meetings/Meeting";
import Trainings from "./components/trainings/Trainings";
import Training from "./components/trainings/Training";
import License from "./components/licenses/License";
import News from "./components/news/News";
import Questionnaire from "./components/questionnaires/Questionnaire";
import Universities from "./components/universities/Universities";
import AuthCategories from "./components/auth_categories/AuthCategories";
import TestStart from "./components/tests/Main";
import Test from "./components/tests/Test";
import Answers from "./components/answers/Answers";
import New from "./components/news/New";
import Type from "./components/types/type";
import International from "./components/users/International";
import MainTest from "./components/tests/MainTest";
import {isAdmin, isApproved, isAuth, isModerator} from "./utils";



const routes = [
    {
        path: '/',
        name: 'Home',
        element: <Home/>,
        auth: false,
        includeNav: true,
    },

    {
        path: '/users',
        name: 'Users',
        element: <Users/>,
        auth: false,
        includeNav: true,
    },

    {
        path: '/users/international/',
        name: 'International',
        element: <International/>,
        auth: false,
        includeNav: true,
    },

    {
        path: '/top_users',
        name: 'Top Users',
        element: <TopUsers/>,
        auth: false,
        includeNav: false,
    },
    {
        path: '/categories',
        name: 'Categories',
        element: <Categories/>,
        auth: false,
        includeNav: true,
    },
    {
        path: '/category/:id',
        name: 'Category',
        element: <Category/>,
        auth: false,
        props: true,
        includeNav: false,
    },

    {
        path: '/types/:id',
        name: 'Type',
        element: <Type/>,
        auth: false,
        props: true,
        includeNav: false,
    },

    {
        path: '/news',
        name: 'News',
        element: <News/>,
        auth: false,
        includeNav: true,
    },

    {
        path: '/news/:id',
        name: 'News',
        element: <New/>,
        auth: false,
        props: true,
        includeNav: false,
    },
    {
        path: '/users/:id',
        name: 'Resume',
        element: <Resume/>,
        auth: false,
        props: true,
        includeNav: false,
    },
];

if (isApproved() === 'true' || isAdmin() === 'Admin' || isModerator() === 'Moderator'){
    routes.push(
        {
            path: '/mediators',
            name: 'For Mediators',
            element: <Navbar/>,
            auth: true,
            includeNav: true,
        },
        {
            path: '/suggestions',
            name: 'Suggestion',
            element: <Suggestions/>,
            auth: true,
            includeNav: false,
        },
        {
            path: '/books',
            name: 'Books',
            element: <Books/>,
            auth: true,
            includeNav: false,
        },
        {
            path: '/meetings',
            name: 'Meetings',
            element: <Meeting/>,
            auth: true,
            includeNav: false,
        },
    )
}

if (isAuth() || isAdmin() === 'Admin' || isModerator() === 'Moderator'){
    routes.push(
        {
            path: '/certificates',
            name: 'Certificates',
            element: <Certificates/>,
            auth: true,
            includeNav: false,
        },

        {
            path: '/works',
            name: 'Work',
            element: <Work/>,
            auth: true,
            includeNav: false,
        },
        {
            path: '/navbar/',
            name: 'Navbar',
            element: <Navbar/>,
            auth: true,
            props: true,
            includeNav: false,
        },
        {
            path: '/questionnaires/',
            name: 'Questionnaires',
            element: <Questionnaire/>,
            auth: true,
            props: true,
            includeNav: true,
        },
        {
            path: '/universities/',
            name: 'Universities',
            element: <Universities/>,
            auth: true,
            props: true,
            includeNav: false,
        },

        {
            path: '/licenses/',
            name: 'Licenses',
            element: <License/>,
            auth: true,
            props: true,
            includeNav: false,
        },
        {
            path: '/training/:id',
            name: 'Training',
            element: <Training />,
            auth: true,
            includeNav: false,
        },
        {
            path: '/trainings/',
            name: 'Trainings',
            element: <Trainings/>,
            auth: true,
            includeNav: false,
        },
        {
            path: '/user/categories',
            name: 'UserCategory',
            element: <AuthCategories />,
            auth: true,
            includeNav: false,
        },
        {
            path: '/test/category/:id',
            name: 'TestCategory',
            element: <TestStart />,
            auth: true,
            includeNav: false,
        },
        {
            path: '/test/start/:id',
            name: 'TestStart',
            element: <Test />,
            auth: true,
            includeNav: false,
        },
        {
            path: '/mainTest/start/:id',
            name: 'TestStart',
            element: <MainTest />,
            auth: true,
            includeNav: false,
        },
        {
            path: '/answers',
            name: 'Answers',
            element: <Answers />,
            auth: true,
            includeNav: false,
        },
    )
}

export default routes;
