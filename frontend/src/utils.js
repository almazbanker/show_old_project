export function isAuth() {
    return localStorage.getItem('token') || false;
}

export function isName(){
    return localStorage.getItem('name') || false;
}

export function AuthId(){
    return localStorage.getItem('id') || false;
}

export function isAdmin(){
    return localStorage.getItem('admin') || false;
}

export function isModerator(){
    return localStorage.getItem('moderator') || false;
}

export function isApproved(){
    return localStorage.getItem('approved') || false;
}

export function isRecaptcha(){
    return '6LcrookeAAAAAFSjDRtVgO_h99Zcq8R2cBapEneh';
}
