$(document).ready(function() {

    $(document).on('click', '.save-subcomment', function (e)   {
        e.preventDefault();

        const element = e.currentTarget;
        const data = $(element).parent().serialize();
        const commentId = $(element).data('comment-id');
        console.log(commentId);
        $.ajax({
            method: 'POST',
            url: `/subcomments/`,
            data: data
        }).done(response => {
            $(`#subcomment-form-${commentId}`).trigger('reset');
            $(`#subcomment-${commentId}`).prepend(response.subcomment);

        }).fail(response => {
            console.log('no');
        });
    });

    $(document).on('click', '.delete-subcomment', function(event) {
        event.preventDefault();
        deleteComment(event);
    });

    const deleteComment = event => {
        const element = event.currentTarget;
        const subcommentId = $(element).data('subcomment-id');
        const token = $("meta[name=csrf-token]").attr('content');
        $.ajax({
            method: 'DELETE',
            url: `/subcomments/${subcommentId}`,
            data: {_token: token}
        }).done(successResponse => {
            $(`#collapse-${subcommentId}-card`).remove();
        }).fail(errorResponse => {
            console.log('no');
        })
    };

});
