$(() => {
    $(document).on('click', '#create-work-btn', function (e) {
        e.preventDefault();
        createWork(e);
    })

    const createWork = e => {
        const element = e.currentTarget;
        const data = $(element).parent().serialize();
        console.log(data)

        $.ajax({
            method: 'POST',
            url: `/works/`,
            data: data
        }).done(successResponse => {
            $('#works').prepend(successResponse.work)
                $('#create-work-form').trigger('reset');
        }).fail(e => {
            console.log('no')
        });
    };


    $(document).on('click', '.delete-work-btn', function (e) {
        e.preventDefault();
        deleteWork(e);
    })

    const deleteWork = e => {
        const element = e.currentTarget;
        const workId = $(element).data('work-id');
        console.log(workId)

        $.ajax({
            method: 'DELETE',
            url: `/works/${workId}`,
            data: {"_token": $('meta[name="csrf-token"]').attr('content')},
        }).done(successResponse => {
            $(`#work-${workId}`).remove();
        }).fail(e => {
            console.log('no')
        });
    };

    $(document).on('click', '.edit-work-btn', function (e) {
        e.preventDefault();
        editWork(e);

    })

    const editWork = e => {
        const element = e.currentTarget;
        const workId = $(element).data('edit-id');
        const name = $(element).parent().parent().parent().find('.name').text();
        const description = $(element).parent().parent().parent().find('.description').text();
        const start_date = $(element).parent().parent().parent().find('.start_date').text();
        const finish_date = $(element).parent().parent().parent().find('.finis_date').text();
        const modal_name = $('#modal_name').val(name);
        const modal_description = $('#modal_description').val(description);
        const modal_start_date = $('#modal_start_date').val(start_date);
        const modal_finish_date = $('#modal_finish_date').val(finish_date);
        const hidden = $('#work_hidden_id').val(workId);
        console.log(finish_date)
    }

    $(document).on('click', '.save', function (e) {
        const element = e.currentTarget;
        const workId = $('#work_hidden_id').val();
        const data = $(element).parent().serialize();

        $.ajax({
            method: 'PUT',
            url: `/works/${workId}`,
            data: data
        }).done(successResponse => {
            $(`#work-${workId}`).replaceWith(successResponse.work);
        })
    })
});
