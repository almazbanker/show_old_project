$(() => {
    $(document).on('click', '.save-question-btn', function (e) {
        e.preventDefault();
        addAnswer(e);
    })

    const addAnswer = e => {
        const element = e.currentTarget;
        const questionId = $(element).data('question-id');
        const data = $(element).parent().serialize();

        $.ajax({
            method: 'POST',
            url: `/tests/`,
            data: data
        }).done(successResponse => {
            $(`#question-${questionId}`).remove();
            $('#questions-test').append(successResponse.question);
        }).fail(e => {
            console.log('no')
        });
    };

});
