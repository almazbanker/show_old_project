$(() => {
    $(document).on('click', '#create-university-btn', function (e) {
        e.preventDefault();
        createUniversity(e);
    })

    const createUniversity = e => {
        const element = e.currentTarget;
        const data = $(element).parent().serialize();
        $.ajax({
            method: 'POST',
            url: `/universities/`,
            data: data
        }).done(successResponse => {
            $('#universities').prepend(successResponse.university)
            $('#create-university-form').trigger('reset');
        }).fail(e => {
            console.log('no')
        });
    };



    $(document).on('click', '.delete-university-btn', function (e) {
        e.preventDefault();
        deleteUniversity(e);
    })

    const deleteUniversity = e => {
        const element = e.currentTarget;
        const universityId = $(element).data('university-id');
        console.log(universityId)

        $.ajax({
            method: 'DELETE',
            url: `/universities/${universityId}`,
            data: {"_token": $('meta[name="csrf-token"]').attr('content')},
        }).done(successResponse => {
            $(`#university-${universityId}`).remove();
        }).fail(e => {
            console.log('no')
        });
    };

    $(document).on('click', '.edit-university-btn', function (e) {
        e.preventDefault();
        editUniversity(e);

    })

    const editUniversity = e => {
        const element = e.currentTarget;
        const universityId = $(element).data('edit-id');
        const name = $(element).parent().parent().parent().find('.name_university').text();
        const description = $(element).parent().parent().parent().find('.description').text();
        const start_date = $(element).parent().parent().parent().find('.start_date').text();
        const finish_date = $(element).parent().parent().parent().find('.finis_date').text();
        const faculty = $(element).parent().parent().parent().find('.faculty').text();
        const modal_name = $('#modal_name').val(name);
        const modal_faculty = $('#modal_faculty').val(faculty);
        const modal_description = $('#modal_description').val(description);
        const modal_start_date = $('#modal_start_date').val(start_date);
        const modal_finish_date = $('#modal_finish_date').val(finish_date);
        const hidden = $('#university_hidden_id').val(universityId);
        console.log(name);
    }

    $(document).on('click', '.save-university', function (e) {
        e.preventDefault();
        const element = e.currentTarget;
        const universityId = $('#university_hidden_id').val();
        const data = $(element).parent().serialize();
        console.log(universityId);
        $.ajax({
            method: 'PUT',
            url: `/universities/${universityId}/`,
            data: data
        }).done(successResponse => {
            console.log('es')
            $(`#university-${universityId}`).replaceWith(successResponse.university);
        })
    })
});
