$(() => {

    $(document).on('click', '#create-certificate-btn', function (e) {
        e.preventDefault();
        createCertificate(e);
    })

    const createCertificate = e => {
        const element = e.currentTarget;
        const productImage = document.querySelector('#create-certificate-form input[name=picture]');
        const formData = new FormData();
        formData.append('name', $("#create-certificate-form input[name=name]").val());
        formData.append('description', $("#create-certificate-form input[name=description]").val());
        formData.append('start_date', $("#create-certificate-form input[name=start_date]").val());
        formData.append('finish_date', $("#create-certificate-form input[name=finish_date]").val());
        if (productImage.files[0] !== undefined){
            formData.append('picture', productImage.files[0]);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url: `/certificates/`,
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
        }).done(successResponse => {
            $('#certificate').prepend(successResponse.certificate)
            $('#create-certificate-form').trigger('reset');
            console.log('es')
        }).fail(e => {
            console.log('no')
        });
    };


    $(document).on('click', '.delete-certificate-btn', function (e) {
        e.preventDefault();
        deleteCertificate(e);
    })

    const deleteCertificate = e => {
        const element = e.currentTarget;
        const certificateId = $(element).data('certificate-id');
        console.log(certificateId)

        $.ajax({
            method: 'DELETE',
            url: `/certificates/${certificateId}`,
            data: {"_token": $('meta[name="csrf-token"]').attr('content')},
        }).done(successResponse => {
            $(`#certificate-${certificateId}`).remove();
        }).fail(e => {
            console.log('no')
        });
    };

    $(document).on('click', '.edit-certificate-btn', function (e) {
        e.preventDefault();
        editCertificate(e);
    })

    const editCertificate = e => {
        const element = e.currentTarget;
        const certificateId = $(element).data('edit-id');
        const name = $(element).parent().parent().parent().parent().find('.name').text();
        const description = $(element).parent().parent().parent().find('.description').text();
        const start_date = $(element).parent().parent().parent().find('.start_date').text();
        const finish_date = $(element).parent().parent().parent().find('.finis_date').text();
        const modal_name = $('#modal_name').val(name);
        const modal_description = $('#modal_description').val(description);
        const modal_start_date = $('#modal_start_date').val(start_date);
        const modal_finish_date = $('#modal_finish_date').val(finish_date);
        const hidden = $('#certificate_hidden_id').val(certificateId);
        console.log(hidden)
    }


    $(document).on('click', '.save-certificate', function (e) {
        const element = e.currentTarget;
        const certificateId = $('#certificate_hidden_id').val();
        const data = $(element).parent().serialize();
        // const certificatePicture = document.querySelector('#edit-certificate-form input[name=picture]');
        // const formData = new FormData();
        // // data.append($(element).parent().serialize());
        // // formData.append('name', $("#edit-certificate-form input[name=name]").val());
        // const aa = $("#edit-certificate-form input[name=name]").val()
        // formData.append('name', aa);
        // formData.append('description', $("#edit-certificate-form input[name=description]").val());
        // formData.append('start_date', $("#edit-certificate-form input[name=start_date]").val());
        // formData.append('finish_date', $("#edit-certificate-form input[name=finish_date]").val());
        // if (certificatePicture.files[0] !== undefined){
        //     formData.append('picture', certificatePicture.files[0]);
        // }
        // data.append('picture', formData.get('picture'));
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        console.log(data);
        $.ajax({
            method: 'PUT',
            url: `/certificates/${certificateId}/`,
            data: data,
            // processData: false,
            // contentType: false,
            // dataType: 'json',
        }).done(successResponse => {
            $(`#certificate-${certificateId}`).replaceWith(successResponse.certificate);
            console.log(successResponse.certificate)
        })
    })
});
