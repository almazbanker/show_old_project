$(document).ready(function() {

    $(document).on('click', '#create-comment-btn', function (e)   {
        e.preventDefault();

        const userId = $('#user_id').val();
        const data = $('#create-comment').serialize();
        console.log(userId)
        $.ajax({
            method: 'POST',
            url: `/users/${userId}/comments`,
            data: data
        }).done(response => {
            renderComment(response.comment);
        }).fail(response => {
            console.log('no');
        });
    });

    const renderComment = comment => {
        const commentsBlock = $('.scrollit');
        $(commentsBlock).prepend(comment);
        resetForm();
    };

    const resetForm = () => {
        $('#create-comment').trigger('reset');
    }

    $(document).on('click', '.delete-comment', function(event) {
        event.preventDefault();
        deleteComment(event);
    });

    const deleteComment = event => {
        const element = event.currentTarget;
        const commentId = $(element).data('comment-id');
        const token = $("meta[name=csrf-token]").attr('content');
        $.ajax({
            method: 'DELETE',
            url: `/comments/${commentId}`,
            data: {_token: token}
        }).done(successResponse => {
            $(`#comment-${commentId}`).remove();
        }).fail(errorResponse => {
            console.log('error ===> ', errorResponse);
        })
    };

});

