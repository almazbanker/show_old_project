@extends('layouts.app')
@section('content')
    <div style="margin: 200px 0">
        <h3>{{$category->name}}</h3>

        <div class="row">
            <form method="GET" action="{{route('tests.index')}}">
                @csrf
                <input type="hidden" value="{{$category->id}}" name="category_id">
                <input type="hidden" value="1" name="level">
                <button class="boxed-btn5" type="submit">Start</button>
            </form>
        </div>
    </div>
@endsection
