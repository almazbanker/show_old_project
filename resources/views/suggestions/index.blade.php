@extends('layouts.app')

@section('content')

    @foreach($suggestions as $suggestion)
        <div class="card mt-3">
            <div class="card-header">
                {{__('Valid')}} {{__('from')}} {{$suggestion->start_date}} {{__('until')}} {{$suggestion->finish_date}}
            </div>
            <div class="card-body">
                <h5 class="card-title">{{__('Suggestion')}} # {{$loop->iteration}}</h5>
                <p class="card-text">{{$suggestion->suggestion}}</p>
                <p class="card-text">{{__('For')}} {{$suggestion->votes->where('agree', true)->count()}}</p>
                <p class="card-text">{{__('Against')}} {{$suggestion->votes->where('agree', false)->count()}}</p>
                @if(\Illuminate\Support\Facades\Auth::user()->votes->where('suggestion_id', $suggestion->id)->first())
                    <form method="POST" action="{{route('votes.destroy', ['vote' => \Illuminate\Support\Facades\Auth::user()->votes->where('suggestion_id', $suggestion->id)->first()])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-primary">{{__('Change my mind')}}</button>
                    </form>
                @else
                    <div class="d-flex">
                        <form method="POST" action="{{route('votes.store')}}">
                            @csrf
                            <input type="hidden" value="{{$suggestion->id}}" name="suggestion_id">
                            <input type="hidden" name="agree" value="1">
                            <button class="btn btn-outline-success" type="submit">{{__('Agree')}}</button>
                        </form>
                        <form class="mx-2" method="POST" action="{{route('votes.store')}}">
                            @csrf
                            <input type="hidden" value="{{$suggestion->id}}" name="suggestion_id">
                            <button class="btn btn-outline-danger" type="submit">{{__('Disagree')}}</button>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    @endforeach

@endsection
