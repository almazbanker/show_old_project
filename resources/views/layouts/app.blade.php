 <!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive HTML5 Resume/CV Template for Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>




    <title>Judicial &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">
    <link href="{{ asset('css/table.css') }}" rel="stylesheet">
    <link href="{{ asset('css/resume.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form-callback.css') }}" rel="stylesheet">
    <link href="{{ asset('css/cards.css') }}" rel="stylesheet">
    <link href="{{ asset('css/card-mediators.css') }}" rel="stylesheet">
    <link href="{{ asset('css/categories.css') }}" rel="stylesheet">


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Oswald:400,700">

    <link rel="stylesheet" href="/css/bootstrap.min.css">


    <link rel="stylesheet" href="/css/style.css">

    <style>
        .laravel-embed__responsive-wrapper { position: relative; height: 0; overflow: hidden; max-width: 100%; }
        .laravel-embed__fallback { background: rgba(0, 0, 0, 0.15); color: rgba(0, 0, 0, 0.7); display: flex; align-items: center; justify-content: center; }
        .laravel-embed__fallback,
        .laravel-embed__responsive-wrapper iframe,
        .laravel-embed__responsive-wrapper object,
        .laravel-embed__responsive-wrapper embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
    </style>

</head>
<body>



<header>
    <div class="header-area ">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center justify-content-between no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img">
                            <img src="{{asset('img/logo.png')}}" alt="">
                        </div>
                    </div>

                    <div class="col-xl-7 col-lg-8">
                        <div class="main-menu  d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a href="{{route('companies.index')}}">{{__('Main')}}</a></li>
                                    <li><a href="{{route('users.index')}}">{{__('Mediators')}}</a></li>
                                    <li><a href="{{route('partners.index')}}">{{__('Partners')}}</a></li>
                                    <li><a href="#">Categories <i class="ti-angle-down"></i></a>
                                        <ul class="submenu">
                                            @foreach(categories() as $category)
                                                <li><a href="{{route('categories.show', ['category' => $category])}}">{{$category->name}}</a></li>
                                            @endforeach
                                            <li><a href="single-blog.html">single-blog</a></li>
                                        </ul>
                                    </li>

                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <li>
                                        <a href="{{route('questionnaires.create')}}">{{__('Questionnaire')}}</a>
                                        </li>
                                    @endif
                                    @can('view', \App\Models\User::class)
                                      <li><a href="{{route('users.mediators')}}">For Mediator</a></li>
                                    @endcan
                                </ul>



                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-2 d-none d-lg-block">
                            <ul class="d-flex">
                            @guest
                                @if (Route::has('login'))
                                    <li class="mx-2">
                                        <a style="color: white" class="btn btn-outline-warning" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif

                                @if (Route::has('register'))
                                    <li>
                                        <a style="color: white" class="btn btn-outline-warning" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ Auth::user()->name }}
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest

                                <li>
                                    <select id="language"
                                            style="color: #fff; border-radius: 0; background-color: #0b0b0b; border-color: #F2C64D;"
                                            class="custom-select btn-outline-warning" onchange="location = this.value;">
                                        <option @if(session('locale') == 'en') selected
                                                @endif value="{{route('language.switcher', ['locale' => 'en'])}}">EN
                                        </option>
                                        <option @if(session('locale') == 'ru') selected
                                                @endif value="{{route('language.switcher', ['locale' => 'ru'])}}">RU
                                        </option>
                                    </select>
                                </li>
                            </ul>
                        @include('callbacks.callback')
                    </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>




<div class="container">
{{--    @include('notifications.notifications')--}}
    @yield('content')
</div>
<div class="d-block">
    @yield('main')
</div>



<footer class="footer mt-5">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <div class="footer_logo">
                            <a href="index.html">
                                <img src="img/logo.png" alt="">
                            </a>
                        </div>
                        <p class="footer_text">200, A-block, Green road, USA <br>
                            +10 367 267 2678 <br>
                            <a class="domain" href="#">lawyer@contact.com</a></p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-md-6 col-lg-2">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Practice Area
                        </h3>
                        <ul>
                            <li><a href="#">Business law
                                </a></li>
                            <li><a href="#">Finance law</a></li>
                            <li><a href="#">Education law</a></li>
                            <li><a href="#">Family law</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-md-6 col-lg-2">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Useful Links
                        </h3>
                        <ul>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#"> Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Subscribe
                        </h3>
                        <form action="#" class="newsletter_form">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit">Sign Up</button>
                        </form>
                        <p class="newsletter_text">Subscribe newsletter to get updates</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This website is made <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">PHP developer team</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
