@extends('layouts.app')

@section('content')
    @include('navbar.navbar')
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                    @if(isset($user->questionnaire->picture))
                        <img class="rounded-circle mt-5" width="150px" height="150px"
                             src="{{asset('/storage/' . $user->questionnaire->picture)}}" alt="#">
                    @else
                        <img class="rounded-circle mt-5" width="150px" height="150px"
                             src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"
                             alt="#">
                    @endif
                    <span class="font-weight-bold">{{Auth::user()->name}}</span>
                    <span class="text-black-50">{{Auth::user()->email}}</span>
                </div>
            </div>
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">{{__('License')}}</h4>
                    </div>
                    <div class="row mt-3">
                        <form action="{{route('licenses.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <label for="start_date" class="form-label @error('description') is-invalid @enderror"> {{__('Start date')}}</label>
                                <input name="start_date" type="date" class="form-control" id="start_date"
                                       placeholder="Start date">
                            </div>
                            <div class="col-md-12">
                                <label for="finish_date" class="form-label @error('description') is-invalid @enderror"> {{__('Finish date')}}</label>
                                <input name="finish_date" type="date" class="form-control" id="finish_date"
                                       placeholder="Finish date">
                            </div>
                            <div class="col-md-12">
                                <label for="description" class="form-label">{{__('Description')}}</label>
                                <textarea class="form-control @error('description') is-invalid @enderror"
                                          id="description" rows="3" name="description">{{  old('description') }}</textarea>
                                @error('description')
                                <span class="invalid-feedback"
                                      role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="col-md-12">
                                <label for="number" class="form-label">{{__('Number')}}</label>
                                <input class="form-control @error('number') is-invalid @enderror" type="text"
                                       id="number" name="number" value="{{ old('number') }}"/>
                                @error('number')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col mt-3 ml-3" style="width: 375px; text-align: center;">
                                <input type="file" class="custom-file-input" id="picture" name="picture">
                                <label class="custom-file-label" for="picture">{{__('Choose file')}}</label>
                            </div>
                            <button type="submit" class="boxed-btn5 mt-5 w-100">{{__('Create')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
