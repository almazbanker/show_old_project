@extends('layouts.app')

@section('content')
    @include('navbar.navbar')
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                    <img class="rounded-circle mt-5" height="150px" width="150px"
                         src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"
                         alt="#">
                    <span class="font-weight-bold">{{$user->name}}</span>
                    <span class="text-black-50">{{$user->email}}</span>
                </div>
            </div>
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">{{__('Create')}} {{__('Questionnaire')}}</h4>
                    </div>
                    <div class="row mt-3">
                        <form action="{{route('questionnaires.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <label for="gender"
                                       class="form-label @error('gender') is-invalid @enderror">{{__('Gender')}}</label>
                                <select style="margin-left: 0" class="custom-select" name="gender" id="gender">
                                    <option value="male">male</option>
                                    <option value="female">female</option>
                                </select>
                                @error('gender')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col-md-12">
                                <label for="country_id" class="form-label">{{__('Country')}}</label>
                                <select style="margin-left: 0" class="custom-select" name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="born_date"
                                       class="form-label @error('born_date') is-invalid @enderror">{{__('Born date')}}</label>
                                <input name="born_date" type="date" class="form-control" id="born_date"
                                       value="{{ old('born_date') }}">
                                @error('born_date')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col mt-3 ml-3" style="width: 375px; text-align: center;">
                                <input type="file" class="custom-file-input @error('picture') is-invalid @enderror"
                                       id="picture" name="picture">
                                <label class="custom-file-label" for="picture">{{__('Choose picture')}}</label>
                                @error('picture')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <button type="submit" class="boxed-btn5 mt-5 w-100">{{__('Create')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
