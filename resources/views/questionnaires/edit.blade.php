@extends('layouts.app')

@section('content')
    @include('navbar.navbar')
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                @if(isset($user->questionnaire->picture))
                    <img class="rounded-circle mt-5" width="150px" height="150px"
                         src="{{asset('/storage/' . $user->questionnaire->picture)}}"
                         alt="{{$user->questionnaire->picture}}">
                @else
                    <img class="rounded-circle mt-5" width="150px" height="150px"
                         alt="#"
                         src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                @endif
            </div>
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">{{__('Edit')}} {{__('Questionnaire')}}</h4>
                    </div>
                    <div class="row mt-3">
                        <form action="{{route('questionnaires.update', ['questionnaire' => $questionnaire])}}"
                              method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-md-12">
                                <label for="gender"
                                       class="form-label @error('gender') is-invalid @enderror">{{__('Gender')}}</label>
                                <select style="margin-left: 0" class="custom-select" name="gender" id="gender">
                                    <option @if($user->questionnaire->gender == 'male') selected @endif value="male">male</option>
                                    <option @if($user->questionnaire->gender == 'female') selected @endif value="female">female</option>
                                </select>
                                @error('gender')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col-md-12">
                                <label for="country_id" class="form-label">{{__('Country')}}</label>
                                <select style="margin-left: 0" class="custom-select" name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option @if($user->questionnaire->country->id == $country->id) selected @endif value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="born_date"
                                       class="form-label @error('born_date') is-invalid @enderror">{{__('Born date')}}</label>
                                <input name="born_date" type="date" class="form-control" id="born_date"
                                       value="{{$user->questionnaire->born_date}}">
                                @error('born_date')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <div class="col mt-3 ml-3" style="width: 375px; text-align: center;">
                                <input type="file" class="custom-file-input @error('picture') is-invalid @enderror"
                                       id="picture" name="picture">
                                <label class="custom-file-label" for="picture">{{__('Choose file')}}</label>
                                @error('picture')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                            <button type="submit" class="boxed-btn5 mt-5 w-100">{{__('Update')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
