@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="m-5">
            <div class="text-center">
                <img height="400px" width="85%" src="{{asset('/storage/' . $type->picture)}}" />
            </div>
            <div>
                <div class="section_title text-center mb-60 mb-2 mt-2">
                    <h3 class="text-center">{{$type->name}}</h3>
                </div>

                <div>
                    <p>{{$type->description}}</p>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection
