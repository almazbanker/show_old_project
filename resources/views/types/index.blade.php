@extends('layouts.app')
@section('content')

        @foreach($types as $type)
            <div class="col">
                <div class="card text-white bg-secondary mb-3" >
                    <div class="card-header">
                        <a class="btn btn-sm btn-outline-primary" href="{{route('types.show', ['type' => $type])}}">{{$type->name}}</a>
                    </div>
                    <div class="card-body">
                        <p>{{$type->description}}</p>
                    </div>
                </div>
            </div>
        @endforeach
@endsection
