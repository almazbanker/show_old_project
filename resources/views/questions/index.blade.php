@extends('layouts.app')

@section('content')

    <h1>{{__('Tests')}}</h1>
    @include('questions.question', ['question' => $question])

@endsection
