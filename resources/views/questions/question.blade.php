<div id="questions-test">
    <div class="card question-card" id="question-{{$id}}">
        <div class="card-header">
            {{__('Question numbers')}}: {{$id}}
        </div>
        <div class="card-body">
            <h5 class="card-title">{{$question}}</h5>
            <p class="card-text"></p>
            <form id="question-form" method="POST">
                @csrf
                <input type="hidden" name="category_id" value="{{$category->id}}">
                <input type="hidden" name="test_id" value="{{$test->id}}">
                <input type="hidden" name="level" value="{{$level}}">
                <input id="input_question_id" name="question_id" type="hidden" value="{{$id}}">
                <div class="form-check">
                    <input value="{{$questions[0]}}" class="form-check-input" type="radio" name="answer"
                           id="flexRadioDefault1">
                    <label class="form-check-label" for="flexRadioDefault1">
                        {{$questions[0]}}
                    </label>
                </div>
                <div class="form-check">
                    <input value="{{$questions[1]}}" class="form-check-input" type="radio" name="answer"
                           id="flexRadioDefault2">
                    <label class="form-check-label" for="flexRadioDefault2">
                        {{$questions[1]}}
                    </label>
                </div>
                <div class="form-check">
                    <input value="{{$questions[2]}}" class="form-check-input" type="radio" name="answer"
                           id="flexRadioDefault2">
                    <label class="form-check-label" for="flexRadioDefault2">
                        {{$questions[2]}}
                    </label>
                </div>
                <div class="form-check">
                    <input value="{{$questions[3]}}" class="form-check-input" type="radio" name="answer"
                           id="flexRadioDefault2">
                    <label class="form-check-label" for="flexRadioDefault2">
                        {{$questions[3]}}
                    </label>
                </div>
                <button data-question-id="{{$id}}" class="btn btn-outline-success save-question-btn"
                        type="submit">{{__('Next')}}
                </button>
            </form>
        </div>
    </div>
</div>
