@extends('layouts.app')

@section('content')
    @include('navbar.navbar')


    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">

                        @if(isset(auth()->user()->questionnaire->picture))
                            <img class="rounded-circle mt-5" width="150px" height="150px"
                                 src="{{asset('/storage/' . auth()->user()->questionnaire->picture)}}">
                        @else
                            <img class="rounded-circle mt-5" width="150px" height="150px"
                                 src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                        @endif
                    <span class="font-weight-bold">{{Auth::user()->name}}</span><span class="text-black-50">{{Auth::user()->email}}</span><span> </span></div>
            </div>
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">{{__('Certificate')}}</h4>
                    </div>
                    <div class="row mt-3">
                        <form id="create-certificate-form" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <label for="name" class="form-label">{{__('Certificate name')}}</label>
                                <input name="name" type="text" class="form-control" id="name" placeholder="Name">
                            </div>
                            <div class="col-md-12">
                                <label for="description" class="form-label">{{__('Description')}}</label>
                                <input name="description" type="text" class="form-control" id="description" placeholder="Description">
                            </div>
                            <div class="col-md-12">
                                <label for="exampleFormControlInput1" class="form-label">{{__('Start date')}}</label>
                                <input name="start_date" type="date" class="form-control" id="exampleFormControlInput1">
                            </div>
                            <div class="col-md-12">
                                <label for="exampleFormControlInput1" class="form-label">{{__('Finish date')}}</label>
                                <input name="finish_date" type="date" class="form-control" id="exampleFormControlInput1">
                            </div>
                            <div class="col mt-3 ml-3" style="width: 375px; text-align: center;">
                                <input type="file" class="custom-file-input" id="picture" name="picture">
                                <label class="custom-file-label" for="picture">{{__('Choose file')}}</label>
                            </div>
                            <button id="create-certificate-btn" class="boxed-btn5 mt-5 w-100">{{__('Create')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="event-schedule-area-two bg-color pad100">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="home" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr style="font-size: 14px">
                                        <th scope="col">Start Date</th>
                                        <th scope="col">Finish Date</th>
                                        <th scope="col">Photo</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Description</th>
                                        <th class="text-center" scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="certificate">
                                        @foreach($certificates as $certificate)
                                            @include('certificates.certificate', ['certificate' => $certificate])
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /col end-->
            </div>
            <!-- /row end-->
        </div>
    </div>










    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Edit certificate')}}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="edit-certificate-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="certificate_hidden_id">
                        <div class="mb-3">
                            <label for="modal_name" class="form-label">{{__('Certificate name')}}</label>
                            <input name="name" type="text" class="form-control" id="modal_name" placeholder="Name">
                        </div>
                        <div class="mb-3">
                            <label for="modal_description" class="form-label">{{__('Description')}}</label>
                            <input name="description" type="text" class="form-control" id="modal_description" placeholder="Description">
                        </div>
                        <div class="mb-3">
                            <label for="modal_start_date" class="form-label">{{__('Start date')}}</label>
                            <input name="start_date" type="date" class="form-control" id="modal_start_date">
                        </div>
                        <div class="mb-3">
                            <label for="modal_finish_date" class="form-label">{{__('Finish date')}}</label>
                            <input name="finish_date" type="date" class="form-control" id="modal_finish_date">
                        </div>
                        <button type="button" class="boxed-btn5 mt-5 w-100 save-certificate" data-bs-dismiss="modal">Save changes</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="boxed-btn5 w-100 bg-dark" data-bs-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

