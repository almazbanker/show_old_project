<tr class="inner-box" id="certificate-{{$certificate->id}}">
    <th scope="row">
        <div class="event-date">
            <p class="start_date">{{$certificate->start_date}}</p>
        </div>
    </th>
    <th>
        <div class="event-date">
            <p class="finis_date">{{$certificate->finish_date}}</p>
        </div>
    </th>
    <th>
        <div class="event-date">
            @if($certificate->picture)
                <img style="height: 150px; width: 150px" src="{{asset('/storage/' . $certificate->picture)}}" alt="1">
            @endif
        </div>
    </th>
    <td>
        <div class="event-wrap">
            <h5 class="name">{{$certificate->name}}</h5>
        </div>
    </td>
    <td>
        <div class="r-no">
            <p class="card-text description">{{$certificate->description}}</p>
        </div>
    </td>
    <td>
        <div class="primary-btn">
            <button class="boxed-btn5 edit-work-btn my-btn  edit-certificate-btn" data-edit-id="{{$certificate->id}}"  data-bs-toggle="modal" data-bs-target="#exampleModal">{{__('Edit')}}</button>
            <button data-certificate-id="{{$certificate->id}}" class="boxed-btn5 mt-3  bg-dark delete-certificate-btn">{{__('Delete')}}</button>
        </div>
    </td>
</tr>


