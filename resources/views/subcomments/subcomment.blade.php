<div class="card p-3 mb-2" id="collapse-{{$subcomment->id}}-card">
    <div class="d-flex flex-row"><img
            src="https://cs8.pikabu.ru/post_img/big/2017/07/04/9/1499179580111970674.jpg" height="40"
            width="40"
            class="rounded-circle" alt="#">
        <div class="d-flex flex-column ms-2">
            <h6 class="mb-1 text-primary">{{$subcomment->user->name}}</h6>
            <p class="comment-text">{{$subcomment->content}}</p>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <div class="d-flex flex-row gap-3 align-items-center">
            @can('delete', $subcomment)
                    <a class="delete-subcomment btn btn-outline-danger" id="comment-{{$subcomment->id}}"
                       data-subcomment-id="{{$subcomment->id}}" href="#"><i class="bi bi-trash"></i> Delete</a>
            @endcan
        </div>
        <div class="d-flex flex-row"><span
                class="text-muted fw-normal fs-10">{{$subcomment->created_at->diffForHumans()}}</span></div>
    </div>
</div>
