@extends('layouts.app')

@section('content')
    <div class="mt-5">

        <div>
            @foreach($meetings as $meeting)

                @can('create', \App\Models\User::class)
                    <a target="_blank" href="{{$meeting->start_url}}"><h1>{{__('Start meeting')}}</h1></a>
                    <h4>Password for this test: {{$meeting->password}}</h4>
                    <form method="POST" action="{{route('zoom.destroy', ['meeting' => $meeting])}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="boxed-btn5 bg-dark">Delete</button>
                    </form>
                @endcan
                @can('view', \App\Models\User::class)
                    <a target="_blank" href="{{$meeting->join_url}}">{{__('Join meeting')}}</a>
                @endcan
                <div class="row mb-5">
                    <form method="GET" action="{{route('tests.index')}}">
                        @csrf
                        <input type="hidden" value="{{$meeting->category->id}}" name="category_id">
                        <input type="hidden" value="{{$meeting->id}}" name="meeting_id">
                        <input type="hidden" value="2" name="level">
                        <label for="basic-url" class="form-label">{{ __('Enter the password') }}</label>
                        <div class="input-group mb-3 w-50">
                            <span class="input-group-text" id="password">Password</span>
                            <input name="password" type="text" class="form-control" id="basic-url"
                                   aria-describedby="password">
                        </div>
                        <button class="boxed-btn5" type="submit">Start</button>
                    </form>
                </div>
            @endforeach
        </div>
        @can('create', \App\Models\User::class)
            <div class="mt-5">
                <h3>{{ __('You can create meeting as moderator!') }}</h3>
                <a class="boxed-btn5" href="{{route('meetings.create')}}">Create</a>
            </div>
        @endcan

    </div>

@endsection
