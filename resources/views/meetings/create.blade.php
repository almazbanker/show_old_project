@extends('layouts.app')

@section('content')
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row justify-content-center">
            <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">{{__('Meeting Create')}}</h4>
                    </div>
                    <div class="row mt-3">
                        <form id="create-work-form" method="POST" action="{{route('zoom.store')}}">
                            @csrf
                            <div class="col-md-12">
                                <label for="duration" class="form-label">{{__('Duration')}}</label>
                                <input name="duration" type="number" class="form-control" id="duration">
                            </div>
                            <div class="col-md-12">
                                <label for="topic" class="form-label">{{__('Topic')}}</label>
                                <input name="topic" type="text" class="form-control" id="topic">
                            </div>
                            <div class="col-md-12">
                                <label for="start_time" class="form-label">{{__('Start Time')}}</label>
                                <input name="start_time" type="datetime-local" class="form-control" id="start_time">
                            </div>
                            <div class="col-md-12">
                                <label for="category_id" class="form-label">{{__('Category')}}</label>
                                <select name="category_id" id="category_id" class="form-select">
                                    @foreach(categories() as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="boxed-btn5 mt-5 w-100 ">{{__('Create')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
