<div class="text-center mt-5 d-flex container">
    <a class="boxed-btn5 mr-3" href="{{route('questionnaires.create')}}">{{__('Questionnaires')}}</a>
    <a class="boxed-btn5 mr-3" href="{{route('categories.create')}}">{{__('Categories')}}</a>
    <a class="boxed-btn5 mr-3" href="{{route('works.create')}}">{{__('Works')}}</a>
    <a class="boxed-btn5 mr-3" href="{{route('universities.create')}}">{{__('Universities')}}</a>
    <a class="boxed-btn5 mr-3" href="{{route('certificates.create')}}">{{__('Certificates')}}</a>
    <a class="boxed-btn5" href="{{route('licenses.create')}}">{{__('Licenses')}}</a>
</div>


