<div class="col">
    <div class="card" style="width: 18rem;">
        <img src="{{asset('storage/' . $partner->picture)}}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title"><a href="{{route('partners.show', ['partner' => $partner])}}">{{$partner->name}}</a></h5>
            <p class="card-text">{{$partner->description}}</p>
        </div>
    </div>
</div>
