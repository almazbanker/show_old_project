@extends('layouts.app')
@section('content')

    @include('callbacks.callback')

    <h1>{{__('Partners')}}</h1>
    <div class="row">
        <div class="about_area">
            <div class="opacity_icon d-none d-lg-block">
                <i class="flaticon-balance"></i>
            </div>
            <div class="container">
                <div class="row">
                    @foreach($partners as $partner)
                        <div class="col-xl-6 col-md-6">
                            <div class="single_about_info text-center">
                                <div class="about_thumb">
                                    <img src="{{asset('/storage/' . $partner->picture)}}" alt="">
                                </div>
                                <h3>{{$partner->name}}<br>
                                </h3>
                                <p>{{$partner->description}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
