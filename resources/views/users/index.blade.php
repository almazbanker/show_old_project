@extends('layouts.app')

@section('main')
    <div class="" style="  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;background-image: url('https://jfp.org.ua/system/blog_articles/images/:parent_blog_article_id/original/uk/Fotolia_134397970_Subscription_Monthly_XXL.jpg?1605088623')">
        <div class="slider_area_inner slider_bg_1 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="single_slider">
                            <div class="slider_text">
                                <h3>High Quality Law <br>
                                    Advice and Support</h3>

                                <p>Leading Polish Lawyer in your city</p>
                                <a data-bs-toggle="modal" data-bs-target="#callback" href="#" class="boxed-btn4 "><i class="bi bi-telephone mr-2"></i> Free Consulting</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container mt-5">
    <div class="row">
        @foreach($users as $user)
                @include('users.user')
        @endforeach
    </div>
    <div class="row justify-content-center">
        <div class="col-md-auto">
            {{$users->links('pagination::bootstrap-4')}}
        </div>
    </div>
</div>
@endsection
