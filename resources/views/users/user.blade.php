<div class="col-12 col-sm-6 col-md-3 col-lg-4">
    <div class="our-team">
        <div class="picture">
            @if(!$user->picture)
                <img class="img-fluid" src="{{asset('img/profile_photo.jpg')}}" alt="#">
            @else
                <img class="img-fluid" src="{{asset('/storage/' . $user->picture)}}" alt="">
            @endif
        </div>
        <div class="team-content">
            <h3 class="name"><a href="{{route('users.show', ['user' => $user])}}">{{$user->name}}</a></h3>
            <p>Likes: {{$user->likes->count()}}</p>
            <div class="d-flex justify-content-center">
                <div>
                    @if(\Illuminate\Support\Facades\Auth::check())
                        @if($user->likes()->where('author_id', \Illuminate\Support\Facades\Auth::id())->count() > 0)
                            <form action="{{route('likes.destroy')}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button type="submit" style="border: none; font-size: 25px"
                                        class="btn btn-outline-danger">
                                    <i class="bi bi-heart-fill"></i>
                                </button>
                            </form>
                        @else
                            <form action="{{route('likes.store')}}" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button type="submit" style="border: none; font-size: 25px"
                                        class="btn btn-outline-danger">
                                    <i class="bi bi-heart"></i>
                                </button>
                            </form>
                        @endif
                    @endcan
                </div>
            </div>
        </div>
        <ul class="social">
            @if($user->link->facebook)
                <li><a href="{{$user->link->facebook}}" class="fa fa-facebook" aria-hidden="true"></a>
                </li>
            @endif
            @if($user->link->twitter)
                <li><a href="{{$user->link->twitter}}" class="fa fa-twitter" aria-hidden="true"></a>
                </li>
            @endif
            @if($user->link->instagram)
                <li><a href="{{$user->link->instagram}}" class="fa fa-instagram"
                       aria-hidden="true"></a></li>
            @endif
        </ul>
    </div>
</div>



