@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Resume/CV Design</title>
    <link rel="stylesheet" href="public/css/resume.css">
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
</head>
<body>

<div class="resume">
    <div class="resume_left">
        <div class="resume_profile">
            @if(!$user->picture)
                <img src="{{asset('img/profile_photo.jpg')}}" alt="profile_pic">
            @else
                <img src="{{asset('/storage/' . $user->picture)}}" alt="profile_pic">
            @endif
        </div>
        <div class="resume_content">
            <div class="resume_item resume_info">
                <div class="title">
                    <p class="bold">{{$user->name}}</p>
                    <p class="regular">Mediator</p>
                </div>
                <ul>
                    <li>
                        <div class="icon">
                            <i class="fas fa-map-signs"></i>
                        </div>
                        <div class="data">
                            21 Street, Texas <br/> USA
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="fas fa-mobile-alt"></i>
                        </div>
                        <div class="data">
                            +324 4445678
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="fab fa-weebly"></i>
                        </div>
                        <div class="data">
                            www.stephen.com
                        </div>
                    </li>
                </ul>
            </div>
            <div class="resume_item resume_skills">
                <div class="title">
                    <p class="bold">skill's</p>
                </div>
                <ul>
                    <li>
                        <div class="skill_name">
                            HTML
                        </div>
                        <div class="skill_progress">
                            <span style="width: 80%;"></span>
                        </div>
                        <div class="skill_per">80%</div>
                    </li>
                    <li>
                        <div class="skill_name">
                            CSS
                        </div>
                        <div class="skill_progress">
                            <span style="width: 70%;"></span>
                        </div>
                        <div class="skill_per">70%</div>
                    </li>
                    <li>
                        <div class="skill_name">
                            SASS
                        </div>
                        <div class="skill_progress">
                            <span style="width: 90%;"></span>
                        </div>
                        <div class="skill_per">90%</div>
                    </li>
                    <li>
                        <div class="skill_name">
                            JS
                        </div>
                        <div class="skill_progress">
                            <span style="width: 60%;"></span>
                        </div>
                        <div class="skill_per">60%</div>
                    </li>
                    <li>
                        <div class="skill_name">
                            JQUERY
                        </div>
                        <div class="skill_progress">
                            <span style="width: 88%;"></span>
                        </div>
                        <div class="skill_per">88%</div>
                    </li>
                </ul>
            </div>
            <div class="resume_item resume_social">
                <div class="title">
                    <p class="bold">Social</p>
                </div>
                <ul>
                    <li>
                        <div class="icon">
                            <i class="fab fa-facebook-square"></i>
                        </div>
                        <div class="data">
                            <p class="semi-bold">Facebook</p>
                            <a href="#">Stephen@facebook</a>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="fab fa-twitter-square"></i>
                        </div>
                        <div class="data">
                            <p class="semi-bold">Twitter</p>
                            <a href="#">Stephen@twitter</a>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="fab fa-youtube"></i>
                        </div>
                        <div class="data">
                            <p class="semi-bold">Youtube</p>
                            <a href="#">Stephen@youtube</a>
                        </div>
                    </li>
                    <li>
                        <div class="icon">
                            <i class="fab fa-instagram"></i>
                        </div>
                        <div class="data">
                            <p class="semi-bold">Instagram</p>
                            <a href="#">Stephen@linkedin</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="resume_right">
        <div class="resume_item resume_about">
            <div class="title">
                <p class="bold">About us</p>
            </div>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perspiciatis illo fugit officiis distinctio
                culpa officia totam atque exercitationem inventore repudiandae?</p>
            <p>email: {{$user->email}}</p>
        </div>
        <div class="resume_item resume_work">
            <div class="title">
                <p class="bold">Work Experience</p>
            </div>
            <ul>
                @foreach($user->works as $work)
                    <li>
                        <div class="date">{{$work->start_date}} - {{$work->finish_date}}</div>
                        <div class="info">
                            <p class="semi-bold">{{$work->name}}</p>
                            <p>{{$work->description}}</p>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="resume_item resume_education">
            <div class="title">
                <p class="bold">Education</p>
            </div>
            <ul>
                @foreach($user->universities as $university)
                    <li>
                        <div class="date">{{$university->start_date}} - {{$university->finish_date}}</div>
                        <div class="info">
                            <p class="semi-bold">Texas International School</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, voluptatibus!</p>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="resume_item resume_hobby">
            <div class="title">
                <p class="bold">Hobby</p>
            </div>
            <ul>
                <li><i class="fas fa-book"></i></li>
                <li><i class="fas fa-gamepad"></i></li>
                <li><i class="fas fa-music"></i></li>
                <li><i class="fab fa-pagelines"></i></li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>
@if(Auth::check())
    <h2>{{__('Comment')}}</h2>
    @can('restore', $user)
        <div class="comment-form">
            <form id="create-comment">
                @csrf
                <input type="hidden" id="user_id" value="{{$user->id}}">
                <div class="form-group">
                    <label for="commentFormControl">{{__('Content')}}</label>
                    <textarea name="content" class="form-control" id="commentFormControl" rows="2" required></textarea>
                </div>
                <button id="create-comment-btn" class="btn btn-outline-primary btn-block mt-3">{{__('Add new comment')}}
                </button>
            </form>
        </div>
    @endcan

    <div class="row d-flex mt-5">
        <h2>{{__('Comments')}}</h2>
        <div class="scrollit">
            @foreach($user->reviews as $comment)
                @include('comments.comment', ['comment' => $comment])
            @endforeach
        </div>
    </div>
@endif
@endsection
