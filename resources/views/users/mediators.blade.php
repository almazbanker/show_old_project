@extends('layouts.app')
@section('main')
    <div class="" style="  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;background-image: url('https://www.prostir.ua/wp-content/uploads/2020/10/close-up-scales-justice.jpg')">
        <div class="slider_area_inner slider_bg_1 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="single_slider">
                            <div class="slider_text">
                                <h3>High Quality Law <br>
                                    Advice and Support</h3>

                                <p>Leading Polish Lawyer in your city</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5 d-flex justify-content-center">
        <div class="row g-0">
            <a href="{{route('trainings.index')}}" class="col-md-4 border-right">
                <div class="cards">
                    <div class="first bg-white p-4 text-center"> <i class="bi bi-file-earmark-text my-icons"></i>
                        <h5>{{ __('trainings') }}</h5>
                        <p class="line1">No limitation - Its got everything that you 'll need as you grow</p>
                    </div>
                </div>
            </a>
            <a href="{{route('suggestions.index')}}" class="col-md-4 border-right">
                <div class="cards">
                    <div class=" second bg-white p-4 text-center"> <i class="bi bi-chat-left my-icons"></i>
                        <h5>{{ __('Suggestions') }}</h5>
                        <p class="line2">$50/month gets you 10 users, and you can add more $10 pe user</p>
                    </div>
                </div>
            </a>
            <a href="{{route('answers.index')}}" class="col-md-4">
                <div class="cards">
                    <div class=" third bg-white p-4 text-center"> <i class="bi bi-check-all my-icons"></i>
                        <h5>{{ __('My answers') }}</h5>
                        <p class="line3">We'll help you get started and be there when you have questions</p>
                    </div>
                </div>
            </a>
            <a href="{{route('books.index')}}" class="col-md-4">
                <div class="cards">
                    <div class=" third bg-white p-4 text-center"> <i class="bi bi-book my-icons"></i>
                        <h5>{{ __('Books') }}</h5>
                        <p class="line3">We'll help you get started and be there when you have questions</p>
                    </div>
                </div>
            </a>
            <a href="{{route('meetings.index')}}" class="col-md-4">
                <div class="cards">
                    <div class=" third bg-white p-4 text-center"> <i class="bi bi-camera-video my-icons"></i>
                        <h5>{{ __('Meetings') }}</h5>
                        <p class="line3">We'll help you get started and be there when you have questions</p>
                    </div>
                </div>
            </a>
            <a href="{{route('users.show', ['user' => Auth::user()])}}" class="col-md-4">
                <div class="cards">
                    <div class=" third bg-white p-4 text-center"> <i class="bi bi-person my-icons"></i>
                        <h5>{{ __('My Profile') }}</h5>
                        <p class="line3">We'll help you get started and be there when you have questions</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
