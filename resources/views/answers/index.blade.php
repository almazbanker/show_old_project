@extends('layouts.app')

@section('content')
    <div style="margin: 100px 0">
        @foreach($user->tests as $test)
            <div class="card">
                <div class="card-header">
                    {{__('Test')}} {{$test->id}}
                </div>
                <div class="card-body">
                    <h5 class="card-title"> {{__('Grade')}}: {{$test->grade}} %</h5>
                    <h5 class="card-title"> {{__('Level')}}: {{$test->level}} </h5>
                    <p class="card-text">{{__('Start time')}}: {{$test->start_time}}</p>
                    <p class="card-text">{{__('Finish time')}}: {{$test->finish_time}}</p>
                </div>
            </div>

            <table class="table mt-5">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{__('Question')}}</th>
                    <th scope="col">{{__('Correct answer')}}</th>
                    <th scope="col">{{__('Your answer')}}</th>
                    <th scope="col">{{__('correct')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($test->answers as $answer)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$answer->question->question}}</td>
                        <td>{{$answer->correct_answer}}</td>
                        <td>{{$answer->answer}}</td>
                        @if($answer->correct)
                            <td>{{__('Yes')}}</td>
                        @else
                            <td>{{__('No')}}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endforeach
    </div>
@endsection
