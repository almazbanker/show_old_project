
{{--<div class="card mt-3" id="university-{{$university->id}}">--}}
{{--    <div class="card-header">--}}
{{--       <h4 class="name_university">{{$university->name}}</h4>--}}
{{--    </div>--}}
{{--    <div class="card-body">--}}
{{--        <p class="card-text description">{{$university->description}}</p>--}}
{{--        <p class="faculty">{{$university->faculty}}</p>--}}
{{--        <p class="start_date">{{$university->start_date}}</p>--}}
{{--        <p class="finis_date">{{$university->finish_date}}</p>--}}
{{--        <button class="btn btn-primary edit-university-btn" data-edit-id="{{$university->id}}" data-bs-toggle="modal" data-bs-target="#exampleModal">{{__('Edit')}}</button>--}}
{{--        <button data-university-id="{{$university->id}}" class="btn btn-outline-danger delete-university-btn">{{__('Delete')}}</button>--}}
{{--    </div>--}}
{{--</div>--}}


<tr class="inner-box" id="university-{{$university->id}}">
    <th scope="row">
        <div class="event-date">
            <p class="start_date">{{$university->start_date}}</p>
        </div>
    </th>
    <th>
        <div class="event-date">
            <p class="finis_date">{{$university->finish_date}}</p>
        </div>
    </th>
    <td>
        <div class="event-wrap">
            <h5 class="name_university">{{$university->name}}</h5>
        </div>
    </td><td>
        <div class="event-wrap">
            <h5 class="faculty">{{$university->faculty}}</h5>
        </div>
    </td>
    <td>
        <div class="r-no">
            <p class="card-text description">{{$university->description}}</p>
        </div>
    </td>
    <td>
        <div class="primary-btn">
            <button class="boxed-btn5 edit-university-btn my-btn" data-edit-id="{{$university->id}}" data-bs-toggle="modal" data-bs-target="#exampleModal">{{__('Edit')}}</button>
            <button data-university-id="{{$university->id}}" class="boxed-btn5 mt-3 bg-dark delete-university-btn">{{__('Delete')}}</button>
        </div>
    </td>
</tr>
</tr>
