    <tr class="inner-box" id="work-{{$work->id}}">
        <th scope="row">
            <div class="event-date">
                <p class="start_date">{{$work->start_date}}</p>
            </div>
        </th>
        <th>
            <div class="event-date">
                <p class="finis_date">{{$work->finish_date}}</p>
            </div>
        </th>
        <td>
            <div class="event-wrap">
                <h5 class="name">{{$work->name}}</h5>
            </div>
        </td>
        <td>
            <div class="r-no">
                <p class="card-text description">{{$work->description}}</p>
            </div>
        </td>
        <td>
            <div class="primary-btn">
                <button class="boxed-btn5 edit-work-btn my-btn w-75" data-edit-id="{{$work->id}}"  data-bs-toggle="modal" data-bs-target="#workModal">{{__('Edit')}}</button>
                <button data-work-id="{{$work->id}}" class="boxed-btn5 mt-3 w-75 bg-dark delete-work-btn">{{__('Delete')}}</button>
            </div>
        </td>
    </tr>

