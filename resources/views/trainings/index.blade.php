@extends('layouts.app')
@section('content')
    <div class="d-flex justify-content-center" style="margin: 200px 0">
        @can('view', \Illuminate\Support\Facades\Auth::user())

            @foreach($categories as $category)
                <div class="text-center">
                    <h4>{{$category->name}}</h4>
                    <form method="GET" action="{{route('trainings.show', ['category' => $category])}}">
                        @csrf
                        <button class="boxed-btn5" type="submit">{{__('Start')}}</button>
                    </form>
                </div>
            @endforeach

        @endcan
    </div>

@endsection
