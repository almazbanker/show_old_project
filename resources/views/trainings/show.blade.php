@extends('layouts.app')
@section('content')
    @can('view', \Illuminate\Support\Facades\Auth::user())

        @if($training->videos)
            <div class="mt-5">
                @foreach($training->videos as $video)
                    <x-embed url="{{$video->video}}"  />
                @endforeach
            </div>
        @endif

        <h3 class="mt-5">{{$training->name}}</h3>
        <div>
            <p>{{$training->description}}</p>
        </div>
        <div class="d-flex justify-content-center">
            <form method="POST" action="{{route('trainings.previous')}}">
                @csrf
                <input type="hidden" value="{{$category->id}}" name="category_id">
                <input type="hidden" value="{{$training->id}}" name="training_id">
                <button style="width: 300px;" type="submit" class="boxed-btn5 mt-5  bg-dark">{{__('Previous')}}</button>
            </form>
            <form method="POST" action="{{route('trainings.next')}}">
                @csrf
                <input type="hidden" value="{{$category->id}}" name="category_id">
                <input type="hidden" value="{{$training->id}}" name="training_id">
                <button style="width: 300px" type="submit" class="boxed-btn5 mt-5 mx-2">{{__('Next')}}</button>
            </form>
        </div>

    @endcan



@endsection
