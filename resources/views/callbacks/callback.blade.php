<div class="modal fade" id="callback" tabindex="-1" aria-labelledby="callbackLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-3">
                @if(isset($user))
                    <div class="modal-body">
                            <form method="post" action="{{route('callbacks.store', ['user' => $user])}}">
                                @csrf
                                <div class="mb-3">
                                    <label for="name" class="col-form-label">{{__('Name')}}:</label>
                                    <input value="{{ old('name') }}" name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="phone_number" class="col-form-label">{{__('Phone number')}}:</label>
                                    <input value=" {{ old('phone_number') }}" name="phone_number" type="text"
                                           class="form-control" id="phone_number">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="col-form-label">{{__('Description')}}:</label>
                                    <textarea name="description" class="form-control" id="description">
                                        {{ old('description') }}
                                    </textarea>
                                </div>
                                <div class="modal-footer d-flex justify-content-center">
                                    <button type="button" class="boxed-btn5 mt-5 bg-dark" data-bs-dismiss="modal">{{__('Close')}}</button>
                                    <button type="submit" class="boxed-btn5 mt-5">{{__('Save changes')}}</button>
                                </div>
                            </form>
                        </div>
                @else
                <div class="modal-body">
                            <form method="post" action="{{route('callbacks.store')}}">
                                @csrf
                                <div class="mb-3">
                                    <label for="name" class="col-form-label">{{__('Name')}}:</label>
                                    <input value="{{ old('name') }}" name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="name">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="phone_number" class="col-form-label">{{__('Phone number')}}:</label>
                                    <input value=" {{ old('phone_number') }}" name="phone_number" type="text"
                                           class="form-control @error('phone_number') is-invalid @enderror" id="phone_number">
                                    @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="col-form-label">{{__('Description')}}:</label>
                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description">{{ old('description') }}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="modal-footer d-flex justify-content-center">
                                    <button type="button" class="boxed-btn5 mt-5 bg-dark" data-bs-dismiss="modal">{{__('Close')}}</button>
                                    <button type="submit" class="boxed-btn5 mt-5">{{__('Save')}}</button>
                                </div>
                            </form>
                        </div>
                @endif
        </div>
    </div>
</div>
