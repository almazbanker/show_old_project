@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">{{__('Title')}}</th>
            <th scope="col">{{__('Author')}}</th>
            <th scope="col">{{__('Description')}}</th>
            <th scope="col">{{__('Link')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($books as $book)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td >{{$book->name}}</td>
            <td >{{$book->author}}</td>
            <td >{{$book->description}}</td>
            <td >
                <a href="{{$book->link}}">{{$book->link}}</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
