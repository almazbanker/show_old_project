@extends('layouts.app')
@section('main')

    <div class="" style="  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;background-image: url('https://opravprig.ru/assets/dist/img/index/femida.png')">
        <div class="slider_area_inner slider_bg_1 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="single_slider">
                            <div class="slider_text">
                                <h3>High Quality Law <br>
                                    Advice and Support</h3>

                                <p>Leading Polish Lawyer in your city</p>
                                <a data-bs-toggle="modal" data-bs-target="#callback" href="#" class="boxed-btn4 "><i
                                        class="bi bi-telephone mr-2"></i> Free Consulting</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="practice_area">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section_title text-center mb-60">
                            <h3>{{ __('All Categories') }}</h3>
                        </div>
                    </div>
                </div>
                <div class="container2">
                    @foreach(categories() as $category)
                        <div class="card2">
                            <span></span>
                            <div class="prBox"><img class="imgg" src="{{asset('/storage/' . $category->picture)}}"
                                                    alt="{{$category->picture}}" data-aos="fade-right"></div>
                            <div class="content2">
                                <div>
                                    <h2>
                                        <a href="{{route('categories.show', compact('category'))}}">{{$category->name}}</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="our_loyers">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="section_title text-center mb-60">
                            <h3>Our Partners</h3>
                            <p>Many variations of passages of Lorem Ipsum available, but the majority have <br> suffered
                                alteration in some.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach(partners() as $partner)
                        <div class="col-xl-4 col-md-6 col-lg-4">
                            <div class="single_loyers text-center">
                                <div class="">
                                    <img style="border-radius: 50%; width: 200px; height: 200px"
                                         src="{{asset('/storage/' . $partner->picture)}}" alt="">
                                </div>
                                <h3>{{$partner->name}}</h3>
                                <span>Family Lawyer</span>
                                <div class="social_links">
                                    <ul>
                                        <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                        <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                        <li><a href="#"> <i class="fa fa-instagram"></i> </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
