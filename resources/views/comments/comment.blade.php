<div id="comment-{{$comment->id}}">
    <div class="card p-3 mb-2">
        <div class="d-flex flex-row"><img src="https://static.kulturologia.ru/files/u18046/Florence-Colgate-01.jpg"
                                          height="40" width="40"
                                          class="rounded-circle" alt="">
            <div class="d-flex flex-column ms-2">
                <h6 class="mb-1 text-primary">{{$comment->author->name}}</h6>
                <p class="comment-text">{{$comment->content}}</p>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <div class="d-flex flex-row gap-3 align-items-center">
                <div class="d-flex align-items-center">
                    @can('delete', $comment)
                        <a class="delete-comment btn btn-outline-danger" id="comment-{{$comment->id}}"
                           data-comment-id="{{$comment->id}}" href="#"><i class="bi bi-trash"></i> Delete</a>
                    @endcan
                </div>
            </div>
            <div class="d-flex flex-row"><span
                    class="text-muted fw-normal fs-10">{{$comment->created_at->diffForHumans()}}</span></div>
        </div>
    </div>
    <div class="offset-1">
        <div class="card p-3 mb-2">
            <form id="subcomment-form-{{$comment->id}}">
                @csrf
                <input type="hidden" value="{{$comment->id}}" name="comment_id">
                <label for="content" class="form-label">Оставить ответ</label>
                <textarea name="content" class="form-control my-2" id="content" rows="1"></textarea>
                <button data-comment-id="{{$comment->id}}" type="button" class="btn btn-warning save-subcomment">
                    Сохранить
                </button>
            </form>
        </div>
        <div id="subcomment-{{$comment->id}}">
        @foreach($comment->subcomments as $subcomment)
                @include('subcomments.subcomment', ['subcomment' => $subcomment])
        @endforeach
        </div>
    </div>

</div>




