@extends('layouts.app')
@section('content')

    <div class="container-fluid p-0 mt-5">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-60">
                    <h3>Categories</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="container2">
        @foreach($categories as $category)
            <div class="card2">
                <span></span>
                <div class="prBox"><img class="imgg" src="{{asset('/storage/' . $category->picture)}}" alt="{{$category->picture}}" data-aos="fade-right"></div>
                <div class="content2">
                    <div>
                        <h2><a href="{{route('categories.show', compact('category'))}}">{{$category->name}}</a></h2>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
