@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="m-5">
            <div class="text-center">
                <img height="400px" width="85%" src="{{asset('/storage/' . $category->picture)}}" />
            </div>
            <div>
                <div class="section_title text-center mb-60 mb-2 mt-2">
                    <h3 class="text-center">{{$category->name}}</h3>
                </div>

                <div>
                    <p>{{$category->description}}</p>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="container-fluid p-0 mt-5">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-60">
                    <h1>Mediators</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        @include('callbacks.callback')

        <div class="row">
            @foreach($category->users as $user)
                @include('users.user')
            @endforeach
        </div>
    </div>


    <br>
    <div class="container-fluid p-0 mt-5">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-60">
                    <h1>Types</h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container2">
        @foreach($category->types as $type)
            <div class="card2">
                <span></span>
                <div class="prBox"><img class="imgg" src="{{asset('/storage/' . $type->picture)}}" alt="{{$type->picture}}" data-aos="fade-right"></div>
                <div class="content2">
                    <div>
                        <h2><a href="{{route('types.show', compact('type'))}}">{{$type->name}}</a></h2>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
