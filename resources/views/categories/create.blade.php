@extends('layouts.app')
@section('content')
    @include('navbar.navbar')
    <div class="my-5 w-50">
        <form method="post" action="{{route('categories.store')}}" id="">
            @csrf
            <div class="btn-group d-inline" role="group" aria-label="Basic checkbox toggle button group">
                @foreach($categories as $category)
                    <input name="{{$category->id}}" value="{{$category->id}}" type="checkbox" class="btn-check"
                           id="btncheck-{{$category->id}}" autocomplete="off">
                    <label class="btn btn-outline-warning" for="btncheck-{{$category->id}}">{{$category->name}}</label>
                @endforeach
            </div>
            <div>
                <button type="submit" id="create-category-btn" class="boxed-btn5">{{__('Save')}}</button>
            </div>
        </form>
    </div>

    <ul class="list-group">
        @foreach(Auth::user()->categories as $category)
            <li class="list-group-item d-flex justify-content-between">
                <p>{{$category->name}}</p>
                    <form method="post" action="{{route('categories.destroy', compact('category'))}}">
                        @csrf
                        @method('delete')
                        <button class="boxed-btn5 bg-dark" type="submit">{{__('Delete')}}</button>
                    </form>
            </li>
        @endforeach
    </ul>

@endsection
