<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class QuestionnaireFactory extends Factory
{

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        $gender = ['male', 'female'];
        return [
            'country_id' => rand(1, 4),
            'gender' => $gender[rand(0, 1)],
            'born_date' => $this->faker->date('Y-m-d'),
            'picture' => $this->getImage(),
        ];
    }

    /**
     * @return string
     */
    private function getImage(): string
    {
        $path = storage_path() . "/questionnaire_pictures/" . 1 . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
