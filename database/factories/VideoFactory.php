<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class VideoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'video' => 'https://www.youtube.com/watch?v=BOv-xVPyVx0&list=RDBOv-xVPyVx0&index=2',
        ];
    }

}
