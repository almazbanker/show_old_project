<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CountryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'en' => ['name' => $this->faker->unique()->country() . ' EN'],
            'ru' => ['name' => $this->faker->unique()->country() . ' RU'],
            'code' => $this->faker->randomElement(['996', '125'])
        ];
    }
}
