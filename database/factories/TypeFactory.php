<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TypeFactory extends Factory
{

    /**
     * @return array
     */
    public function definition(): array

    {
        $ru_faker = \Faker\Factory::create('ru_RU');
        return [
            'en' => [
                'name' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
                'description' => $this->faker->paragraph(
                    $nbSentences = 20, $variableNbSentences = true
                ),

            ],
            'ru' => [
                'name' => $ru_faker->name(),
                'description' => $ru_faker->paragraph(
                    $nbSentences = 20, $variableNbSentences = true
                ),

            ],
            'category_id' => rand(1, 4),
            'picture' => $this->getImage(rand(1, 6))
        ];
    }


    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number): string
    {
        $path = storage_path() . "/category_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
