<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class WorkFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => rand(1, 10),
            'start_date' => $this->faker->date('Y-m-d'),
            'finish_date' => $this->faker->date('Y-m-d'),
            'name' => $this->faker->sentence(2),
            'description' => $this->faker->paragraph(3)
        ];
    }
}
