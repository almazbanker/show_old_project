<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PartnerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $ru_faker = \Faker\Factory::create('ru_RU');

        return [
            'en' => [
                'name' => $this->faker->company(),
                'description' => $this->faker->paragraph($nbSentences = 7),
            ],

            'ru' => [
                'name' => $ru_faker->company() . 'RU',
                'description' => $ru_faker->paragraph($nbSentences = 7) . 'RU',
            ],
            'picture' => $this->getImage(rand(1, 4)),
        ];
    }


    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number = 1): string
    {
        $path = storage_path() . "/partner_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
