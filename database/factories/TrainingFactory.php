<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TrainingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        $ru_faker = \Faker\Factory::create('ru_RU');

        return [
            'en' => [
                'name' => $this->faker->word . 'EN',
                'description' => $this->faker->paragraph($nbSentences = 30),
            ],

            'ru' => [
                'name' => $this->faker->word . 'RU',
                'description' => $ru_faker->paragraph($nbSentences = 30),
            ]
        ];
    }
}
