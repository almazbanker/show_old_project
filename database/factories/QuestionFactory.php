<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'en' => [
                'question' => $this->faker->sentence(4) . ' EN',
                'variant_1' => $this->faker->word . ' EN',
                'variant_2' => $this->faker->word . ' EN',
                'variant_3' => $this->faker->word . ' EN',
                'variant_true' => $this->faker->word . ' EN'
            ],
            'ru' => [
                'question' => $this->faker->sentence(4) . ' RU',
                'variant_1' => $this->faker->word . ' RU',
                'variant_2' => $this->faker->word . ' RU',
                'variant_3' => $this->faker->word . ' RU',
                'variant_true' => $this->faker->word . ' RU'
            ],
        ];
    }
}
