<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'content' => $this->faker->paragraph(1),
            'user_id' => 1,
            'author_id' => 1
        ];
    }
}
