<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class UniversityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'start_date' => $this->faker->date('Y-m-d'),
            'finish_date' => $this->faker->date('Y-m-d'),
            'faculty' => $this->faker->sentence(3),
            'description' => $this->faker->paragraph(3),
            'user_id' => 1,
        ];
    }
}
