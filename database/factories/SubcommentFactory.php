<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SubcommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'content' => $this->faker->paragraph(2),
            'comment_id' => 1,
            'user_id' => 1
        ];
    }
}
