<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition():array
    {
        return [
            'user_id' => rand(1, 10),
            'grade' => rand(1, 5),
            'time' => $this->faker->time(),
        ];

    }
}
