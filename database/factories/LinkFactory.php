<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LinkFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'facebook' => $this->faker->email(),
            'instagram' => $this->faker->email(),
            'twitter' => $this->faker->email(),
        ];
    }
}
