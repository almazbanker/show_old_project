<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $ru_faker = \Faker\Factory::create('ru_RU');

        return [
            'en' => [
                'title' => $this->faker->words(4, 5),
                'description' => $this->faker->paragraph($nbSentences = 155),
            ],

            'ru' => [
                'title' => $ru_faker->words(3, 3),
                'description' => $ru_faker->paragraph($nbSentences = 155),
            ],
            'picture' => $this->getImage(rand(1, 5)),
        ];
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number = 1): string
    {
        $path = storage_path() . "/news/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
