<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SuggestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'suggestion' => $this->faker->paragraph(2),
            'start_date' => $this->faker->date('Y-m-d'),
            'finish_date' => $this->faker->date('Y-m-d'),
        ];
    }
}
