<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PictureFactory extends Factory
{
    public function definition(): array
    {
        return [
            'company_id' => 1,
            'picture' => $this->getImage(rand(1,3))
        ];
    }

    private function getImage($image_number = 1): string
    {
        $path = storage_path() . "/book_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }

}
