<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CompanyFactory extends Factory
{
    public function definition(): array
    {
        return [
            'ru' => ['name' => $this->faker->sentence(3) . ' RU','description' => $this->faker->paragraph(2) . ' RU'],
            'en' => ['name' => $this->faker->sentence(3) . ' EN','description' => $this->faker->paragraph(2) . ' EN'],
            'logo' => $this->getImage(),
        ];
    }

    private function getImage(): string
    {
        $path = storage_path() . "/book_pictures/" . 1 . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
