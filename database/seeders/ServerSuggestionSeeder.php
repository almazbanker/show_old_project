<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServerSuggestionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suggestions')->insert([
            'suggestion' => 'Лицо, совершившее уголовный проступок или преступление небольшой или средней тяжести, не связанное с причинением смерти',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
        DB::table('suggestions')->insert([
            'suggestion' => 'Нередко возникают разногласия между бизнес-партнерами, коллегами по работе, между начальником и подчиненным',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
        DB::table('suggestions')->insert([
            'suggestion' => 'Возникшие споры между продавцом и покупателем товара либо услуги',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
        DB::table('suggestions')->insert([
            'suggestion' => 'Не каждый трудовой конфликт можно урегулировать в судебном порядке.',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
        DB::table('suggestions')->insert([
            'suggestion' => 'Eastern Michigan University has agreed to enter into mediation with two dozen current and former students who allege in lawsuits that the college failed them after they reported sexual assaults.',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
        DB::table('suggestions')->insert([
            'suggestion' => 'There are two ways in which mediation of a dispute can be made compulsory.',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
        DB::table('suggestions')->insert([
            'suggestion' => 'The township has moved closer to resolving a long-running religious land-use lawsuit, agreeing to enter mediation on litigation involving an old egg farm on Route 9.',
            'start_date' => '12-02-2007',
            'finish_date' => '24-09-2012'
        ]);
    }
}
