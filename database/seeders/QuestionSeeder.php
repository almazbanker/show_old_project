<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            Question::factory()->count(5)->state(['level' => 2])->for($category)->create();
        }

        foreach ($categories as $category) {
            Question::factory()->count(5)->state(['level' => 1])->for($category)->create();
        }
    }
}
