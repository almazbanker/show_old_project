<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServerWorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('works')->insert([
            'name' => 'NYC Co.',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to work as mediator. You will not find better job than this!',
            'user_id' => 1,
        ]);

        DB::table('works')->insert([
            'name' => 'Chicago Company',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to work as mediator. You will not find better job than this!',
            'user_id' => 2,
        ]);

        DB::table('works')->insert([
            'name' => 'NYC Co.',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to work as mediator. You will not find better job than this!',
            'user_id' => 1,
        ]);

        DB::table('works')->insert([
            'name' => 'Harvard',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to work as mediator. You will not find better job than this!',
            'user_id' => 4,
        ]);
    }
}
