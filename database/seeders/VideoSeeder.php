<?php

namespace Database\Seeders;

use App\Models\Training;
use App\Models\Video;
use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainigs = Training::all();
        foreach ($trainigs as $trainig){
            Video::factory()->for($trainig)->create();
        }
    }
}
