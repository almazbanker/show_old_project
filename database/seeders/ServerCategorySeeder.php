<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ServerCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [
                'ru' => [
                    'name' => 'Гражданская медиация',
                    'description' => 'Не всегда рассмотрение спора в судебном порядке может быть выгодно обеим сторонам конфликтной ситуации. Это и существенные финансовые затраты, и раскрытие подробностей дела, и, порою, затяжной судебный процесс, который может растянуться на недели, а то и месяцы. К тому же, решение суда — это всегда оглашение «победившей» и «проигравшей» стороны. Чтобы найти решение, которое будет приемлемым для обеих сторон, при этом сохранить конфиденциальность и сэкономить денежные средства и время, нужно прибегнуть к процедуре медиации. Суть этого метода заключается в том, что посредник (медиатор) организует переговоры между сторонами, уточняет ключевые моменты, подробно разбирается в конкретном случае и помогает участникам конфликта найти компромисс, при этом сохранив мирные взаимоотношения. Медиативная услуга позволяет участникам конфликта самим подобрать оптимальный вариант решения спора в процессе обсуждения и разбора насущной проблемы, т.к. медиатор — третья нейтральная сторона, он не влияет на принятие решений и получение результата в чью — либо пользу.',
                ],
                'en' => [
                    'name' => 'Civil mediation',
                    'description' => 'Not always consideration of a dispute in court can be beneficial to both parties to the conflict situation. These are significant financial costs, and disclosure of the details of the case, and, at times, a protracted lawsuit that can drag on for weeks or even months. In addition, a court decision is always an announcement of the “winning” and “losing” sides. To find a solution that will be acceptable to both parties, while maintaining confidentiality and saving money and time, you need to resort to mediation. The essence of this method lies in the fact that the mediator (mediator) organizes negotiations between the parties, clarifies key points, analyzes in detail in a particular case and helps the parties to the conflict to find a compromise, while maintaining peaceful relations. The mediation service allows the parties to the conflict to choose the best option for resolving the dispute in the process of discussing and analyzing the pressing problem, because. the mediator is a third neutral party, he does not influence the decision-making and getting the result in anyone\'s favor.',
                ],
                'picture' => $this->getImage(1)
            ],
            [
                'ru' => [
                    'name' => 'Семейная медиация',
                    'description' => 'В семейной жизни супругов, родителей и их детей или других членов семьи могут возникать конфликтные ситуации в случае разногласий в бытовой, социальной, моральной или любой другой сфере. Иногда попытки решить проблему самостоятельно ни к чему не приводят, и, зачастую, ситуация просто заходит в тупик. Если оба участника спора желают решить возникший вопрос мирным путем, и сохранить или даже восстановить взаимоотношения друг с другом, то оптимальным вариантом для них является привлечение медиатора. Сертифицированный специалист по медиации (медиатор) имеет необходимые знания и навыки, чтобы детально изучить сложившиеся обстоятельства, грамотно организовать переговоры двух сторон и направить их диалог в конструктивное русло. При этом, медиатор не принимает чью-либо сторону, он нейтрально относится к участникам конфликта и помогает им вместе прийти к компромиссному решению, которое будет приемлемым для каждого из них. На практике можно столкнуться и с более сложными случаями, когда участники конфликта категорично не желают мириться друг с другом и возобновлять отношения, но при этом хотят каким-то образом решить возникшую проблему. Даже при таких условиях медиация может помочь, так как при наличии независимого посредника, конфликтующие стороны могут обсуждать свой вопрос непосредственно в его присутствии или даже не встречаясь друг с другом, с помощью дистанционного общения через медиатора, донося до него свою позицию и раскрывая необходимые сведения для урегулирования спорной ситуации. Такой подход позволяет избежать нелицеприятных высказываний, озвучивания личных претензий, негативных эмоций и реакций на слова оппонента в споре, и сосредоточиться на поиске оптимального решения. При этом, вся информация остается конфиденциальной и не разглашается посторонним лицам, что дает возможность прибегать к медиации даже в случае достаточно личных ситуаций. Преимущество применения медиативной процедуры для разрешения спорной ситуации, заключается еще и в том, что решение принимается совместно обеими сторонами и фиксируется в специальном документе — медиативном соглашении. Оно подписывается участниками спора, тем самым подтверждая их согласие и намерение исполнения выбранного решения.',
                ],
                'en' => [
                    'name' => 'Family mediation',
                    'description' => 'In the family life of spouses, parents and their children or other family members, conflict situations may arise in case of disagreements in everyday life, social, moral or any other sphere. Sometimes attempts to solve the problem on their own lead to nothing, and, often, the situation simply comes to a standstill. If both parties to the dispute wish to resolve the issue amicably, and maintain or even restore relations with each other, then the best option for them is to involve a mediator. A certified mediation specialist (mediator) has the necessary knowledge and skills to study the circumstances in detail, competently organize negotiations between the two parties and direct their dialogue in a constructive direction. At the same time, the mediator does not take sides, he is neutral towards the parties to the conflict and helps them come together to a compromise solution that will be acceptable to each of them. In practice, one may encounter more complex cases when the parties to the conflict categorically do not want to put up with each other and renew relations, but at the same time they want to somehow solve the problem that has arisen. Even under such conditions, mediation can help, since if there is an independent mediator, the conflicting parties can discuss their issue directly in his presence or even without meeting each other, using remote communication through the mediator, conveying their position to him and disclosing the necessary information for settlement of a dispute. This approach allows you to avoid impartial statements, voicing personal claims, negative emotions and reactions to the opponent\'s words in a dispute, and focus on finding the best solution. At the same time, all information remains confidential and is not disclosed to third parties, which makes it possible to resort to mediation even in the case of rather personal situations. The advantage of using a mediation procedure to resolve a dispute is also that the decision is made jointly by both parties and is recorded in a special document - a mediation agreement. It is signed by the parties to the dispute, thereby confirming their consent and intention to implement the chosen solution.',
                ],
                'picture' => $this->getImage(2)
            ],
            [
                'ru' => [
                    'name' => 'Трудовая медиация',
                    'description' => 'Не каждый трудовой конфликт можно урегулировать в судебном порядке, так как нередко требуется соблюсти конфиденциальность или решить вопрос в сжатые сроки. К тому же, не все виды таких споров рассматриваются в суде, некоторые случаи просто не урегулированы законодательно. Тут вы можете обратиться к медиатору. В его функции входит детальное ознакомление с делом, организация и контроль конструктивностипереговоров конфликтующих сторон, беспристрастное содействие в нахождении наилучшего варианта из всех приемлемых для решения конфликта. Медиативная услуга — эффективный метод решения многих спорных ситуаций в трудовых отношениях между работодателем и работником. Помимо того, что медиация может сохранить мирные отношения между участниками конфликта, она также позволяет зафиксировать выбранное решение документально с подписями обеих сторон, гарантирующими согласие с этим решением и обязанность по его исполнению.',
                ],
                'en' => [
                    'name' => 'Labor mediation',
                    'description' => 'Not always consideration of a dispute in court can be beneficial to both parties to the conflict situation. These are significant financial costs, and disclosure of the details of the case, and, at times, a protracted lawsuit that can drag on for weeks or even months. In addition, a court decision is always an announcement of the “winning” and “losing” sides. To find a solution that will be acceptable to both parties, while maintaining confidentiality and saving money and time, you need to resort to mediation. The essence of this method lies in the fact that the mediator (mediator) organizes negotiations between the parties, clarifies key points, analyzes in detail in a particular case and helps the parties to the conflict to find a compromise, while maintaining peaceful relations. The mediation service allows the parties to the conflict to choose the best option for resolving the dispute in the process of discussing and analyzing the pressing problem, because. the mediator is a third neutral party, he does not influence the decision-making and getting the result in anyone\'s favor.',
                ],
                'picture' => $this->getImage(3)
            ],
            [
                'ru' => [
                    'name' => 'Межкорпоративная медиация',
                    'description' => 'Возникшие споры между продавцом и покупателем товара либо услуги, между арендатором и арендодателем, между клиентом и поставщиком могут решаться как в личном, так и судебном порядке. В первом случае, конфликт может перерасти в серьезную ссору и дальнейшую вражду, во втором случае возможны финансовые издержки, огласка дела и длительное ожидание. Альтернативным приемом для решения конфликтной ситуации служит медиация. Эта услуга предназначена для мирного урегулирования спора, путем проведения конструктивных переговоров и поиска оптимального решения, удовлетворяющего обоих участников спора Организовывает и контролирует процесс сертифицированный специалист — медиатор, который исполняет роль посредника, сохраняющего нейтралитет между двумя конфликтующими сторонами. Проведение процедуры медиации осуществляется добровольно по обоюдному согласию обоих участников конфликта, при этом каждый из них является равноправным членом процесса. В результате выбранное решение фиксируется в специальном документе (медиативное соглашение) и выполняется в определенный срок, если такой согласован обеими сторонами и прописан в тексте.',
                ],
                'en' => [
                    'name' => 'Inter-corporate mediation',
                    'description' => 'Disputes that have arisen between the seller and the buyer of goods or services, between the lessee and the lessor, between the client and the supplier can be resolved both in person and in court. In the first case, the conflict can develop into a serious quarrel and further enmity; in the second case, financial costs, publicity of the case and a long wait are possible. Mediation is an alternative method for resolving a conflict situation. This service is intended for the peaceful settlement of the dispute, through constructive negotiations and finding the best solution that satisfies both parties to the dispute. The process is organized and controlled by a certified specialist - mediator, who acts as an intermediary that maintains neutrality between the two conflicting parties. The mediation procedure is carried out voluntarily by mutual agreement of both parties to the conflict, while each of them is an equal member of the process. As a result, the chosen solution is fixed in a special document (mediation agreement) and is executed within a certain period, if agreed by both parties and written in the text.',
                ],
                'picture' => $this->getImage(4)
            ],
            [
                'ru' => [
                    'name' => 'Внутрикорпоративная медиация',
                    'description' => 'Нередко возникают разногласия между бизнес-партнерами, коллегами по работе, между начальником и подчиненным, между другими участниками предпринимательской деятельности. Причиной конфликта может быть столкновение интересов в сфере финансовой безопасности, репутации, признания, власти или личных амбиций. Зачастую, разрешение спора производится в судебном порядке, но существует и альтернативный метод — заключение медиативного соглашения. Медиация — практика внесудебного урегулирования внутрикорпоративных споров с привлечением третей независимой стороны, основанная на применении примирительной процедуры, в ходе которой выясняются все нюансы возникшего спора, находится способ его разрешения, который бы удовлетворял все стороны, согласовываются и фиксируются детали соглашения. Негативными последствиями внутрикорпоративных споров могут стать: разрыв партнерских и человеческих связей, подрыв деловой репутации, утечка конфиденциальной информации, потеря прибыли и разрушение бизнеса. Применение медиации позволяет избежать всего этого, помогает ускорить принятие мирного решения и может помочь сохранить хорошие отношение со всеми участниками конфликта.',
                ],
                'en' => [
                    'name' => 'Intra-corporate mediation',
                    'description' => 'Disagreements often arise between business partners, work colleagues, between a boss and a subordinate, and between other participants in entrepreneurial activity. The cause of the conflict may be a clash of interests in the field of financial security, reputation, recognition, power or personal ambition. Often, the dispute is resolved in court, but there is an alternative method - the conclusion of a mediation agreement. Mediation is the practice of out-of-court settlement of intra-corporate disputes with the involvement of an independent third party, based on the use of a conciliation procedure, during which all the nuances of the dispute are clarified, a way to resolve it is found that would satisfy all parties, the details of the agreement are agreed upon and recorded. Negative consequences of intra-corporate disputes can be: severing partnerships and human ties, undermining business reputation, leaking confidential information, loss of profits and business destruction. The use of mediation avoids all this, helps speed up a peaceful solution, and can help maintain good relations with all parties to the conflict.',
                ],
                'picture' => $this->getImage(5)
            ],
            [
                'ru' => [
                    'name' => 'Уголовная медиация',
                    'description' => 'Лицо, совершившее уголовный проступок или преступление небольшой или средней тяжести, не связанное с причинением смерти, подлежит освобождению от уголовной ответственности, если оно примирилось с потерпевшим, заявителем в порядке медиации и загладило причиненный вред. Нередки, ситуации, когда мы становимся заложниками случая, к примеру, вы попали в ДТП в котором пострадал человек и тут же вы становитесь участником уголовного преступления. Либо, попали в потасовку и вот вы уже находитесь под следствием. Но большинство таких ситуаций разрешимы, если вы обратитесь к процедуре медиации. В случае же, если вы стали участником тяжкого преступления, заключение медиативного соглашения может смягчить наказание.',
                ],
                'en' => [
                    'name' => 'Criminal mediation',
                    'description' => 'A person who has committed a criminal offense or a crime of small or medium gravity, not related to causing death, is subject to release from criminal liability if he has reconciled with the victim, the applicant in the manner of mediation and made amends for the damage caused. It is not uncommon for situations when we become hostages of a case, for example, you got into an accident in which a person was injured and immediately you become a participant in a criminal offense. Or, you got into a fight and now you are already under investigation. But most of these situations can be resolved if you turn to the mediation procedure. In the event that you became a participant in a serious crime, the conclusion of a mediation agreement may mitigate the punishment.',
                ],
                'picture' => $this->getImage(6)
            ]
        ];
        foreach ($states as $state) {
            Category::factory()->state($state)->create();
        }
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number): string
    {
        $path = storage_path() . "/category_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
