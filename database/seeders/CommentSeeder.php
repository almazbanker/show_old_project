<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Link;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user){
            $another_user = $users->except($user->id)->random();
            Comment::factory()->count(rand(1,3))->for($user)->for($another_user, 'author')->create();
        }
    }
}
