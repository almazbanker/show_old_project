<?php

namespace Database\Seeders;

use App\Models\Certificate;
use App\Models\License;
use App\Models\Link;
use App\Models\Phone;
use App\Models\Questionnaire;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $users = User::factory()->count(10)->state(['approved' => true])->has(
            Certificate::factory()->count(rand(2, 4))
        )->has(
            Link::factory()
        )->has(
            License::factory()
        )->has(
            Questionnaire::factory()
        )->create();
        Phone::factory()->for($users->random())->create();
        $this->call(CategorySeeder::class);
        $this->call(UniversitySeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(WorkSeeder::class);
        $this->call(PartnerSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(TrainingSeeder::class);
        $this->call(BookSeeder::class);
        $this->call(SubcommentSeeder::class);
        $this->call(LikeSeeder::class);
        $this->call(SuggestionSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(PictureSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(NewsSeeder::class);
    }
}
