<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServerBookSeeder extends Seeder
{
    public function run()
    {
        DB::table('books')->insert([
            'name' => 'The Mediation Process: Practical Strategies for Resolving Conflict',
            'author' => 'Christopher W. Moore',
            'link' => 'https://books.google.kg/books/about/The_Mediation_Process.html?id=cRxHAAAAMAAJ&source=kp_book_description&redir_esc=y'
        ]);

        DB::table('books')->insert([
            'name' => 'Mediating Dangerously: The Frontiers of Conflict Resolution',
            'author' => 'Kenneth Cloke',
            'link' => 'https://books.google.kg/books/about/Mediating_Dangerously.html?id=t2Bh1B_CK1cC&source=kp_book_description&redir_esc=y'
        ]);

        DB::table('books')->insert([
            'name' => 'The Promise of Mediation: The Transformative Approach to Conflict',
            'author' => 'Joseph P. Folger and Robert A.',
            'link' => 'https://books.google.kg/books/about/The_Mediation_Process.html?id=cRxHAAAAMAAJ&source=kp_book_description&redir_esc=y'
        ]);

        DB::table('books')->insert([
            'name' => 'The Mediator\'s Toolkit: Formulating and Asking Questions for Successful Outcomes',
            'author' => 'Gerry O\'Sullivan',
            'link' => 'https://books.google.kg/books/about/The_Mediation_Process.html?id=cRxHAAAAMAAJ&source=kp_book_description&redir_esc=y'
        ]);

        DB::table('books')->insert([
            'name' => 'The Mediator\'s Handbook: Revised & Expanded Fourth Edition',
            'author' => 'Caroline C. Packard and Jennifer E. Beer',
            'link' => 'https://books.google.kg/books/about/The_Mediation_Process.html?id=cRxHAAAAMAAJ&source=kp_book_description&redir_esc=y'
        ]);

        DB::table('books')->insert([
            'name' => 'The Mediation Process: Practical Strategies for Resolving Conflict',
            'author' => 'Christopher W. Moore',
            'link' => 'https://books.google.kg/books/about/The_Mediation_Process.html?id=cRxHAAAAMAAJ&source=kp_book_description&redir_esc=y'
        ]);

        DB::table('books')->insert([
            'name' => 'The Mediator\'s Handbook',
            'author' => 'Eileen Stief and Jennifer E. Beer',
            'link' => 'https://books.google.kg/books/about/The_Mediation_Process.html?id=cRxHAAAAMAAJ&source=kp_book_description&redir_esc=y'
        ]);
    }
}
