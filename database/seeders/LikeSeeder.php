<?php

namespace Database\Seeders;

use App\Models\Like;
use App\Models\User;
use Illuminate\Database\Seeder;

class LikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            $another_user = $users->random();
            Like::factory()->for($user,'user')->for($another_user, 'author')->create();
        }
    }
}
