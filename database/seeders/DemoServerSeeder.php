<?php

namespace Database\Seeders;

use App\Models\Certificate;
use App\Models\License;
use App\Models\Link;
use App\Models\Phone;
use App\Models\Questionnaire;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DemoServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $users = User::factory()->count(10)->state(['approved' => true])->has(
            Certificate::factory()->count(rand(2, 4))
        )->has(
            Link::factory()
        )->has(
            License::factory()
        )->has(
            Questionnaire::factory()
        )->create();
        Phone::factory()->for($users->random())->create();
        $this->call(ServerCategorySeeder::class);
        $this->call(ServerNewsSeeder::class);
        $this->call(ServerTypeSeeder::class);
        $this->call(ServerSuggestionSeeder::class);
        $this->call(ServerCommentSeeder::class);
        $this->call(ServerSubcommentSeeder::class);
        $this->call(ServerPartnerSeeder::class);
        $this->call(ServerCompanySeeder::class);
        $this->call(ServerBookSeeder::class);
        $this->call(ServerTrainingSeeder::class);
        $this->call(ServerUniversitySeeder::class);
        $this->call(ServerWorkSeeder::class);
        $this->call(ServerCertificateSeeder::class);
        $this->call(ServerLicenseSeeder::class);
        $this->call(ServerQuestionSeeder::class);

        $this->call(LikeSeeder::class);
        $this->call(PictureSeeder::class);
        $this->call(NewsSeeder::class);
    }
}
