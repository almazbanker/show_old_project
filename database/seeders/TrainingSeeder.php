<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Training;
use Illuminate\Database\Seeder;

class TrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();

        foreach ($categories as $category) {
            Training::factory()->count(5)->for($category)->create();
        }
    }
}
