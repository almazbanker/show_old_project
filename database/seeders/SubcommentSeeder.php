<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Subcomment;
use App\Models\User;
use Illuminate\Database\Seeder;

class SubcommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $comments = Comment::all();

        foreach ($comments as $comment) {
            Subcomment::factory()->for($users->random())->for($comment)->count(rand(3, 5))->create();
        }
    }
}
