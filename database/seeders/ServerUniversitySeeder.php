<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServerUniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('universities')->insert([
            'name' => 'Harvard',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'faculty' => 'Mediators case',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 1,
        ]);

        DB::table('universities')->insert([
            'name' => 'Harvard',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'faculty' => 'Mediators case',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 2,
        ]);

        DB::table('universities')->insert([
            'name' => 'Harvard',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'faculty' => 'Mediators case',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 3,
        ]);

        DB::table('universities')->insert([
            'name' => 'Harvard',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'faculty' => 'Mediators case',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 4,
        ]);
    }
}
