<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ServerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [
                'ru' => [
                    'name' => 'Медиация по земельным вопросам',
                    'description' => 'Земельные споры – это конфликты, основанием для которых служит появление разногласий между субъектами земельных правоотношений в связи с возникновением либо прекращением прав на землю, а также в совершении конкретных действий (нарушения земельных сервитутов и правил добрососедства, неисполнение обязательств, связанных с совершением сделок с землей и т.д.). Земельные споры возникают по поводу земли в связи с отказом предоставления земельного участка, его изъятием, ограничением прав на землю, нарушением границ землепользования, самовольным занятием и в других случаях, когда нарушено субъективное право лица. Спор может возникнуть и в связи с тем, что лицо заблуждалось в отношении своего действительного права. Медиацией принято называть особую форму досудебного урегулирования спора при помощи нейтральной, беспристрастной стороны — медиатора. Так принято называть специалиста по урегулированию конфликтов, который выступает посредником и помогает спорящим сторонам выработать мирное и взаимовыгодное решение. Сегодня медиация является одним из наиболее эффективных способов досудебного урегулирования споров и привлекает своим мирным подходом.',
                ],
                'en' => [
                    'name' => 'Mediation on land issues',
                    'description' => 'Northern Community Mediation has been working with five of teacher Matt Hamilton’s students at East Jordan High School to produce videos promoting mediation. Northern Community Mediation executive director Dr. Jane Millar met with the class in November to explain mediation and the services offered by the nonprofit organization.',
                ],
                'picture' => $this->getImage(1),
                'category_id' => 1
            ],
            [
                'ru' => [
                    'name' => 'Медиация по возврату долга',
                    'description' => 'Перспективным направлением в урегулировании кредитных споров является медиация. Независимая компания или частное лицо, привлечённое для этой процедуры, способствует проведению переговоров и выработке единого решения, которое устроит все стороны. Если мы говорим о возврате задолженности, то компромиссное решение может представлять собой пересмотренный график платежей, позволяющий должнику отсрочить выплату или снизить штрафные санкции за просрочку, а кредитору – получить свои средства, не входя в расходы по ведению дел в суде. У процедуры медиации есть несколько важных достоинств. Во-первых, это возможность избежать огласки. В отличии от судебных слушаний, переговоры в рамках медиации как правило имеют приватный характер. Во-вторых, урегулирования конфликтов через мирное соглашение гораздо чаще ведёт к продолжению конструктивных взаимодействий между организациями.',
                ],
                'en' => [
                    'name' => 'Debt recovery mediation',
                    'description' => 'A mediator reported on Tuesday that members of the Sackler family that own Purdue Pharma and U.S. states opposed to the OxyContin-maker\'s bankruptcy exit plan are "even closer" to a settlement over claims that the company fueled a U.S. opioid epidemic.',
                ],
                'picture' => $this->getImage(2),
                'category_id' => 1
            ],
            [
                'ru' => [
                    'name' => 'Медиация по вопросам интеллектуальной собственности',
                    'description' => 'Споры, связанные с охраной интеллектуальной собственности согласно классификации категорий споров по критерию их правовой сложности, относятся к сложным делам. В этой связи их разрешение с помощью примирительных процедур с участием посредника видится весьма эффективным механизмом не только «разгрузки» судов, но и разрешения правовых конфликтов в наиболее короткие сроки. Процедура медиации является способом урегулирования споров при содействии медиатора на основе добровольного согласия сторон в целях достижения ими обоюдно принятого взаимоприемлемого решения. Медиаторы — это независимые физические лица, привлекаемые сторонами в качестве посредников для урегулирования спора и нахождения оптимального решения по существу вопроса. Процедура медиации проводится при взаимном волеизъявлении сторон на основе принципов добровольности, конфиденциальности, сотрудничества и равноправия сторон, беспристрастности и независимости медиатора. В результате проведения данной процедуры стороны заключают медиативное соглашение, которое является соглашением, достигнутым сторонами в результате применения процедуры медиации к спору или спорам, к отдельным разногласиям по спору и заключенное в письменной форме.',
                ],
                'en' => [
                    'name' => 'Mediation on intellectual property issues',
                    'description' => 'Major League Baseball on Thursday requested the immediate assistance of a federal mediator to help resolve the sport\'s lockout, sources told ESPN, potentially inserting the presence of a neutral party to end a work stoppage now in its third month.',
                ],
                'picture' => $this->getImage(3),
                'category_id' => 1
            ],
            [
                'ru' => [
                    'name' => 'Медиация по вопросам чести и достоинства',
                    'description' => 'Защита чести, достоинства и деловой репутации в условиях с учетом бурно развивающихся экономических отношений, а также возрастающих возможностей распространения информации приобретает особое значение. Процедура медиации может рассматриваться как действенное средство защиты указанных нематериальных благ. Защита эта базируется на принципах указанной процедуры (добровольность, конфиденциальность, сотрудничество и равноправие сторон, беспристрастие и независимость медиатора). В особенности это касается принципа конфиденциальности. Конфиденциальность заключается в том, что вся информация, которая становится известной в ходе проведения медиации, является закрытой и ограничивается кругом лиц, участвующих в переговорах. Медиатор предупреждает об этом стороны и по окончании медиации уничтожает все записи, которые он вел в ходе переговоров. Точно так же медиатор не может сообщить одной из сторон информацию, полученную от другой, передача информации сообщается только с согласия стороны. Безусловно, если в ходе переговоров появляется информация о готовящемся или совершенном преступлении, принцип конфиденциальности не будет работать, об этом перед началом процесса переговоров медиатор сообщает сторонам, а также и о том, что если он будет вызван в суд в качестве свидетеля, то сообщать суду сведения, полученные в ходе медиации, он не будет.',
                ],
                'en' => [
                    'name' => 'Mediation on issues of honor and dignity',
                    'description' => 'Eastern Michigan University has agreed to enter into mediation with two dozen current and former students who allege in lawsuits that the college failed them after they reported sexual assaults.',
                ],
                'picture' => $this->getImage(4),
                'category_id' => 2
            ],
            [
                'ru' => [
                    'name' => 'Медиация по дорожно-транспортным происшествиям',
                    'description' => 'Дорожно-транспортное происшествие всегда осложняется стрессом участников, которые не могут адекватно воспринимать ситуацию и взвешенно принимать решения. Их поведение часто эмоционально окрашено, что играет во вред обеим сторонам. Поэтому профессиональная  помощь медиатора в подобных случаях помогает не только избежать возможного наказания за нарушение правил дорожного движения (хорошо известно, что спор лучше не доводить до суда), но и существенно сэкономить время, которое иногда стоит больше, чем страховая выплата. Медиация является процедурой урегулирования спора с участием коммуникатора — посредника, который помогает найти взаимовыгодное решение конфликта. Фактически медиатор не принимает никаких решений, его профессиональные предложения и заключения носят рекомендательный характер и способствуют достижению согласия между сторонами и выработке общего решения.',
                ],
                'en' => [
                    'name' => 'Mediation in traffic accidents',
                    'description' => 'There are two ways in which mediation of a dispute can be made compulsory. The first is a matter of contract, in which the parties include in their dispute resolution clause a multi-step process which obliges them to go to mediation as a precondition to commencing litigation or arbitration. The second is legislation requiring all or certain cases to go to mediation before or at an early stage of litigation.',
                ],
                'picture' => $this->getImage(5),
                'category_id' => 2
            ],
            [
                'ru' => [
                    'name' => 'Медиация по вопросам недвижимости',
                    'description' => 'Достаточно часто возникают ситуации, когда арендаторы и арендодатели, продавцы и покупатели недвижимости, собственники и иные участники вступают в спор, разрешение которого возможно только с привлечением судебных инстанций. Однако, вступая в процесс, стороны в большинстве случаев плохо представляют себе, чем и когда он может закончиться и какие повлечет затраты. Между тем существуют услуги медиации — процедуры внесудебного разрешения спора, использование которой позволяет значительно сократить издержки и сроки урегулирования спора, избежать нежелательной огласки и достичь взаимоприемлемых условий урегулирования спора. Судебная процедура может стать довольно длительной, особенно если противная сторона применяет многочисленные процессуальные приемы для искусственного затягивания разбирательства. Зачастую стоимость арбитражного процесса также может приближаться или даже превосходить материальную ценность результата. Кроме того, даже если решение суда вынесено быстро и в вашу пользу, на этапе исполнения решение может завязнуть в исполнительных инстанциях, а сам процесс его реализации может потребовать дополнительных усилий и расходов. В то же время медиация позволяет избежать всех этих проблем и принести гораздо более весомый результат, нежели судебное решение.',
                ],
                'en' => [
                    'name' => 'Real estate mediation',
                    'description' => 'The township has moved closer to resolving a long-running religious land-use lawsuit, agreeing to enter mediation on litigation involving an old egg farm on Route 9.',
                ],
                'picture' => $this->getImage(3),
                'category_id' => 2
            ],
            [
                'ru' => [
                    'name' => 'Медиация по совместному воспитание/содержанию детей в разводе',
                    'description' => 'Специфика деятельности по разрешению споров, связанных с воспитанием детей, заключается в том, что урегулированию в данном случае подлежит не только правовой, но и психологический конфликт. В основу процедуры медиации положены принципы добровольности, конфиденциальности, сотрудничества и равноправия сторон. При рассмотрении споров об определении места жительства ребенка, порядка общения с ним отдельно проживающего родителя и исполнении судебных решений по этим категориям дел важно, чтобы у родителя было сформировано намерение разрешить семейный конфликт, не причиняя вреда ребенку. В отличие от судебного разбирательства, в результате которого один из спорящих родителей оказывается победившим, процедура медиации позволяет сделать «выигравшим» в споре именно ребенка. Как правило, в большинстве семейных споров, в том числе касающихся определения места жительства ребенка, спорящие родители вовлекают детей в конфликт, тем самым, развивая и углубляя его. Медиация помогает найти компромиссное решение для обоих родителей в вопросе совместного воспитания и содержания детей в разводе.',
                ],
                'en' => [
                    'name' => 'Mediation on the joint upbringing / maintenance of children in a divorce',
                    'description' => 'Northern Community Mediation has been working with five of teacher Matt Hamilton’s students at East Jordan High School to produce videos promoting mediation. Northern Community Mediation executive director Dr. Jane Millar met with the class in November to explain mediation and the services offered by the nonprofit organization.',
                ],
                'picture' => $this->getImage(1),
                'category_id' => 3
            ],
            [
                'ru' => [
                    'name' => 'Медиация по вопросу наследования',
                    'description' => 'Не редко возникают конфликты при формировании наследственной массы, споры между правопреемниками семейного предприятия, конфликты, касающиеся толкования договора о порядке наследования или завещания. При решении наследственного спора в судебном порядке многое упирается в финансовые вопросы. Однако зачастую наследственные споры разгораются вовсе не из-за имущества. Последнее является лишь поводом. Основная причина заключается в обидах близких людей, взаимных претензиях, недовольстве друг другом и так далее. Одни из родственников считают, что имеют больше прав на наследуемое имущество, больше сделали для умершего и так далее. В центре внимания медиации оказываются взаимоотношения между родственниками и сохранение дружественных отношений между членами семьи и недопущение дальнейшего развития конфликта сохранение. С помощью медиатора наследники могут урегулировать материальную сторону вопроса в безконфликтной обстановке, в обстановке, когда медиатор подробно выясняет у сторон их истинные интересы и цели.',
                ],
                'en' => [
                    'name' => 'Mediation on the issue of inheritance',
                    'description' => 'A mediator reported on Tuesday that members of the Sackler family that own Purdue Pharma and U.S. states opposed to the OxyContin-maker\'s bankruptcy exit plan are "even closer" to a settlement over claims that the company fueled a U.S. opioid epidemic.',
                ],
                'picture' => $this->getImage(2),
                'category_id' => 3
            ],
            [
                'ru' => [
                    'name' => 'Посредничество при разделе имущества',
                    'description' => 'Такая альтернативная процедура урегулирования спора, как медиация, именно в сфере отношений связанных с разделом имущества, является одной из самых эффективных с точки зрения возможного достижения мирового соглашения между супругами. Если супруги согласились на медиацию, то у них действительно есть реальное желание разрешить конфликтную ситуацию, не прибегая к помощи суда или административных органов. Законом уже установлен режим раздела совместно нажитого имущества супругов. Поэтому при начале обсуждения стороны уже понимают, на что могут рассчитывать. Это позволяет не только ускорить процесс, но в большинстве случаев и вообще свести обсуждение только к выбору наиболее приемлемых способов передачи имущества друг другу или вариантов выплаты компенсации при разнице в стоимости приобретаемого имущества. Практически всегда в подобном случае достижение согласия между сторонами осложняется единственным фактором – негативной ситуацией, на фоне которой происходит раздел имущества. Различные бытовые конфликты, которые привели к разводу, существенно усложняют достижение компромисса. Именно разрешение подобных случаев и есть основная задачи медиатора, он помогает сторонам откинуть все посторонние конфликты и сконцентрироваться на решении текущего вопроса, создавая тем самым благоприятный климат для урегулирования спора. Задачей же медиатора остается только определить истинные пожелания и мотивы обеих сторон конфликта, а также найти между их пожеланиями золотую середину. Таким образом, раздел совместно нажитого имущества можно провести быстро и эффективно, без привлечения судебных органов и на добровольной основе.',
                ],
                'en' => [
                    'name' => 'Mediation on the division of property',
                    'description' => 'Major League Baseball on Thursday requested the immediate assistance of a federal mediator to help resolve the sport\'s lockout, sources told ESPN, potentially inserting the presence of a neutral party to end a work stoppage now in its third month.',
                ],
                'picture' => $this->getImage(3),
                'category_id' => 3
            ],
            [
                'ru' => [
                    'name' => 'Медиация по вопросу увольнения',
                    'description' => 'Трудовой спор зачастую является не столько юридическим, сколько психологическим конфликтом. За формальными основаниями увольнения на самом деле кроется простое недопонимание руководителя и подчиненного. Обращение в суд не позволяет решить проблему до конца – даже если расторжение трудового договора признают незаконным, конфликт остается неисчерпанным. В связи с этим, когда работник восстанавливается на работе, он часто встречается с негативным отношением к себе со стороны работодателя. Его ограничивают в возможностях исполнять свои обязанности, стараются не вводить в курс дел и т.д. В итоге все, как правило, снова заканчивается увольнением. В этой ситуации поможет процедура медиации, которая, в отличие от суда, позволяет урегулировать спор с учетом интересов обеих сторон. Медиация помогает работнику и работодателю понять, что действительно стоит за словами «я не хочу больше трудиться в этой компании» и «с ним просто невозможно работать». Одно из главных достоинств медиации в том, что она позволяет сторонам расстаться, сохранив понимание и деловые отношения, а порой и продолжить сотрудничество, возможно, в какой-то иной форме. В результате на практике порой удается достичь не только приемлемого, но и выгодного для обеих сторон решения.',
                ],
                'en' => [
                    'name' => 'Mediation on dismissal',
                    'description' => 'Eastern Michigan University has agreed to enter into mediation with two dozen current and former students who allege in lawsuits that the college failed them after they reported sexual assaults.',
                ],
                'picture' => $this->getImage(4),
                'category_id' => 4
            ],
            [
                'ru' => [
                    'name' => 'Медиация по выплатам и компенсациям',
                    'description' => 'У работников и работодателей появилась новая возможность для разрешения трудовых конфликтов по выплатам и компенсациям — медиация, или урегулирование спора с участием посредника. На практике такое решение подразумевает различные варианты — компромисс, консенсус и даже взаимную выгоду. Иными словами, иногда работнику и работодателю удается договориться друг с другом таким образом, что выработанное соглашение может иметь для них гораздо больше преимуществ, чем исход возможного судебного разбирательства. Самым простым способом привести стороны за стол переговоров является установление изначальной договоренности о том, что все споры между нанимателем и работником стороны будут стараться разрешать путем переговоров с участием медиатора. Видится целесообразным, что предложение о медиации, выбор самого медиатора, а также оплата его услуг (в случае их платности) следует делать именно нанимателю. Это может быть продиктовано большими знаниями о процедуре медиации и ее возможной эффективностью, материальными возможностями, большей заинтересованностью вывести конфликт в мирное русло и прочими причинами. Однако не стоит убирать возможность участия самого сотрудника в выборе медиатора. Это может повысить лояльность работника и усилить его доверие к проводимой процедуре.',
                ],
                'en' => [
                    'name' => 'Mediation on payments and compensation',
                    'description' => 'There are two ways in which mediation of a dispute can be made compulsory. The first is a matter of contract, in which the parties include in their dispute resolution clause a multi-step process which obliges them to go to mediation as a precondition to commencing litigation or arbitration. The second is legislation requiring all or certain cases to go to mediation before or at an early stage of litigation.',
                ],
                'picture' => $this->getImage(5),
                'category_id' => 4
            ],
            [
                'ru' => [
                    'name' => 'Медиация по оплате труда',
                    'description' => 'Основанием для конфликтов в сфере трудовых отношений выступают многие причины, в том числе оплата труда, что может привести работника и нанимателя к конфликту. Когда все возможные внутрикорпоративные способы решения спора исчерпываются, стороны вынуждены обратиться за внешней помощью. Данный принцип предполагает, что при проведении медиации стороны общаются друг с другом как партнеры, на равных — несмотря на то, что в ходе исполнения трудовых обязанностей работник находится в подчинении у работодателя. Особенно это актуально, когда в процессе медиации участвует не представитель компании (например, юрист или HR-менеджер), а непосредственный руководитель сотрудника либо иное лицо, которому он подчиняется в силу должностных обязанностей.',
                ],
                'en' => [
                    'name' => 'Pay mediation',
                    'description' => 'The township has moved closer to resolving a long-running religious land-use lawsuit, agreeing to enter mediation on litigation involving an old egg farm on Route 9.',
                ],
                'picture' => $this->getImage(3),
                'category_id' => 4
            ],
            [
                'ru' => [
                    'name' => 'Посредничество в условиях труда',
                    'description' => 'Для работодателя очень важна репутация: никакой работодатель не хотел бы, чтобы спор вышел на всеобщее обозрение и стал известен широкому кругу лиц. Медитация предусматривает конфиденциальное проведение процедуры, стороны совместно с медиатором не вправе выносить за пределы медиативной площадки какие-либо вопросы спора. Процедура медиации является добровольной, в ней заинтересованы обе стороны спора, это может способствовать сохранению добрых взаимоотношений между сторонами, поскольку добровольность предполагает, что стороны готовы идти на взаимные компромиссы. Кроме того, из процедуры медиации можно выйти в любое время, и это может быть реализовано любой из сторон, если она увидит, что ее продолжение не будет конструктивным. Процедура медиации является более короткой, чем судебное разбирательство. Стороны сами могут определить срок, в который будет проводиться медиация. Кроме того, судья всегда рассматривает спор строго с точки зрения права, тогда как медиатор помогает сторонам спора взглянуть на него с новой стороны и найти приемлемое для обеих сторон решение.',
                ],
                'en' => [
                    'name' => 'Mediation on working conditions',
                    'description' => 'Eastern Michigan University has agreed to enter into mediation with two dozen current and former students who allege in lawsuits that the college failed them after they reported sexual assaults.',
                ],
                'picture' => $this->getImage(4),
                'category_id' => 5
            ],
            [
                'ru' => [
                    'name' => 'Медиация по договору аренды и др.',
                    'description' => 'Достаточно часто возникают ситуации, когда арендаторы и арендодатели вступают в спор, разрешение которого возможно только с привлечением судебных инстанций. Однако, вступая в процесс, стороны в большинстве случаев плохо представляют себе, чем и когда он может закончиться и какие повлечет затраты. Между тем существуют услуги медиации — процедуры внесудебного разрешения спора, использование которой позволяет значительно сократить издержки и сроки урегулирования спора, избежать нежелательной огласки и достичь взаимоприемлемых условий урегулирования спора. Судебная процедура может стать довольно длительной, особенно если противная сторона применяет многочисленные процессуальные приемы для искусственного затягивания разбирательства. Зачастую стоимость арбитражного процесса также может приближаться или даже превосходить материальную ценность результата. Кроме того, даже если решение суда вынесено быстро и в вашу пользу, на этапе исполнения решение может завязнуть в исполнительных инстанциях, а сам процесс его реализации может потребовать дополнительных усилий и расходов. В то же время медиация позволяет избежать всех этих проблем и принести гораздо более весомый результат, нежели судебное решение.',
                ],
                'en' => [
                    'name' => 'Mediation under a lease agreement, etc.',
                    'description' => 'Quite often, situations arise when tenants and landlords enter into a dispute, the resolution of which is possible only with the involvement of the courts. However, entering into the process, the parties in most cases have little idea of ​​how and when it can end and what costs it will entail. Meanwhile, there are mediation services - out-of-court dispute resolution procedures, the use of which can significantly reduce the costs and terms of dispute resolution, avoid unwanted publicity and achieve mutually acceptable conditions for resolving the dispute. Litigation can become quite lengthy, especially if the opposing party uses numerous procedural devices to artificially delay the proceedings. Often, the cost of the arbitration process can also approach or even exceed the material value of the result. In addition, even if the court decision is made quickly and in your favor, at the execution stage, the decision may get stuck in the enforcement authorities, and the process of its implementation may require additional efforts and expenses. At the same time, mediation allows you to avoid all these problems and bring a much more significant result than a court decision.',
                ],
                'picture' => $this->getImage(5),
                'category_id' => 5
            ],
            [
                'ru' => [
                    'name' => 'Медиация по договору оказания услуг',
                    'description' => 'Если возникли недопонимания между клиентом и юридическим или физическим лицом, которое предоставляет услуги по договору, то на помощь приходит примирительная процедура — медиация. Одно из главных преимуществ медиации – скорость разрешения спора. Медиация лишена формальных ограничений, которые существенно замедляют скорость рассмотрения дела в суде. Помимо скорости разрешения спора, медиация имеет следующие преимущества:
            Доверие. В отличие от судебных инстанций, медиатор выбирается сторонами самостоятельно, исходя из личных побуждений. Поэтому стороны могут выбрать лицо, которое гарантированно будет устраивать всех.
Минимальные затраты. Расходы на оплату представителей, государственных пошлин и иные судебные издержки выливаются в достаточно крупные расходы, которые готова нести не каждая организация. В то же время в случае с медиацией достаточно оплатить лишь услуги специалиста, других расходов просто не будет.
Глубина разрешения конфликта. Медиатор, в отличие от судьи, всегда разрешает конфликт полностью и глубоко. Он сосредотачивается на разрешении причин конфликтной ситуации, а не следствий в виде хозяйственного спора.
В результате можно сделать вывод о том, что процедура медиации позволяет не только быстрее и дешевле разрешить конфликтную ситуацию. Но и сделать это полностью, без вреда для дальнейших взаимоотношений между сторонами.',
                ],
                'en' => [
                    'name' => 'Mediation under a service agreement',
                    'description' => 'If there are misunderstandings between the client and the legal or natural person that provides services under the contract, then a conciliation procedure comes to the rescue - mediation. One of the main advantages of mediation is the speed of dispute resolution. Mediation is devoid of formal restrictions, which significantly slow down the speed of consideration of the case in court. In addition to the speed of dispute resolution, mediation has the following advantages:
            Confidence. Unlike the courts, the mediator is chosen by the parties on their own, based on personal motives. Therefore, the parties can choose a person who is guaranteed to suit everyone.
Minimum costs. The costs of paying representatives, state fees and other legal costs result in fairly large expenses that not every organization is ready to bear. At the same time, in the case of mediation, it is enough to pay only for the services of a specialist, there will simply be no other costs.
Depth of conflict resolution. The mediator, unlike the judge, always resolves the conflict completely and deeply. He focuses on resolving the causes of the conflict situation, and not the consequences in the form of an economic dispute.
As a result, we can conclude that the mediation procedure allows not only to resolve the conflict situation faster and cheaper. But also to do it completely, without harm to further relations between the parties.',
                ],
                'picture' => $this->getImage(3),
                'category_id' => 5
            ],
            [
                'ru' => [
                    'name' => 'Медиация по договору поставки',
                    'description' => 'Договор поставки имеет огромное значение для контрагентов, так как предполагает длительные отношения между предпринимателями. Зачастую именно вокруг поставки определенных товаров строится вся хозяйственная деятельность одной из сторон, поэтому особую важность приобретает быстрое и безболезненное разрешение разногласий. Одно из главных преимуществ медиации – скорость разрешения спора. Медиация лишена формальных ограничений, которые существенно замедляют скорость рассмотрения дела в суде. Помимо скорости разрешения спора, медиация имеет следующие преимущества:
Доверие. В отличие от судебных инстанций, медиатор выбирается сторонами самостоятельно, исходя из личных побуждений. Поэтому стороны могут выбрать лицо, которое гарантированно будет устраивать всех.
Минимальные затраты. Расходы на оплату представителей, государственных пошлин и иные судебные издержки выливаются в достаточно крупные расходы, которые готова нести не каждая организация. В то же время в случае с медиацией достаточно оплатить лишь услуги специалиста, других расходов просто не будет.
Глубина разрешения конфликта. Медиатор, в отличие от судьи, всегда разрешает конфликт полностью и глубоко. Он сосредотачивается на разрешении причин конфликтной ситуации, а не следствий в виде хозяйственного спора.
В результате можно сделать вывод о том, что процедура медиации позволяет не только быстрее и дешевле разрешить конфликтную ситуацию. Но и сделать это полностью, без вреда для дальнейших взаимоотношений между сторонами.'],
                'en' => [
                    'name' => 'Mediation under a supply agreement',
                    'description' => 'The supply agreement is of great importance for contractors, as it involves long-term relationships between entrepreneurs. Often, it is around the supply of certain goods that all the economic activities of one of the parties are built, therefore, a quick and painless resolution of disagreements is of particular importance. One of the main advantages of mediation is the speed of dispute resolution. Mediation is devoid of formal restrictions, which significantly slow down the speed of consideration of the case in court. In addition to the speed of dispute resolution, mediation has the following advantages:
Confidence. Unlike the courts, the mediator is chosen by the parties on their own, based on personal motives. Therefore, the parties can choose a person who is guaranteed to suit everyone.
    Minimum costs. The costs of paying representatives, state fees and other legal costs result in fairly large expenses that not every organization is ready to bear. At the same time, in the case of mediation, it is enough to pay only for the services of a specialist, there will simply be no other costs.
    Depth of conflict resolution. The mediator, unlike the judge, always resolves the conflict completely and deeply. He focuses on resolving the causes of the conflict situation, and not the consequences in the form of an economic dispute.
As a result, we can conclude that the mediation procedure allows not only to resolve the conflict situation faster and cheaper. But also to do it completely, without harm to further relations between the parties.',
                ],
                'picture' => $this->getImage(4),
                'category_id' => 6
            ],
            [
                'ru' => [
                    'name' => 'Медиация между собственниками бизнеса',
                    'description' => 'Причины конфликта между собственниками бизнеса могут быть совершенно разными:
Противоречивые мнения по одинаковым вопросам.
Различные цели или средства их достижений.
Несовпадение личных интересов и желаний.
Различный уровень профессионального и личного развития.
Неформализованное урегулирование спора обладает одним очень важным преимуществом, а именно возможностью сторон принимать самостоятельные решения. То, что сами стороны, а не кто-то другой, могут определить свою судьбу, является очень ценным качеством при выборе метода урегулирования спора. Характеристики'],
                'en' => [
                    'name' => 'Mediation between business owners',
                    'description' => 'The reasons for the conflict between business owners can be completely different:
Conflicting opinions on the same issues.
Various goals or means of achieving them.
Mismatch of personal interests and desires.
Various levels of professional and personal development.
Informalized dispute resolution has one very important advantage, namely the ability of the parties to make independent decisions. The fact that the parties themselves, and not someone else, can determine their fate is a very valuable quality when choosing a dispute resolution method. Characteristics',
                ],
                'picture' => $this->getImage(5),
                'category_id' => 6
            ],
            [
                'ru' => [
                    'name' => 'Медиация между сотрудниками',
                    'description' => 'Возникновение споров в сфере корпоративных отношений — это вполне нормальное и частое явление, которое сопровождает многие бизнес-процессы. При этом, вплоть до последнего времени, в нашем обществе преобладали два основных способа урегулирования таких споров. Первый — это судебное, либо третейское разбирательство, где решение принимает судья или третейский судья, а его исполнение обеспечивается силой государственного принуждения. Второй вариант — неформальное урегулирование, как с привлечением третьих лиц, так и без них, только лишь непосредственными участниками спора. Неформализованное урегулирование спора обладает одним очень важным преимуществом, а именно возможностью сторон принимать самостоятельные решения. То, что сами стороны, а не кто-то другой, могут определить свою судьбу, является очень ценным качеством при выборе метода урегулирования спора. Медиация традиционно относится к альтернативным методам разрешения споров. Это метод, позволяющий сторонам при содействии третьего, нейтрального, беспристрастного лица — медиатора, на добровольной основе выработать взаимовыгодное, жизнеспособное решение, отвечающее их интересам. Медиация отличается от традиционного разрешения споров в суде или арбитраже прежде всего тем, что медиатор не принимает решения по спору, оставляя стороны собственниками процесса урегулирования. Лишь стороны вправе принимать то или иное решение, которое будет обязательным для них. При этом они сами, при содействии медиатора рассматривают возможные варианты и определяют наиболее подходящие из них.',
                ],
                'en' => [
                    'name' => 'Mediation between employees',
                    'description' => 'The township has moved closer to resolving a long-running religious land-use lawsuit, agreeing to enter mediation on litigation involving an old egg farm on Route 9.',
                ],
                'picture' => $this->getImage(3),
                'category_id' => 6
            ],
        ];
        foreach ($states as $state) {
            Type::factory()->state($state)->create();
        }
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number): string
    {
        $path = storage_path() . "/category_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
