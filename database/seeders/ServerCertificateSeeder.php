<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ServerCertificateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('certificates')->insert([
            'name' => 'NYC Co.',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 1,
            'picture' => $this->getImage(rand(1, 5))
        ]);

        DB::table('certificates')->insert([
            'name' => 'Chicago Company',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 2,
            'picture' => $this->getImage(rand(1, 5))
        ]);

        DB::table('certificates')->insert([
            'name' => 'NYC Co.',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 1,
            'picture' => $this->getImage(rand(1, 5))
        ]);

        DB::table('certificates')->insert([
            'name' => 'Harvard',
            'start_date' => '06-09-2008',
            'finish_date' => '06-09-2012',
            'description' => 'Best place to studying as mediator. You will not find better university than this!',
            'user_id' => 4,
            'picture' => $this->getImage(rand(1, 5))
        ]);
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number = 1): string
    {
        $path = storage_path() . "/certificate_pictures/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
