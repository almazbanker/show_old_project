<?php

namespace Database\Seeders;

use App\Models\News;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ServerNewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [
                'ru' => [
                    'title' => 'Учащиеся средней школы Восточной Иордании снимают видеоролики для посредничества Северного сообщества',
                    'description' => 'Посредничество Северного сообщества работало с пятью учениками учителя Мэтта Гамильтона в средней школе Ист-Джордан для создания видеороликов, продвигающих посредничество. Исполнительный директор посредничества Северного сообщества д-р Джейн Миллар встретилась с классом в ноябре, чтобы объяснить посредничество и услуги, предлагаемые некоммерческой организацией.',
                ],
                'en' => [
                    'title' => 'East Jordan High School students produce videos for Northern Community Mediation',
                    'description' => 'Northern Community Mediation has been working with five of teacher Matt Hamilton’s students at East Jordan High School to produce videos promoting mediation. Northern Community Mediation executive director Dr. Jane Millar met with the class in November to explain mediation and the services offered by the nonprofit organization.',
                ],
                'picture' => $this->getImage(1)
            ],
            [
                'ru' => [
                    'title' => 'Медиатор по банкротству Purdue Pharma говорит, что Саклерс и штаты США ближе к сделке по искам об опиоидах',
                    'description' => 'Посредник сообщил во вторник, что члены семьи Саклер, владеющие Purdue Pharma, и штаты США, выступающие против плана выхода из банкротства производителя OxyContin, «еще ближе» к урегулированию споров по обвинениям в том, что компания разжигала опиоидную эпидемию в США.',
                ],
                'en' => [
                    'title' => 'Purdue Pharma bankruptcy mediator says Sacklers, US states closer to deal over opioid claims',
                    'description' => 'A mediator reported on Tuesday that members of the Sackler family that own Purdue Pharma and U.S. states opposed to the OxyContin-maker\'s bankruptcy exit plan are "even closer" to a settlement over claims that the company fueled a U.S. opioid epidemic.',
                ],
                'picture' => $this->getImage(2)
            ],
            [
                'ru' => [
                    'title' => 'Источники говорят, что Высшая лига бейсбола надеется на федерального посредника, чтобы помочь положить конец локауту в спорте',
                    'description' => 'Высшая лига бейсбола в четверг запросила немедленную помощь федерального посредника, чтобы помочь разрешить локаут в спорте, сообщили источники ESPN, что потенциально может добавить присутствие нейтральной стороны, чтобы положить конец остановке работы уже третий месяц. ',
                ],
                'en' => [
                    'title' => 'Major League Baseball looks to federal mediator to help end sport\'s lockout, sources say',
                    'description' => 'Major League Baseball on Thursday requested the immediate assistance of a federal mediator to help resolve the sport\'s lockout, sources told ESPN, potentially inserting the presence of a neutral party to end a work stoppage now in its third month.',
                ],
                'picture' => $this->getImage(3)
            ],
            [
                'ru' => [
                    'title' => 'Восточный Мичиган, женщины и мужчины, заявляющие о сокрытии сексуального насилия, вступают в посредничество',
                    'description' => 'Университет Восточного Мичигана согласился вступить в посредничество с двумя десятками нынешних и бывших студентов, которые в судебных процессах утверждают, что колледж подвел их после того, как они сообщили о сексуальных домогательствах.',
                ],
                'en' => [
                    'title' => 'Eastern Michigan, women and men alleging coverup of sexual assault to enter mediation',
                    'description' => 'Eastern Michigan University has agreed to enter into mediation with two dozen current and former students who allege in lawsuits that the college failed them after they reported sexual assaults.',
                ],
                'picture' => $this->getImage(4)
            ],
            [
                'ru' => [
                    'title' => 'Принудительное посредничество: есть ли плюсы?',
                    'description' => 'Существует два способа сделать посредничество в споре обязательным. Во-первых, это договор, в котором стороны включают в свою оговорку об урегулировании споров многоэтапный процесс, который обязывает их прибегнуть к посредничеству в качестве предварительного условия для начала судебного или арбитражного разбирательства. Во-вторых, это законодательство, требующее, чтобы все или определенные дела передавались на рассмотрение до или на ранней стадии судебного разбирательства.',
                ],
                'en' => [
                    'title' => 'Compulsory Mediation: Is There an Upside?',
                    'description' => 'There are two ways in which mediation of a dispute can be made compulsory. The first is a matter of contract, in which the parties include in their dispute resolution clause a multi-step process which obliges them to go to mediation as a precondition to commencing litigation or arbitration. The second is legislation requiring all or certain cases to go to mediation before or at an early stage of litigation.',
                ],
                'picture' => $this->getImage(5)
            ],
            [
                'ru' => [
                    'title' => 'Посредничество Toms River OAK в судебном процессе о покупке яичной фермы было направлено на то, чтобы не допустить ортодоксов',
                    'description' => 'Городок приблизился к разрешению давнего религиозного судебного процесса о землепользовании, согласившись вступить в посредничество в судебном процессе, касающемся старой яичной фермы на шоссе 9.',
                ],
                'en' => [
                    'title' => 'Toms River OKs mediation on lawsuit claiming egg farm purchase was to keep Orthodox out',
                    'description' => 'The township has moved closer to resolving a long-running religious land-use lawsuit, agreeing to enter mediation on litigation involving an old egg farm on Route 9.',
                ],
                'picture' => $this->getImage(3)
            ]
        ];
        foreach ($states as $state) {
            News::factory()->state($state)->create();
        }
    }

    /**
     * @param int $image_number
     * @return string
     */
    private function getImage(int $image_number = 1): string
    {
        $path = storage_path() . "/news/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }

}
