<?php

namespace Database\Seeders;

use App\Models\University;
use App\Models\User;
use Illuminate\Database\Seeder;

class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $users = User::all();

            foreach ($users as $user)
            {
                University::factory()->for($user)->count(rand(2, 5))->create();
            }
    }
}
