<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServerSubcommentseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subcomments')->insert([
            'content' => 'Согласен!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Лучший медиатор!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Сделал работу на отлично!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Мой любимый медиатор!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Я тоже!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Выше всвяких похвал!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Superb!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Absolutely the best!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Great job!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);
        DB::table('subcomments')->insert([
            'content' => 'Согласен!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Лучший медиатор!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Сделал работу на отлично!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Мой любимый медиатор!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Я тоже!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Выше всвяких похвал!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Superb!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Absolutely the best!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);

        DB::table('subcomments')->insert([
            'content' => 'Great job!',
            'user_id' => rand(1, 5),
            'comment_id' => rand(1, 5),
            'created_at' => now()->format('Y-m-d')
        ]);
    }
}
