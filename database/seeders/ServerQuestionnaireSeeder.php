<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ServerQuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = ['male', 'female'];
        DB::table('questionnaires')->insert([
            'user_id' => 1,
            'country_id' => rand(1, 4),
            'gender' => $gender[rand(0, 1)],
            'born_date' => '01.01.1999',
            'picture' => $this->getImage(),
        ]);

        DB::table('questionnaires')->insert([
            'user_id' => 2,
            'country_id' => rand(1, 4),
            'gender' => $gender[rand(0, 1)],
            'born_date' => '01.01.1999',
            'picture' => $this->getImage(),
        ]);

        DB::table('questionnaires')->insert([
            'user_id' => 3,
            'country_id' => rand(1, 4),
            'gender' => $gender[rand(0, 1)],
            'born_date' => '01.01.1999',
            'picture' => $this->getImage(),
        ]);

        DB::table('questionnaires')->insert([
            'user_id' => 4,
            'country_id' => rand(1, 4),
            'gender' => $gender[rand(0, 1)],
            'born_date' => '01.01.1999',
            'picture' => $this->getImage(),
        ]);

        DB::table('questionnaires')->insert([
            'user_id' => 5,
            'country_id' => rand(1, 4),
            'gender' => $gender[rand(0, 1)],
            'born_date' => '01.01.1999',
            'picture' => $this->getImage(),
        ]);
    }

    /**
     * @return string
     */
    private function getImage(): string
    {
        $path = storage_path() . "/questionnaire_pictures/" . 1 . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return '/storage/pictures/' . $image_name;
    }
}
