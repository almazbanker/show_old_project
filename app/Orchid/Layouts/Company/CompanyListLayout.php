<?php

namespace App\Orchid\Layouts\Company;

use App\Models\Company;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CompanyListLayout extends Table
{
    /**
     * @var string
     */
    protected $target = 'companies';

    /**
     * @return array
     */
    protected function columns(): array
    {
        return [
            TD::make('', 'Title RU')->render(function (Company $company) {return Link::make($company->translate('ru')->name)->route('platform.company', $company);}),
            TD::make('', 'Title EN')->render(function (Company $company) {return Link::make($company->translate('en')->name)->route('platform.company', $company);}),
            TD::make('created_at', 'Created'),
            TD::make('updated_at', 'Last updated')
        ];
    }
}
