<?php

namespace App\Orchid\Layouts\Phone;

use App\Models\Phone;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PhoneListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'phones';


    protected function columns(): array
    {
        return [
            TD::make('number', 'Number'),
            TD::make('Users')
                ->render(function (Phone $phone) {
                    return $phone->user->name;
                }),
            TD::make('created_at', 'Created at'),
            TD::make('updated_at', 'Updated'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Phone $phone) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.phones.edit', $phone->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $phone->id,
                                ]),
                        ]);
                }),
        ];
    }
}
