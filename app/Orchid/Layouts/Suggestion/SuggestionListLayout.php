<?php

namespace App\Orchid\Layouts\Suggestion;

use App\Models\Suggestion;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class SuggestionListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'Suggestions';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('suggestion', 'suggestion')->render(function (Suggestion $suggestion) {
                return Link::make($suggestion->suggestion)
                    ->route('platform.suggestions.edit', $suggestion);
            }),
            TD::make('start_date', 'Start date')
                ->render(function (Suggestion $suggestion) {
                    return $suggestion->start_date;
                }),

            TD::make('finish_date', 'Finish date')
                ->render(function (Suggestion $suggestion) {
                    return $suggestion->finish_date;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Suggestion $suggestion) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.suggestions.edit', $suggestion->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $suggestion->id,
                                ]),
                        ]);
                }),
        ];
    }
}
