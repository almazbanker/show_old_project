<?php

namespace App\Orchid\Layouts\Work;

use App\Models\Work;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class WorkListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'works';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Users')
                ->render(function (Work $work) {
                    return $work->user->name;
                }),
            TD::make('name', 'Title')
            ->render(function (Work $work){
                return Link::make($work->name)->route('platform.works.edit', $work);
            }),
            TD::make('start_date', 'Start date'),
            TD::make('finish_date', 'Finish date'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Work $work) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Edit'))
                                ->route('platform.works.edit', $work->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $work->id,
                                ]),
                        ]);
                }),
        ];
    }
}
