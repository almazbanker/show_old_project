<?php

namespace App\Orchid\Layouts\Comment;

use App\Models\Comment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CommentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'Comments';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Content')
                ->render(function (Comment $comment) {
                    return $comment->content;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Comment $comment) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Edit'))
                                ->route('platform.comments.edit', $comment->id)
                                ->icon('pencil'),
                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $comment->id,
                                ]),
                        ]);
                }),
        ];
    }
}

