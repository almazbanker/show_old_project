<?php

namespace App\Orchid\Layouts\Type;

use App\Models\Category;
use App\Models\Type;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class TypeListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'types';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Picture')
                ->render(function (Type $type) {
                    $picture=asset($type->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('name', 'Name RU')
                ->render(function (Type $type) {
                    return $type->translate('ru')->name;
                }),
            TD::make('name', 'Name EN')
                ->render(function (Type $type) {
                    return $type->translate('en')->name;
                }),
            TD::make('Category Ru')
                ->render(function (Type $type) {
                    return $type->category->translate('ru')->name;
                }),
            TD::make('Category En')
                ->render(function (Type $type) {
                    return $type->category->translate('en')->name;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Type $type) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Edit'))
                                ->route('platform.types.edit', $type->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $type->id,
                                ]),
                        ]);
                }),
        ];
    }
}


