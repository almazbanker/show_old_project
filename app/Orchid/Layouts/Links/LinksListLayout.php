<?php

namespace App\Orchid\Layouts\Links;

use App\Models\Link;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class LinksListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'links';


    protected function columns(): array
    {
        return [
            TD::make('twitter', 'twitter'),
            TD::make('instagram', 'instagram'),
            TD::make('facebook', 'facebook'),
            TD::make('Users')
                ->render(function (Link $link) {
                    return $link->user->name;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Link $link) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Edit'))
                                ->route('platform.links.edit', $link->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $link->id,
                                ]),
                        ]);
                }),
        ];
    }

}
