<?php

namespace App\Orchid\Layouts\Country;

use App\Models\Country;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CountryListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'countries';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('name', 'Title RU')->render(function (Country $country) {return Link::make($country->translate('ru')->name)->route('platform.countries.list');}),
            TD::make('name', 'Title EN')->render(function (Country $country) {return Link::make($country->translate('en')->name)->route('platform.countries.list');}),
            TD::make('code', 'Code'),
            TD::make('created_at', 'Created'),
            TD::make('updated_at', 'Last updated'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Country $country) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Edit'))
                                ->route('platform.countries.edit', $country->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $country->id,
                                ]),
                        ]);
                }),
        ];
    }
}





