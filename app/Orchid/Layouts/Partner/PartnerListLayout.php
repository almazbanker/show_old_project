<?php

namespace App\Orchid\Layouts\Partner;

use App\Models\Partner;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PartnerListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'partners';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Picture')
                ->render(function (Partner $partner) {
                    $picture=asset($partner->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('name', 'Name RU')
            ->render(function (Partner $partner) {
                return $partner->translate('ru')->name;
            }),
            TD::make('name', 'Name EN')
                ->render(function (Partner $partner) {
                    return $partner->translate('en')->name;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Partner $partner) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.partners.edit', $partner->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $partner->id,
                                ]),
                        ]);
                }),
        ];
    }
}
