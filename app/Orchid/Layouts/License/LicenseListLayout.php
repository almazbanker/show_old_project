<?php

namespace App\Orchid\Layouts\License;

use App\Models\License;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class LicenseListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'licenses';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Picture')
                ->render(function (License $license) {
                    $picture=asset($license->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('Users')
                ->render(function (License $license) {
                    return $license->user->name;
                }),
            TD::make('number', 'Number'),
            TD::make('start_date', 'Start date'),
            TD::make('finish_date', 'Finish date'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (License $license) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.licenses.edit', $license->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $license->id,
                                ]),
                        ]);
                }),
        ];
    }
}
