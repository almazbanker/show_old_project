<?php

namespace App\Orchid\Layouts\Test;

use App\Models\Test;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use function __;


class TestListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'tests';




    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Users')
                ->render(function (Test $test) {
                    return $test->user->name;
                }),
            TD::make('start_time', 'Start time'),
            TD::make('finish_time', 'Finish time'),
            TD::make('grade', 'Grade'),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Test $test) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Show'))
                                ->route('platform.tests.show', $test->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $test->id,
                                ]),
                        ]);
                }),
        ];
    }



}
