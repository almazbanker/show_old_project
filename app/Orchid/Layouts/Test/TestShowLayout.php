<?php

namespace App\Orchid\Layouts\Test;

use App\Models\Answer;
use App\Models\Test;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class TestShowLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'answers';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Question')
                ->render(function (Answer $answer) {
                    return $answer->question->question;
                }),
            TD::make('True variant')
                ->render(function (Answer $answer) {
                    return $answer->question->variant_true;
                }),
            TD::make('answer', 'Answer'),
            TD::make('Correct')
                ->render(function (Answer $answer) {
                    if($answer->correct) {
                        return "Yes";
                    } else {
                        return "No";
                    }
                }),
        ];
    }
}
