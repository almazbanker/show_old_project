<?php

namespace App\Orchid\Layouts\Certificate;

use App\Models\Certificate;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CertificatesLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'certificates';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Picture')
                ->render(function (Certificate $certificate) {
                    $picture=asset($certificate->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('', 'Title')->render(function (Certificate $certificate) {return Link::make($certificate->name)->route('platform.certificate', $certificate);}),
            TD::make('start_date', 'Start date'),
            TD::make('finish_date', 'Finish date'),
        ];
    }
}
