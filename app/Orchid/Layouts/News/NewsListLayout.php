<?php

namespace App\Orchid\Layouts\News;

use App\Models\News;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class NewsListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'newses';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('Picture')
                ->render(function (News $news) {
                    $picture=asset($news->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('title', 'Title RU')
                ->render(function (News $news) {
                    return $news->translate('ru')->title;
                }),
            TD::make('title', 'Title EN')
                ->render(function (News $news) {
                    return $news->translate('en')->title;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (News $news) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.newses.edit', $news->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $news->id,
                                ]),
                        ]);
                }),
        ];
    }
}
