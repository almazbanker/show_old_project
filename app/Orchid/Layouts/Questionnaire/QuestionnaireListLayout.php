<?php

namespace App\Orchid\Layouts\Questionnaire;

use App\Models\Questionnaire;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class QuestionnaireListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'questionnaires';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return[
            TD::make('Users')
                ->render(function (Questionnaire $questionnaire) {
                    return Link::make($questionnaire->user->name)
                        ->route('platform.questionnaires.edit', $questionnaire);
                }),
            TD::make('Country')
                ->render(function (Questionnaire $questionnaire) {
                    return $questionnaire->country->name;
                }),

            TD::make('gender', 'Gender')
                ->render(function (Questionnaire $questionnaire) {
                    return $questionnaire->gender;
                }),
            TD::make('born_date', 'Born date')
                ->render(function (Questionnaire $questionnaire) {
                    return $questionnaire->born_date;
                }),
            TD::make('Picture')
                ->render(function (Questionnaire $license) {
                    $picture=asset($license->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('updated_at', 'Last updated'),
        ];
    }
}

