<?php

namespace App\Orchid\Layouts\Question;

use App\Models\Question;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class QuestionListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'questions';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {


        return [
            TD::make('question', 'Question')->render(function (Question $question) {
                return Link::make($question->question)
                    ->route('platform.questions.edit', $question);
            }),
            TD::make('Category Ru')
                ->render(function (Question $question) {
                    return $question->category->translate('ru')->name;
                }),
            TD::make('Category En')
                ->render(function (Question $question) {
                    return $question->category->translate('en')->name;
                }),
            TD::make('level', 'Level'),
        ];
    }
}
