<?php

namespace App\Orchid\Layouts\Training;

use App\Models\Partner;
use App\Models\Training;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class TrainingListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'trainings';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('name', 'Name RU')
                ->render(function (Training $training) {
                    return $training->translate('ru')->name;
                }),
            TD::make('name', 'Name EN')
                ->render(function (Training $training) {
                    return $training->translate('en')->name;
                }),
            TD::make('category', 'Category')
                ->render(function (Training $training) {
                    return $training->category->name;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (Training $training) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            \Orchid\Screen\Actions\Link::make(__('Edit'))
                                ->route('platform.trainings.edit', $training->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $training->id,
                                ]),
                        ]);
                }),
        ];
    }
}
