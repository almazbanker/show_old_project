<?php

namespace App\Orchid\Layouts\University;

use App\Models\University;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class UniversityListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'Universities';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('name', 'name')->render(function (University $university) {
                return Link::make($university->name)
                    ->route('platform.universities.edit', $university);
            }),
            TD::make('start_date', 'Start date')
                ->render(function (University $university) {
                    return $university->start_date;
                }),

            TD::make('finish_date', 'Finish date')
                ->render(function (University $university) {
                    return $university->finish_date;
                }),
            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (University $university) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.universities.edit', $university->id)
                                ->icon('pencil'),

                            Button::make(__('Delete'))
                                ->icon('trash')
                                ->method('remove', [
                                    'id' => $university->id,
                                ]),
                        ]);
                }),
        ];
    }
}

