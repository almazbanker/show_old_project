<?php

namespace App\Orchid\Screens\Training;

use App\Models\Training;
use App\Orchid\Layouts\Training\TrainingListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class TrainingListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TrainingListScreen';

    public $description = 'TrainingListScreen';

    public $exists = false;
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
            return [
                'trainings' => Training::orderBy('created_at')->paginate()
            ];

    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')
                ->route('platform.trainings.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            TrainingListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Training::findOrFail($request->get('id'))->delete();
        Toast::info(__('trainings was removed'));
    }
}
