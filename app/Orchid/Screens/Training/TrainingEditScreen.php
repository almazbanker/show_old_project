<?php

namespace App\Orchid\Screens\Training;

use App\Http\Requests\TrainingOrchidRequest;
use App\Models\Category;
use App\Models\Training;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class TrainingEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TrainingEditScreen';

    public $description = 'TrainingEditScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Training $training
     * @return array
     */
    public function query(Training $training): array
    {
        $this->exists = $training->exists;

        if ($this->exists) {
            $this->name = 'Edit partner';
            return [
                'nameRu' => $training->translate('ru')->name,
                'nameEn' => $training->translate('en')->name,
                'descriptionRu' => $training->translate('ru')->description,
                'descriptionEn' => $training->translate('en')->description,
                'category_id' => $training->category->id
            ];
        }

        return [
            'training' => $training
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create license')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('nameRu')
                    ->title('Title RU'),
                Input::make('nameEn')
                    ->title('Title EN'),
                TextArea::make('descriptionRu')
                    ->title('Description RU')
                    ->rows(10),
                TextArea::make('descriptionEn')
                    ->title('Description EN')
                    ->rows(10),
                Quill::make('descriptionEn')
                    ->toolbar(["text", "color", "header", "list"]),
                Quill::make('descriptionRu')
                    ->toolbar(["text", "color", "header", "list"]),
                Relation::make('category_id')
                    ->title('Category')
                    ->fromModel(Category::class, 'id', 'id')
                    ->required(),
            ])
        ];
    }

    public function createOrUpdate(Training $training, TrainingOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' => [
                'name' => $request->input('nameEn'),
                'description' => $request->input('descriptionEn')],
            'ru' => [
                'name' => $request->input('nameRu'),
                'description' => $request->input('descriptionRu')],
            'category_id' => $request->get('category_id')
        ];
        $training->fill($data)->save();

        Alert::info('You have successfully created a training.');

        return redirect()->route('platform.trainings.list');
    }
}
