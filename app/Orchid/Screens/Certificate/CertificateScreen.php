<?php

namespace App\Orchid\Screens\Certificate;

use App\Http\Requests\CertificateOrchidRequest;
use App\Models\Certificate;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class CertificateScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Add certificate';
    public $exists = false;

    /**
     * @param Certificate $certificate
     * @return Certificate[]
     */
    public function query(Certificate $certificate): array
    {
        $this->exists = $certificate->exists;
        if ($this->exists) {
            $this->name = 'Certificate edit';
        }
        return ['certificate' => $certificate];
    }

    public function commandBar(): array
    {
        return [
            Button::make('Add')->icon('plus')->method('store')->canSee(!$this->exists),
            Button::make('Update')->icon('plus')->method('update')->canSee($this->exists),
            Button::make('Delete')->icon('trash')->method('destroy')->canSee($this->exists)
        ];
    }

    public function layout(): array
    {
        return [Layout::rows([
            Input::make('certificate.name')->title(__('Title')),
            DateTimer::make('certificate.start_date')->title(__('Start date')),
            DateTimer::make('certificate.finish_date')->title(__('Finish date')),
            Quill::make('certificate.description')->title(__('Description')),
            Picture::make('certificate.picture')->targetRelativeUrl()->title(__('Picture'))->required(),
            Select::make('certificate.user_id')->title(__('Users'))->fromModel(User::class, 'name')
        ])];
    }

    /**
     * @param CertificateOrchidRequest $request
     * @return RedirectResponse
     */
    public function store(CertificateOrchidRequest $request): RedirectResponse
    {
        $certificate = new Certificate($request->get('certificate'));
        $certificate->save();
        return redirect()->route('platform.certificates');
    }

    /**
     * @param Certificate $certificate
     * @param CertificateOrchidRequest $request
     * @return RedirectResponse
     */
    public function update(Certificate $certificate, CertificateOrchidRequest $request): RedirectResponse
    {
        $certificate->update($request->get('certificate'));
        return redirect()->route('platform.certificates');
    }

    /**
     * @param Certificate $certificate
     * @return RedirectResponse
     */
    public function destroy(Certificate $certificate): RedirectResponse
    {
        $certificate->delete();
        return redirect()->route('platform.certificates');
    }
}
