<?php

namespace App\Orchid\Screens\Certificate;

use App\Models\Certificate;
use App\Orchid\Layouts\Certificate\CertificatesLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CertificateListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Certificates';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return ['certificates' => Certificate::all()];
    }

    public function commandBar(): array
    {
        return [
            Link::make('Add certificate')->icon('plus')->route('platform.certificate')
        ];
    }

    public function layout(): array
    {
        return [CertificatesLayout::class];
    }
}
