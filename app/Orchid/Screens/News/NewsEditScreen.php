<?php

namespace App\Orchid\Screens\News;

use App\Http\Requests\NewsOrchidRequest;
use App\Http\Requests\NewsRequest;
use App\Models\News;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class NewsEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create news';

    public $description = 'Create news';

    public $exists = false;


    public function query(News $news): array
    {
        $this->exists = $news->exists;

        if ($this->exists) {
            $this->name = 'Edit news';
            return [
                'titleRu' => $news->translate('ru')->title,
                'titleEn' => $news->translate('en')->title,
                'descriptionRu' => $news->translate('ru')->description,
                'descriptionEn' => $news->translate('en')->description,
                'picture' => $news->picture
            ];
        }

        return [
            'news' => $news
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create news')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('titleRu')
                    ->title(__('Title RU')),
                Input::make('titleEn')
                    ->title(__('Title EN')),
                Quill::make('descriptionRu')
                    ->toolbar(["text", "color", "header", "list"])->title(__('Description RU')),
                Quill::make('descriptionEn')
                    ->toolbar(["text", "color", "header", "list"])->title(__('Description EN')),
                Picture::make('picture')
                    ->title(__('Picture'))
                    ->targetRelativeUrl()
            ])
        ];
    }


    /**
     * @param News $news
     * @param NewsOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(News $news, NewsOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' => [
                'title' => $request->input('titleEn'),
                'description' => $request->input('descriptionEn')],
            'ru' => [
                'title' => $request->input('titleRu'),
                'description' => $request->input('descriptionRu')],
            'picture' => $request->get('picture')
        ];
        $news->fill($data)->save();
        Alert::info('You have successfully created a news.');
        return redirect()->route('platform.newses.list');
    }
}
