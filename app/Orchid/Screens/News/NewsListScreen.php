<?php

namespace App\Orchid\Screens\News;

use App\Models\News;
use App\Orchid\Layouts\News\NewsListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class NewsListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'NewsListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'NewsListScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */

    public function query(News $news): array
    {
        return [
            'newses' => News::orderBy('created_at')->paginate()
        ];

    }

    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.newses.edit')
                ->icon('plus')
        ];
    }


    /**
     * @return string[]
     */
    public function layout(): array
    {
        return [
            NewsListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        News::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('News was removed'));
    }
}
