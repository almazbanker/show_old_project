<?php

namespace App\Orchid\Screens\University;

use App\Models\University;
use App\Orchid\Layouts\University\UniversityListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class UniversityListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Universities';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'Universities' => University::orderBy('created_at')->paginate()
        ];
    }


    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.universities.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            UniversityListLayout::class
        ];
    }


    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        University::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('University was removed'));
    }

}
