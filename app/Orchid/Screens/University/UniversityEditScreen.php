<?php

namespace App\Orchid\Screens\University;

use App\Http\Requests\UniversityRequest;
use App\Models\University;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class UniversityEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create University';
    public $description = 'Create University screen';

    public $exists = false;

    /**
     * Query data.
     *
     * @param University $university
     * @return array
     */
    public function query(University $university): array
    {
        $this->exists = $university->exists;
        if ($this->exists) {
            $this->name = 'Edit University';
            $this->description = 'Edit University description';
        }
        return [
            'university' => $university
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create university')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update University')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete University')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('university.name')
                    ->title('Name'),
                Input::make('university.faculty')
                    ->title('Faculty'),
                Quill::make('university.description')
                    ->toolbar(["text", "color", "header", "list"])
                    ->title('Description'),
                DateTimer::make('university.start_date')
                    ->title(__('Start date')),
                DateTimer::make('university.finish_date')
                    ->title(__('Finish date')),
                Relation::make('university.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),

            ])
        ];
    }

    /**
     * @param University $university
     * @param UniversityRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(University $university, UniversityRequest $request): RedirectResponse
    {
        $data = $request->get('university');
        $university->fill($data)->save();
        Alert::info('Successfully saved');
        return redirect()->route('platform.universities.list');
    }

}
