<?php

namespace App\Orchid\Screens\Type;

use App\Http\Requests\TypeOrchidRequest;
use App\Models\Category;
use App\Models\Type;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

use Orchid\Screen\Screen;

class TypeEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TypeEditScreen';

    /**
     * @var string
     */
    public $description = 'TypeEditScreen';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @param Type $type
     * @return array
     */
    public function query(Type $type): array
    {
        $this->exists = $type->exists;

        if ($this->exists) {
            $this->name = 'Edit type';
            return [
                'nameRu' => $type->translate('ru')->name,
                'nameEn' => $type->translate('en')->name,
                'descriptionRu' => $type->translate('ru')->description,
                'descriptionEn' => $type->translate('en')->description,
                'picture' => $type->picture,
                'category_id' => $type->category_id,
            ];
        }

        return [
            'type' => $type
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create type')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('nameRu')
                    ->title(__('Name RU')),
                Input::make('nameEn')
                    ->title(__('Name EN')),
                Quill::make('descriptionRu')
                    ->toolbar(["text", "color", "header", "list"])->title(__('Description RU')),
                Quill::make('descriptionEn')
                    ->toolbar(["text", "color", "header", "list"])->title(__('Description EN')),
                Picture::make('picture')
                    ->title(__('Picture'))
                    ->targetRelativeUrl(),
                Relation::make('category_id')
                    ->fromModel(Category::class, 'id')
                    ->displayAppend('full')
                    ->title('Category'),
            ])
        ];
    }


    /**
     * @param Type $type
     * @param TypeOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Type $type, TypeOrchidRequest $request): RedirectResponse
    {

        $data = [
            'en' => [
                'name' => $request->input('nameEn'),
                'description' => $request->input('descriptionEn')],
            'ru' => [
                'name' => $request->input('nameRu'),
                'description' => $request->input('descriptionRu')],
            'picture' => $request->get('picture'),
            'category_id' => $request->get('category_id')
        ];
        $type->fill($data)->save();

        Alert::info('You have successfully created a type.');

        return redirect()->route('platform.types.list');
    }
}


