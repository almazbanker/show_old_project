<?php

namespace App\Orchid\Screens\Type;

use App\Models\Type;
use App\Orchid\Layouts\Type\TypeListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;


class TypeListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TypeListScreen';

    /**
     * @var string
     */
    public $description = 'TypeListScreen';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @param Type $type
     * @return array
     */
    public function query(Type $type): array
    {
        return [
            'types' => Type::orderByDesc('created_at')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')
                ->route('platform.types.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            TypeListLayout::class
        ];
    }


    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Type::findOrFail($request->get('id'))->delete();
        Toast::info(__('Type was removed'));
    }
}



