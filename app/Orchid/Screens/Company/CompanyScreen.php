<?php

namespace App\Orchid\Screens\Company;

use App\Http\Requests\CompanyOrchidRequest;
use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class CompanyScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create company';
    public $exists = false;

    /**
     * @param Company $company
     * @return array
     */
    public function query(Company $company): array
    {
        $this->exists = $company->exists;
        if($this->exists) {
            $this->name = 'Edit company';
            return [
                'nameRu' => $company->translate('ru')->name,
                'nameEn' => $company->translate('en')->name,
                'descriptionRu' => $company->translate('ru')->description,
                'descriptionEn' => $company->translate('en')->description,
                'picture' => $company->picture,
            ];
        }
        return ['company' => $company];
    }

    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Add'))->icon('plus')->method('store')->canSee(!$this->exists),
            Button::make(__('Update'))->icon('plus')->method('update')->canSee($this->exists),
            Button::make(__('Delete'))->icon('trash')->method('destroy')->canSee($this->exists)
        ];
    }

    /**
     * @return array
     */
    public function layout(): array
    {
        return [Layout::rows([
            Input::make('nameRu')->title(__('Title RU')),
            Input::make('nameEn')->title(__('Title EN')),
            Quill::make('descriptionRu')
                ->toolbar(["text", "color", "header", "list"]),
            Quill::make('descriptionEn')
                ->toolbar(["text", "color", "header", "list"]),
            Picture::make('picture')->targetRelativeUrl()->title(__('Picture'))
        ])
        ];
    }

    /**
     * @param CompanyOrchidRequest $request
     * @return RedirectResponse
     */
    public function store(CompanyOrchidRequest $request): RedirectResponse
    {
        $data = ['picture' => $request->input('picture'), 'en' => ['name' => $request->input('nameEn'), 'description' => $request->input('descriptionEn')], 'ru' => ['name' => $request->input('nameRu'), 'description' => $request->input('descriptionRu')]];
        $company = new Company($data);
        $company->save();
        return redirect()->route('platform.companies');
    }

    /**
     * @param Company $company
     * @param CompanyOrchidRequest $request
     * @return RedirectResponse
     */
    public function update(Company $company, CompanyOrchidRequest $request): RedirectResponse
    {
        $data = ['picture' => $request->input('picture'), 'en' => ['name' => $request->input('nameEn'), 'description' => $request->input('descriptionEn')], 'ru' => ['name' => $request->input('nameRu'), 'description' => $request->input('descriptionRu')]];
        $company->update($data);
        return redirect()->route('platform.companies');
    }

    /**
     * @param Company $company
     * @return RedirectResponse
     */
    public function destroy(Company $company): RedirectResponse
    {
        $company->delete();
        return redirect()->route('platform.companies');
    }
}
