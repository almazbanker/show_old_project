<?php

namespace App\Orchid\Screens\Company;

use App\Models\Company;
use App\Orchid\Layouts\Company\CompanyListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CompanyListScreen extends Screen
{
    /**
     * @var string
     */
    public $name = 'Companies';

    /**
     * @return array
     */
    public function query(): array
    {
        return ['companies' => Company::all()];
    }

    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')->icon('plus')->route('platform.company')
        ];
    }

    /**
     * @return string[]
     */
    public function layout(): array
    {
        return [CompanyListLayout::class];
    }
}
