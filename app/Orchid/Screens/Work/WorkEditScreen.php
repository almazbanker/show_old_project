<?php

namespace App\Orchid\Screens\Work;

use App\Http\Requests\WorkOrchidRequest;
use App\Models\User;
use App\Models\Work;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class WorkEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Work create';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Work $work
     * @return array
     */
    public function query(Work $work): array
    {
        $this->exists = $work->exists;
        if ($this->exists){
            $this->name = 'Edit work';
        }
        return [
            'work' => $work
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create')
            ->icon('icon-pencil')
            ->method('createOrUpdate')
            ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('work.name')
                ->title('Title')
                ->required(),
                DateTimer::make('work.start_date')
                ->title('Start date')
                ->required(),
                DateTimer::make('work.finish_date')
                ->title('Finish date')
                ->required(),
                Quill::make('work.description')
                    ->toolbar(["text", "color", "header", "list"])
                    ->title('Description'),
                Relation::make('work.user_id')
                ->title('Users')
                ->fromModel(User::class, 'name', 'id')
            ])
        ];
    }

    /**
     * @param Work $work
     * @param WorkOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Work $work, WorkOrchidRequest $request): RedirectResponse
    {
        $work->fill($request->get('work'))->save();
        return redirect()->route('platform.works.list');
    }

}
