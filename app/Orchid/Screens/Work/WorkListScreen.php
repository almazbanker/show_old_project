<?php

namespace App\Orchid\Screens\Work;

use App\Http\Requests\WorkRequest;
use App\Models\Work;
use App\Orchid\Layouts\Work\WorkListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class WorkListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Works';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'works' => Work::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('Create new'))
            ->icon('icon-plus')
            ->route('platform.works.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            WorkListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(WorkRequest $request): void
    {
        Work::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Work was removed'));
    }
}
