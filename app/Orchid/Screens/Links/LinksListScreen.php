<?php

namespace App\Orchid\Screens\Links;

use App\Models\Link;
use App\Orchid\Layouts\Links\LinksListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class LinksListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Links';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Link $link
     * @return array
     */
    public function query(Link $link): array
    {
        return [
            'links' => Link::orderByDesc('created_at')->paginate()
        ];

    }

    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.links.edit')
                ->icon('plus')
        ];
    }


    public function layout(): array
    {
        return [
            LinksListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Link::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Book was removed'));
    }

}

