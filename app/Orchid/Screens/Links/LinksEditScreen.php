<?php

namespace App\Orchid\Screens\Links;

use App\Http\Requests\LinkOrchidRequest;
use App\Models\Link;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;

class LinksEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create link';

    public $exists = false;


    /**
     * @param Link $link
     * @return Link[]|array
     */
    public function query(Link $link): array
    {
        $this->exists = $link->exists;

        if ($this->exists) {
            $this->name = 'Edit link';
        }

        return [
            'link' => $link
        ];
    }

    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }


    /**
     * @return array
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('link.twitter')
                    ->title('Twitter'),
                Input::make('link.instagram')
                    ->title('Instagram'),
                Input::make('link.facebook')
                    ->title('Facebook'),

                Relation::make('link.user_id')
                    ->title('User')
                    ->fromModel(User::class, 'name', 'id'),
            ])
        ];
    }


    /**
     * @param Link $book
     * @param LinkOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Link $book, LinkOrchidRequest $request): RedirectResponse
    {
        $book->fill($request->get('link'))->save();

        Alert::info('You have successfully created a link.');

        return redirect()->route('platform.links.list');
    }
}
