<?php

namespace App\Orchid\Screens\Subcomment;

use App\Http\Requests\SubcommentRequest;
use App\Models\Comment;
use App\Models\Subcomment;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class SubcommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'SubcommentEditScreen';

    /**
     * @var string
     */
    public $description = 'SubcommentEditScreen';

    /**
     * @var bool
     */
    public $exists = false;


    /**
     * @param Subcomment $subcomment
     * @return Subcomment[]
     */
    public function query(Subcomment $subcomment): array
    {
        $this->exists = $subcomment->exists;
        if ($this->exists) {
            $this->content = 'Edit Subcomment';
        }
        return [
            'subcomment' => $subcomment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create subcomment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }


    /**
     * @return array
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('subcomment.content')
                    ->title('Content'),
                Relation::make('subcomment.user_id')
                    ->title('Users')
                    ->fromModel(User::class, 'name', 'id'),
                Relation::make('subcomment.comment_id')
                    ->title('Comment')
                    ->fromModel(Comment::class, 'content', 'id'),
            ])
        ];
    }

    /**
     * @param Subcomment $subcomment
     * @param SubcommentRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Subcomment $subcomment, SubcommentRequest $request): RedirectResponse
    {
        $subcomment->fill($request->get('subcomment'))->save();
        Alert::info('You have successfully created a subcomment.');
        return redirect()->route('platform.subcomments.list');
    }
}
