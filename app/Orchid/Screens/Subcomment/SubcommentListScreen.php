<?php

namespace App\Orchid\Screens\Subcomment;

use App\Models\Subcomment;
use App\Orchid\Layouts\Subcomment\SubcommentListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class SubcommentListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Subcomments';

    /**
     * @var string
     */
    public $description = 'Subcomments';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'Subcomments' => Subcomment::orderBy('created_at')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')
                ->route('platform.subcomments.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SubcommentListLayout::class
        ];
    }

    /**
     * @param Request $request
     * @return void
     */
    public function remove(Request $request): void
    {
        Subcomment::findOrFail($request->get('id'))->delete();
        Toast::info(__('Subcomment was removed'));
    }
}
