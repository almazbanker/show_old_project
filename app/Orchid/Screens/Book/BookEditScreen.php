<?php

namespace App\Orchid\Screens\Book;

use App\Http\Requests\BookOrchidRequest;
use App\Models\Book;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class BookEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create book';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Book $book
     * @return array
     */
    public function query(Book $book): array
    {
        $this->exists = $book->exists;

        if ($this->exists) {
            $this->name = __('Edit book');
        }

        return [
            'book' => $book
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create license')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('book.name')
                    ->title(__('Title')),
                Input::make('book.author')
                    ->title(__('Author')),
                Quill::make('book.description')
                    ->toolbar(["text", "color", "header", "list",])->title(__('Description')),
                Input::make('book.link')
                    ->title(__('Link')),
            ])
        ];
    }


    /**
     * @param Book $book
     * @param BookOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Book $book, BookOrchidRequest $request): RedirectResponse
    {
        $book->fill($request->get('book'))->save();

        Alert::info('You have successfully created a book.');

        return redirect()->route('platform.books.list');
    }
}
