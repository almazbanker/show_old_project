<?php

namespace App\Orchid\Screens\Book;

use App\Models\Book;
use App\Orchid\Layouts\Book\BookListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class BookListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'BookListScreen';


    /**
     * Query data.
     *
     * @param Book $book
     * @return array
     */
    public function query(Book $book): array
    {
        return [
            'books' => Book::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')
                ->route('platform.books.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            BookListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Book::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('Book was removed'));
    }
}
