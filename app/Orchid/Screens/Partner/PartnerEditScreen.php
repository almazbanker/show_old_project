<?php

namespace App\Orchid\Screens\Partner;

use App\Http\Requests\PartnerOrchidRequest;
use App\Models\Partner;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class PartnerEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'PartnerEditScreen';

    public $description = 'PartnerEditScreen';

    public $exists = false;


    public function query(Partner $partner): array
    {
        $this->exists = $partner->exists;

        if ($this->exists) {
            $this->name = 'Edit partner';
            return [
                'nameRu' => $partner->translate('ru')->name,
                'nameEn' => $partner->translate('en')->name,
                'descriptionRu' => $partner->translate('ru')->description,
                'descriptionEn' => $partner->translate('en')->description,
                'picture' => $partner->picture
            ];
        }

        return [
            'partner' => $partner
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create license')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('nameRu')
                    ->title(__('Title RU')),
                Input::make('nameEn')
                    ->title(__('Title EN')),
                Quill::make('descriptionRu')
                    ->toolbar(["text", "color", "header", "list"]),
                Quill::make('descriptionEn')
                    ->toolbar(["text", "color", "header", "list"]),
                Picture::make('picture')
                    ->title(__('picture'))
                    ->targetRelativeUrl()
            ])
        ];
    }


    /**
     * @param Partner $partner
     * @param PartnerOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Partner $partner, PartnerOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' => [
                'name' => $request->input('nameEn'),
                'description' => $request->input('descriptionEn')
            ],
            'ru' => [
                'name' => $request->input('nameRu'),
                'description' => $request->input('descriptionRu')
            ],
            'picture' => $request->input('picture')
            ];

        $partner->fill($data)->save();

        Alert::info('You have successfully created a partner.');

        return redirect()->route('platform.partners.list');
    }
}
