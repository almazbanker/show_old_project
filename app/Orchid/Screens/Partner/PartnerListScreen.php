<?php

namespace App\Orchid\Screens\Partner;

use App\Models\Partner;
use App\Orchid\Layouts\Partner\PartnerListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class PartnerListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'PartnersListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'PartnersListScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */

    public function query(Partner $partner): array
    {
        return [
            'partners' => Partner::orderBy('created_at')->paginate()
        ];

    }

    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.partners.edit')
                ->icon('plus')
        ];
    }


    public function layout(): array
    {
        return [
            PartnerListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Partner::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Partner was removed'));
    }
}
