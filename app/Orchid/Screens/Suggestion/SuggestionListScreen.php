<?php

namespace App\Orchid\Screens\Suggestion;

use App\Models\Suggestion;
use App\Orchid\Layouts\Suggestion\SuggestionListLayout;
use App\Orchid\Layouts\University\UniversityListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class SuggestionListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Suggestions';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'Suggestions' => Suggestion::orderBy('created_at')->paginate()
        ];
    }


    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.suggestions.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            SuggestionListLayout::class
        ];
    }


    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Suggestion::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Suggestion was removed'));
    }
}
