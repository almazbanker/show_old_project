<?php

namespace App\Orchid\Screens\Suggestion;

use App\Http\Requests\SuggestionOrchidRequest;
use App\Models\Suggestion;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class SuggestionEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create Suggestion';
    public $description = 'Create Suggestion screen';

    public $exists = false;


    /**
     * @param Suggestion $suggestion
     * @return Suggestion[]
     */
    public function query(Suggestion $suggestion): array
    {
        $this->exists = $suggestion->exists;
        if ($this->exists) {
            $this->name = 'Edit Suggestion';
            $this->description = 'Edit Suggestion description';
        }
        return [
            'suggestion' => $suggestion
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create suggestion')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update Suggestion')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete Suggestion')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('suggestion.suggestion')
                    ->title('Suggestion'),
                DateTimer::make('suggestion.start_date')
                    ->title(__('Start date')),
                DateTimer::make('suggestion.finish_date')
                    ->title(__('Finish date')),
            ])
        ];
    }


    /**
     * @param Suggestion $suggestion
     * @param SuggestionOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Suggestion $suggestion, SuggestionOrchidRequest $request): RedirectResponse
    {
        $data = $request->get('suggestion');
        $suggestion->fill($data)->save();
        Alert::info('Successfully saved');
        return redirect()->route('platform.suggestions.list');
    }
}
