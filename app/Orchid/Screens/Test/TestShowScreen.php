<?php

namespace App\Orchid\Screens\Test;

use App\Models\Category;
use App\Models\Test;
use App\Orchid\Layouts\Test\TestShowLayout;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class TestShowScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'TestShowScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Test $test): array
    {
        return [
            'answers' => $test->answers,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            TestShowLayout::class
        ];
    }
}
