<?php

namespace App\Orchid\Screens\Test;

use App\Http\Requests\TestRequest;
use App\Models\Test;
use App\Orchid\Layouts\Test\TestListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class TestListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Tests';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'tests' => Test::paginate()
        ];
    }


    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            TestListLayout::class
        ];
    }

    /**
     * @param TestRequest $request
     */
    public function remove(Request $request): void
    {
        Test::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Test was removed'));
    }
}
