<?php

namespace App\Orchid\Screens\Question;

use App\Models\Question;
use App\Orchid\Layouts\Question\QuestionListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class QuestionListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Questions';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'questions' => Question::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')->icon('plus')->route('platform.questions.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            QuestionListLayout::class
        ];
    }

}
