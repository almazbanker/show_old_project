<?php

namespace App\Orchid\Screens\Question;

use App\Http\Requests\QuestionOrchidRequest;
use App\Http\Requests\QuestionRequest;
use App\Models\Category;
use App\Models\Question;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class QuestionEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Add Question';
    public $exists = false;

    /**
     * Query data.
     *
     * @param Question $question
     * @return array
     */
    public function query(Question $question): array
    {
        $this->exists = $question->exists;
        if ($this->exists) {
            $this->name = 'Question edit';
            return [
                'questionRu' => $question->translate('ru')->question,
                'questionEn' => $question->translate('en')->question,
                'variant_1Ru' => $question->translate('ru')->variant_1,
                'variant_1En' => $question->translate('en')->variant_1,
                'variant_2Ru' => $question->translate('ru')->variant_2,
                'variant_2En' => $question->translate('en')->variant_2,
                'variant_3Ru' => $question->translate('ru')->variant_3,
                'variant_3En' => $question->translate('en')->variant_3,
                'variant_trueRu' => $question->translate('ru')->variant_true,
                'variant_trueEn' => $question->translate('en')->variant_true,
                'level' => $question->level,
                'category_id' => $question->category->id
            ];
        }
        return ['question' => $question];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Add')->icon('pencil')->method('store')->canSee(!$this->exists),
            Button::make('Update')->icon('pencil')->method('update')->canSee($this->exists),
            Button::make('Delete')->icon('trash')->method('destroy')->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [Layout::rows([
            Input::make('questionRu')->title('Question RU'),
            Input::make('questionEn')->title('Question EN'),
            Input::make('level')->title('level'),
            Input::make('variant_1Ru')->title('Variant 1 RU'),
            Input::make('variant_1En')->title('Variant 1 EN'),
            Input::make('variant_2Ru')->title('Variant 2 RU'),
            Input::make('variant_2En')->title('Variant 2 EN'),
            Input::make('variant_3Ru')->title('Variant 3 RU'),
            Input::make('variant_3En')->title('Variant 3 EN'),
            Input::make('variant_trueRu')->title('Variant true RU'),
            Input::make('variant_trueEn')->title('Variant true EN'),
            Relation::make('category_id')
                ->title('Category')
                ->fromModel(Category::class, 'id', 'id'),
        ])];
    }


    /**
     * @param QuestionOrchidRequest $request
     * @return RedirectResponse
     */
    public function store(QuestionOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' =>
                [
                    'question' => $request->input('questionEn'),
                    'variant_1' => $request->input('variant_1En'),
                    'variant_2' => $request->input('variant_2En'),
                    'variant_3' => $request->input('variant_3En'),
                    'variant_true' => $request->input('variant_trueEn'),
                ],
            'ru' =>
                [
                    'question' => $request->input('questionRu'),
                    'variant_1' => $request->input('variant_1Ru'),
                    'variant_2' => $request->input('variant_2Ru'),
                    'variant_3' => $request->input('variant_3Ru'),
                    'variant_true' => $request->input('variant_trueRu'),
                ],
                ];
        $question = new Question($data);
        $question->level = $request->input('level');
        $question->category_id = $request->input('category_id');
        $question->save();
        Alert::info('question successfully created');
        return redirect()->route('platform.questions.list');
    }


    /**
     * @param Question $question
     * @param QuestionOrchidRequest $request
     * @return RedirectResponse
     */
    public function update(Question $question, QuestionOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' =>
                [
                    'question' => $request->input('questionEn'),
                    'variant_1' => $request->input('variant_1En'),
                    'variant_2' => $request->input('variant_2En'),
                    'variant_3' => $request->input('variant_3En'),
                    'variant_true' => $request->input('variant_trueEn'),
                ],
            'ru' =>
                [
                    'question' => $request->input('questionRu'),
                    'variant_1' => $request->input('variant_1Ru'),
                    'variant_2' => $request->input('variant_2Ru'),
                    'variant_3' => $request->input('variant_3Ru'),
                    'variant_true' => $request->input('variant_trueRu'),
                ],
        ];
        $question->level = $request->input('level');
        $question->category_id = $request->input('category_id');
        $question->update($data);
        Alert::info('question successfully updated');
        return redirect()->route('platform.questions.list');
    }

    /**
     * @param Question $question
     * @return RedirectResponse
     */
    public function destroy(Question $question): RedirectResponse
    {
        $question->delete();
        Alert::info('question successfully deleted');
        return redirect()->route('platform.questions.list');
    }
}
