<?php

namespace App\Orchid\Screens\Callback;

use App\Http\Requests\CallbackOrchidRequest;
use App\Models\Callback;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;


class CallbackEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create callback';
    public $description = 'Create callback';
    public $exists = false;


    public function query(Callback $callback): array
    {
        $this->exists = $callback->exists;

        if ($this->exists) {
            $this->name = 'Edit callback';
        }

        return [
            'callback' => $callback
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create callback')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }


    /**
     * @return array
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('callback.name')
                    ->title(__('Name')),
                Input::make('callback.phone_number')
                    ->title(__('Phone number')),
                Quill::make('callback.description')
                    ->toolbar(["text", "color", "header", "list"])
                    ->title('Description'),
            ])
        ];
    }


    public function createOrUpdate(Callback $callback, CallbackOrchidRequest $request): RedirectResponse
    {
        $data = $request->get('callback');
        $callback->fill($data)->save();
        Alert::info('successfully saved');
        return redirect()->route('platform.callbacks.list');
    }
}
