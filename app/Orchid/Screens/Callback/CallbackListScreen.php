<?php

namespace App\Orchid\Screens\Callback;

use App\Models\Callback;
use App\Orchid\Layouts\Callback\CallbackListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class CallbackListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Callbacks';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Callbacks';

    public $exists = false;


    public function query(Callback $callback): array
    {
        return [
            'callbacks' => Callback::orderBy('created_at')->paginate()
        ];

    }

    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.callbacks.edit')
                ->icon('plus')
        ];
    }


    public function layout(): array
    {
        return [
            CallbackListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Callback::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Callback was removed'));
    }
}
