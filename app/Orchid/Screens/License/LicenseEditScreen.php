<?php

namespace App\Orchid\Screens\License;


use App\Http\Requests\LicenseOrchidRequest;
use App\Models\License;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class LicenseEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create license';

    public $description = 'Create license';

    public $exists = false;


    /**
     * @param License $license
     * @return License[]|array
     */
    public function query(License $license): array
    {
        $this->exists = $license->exists;

        if ($this->exists) {
            $this->name = 'Edit license';
        }

        return [
            'license' => $license
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create license')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('license.number')
                    ->title(__('Number')),
                DateTimer::make('license.start_date')
                    ->title(__('Start date')),
                DateTimer::make('license.finish_date')
                    ->title(__('Finish date')),
                Relation::make('license.user_id')
                    ->title(__('Author'))
                    ->fromModel(User::class, 'name', 'id'),
                Quill::make('license.description')
                    ->toolbar(["text", "color", "header", "list"])
                    ->title('Description'),
                Picture::make('license.picture')
                    ->title(__('Picture'))
                    ->targetRelativeUrl()
            ])
        ];
    }

    /**
     * @param License $license
     * @param LicenseOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(License $license, LicenseOrchidRequest $request): RedirectResponse
    {
        $data = $request->get('license');
        $license->fill($data)->save();
        Alert::info('You have successfully created a license.');
        return redirect()->route('platform.licenses.list');
    }
}
