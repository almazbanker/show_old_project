<?php

namespace App\Orchid\Screens\License;

use App\Models\License;
use App\Models\Link;
use App\Orchid\Layouts\License\LicenseListLayout;
use App\Orchid\Layouts\Links\LinksListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;

class LicenseListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Licenses';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Licenses';

    public $exists = false;

    /**
     * Query data.
     *
     * @param License $license
     * @return array
     */

    public function query(License $license): array
    {
        return [
            'licenses' => License::orderBy('created_at')->paginate()
        ];

    }

    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.licenses.edit')
                ->icon('plus')
        ];
    }


    public function layout(): array
    {
        return [
            LicenseListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        License::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('License was removed'));
    }
}
