<?php

namespace App\Orchid\Screens\Phone;

use App\Models\Phone;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class PhoneEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create phone';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Phone $phone
     * @return array
     */
    public function query(Phone $phone): array
    {
        $this->exists = $phone->exists;
        if ($this->exists){
            $this->name = 'Edit phone';
        }
        return [
            'phone' => $phone
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create phone')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Edit')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('phone.number')
                ->title(__('Number')),
                Relation::make('phone.user_id')
                ->title(__('Users'))
                ->fromModel(User::class, 'name', 'id')
            ])
        ];
    }

    /**
     * @param Phone $phone
     * @param Request $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Phone $phone, Request $request): RedirectResponse
    {
        $request->validate(['phone.number' => 'required', 'phone.user_id' => 'required|numeric']);
        $phone->fill($request->get('phone'))->save();
        Alert::info('You have successfully created phone');
        return redirect()->route('platform.phones.list');
    }

    /**
     * @param Phone $phone
     * @return RedirectResponse
     */
    public function remove(Phone $phone): RedirectResponse
    {
        $phone->delete() ? Alert::info('Phone successfully deleted') : Alert::error('Error!');
        return redirect()->route('platform.phones.list');
    }
}
