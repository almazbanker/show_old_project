<?php

namespace App\Orchid\Screens\Phone;

use App\Models\Phone;
use App\Orchid\Layouts\Phone\PhoneListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class PhoneListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Phone';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'phones' => Phone::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
            ->icon('icon-plus')
            ->route('platform.phones.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PhoneListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Phone::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('Phone was removed'));
    }
}
