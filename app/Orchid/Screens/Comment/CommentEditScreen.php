<?php

namespace App\Orchid\Screens\Comment;

use App\Http\Requests\CommentOrchidRequest;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Alert;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create comment';

    /**
     * @var string
     */
    public $description = 'Create comment';

    /**
     * @var bool
     */
    public $exists = false;


    /**
     * Query data.
     *
     * @param Comment $comment
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->exists = $comment->exists;
        if ($this->exists) {
            $this->content = 'Edit Comment';
        }
        return [
            'comment' => $comment
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create comment')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }


    /**
     * @return array
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('comment.content')
                    ->title('Content'),
                Relation::make('comment.user_id')
                    ->title('Users')
                    ->fromModel(User::class, 'name', 'id'),
                Relation::make('comment.author_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
            ])
        ];
    }

    /**
     * @param Comment $comment
     * @param CommentOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Comment $comment, CommentOrchidRequest $request): RedirectResponse
    {
        $comment->fill($request->get('comment'))->save();
        Alert::info('You have successfully created a comment.');
        return redirect()->route('platform.comments.list');
    }
}
