<?php

namespace App\Orchid\Screens\Comment;

use App\Models\Comment;
use App\Orchid\Layouts\Comment\CommentListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class CommentListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Comments';

    /**
     * @var string
     */
    public $description = 'Comments';

    /**
     * @var bool
     */
    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'Comments' => Comment::orderBy('created_at')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Add')
                ->route('platform.comments.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            CommentListLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Comment::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('Comment was removed'));
    }
}
