<?php

namespace App\Orchid\Screens\Country;

use App\Models\Country;
use App\Orchid\Layouts\Country\CountryListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class CountryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CountryListScreen';

    /**
     * @var string
     */
    public $description = 'CountryListScreen';

    /**
     * @var bool
     */
    public $exists = false;



    public function query(Country $country): array
    {
        return [
            'countries' => Country::orderByDesc('created_at')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.countries.edit')
                ->icon('plus')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [CountryListLayout::class];
    }


    public function remove(Request $request): void
    {
        Country::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Country was removed'));
    }
}




