<?php

namespace App\Orchid\Screens\Country;

use App\Http\Requests\CountryOrchidRequest;
use App\Models\Country;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class CountryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CountryEditScreen';

    /**
     * @var string
     */
    public $description = 'CountryEditScreen';

    /**
     * @var bool
     */
    public $exists = false;

    public function query(Country $country): array
    {
        $this->exists = $country->exists;
        if ($this->exists) {
            $this->name = 'Category edit';
            return [
                'nameRu' => $country->translate('ru')->name,
                'nameEn' => $country->translate('en')->name,
                'code' => $country->code,
            ];
        }
        return ['country' => $country];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Add'))->icon('pencil')->method('store')->canSee(!$this->exists),
            Button::make(__('Update'))->icon('pencil')->method('update')->canSee($this->exists),
            Button::make(__('Delete'))->icon('trash')->method('destroy')->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [Layout::rows([
            Input::make('nameRu')->title(__('Title RU')),
            Input::make('nameEn')->title(__('Title EN')),
            Input::make('code')->title(__('Code')),
        ])];
    }

    /**
     * @param CountryOrchidRequest $request
     * @return RedirectResponse
     */
    public function store(CountryOrchidRequest $request):RedirectResponse
    {
        $data = [
            'en' => ['name' => $request->input('nameEn')],
            'ru' => ['name' => $request->input('nameRu')],
            'code' => $request->input('code')
        ];
        $country = new Country($data);
        $country->save();
        return redirect()->route('platform.countries.list');
    }


    /**
     * @param Country $country
     * @param CountryOrchidRequest $request
     * @return RedirectResponse
     */
    public function update(Country $country, CountryOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' => ['name' => $request->input('nameEn'), 'code' => $request->input('codeEn')],
            'ru' => ['name' => $request->input('nameRu'), 'code' => $request->input('codeRu')]
        ];
        $country->update($data);
        return redirect()->route('platform.countries.list');
    }


    public function destroy(Country $country): RedirectResponse
    {
        $country->delete();
        return redirect()->route('platform.countries.list');
    }
}



