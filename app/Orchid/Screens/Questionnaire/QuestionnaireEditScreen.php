<?php

namespace App\Orchid\Screens\Questionnaire;

use App\Http\Requests\QuestionnaireOrchidRequest;
use App\Models\Country;
use App\Models\Questionnaire;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class QuestionnaireEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create questionnaire';


    /**
     * @var string
     */
    public $description = 'Create questionnaire screen';


    /**
     * @var bool
     */
    public $exists = false;


    /**
     * Query data.
     *
     * @param Questionnaire $questionnaire
     * @return array
     */
    public function query(Questionnaire $questionnaire): array
    {
        $this->exists = $questionnaire->exists;
        if ($this->exists) {
            $this->name = "Edit questionnaire";
        }
        return [
            'questionnaire' => $questionnaire,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create questionnaire')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     * @throws BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Relation::make('questionnaire.user_id')
                    ->title(__('Users'))
                    ->fromModel(User::class, 'name', 'id')
                    ->required(),
                Relation::make('questionnaire.country_id')
                    ->title('Country')
                    ->fromModel(Country::class, 'id', 'id'),
                Select::make('questionnaire.gender')
                    ->title('Gender')
                    ->options([
                        1  => 'Men',
                        2  => 'Women',
                    ])
                    ->required(),
                DateTimer::make('questionnaire.born_date')
                    ->title('Born date')
                    ->required(),

                Picture::make('questionnaire.picture')
                    ->targetRelativeUrl()
                    ->title('Picture')
                    ->required(),
            ])

        ];
    }

    /**
     * @param Questionnaire $questionnaire
     * @param QuestionnaireOrchidRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Questionnaire $questionnaire, QuestionnaireOrchidRequest $request):RedirectResponse
    {
        $questionnaire->fill($request->get('questionnaire'))->save();
        Alert::info('Your successfully created comment');
        return redirect()->route('platform.questionnaires.list');
    }


    /**
     * @param Questionnaire $questionnaire
     * @return RedirectResponse
     */
    public function remove(Questionnaire $questionnaire):RedirectResponse
    {
        $questionnaire->delete() ? Alert::info('Questionnaire successfully deleted.') : Alert::error('Error!');
        return redirect()->route('platform.questionnaires.list');

    }
}
