<?php

namespace App\Orchid\Screens\Questionnaire;

use App\Models\Questionnaire;
use App\Orchid\Layouts\Questionnaire\QuestionnaireListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class QuestionnaireListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Questionnaire';

    /**
     * @var string
     */
    public $description = "Questionnaire";

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'questionnaires' => Questionnaire::paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-plus')
                ->route('platform.questionnaires.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            QuestionnaireListLayout::class
        ];
    }
}
