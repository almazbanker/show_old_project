<?php

namespace App\Orchid\Screens\Category;

use App\Models\Category;
use App\Orchid\Layouts\Category\CategoryLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Toast;

class CategoryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CategoriesListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'CategoriesListScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */

    public function query(Category $category): array
    {
        return [
            'categories' => Category::orderBy('created_at')->paginate()
        ];

    }

    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            \Orchid\Screen\Actions\Link::make('Add')
                ->route('platform.categories.edit')
                ->icon('plus')
        ];
    }


    /**
     * @return string[]
     */
    public function layout(): array
    {
        return [
            CategoryLayout::class
        ];
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Category::findOrFail($request->get('id'))
            ->delete();

        \Orchid\Support\Facades\Toast::info(__('Category was removed'));
    }
}
