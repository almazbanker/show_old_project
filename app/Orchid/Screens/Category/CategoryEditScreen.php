<?php

namespace App\Orchid\Screens\Category;

use App\Http\Requests\CategoryOrchidRequest;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;
use App\Models\Category;

class CategoryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create category';

    public $description = 'Create category';

    public $exists = false;


    /**
     * @param Category $category
     * @return array|Category[]
     */
    public function query(Category $category): array
    {
        $this->exists = $category->exists;

        if ($this->exists) {
            $this->name = 'Edit category';
            return [
                'nameRu' => $category->translate('ru')->name,
                'nameEn' => $category->translate('en')->name,
                'descriptionRu' => $category->translate('ru')->description,
                'descriptionEn' => $category->translate('en')->description,
                'picture' => $category->picture
            ];
        }

        return [
            'category' => $category
        ];
    }


    /**
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create category')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists)
        ];
    }


    /**
     * @return array
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('nameRu')
                    ->title(__('Name RU')),
                Input::make('nameEn')
                    ->title(__('Name EN')),
                Quill::make('descriptionRu')
                    ->toolbar(["text", "color", "header", "list"])->title(__('Description RU')),
                Quill::make('descriptionEn')
                    ->toolbar(["text", "color", "header", "list"])->title(__('Description EN')),
                Picture::make('picture')
                    ->title(__('Picture'))
                    ->targetRelativeUrl()
            ])
        ];
    }



    public function createOrUpdate(Category $category, CategoryOrchidRequest $request): RedirectResponse
    {
        $data = [
            'en' => [
                'name' => $request->input('nameEn'),
                'description' => $request->input('descriptionEn')],
            'ru' => [
                'name' => $request->input('nameRu'),
                'description' => $request->input('descriptionRu')],
            'picture' => $request->get('picture')
        ];
        $category->fill($data)->save();
        Alert::info('successfully saved');
        return redirect()->route('platform.categories.list');
    }
}
