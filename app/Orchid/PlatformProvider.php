<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Access rights')),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles'),

            Menu::make('Links')
                ->icon('database')
                ->route('platform.links.list'),

            Menu::make('Licenses')
                ->icon('database')
                ->route('platform.licenses.list')
                ,

            Menu::make('Questions')
                ->icon('database')
                ->route('platform.questions.list')
                ,

            Menu::make('Partners')
                ->icon('database')
                ->route('platform.partners.list')
                ,

            Menu::make('News')
                ->icon('database')
                ->route('platform.newses.list')
                ,

            Menu::make('Trainings')
                ->icon('database')
                ->route('platform.trainings.list')
                ,

            Menu::make('Questionnaire')
                ->icon('database')
                ->route('platform.questionnaires.list')
                ,

            Menu::make('Works')
                ->icon('database')
                ->route('platform.works.list')
               ,

            Menu::make('Categories')
                ->icon('database')
                ->route('platform.categories.list')
                ,

            Menu::make('Phones')
                ->icon('database')
                ->route('platform.phones.list')
                ,

            Menu::make('Tests')
                ->icon('database')
                ->route('platform.tests.list')
               ,

            Menu::make('Companies')
                ->icon('database')
                ->route('platform.companies')
                ,

            Menu::make('Certificates')
                ->icon('database')
                ->route('platform.certificates')
                ,

            Menu::make('Types')
                ->icon('database')
                ->route('platform.types.list')
                ,


            Menu::make('Countries')
                ->icon('database')
                ->route('platform.countries.list')
                ,

            Menu::make('Books')
                ->icon('database')
                ->route('platform.books.list')
                ,

            Menu::make('Comments')
                ->icon('database')
                ->route('platform.comments.list')
                ,

            Menu::make('Subcomments')
                ->icon('database')
                ->route('platform.subcomments.list')
                ,
            Menu::make('Universities')
                ->icon('database')
                ->route('platform.universities.list')
                ,
            Menu::make('Suggestions')
                ->icon('database')
                ->route('platform.suggestions.list')
                ,
            Menu::make('Callbacks')
                ->icon('database')
                ->route('platform.callbacks.list')
                ,

        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
