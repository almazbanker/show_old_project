<?php

function categories()
{
    return App\Models\Category::all();
}

function companies()
{
    return App\Models\Company::all();
}

function partners()
{
    return App\Models\Partner::all();
}
