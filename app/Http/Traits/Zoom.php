<?php

namespace App\Http\Traits;

use DateTime;
use Exception;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

trait Zoom
{

    public $client;
    public $jwt;
    public $headers;


    /**
     * @return string
     */
    public function generateZoomToken(): string
    {
        $key = env('ZOOM_API_KEY', '');
        $secret = env('ZOOM_API_SECRET', '');
        $payload = [
            'iss' => $key,
            'exp' => strtotime('+1 minute'),
        ];

        return JWT::encode($payload, $secret, 'HS256');
    }


    /**
     * @return mixed
     */
    private function retrieveZoomUrl()
    {
        return env('ZOOM_API_URL', '');
    }


    /**
     * @param string $dateTime
     * @return string
     * @throws Exception
     */
    public function toZoomTimeFormat(string $dateTime): string
    {
        $date = new DateTime($dateTime);

        return $date->format('Y-m-d\TH:i:s');
    }

    /**
     * @param $data
     * @return array
     * @throws GuzzleException
     */
    public function create($data): array
    {
        $this->client = new Client([
            'base_uri' => 'https://zoom.us',
            'timeout' => 2.0,
            'verify' => false,
        ]);
        $this->jwt = $this->generateZoomToken();
        $this->headers = [
            'Authorization' => 'Bearer ' . $this->jwt,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $path = 'users/me/meetings';
        $url = $this->retrieveZoomUrl();

        $body = [
            'headers' => $this->headers,
            'body' => json_encode([
                'topic' => $data['topic'],
                'type' => 2,
                'start_time' => $this->toZoomTimeFormat($data['start_time']),
                'duration' => $data['duration'],
                'agenda' => null,
                'timezone' => 'UTC',
                'settings' => [
                    'host_video' => false,
                    'participant_video' => false,
                    'waiting_room' => true,
                ],
            ]),
        ];

        $response = $this->client->post($url . $path, $body);

        return [
            'data' => json_decode($response->getBody(), true),
        ];
    }

    /**
     * @param string $id
     *
     * @return bool[]
     */
    public function delete(string $id): array
    {
        $this->client = new Client([
            'base_uri' => 'https://zoom.us',
            'timeout' => 2.0,
            'verify' => false,
        ]);
        $this->jwt = $this->generateZoomToken();
        $this->headers = [
            'Authorization' => 'Bearer ' . $this->jwt,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
        $path = 'meetings/' . $id;
        $url = $this->retrieveZoomUrl();
        $body = [
            'headers' => $this->headers,
            'body' => json_encode([]),
        ];

        $response = $this->client->delete($url . $path, $body);

        return [
            'success' => $response->getStatusCode() === 204,
        ];
    }
}
