<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'name' => ['required', 'min:2', 'string', 'regex:/([^(<)(>)])$/u',],
            'email' => ['required', 'email:rfc', 'unique:users,email', 'regex:/([^(<)(>)])$/u',],
            'password' => ['required', 'string', 'min:6', 'confirmed', 'regex:/([^(<)(>)])$/u',],
        ];
        if(config('app.env') !== 'testing'){
            $rules = array_merge($rules, ['g-recaptcha-response' => ['required', 'captcha'],]);
        }

        return $rules;
    }
}
