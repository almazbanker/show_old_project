<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartnerOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nameRu' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'nameEn' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'picture' => ['bail', 'nullable', ],
            'descriptionRu' => ['bail', 'required', 'string',],
            'descriptionEn' => ['bail', 'required', 'string',],
        ];
    }
}
