<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'level' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'questionRu' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'questionEn' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_1Ru' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_1En' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_2Ru' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_2En' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_3Ru' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_3En' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_trueRu' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_trueEn' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
