<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LicenseOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'license.start_date' => ['bail', 'required', 'date', 'regex:/([^(<)(>)])$/u',],
            'license.finish_date' => ['bail', 'required', 'date', 'regex:/([^(<)(>)])$/u',],
            'license.user_id' => ['bail', 'required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'license.description' => ['bail', 'required', 'string', ],
            'license.picture' => ['bail', 'nullable',],
            'license.number' => ['bail', 'required', 'regex:/([^(<)(>)])$/u',]
        ];
    }
}
