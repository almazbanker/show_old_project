<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallbackOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'callback.name' => ['required', 'min:3', 'max:255', 'regex:/([^(<)(>)])$/u',],
            'callback.phone_number' => ['required', 'regex:/([^(<)(>)])$/u', 'regex:/^[\+\d+$]/u',],
            'callback.description' => ['min:3', 'max:255',],
        ];
    }
}
