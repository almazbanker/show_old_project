<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LinkOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'link.twitter' => ['bail', 'nullable', 'string', 'regex:/([^(<)(>)])$/u',],
            'link.instagram' => ['bail', 'nullable', 'string', 'regex:/([^(<)(>)])$/u',],
            'link.facebook' => ['bail', 'nullable', 'string', 'regex:/([^(<)(>)])$/u',],
            'link.user_id' => ['bail', 'required', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
