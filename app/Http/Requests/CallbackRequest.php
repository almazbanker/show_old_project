<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        $rules = [
            'name' => ['required', 'min:3', 'max:255', 'regex:/([^(<)(>)])$/u',],
            'phone_number' => ['required', 'regex:/([^(<)(>)])$/u', 'regex:/^[\+\d+$]/u',],
            'description' => ['min:3', 'max:255', 'regex:/([^(<)(>)])$/u',],
        ];


        if(config('app.env') !== 'testing'){
            $rules = array_merge($rules, [ 'g-recaptcha-response' => ['required','captcha'],]);
        }

        return $rules;
    }
}
