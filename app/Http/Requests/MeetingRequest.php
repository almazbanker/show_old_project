<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'start_url' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'join_url' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'password' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'start_time' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'meeting_id' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
