<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CertificateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'start_date' => ['bail', 'required', 'date', 'regex:/([^(<)(>)])$/u',],
            'finish_date' => ['bail', 'required', 'date', 'regex:/([^(<)(>)])$/u',],
            'description' => ['bail', 'nullable', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
