<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'titleRu' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'titleEn' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'descriptionRu' => ['required', 'string',],
            'descriptionEn' => ['required', 'string',],
            'picture' => ['required',],
        ];
    }
}
