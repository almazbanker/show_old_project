<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CertificateOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'certificate.name' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'certificate.start_date' => ['required', 'date', 'regex:/([^(<)(>)])$/u',],
            'certificate.finish_date' => ['required', 'date', 'regex:/([^(<)(>)])$/u',],
            'certificate.description' => ['required', 'string',],
            'certificate.picture' => ['required'],
            'certificate.user_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
