<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TypeOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nameEn' => ['required','string', 'regex:/([^(<)(>)])$/u',],
            'nameRu' => ['required','string', 'regex:/([^(<)(>)])$/u',],
            'descriptionEn' => ['required','string',],
            'descriptionRu' => ['required', 'string',],
            'picture' => ['required'],
            'category_id' => ['required', 'numeric',],
        ];
    }
}
