<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['bail','required', 'string', 'regex:/([^(<)(>)])$/u',],
            'author' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'link' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'description' => ['bail', 'nullable', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
