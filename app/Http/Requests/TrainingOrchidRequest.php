<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainingOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'nameRu' => ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'nameEn' =>  ['required', 'string', 'regex:/([^(<)(>)])$/u',],
            'descriptionRu' =>  ['required', 'string',],
            'descriptionEn' => ['required', 'string',],
        ];
    }
}
