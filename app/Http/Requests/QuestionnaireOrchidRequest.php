<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'questionnaire.user_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'questionnaire.country_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'questionnaire.gender' => ['required', 'regex:/([^(<)(>)])$/u',],
            'questionnaire.born_date' => ['required', 'date', 'regex:/([^(<)(>)])$/u',],
            'questionnaire.picture' => ['required',],
        ];
    }
}
