<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'gender' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'country_id' => ['bail', 'required', 'numeric', 'exists:App\Models\Country,id', 'regex:/([^(<)(>)])$/u',],
            'born_date' => ['bail', 'required', 'date', 'before:today', 'regex:/([^(<)(>)])$/u',],
            'picture' => ['bail', 'nullable', 'image',],
        ];
    }
}
