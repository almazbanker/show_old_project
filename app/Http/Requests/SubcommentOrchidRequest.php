<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubcommentOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subcomment.content' => ['required', 'string',],
            'subcomment.user_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
            'subcomment.comment_id' => ['required', 'numeric', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
