<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookOrchidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'book.name' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'book.description' => ['bail', 'required' ,'string',],
            'book.author' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'book.link' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
