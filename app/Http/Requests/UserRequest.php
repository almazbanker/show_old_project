<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'email' => ['bail', 'email', 'required', 'regex:/([^(<)(>)])$/u',],
            'password' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'permissions' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'phone_number' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
