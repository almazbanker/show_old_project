<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'question' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_1' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_2' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_3' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_4' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'variant_true' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
            'level' => ['bail', 'required', 'string', 'regex:/([^(<)(>)])$/u',],
        ];
    }
}
