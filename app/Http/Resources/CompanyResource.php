<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'picture' => asset($this->resource->picture),
            'en' => ['name' => $this->resource->translate('en')->name, 'description' => $this->resource->translate('en')->description],
            'ru' => ['name' => $this->resource->translate('ru')->name, 'description' => $this->resource->translate('ru')->description]
        ];
    }
}
