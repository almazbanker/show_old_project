<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionnaireResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'born_date' => $this->resource->born_date,
            'country_id' => $this->resource->country->id,
            'picture_url' => is_null($this->resource->picture) ? null : asset($this->resource->picture),
            'gender' => $this->resource->gender
        ];
    }
}
