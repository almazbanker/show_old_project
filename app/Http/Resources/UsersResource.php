<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return[
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'email' => $this->resource->email,
            'link' => new LinkResource($this->resource->link),
            'likes' => LikeResource::collection($this->resource->likes),
            'picture' => asset($this->resource->questionnaire->picture) ? asset($this->resource->questionnaire->picture) : null,
        ];
    }
}
