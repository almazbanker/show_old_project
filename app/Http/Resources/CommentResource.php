<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'content' => $this->resource->content,
            'user' => $this->resource->author->name,
            'author_id' => $this->resource->author_id,
            'created_at' => $this->resource->created_at->format('Y-m-d'),
            'subcomments' => SubcommentResource::collection($this->resource->subcomments)
            ];
    }
}
