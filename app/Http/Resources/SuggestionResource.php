<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SuggestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'suggestion' => $this->resource->suggestion,
            'start_date' => $this->resource->start_date,
            'finish_date' => $this->resource->finish_date,
        ];
    }
}
