<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return[
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'email' => $this->resource->email,
            'link' => new LinkResource($this->resource->link),
            'likes' => $this->resource->likes->count(),
            'comments' => CommentResource::collection($this->resource->comments),
            'works' => WorkResource::collection($this->resource->works),
            'universities' => UniversityResource::collection($this->resource->universities),
            'certificates' => CertificateResource::collection($this->resource->certificates),
            'license' => new LicenseResource($this->resource->license),
            'categories' => UserCategoriesResource::collection($this->resource->categories),
            'questionnaire' => new QuestionnaireResource($this->resource->questionnaire),
        ];
    }
}
