<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LinkResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'twitter' => $this->resource->twitter,
            'instagram' => $this->resource->instagram,
            'facebook' => $this->resource->facebook
        ];
    }
}
