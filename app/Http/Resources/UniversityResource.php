<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UniversityResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'start_date' => $this->resource->start_date->format('Y-m-d'),
            'finish_date' => $this->resource->finish_date->format('Y-m-d'),
            'faculty' => $this->resource->faculty,
            'description' => $this->resource->description
        ];
    }
}
