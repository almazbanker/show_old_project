<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {
        return [
            'id' => $this->resource->id,
            'en' => ['question' => $this->resource->translate('en')->question,
                'variant_1' => $this->resource->translate('en')->variant_1,
                'variant_2' => $this->resource->translate('en')->variant_2,
                'variant_3' => $this->resource->translate('en')->variant_3,
                'variant_true' => $this->resource->translate('en')->variant_true,
            ],
            'ru' => ['question' => $this->resource->translate('ru')->question,
                'variant_1' => $this->resource->translate('ru')->variant_1,
                'variant_2' => $this->resource->translate('ru')->variant_2,
                'variant_3' => $this->resource->translate('ru')->variant_3,
                'variant_true' => $this->resource->translate('ru')->variant_true,
            ],
        ];
    }
}

