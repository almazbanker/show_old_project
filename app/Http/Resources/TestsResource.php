<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TestsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'grade' => $this->resource->grade,
            'start_time' => $this->resource->start_time->format('Y-m-d : H:m:s'),
            'finish_time' => $this->resource->finish_time->format('Y-m-d : H:m:s'),
            'level' => $this->resource->level,
            'answers' => AnswersResource::collection($this->resource->answers),
        ];
    }
}
