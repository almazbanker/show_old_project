<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LicenseResource extends JsonResource
{

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {
        return [
            'id' => $this->resource->id,
            'start_date' => $this->resource->start_date,
            'finish_date' => $this->resource->finish_date,
            'number' => $this->resource->number,
            'description' => $this->resource->description,
            'picture' => is_null($this->resource->picture) ? null : asset($this->resource->picture)
        ];
    }
}
