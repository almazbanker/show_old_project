<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AnswersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'question' => $this->resource->question->question,
            'answer' => $this->resource->answer,
            'correct_answer' => $this->resource->correct_answer,
            'correct' => $this->resource->correct,
        ];
    }
}
