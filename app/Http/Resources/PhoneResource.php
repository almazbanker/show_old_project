<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PhoneResource extends JsonResource
{

    public function toArray($request)
    {
        return [
          'id' => $this->resource->id,
          'number' => $this->resource->number,
          'user' => new UserResource($this->resource->user)
        ];
    }
}
