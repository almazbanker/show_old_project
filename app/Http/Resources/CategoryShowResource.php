<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryShowResource extends JsonResource
{

    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'picture' => asset($this->resource->picture),
            'types' => TypeResource::collection($this->resource->types),
            'users' => UsersResource::collection($this->resource->users),
        ];
    }
}
