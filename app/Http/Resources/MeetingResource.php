<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


class MeetingResource extends JsonResource
{

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'user' => $this->resource->user->name,
            'category' => new CategoryResource($this->resource->category),
            'start_url' => $this->resource->start_url,
            'join_url' => $this->resource->join_url,
            'password' => $this->resource->password,
            'start_time' => $this->resource->start_time->format('Y-m-d H:i:s'),
            'meeting_id' => $this->resource->meeting_id,
            'topic' => $this->resource->topic,
        ];
    }
}
