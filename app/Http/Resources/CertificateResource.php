<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CertificateResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'start_date' => $this->resource->start_date->format('Y-m-d'),
            'finish_date' => $this->resource->finish_date->format('Y-m-d'),
            'picture' => asset($this->resource->picture),
            'description' => $this->resource->description,
            'user_id' => $this->resource->user_id,
        ];
    }
}
