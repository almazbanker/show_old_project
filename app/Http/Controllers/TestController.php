<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Category;
use App\Models\Meeting;
use App\Models\Question;
use App\Models\Test;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class TestController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(Request $request)
    {
        $level = $request->input('level');
        if ($level == 2) {
            $meeting = Meeting::findOrFail($request->input('meeting_id'));
            if ($meeting->password != $request->input('password')) {
                return redirect()->back()->with('error', 'Wrong password!');
            }
        }

        $category = Category::findOrFail($request->input('category_id'));

        if (Auth::user()->tests()->where('grade', '!=', null)->where('category_id', $category->id)->count() == 1) {
            return redirect()->route('home')->with('error', 'You can not pass the test twice');
        }
        $this->sessionPutCategoryId($category, $level);

        $test = $this->checkDidUserPassTheTest($category, $level);

        $question1 = Question::findOrFail(session()->get('category ' . $level . $category->id)['question']);
        $id = $question1->id;
        $question = $question1->question;
        $questions = [$question1->variant_1, $question1->variant_2, $question1->variant_3, $question1->variant_true];
        shuffle($questions);

        return view('questions.index', compact('questions', 'id', 'question', 'test', 'category', 'level'));
    }


    /**
     * @param Category $category
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Category $category)
    {
        $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();
        if ($permission && $permission->approved) {
            $permission->delete();
            return view('tests.categories', compact('category'));
        }
        return redirect()->route('trainings.index')->with('error', __('You need read all pages'));
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function store(Request $request): JsonResponse
    {
        $level = $request->input('level');
        $category = Category::findOrFail($request->input('category_id'));
        $questions = $category->questions->where('level', $level)->count();
        $last_question = $category->questions->where('level', $level)->last()->id;
        $test = Test::findOrFail($request->input('test_id'));
        $answer = new Answer();
        $answer->user_id = Auth::id();
        $question = Question::findOrFail(session()->get('category ' . $level . $category->id)['question']);
        $answer->question_id = $question->id;
        $answer->test_id = $request->input('test_id');
        $answer->answer = $request->input('answer');
        $answer->correct_answer = $question->variant_true;
        $session = session()->get('category ' . $level . $category->id);
        if ($request->input('answer') == $question->variant_true) {
            $answer->correct = true;
            if (isset($session['counter'])) {
                $counter = session()->get('category ' . $level . $category->id)['counter'] + 1;
                $session['counter'] = $counter;
            } else {
                $session['counter'] = 1;
            }
            session()->put('category ' . $level . $category->id, $session);
        }
        $answer->save();
        if (strtotime(now()) > (strtotime($test->start_time) + 30) || $last_question == $request->input('question_id')) {
            if (isset($session['counter'])) {
                $grade = (session()->get('category ' . $level . $category->id)['counter'] / $questions) * 100;
                $true = session()->get('category ' . $level . $category->id)['counter'];
            } else {
                $grade = 0;
                $true = 0;
            }
            $test->grade = $grade;
            $test->finish_time = now('UTC');
            $test->update();
            session()->remove('category ' . $level . $category->id);
            session()->regenerate();
            return response()->json(
                [
                    'question' => view('tests.final', ['questions' => $questions, 'grade' => $grade, 'answers' => $true])->render(),
                ]
            );
        }
        $session['question'] = $category->questions->where('level', $level)->where('id', '>', $session['question'])->first()->id;
        session()->put('category ' . $level . $category->id, $session);
        $next_question = Question::findOrFail($session['question']);

        $new_variants = [
            $next_question->variant_1,
            $next_question->variant_2,
            $next_question->variant_3,
            $next_question->variant_true
        ];
        shuffle($new_variants);
        return response()->json(
            [
                'question' => view('questions.question', ['category' => $category, 'test' => $test, 'questions' => $new_variants, 'id' => $next_question->id, 'question' => $next_question->question, 'level' => $level])->render(),
            ]
        );
    }

    /**
     * @param $category
     * @param $level
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function sessionPutCategoryId($category, $level)
    {
        if (!session()->has('category ' . $level . $category->id)) {
            session()->put('category ' . $level . $category->id);
            $session = session()->get('category ' . $level . $category->id);
            $session['question'] = $category->questions->where('level', $level)->first()->id;
            session()->put('category ' . $level . $category->id, $session);
        }
    }

    /**
     * @param $category
     * @param $level
     * @return Test
     */
    private function checkDidUserPassTheTest($category, $level): Test
    {
        if (Auth::user()->tests()->where('category_id', $category->id)->where('level', $level)->first()) {
            $test = Auth::user()->tests()->where('category_id', $category->id)->where('level', $level)->first();
        } else {
            $test = new Test();
            $test->user_id = Auth::id();
            $test->category_id = $category->id;
            $test->start_time = now('UTC');
            $test->level = $level;
            $test->save();
        }

        return $test;
    }
}
