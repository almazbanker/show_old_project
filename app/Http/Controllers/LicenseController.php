<?php

namespace App\Http\Controllers;

use App\Http\Requests\LicenseRequest;
use App\Models\License;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class LicenseController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $user = Auth::user();
        if ($user->license()->count() == 1)
        {
            $license = $user->license;
            return view('licenses.edit', compact('license', 'user'));
        } else {
            return view('licenses.create', compact('user'));
        }
    }

    /**
     * @param LicenseRequest $request
     * @return RedirectResponse
     */
    public function store(LicenseRequest $request): RedirectResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $license = new License($data);
        $license->user_id = Auth::id();
        $license->save();
        Auth::user()->approved = true;
        Auth::user()->save();
        return redirect()->route('companies.index');
    }


    /**
     * @param LicenseRequest $request
     * @param License $license
     * @return RedirectResponse
     */
    public function update(LicenseRequest $request, License $license): RedirectResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $license->update($data);
        return redirect()->route('companies.index');
    }
}
