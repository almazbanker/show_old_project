<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkRequest;
use App\Models\Work;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class WorkController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $works = Auth::user()->works;
        $user = Auth::user();
        return view('works.create', compact('works', 'user'));
    }

    /**
     * @param WorkRequest $request
     * @return JsonResponse
     */
    public function store(WorkRequest $request): JsonResponse
    {
        $work = new Work($request->all());
        $user = Auth::user();
        $work->user_id = Auth::id();
        $work->save();
        return response()->json(
            [
                'work' => view('works.work', ['work' => $work, 'user' => $user])->render(),
            ]
        );
    }

    /**
     * @param WorkRequest $request
     * @param Work $work
     * @return JsonResponse
     */
    public function update(WorkRequest $request, Work $work): JsonResponse
    {
        $work->update($request->all());
        $user = Auth::user();
        return response()->json(
            [
                'work' => view('works.work', ['work' => $work, 'user' => $user])->render(),
            ],
            201
        );
    }

    /**
     * @param Work $work
     * @return JsonResponse
     */
    public function destroy(Work $work): JsonResponse
    {
        $work->delete();
        return response()->json([], 204);
    }
}
