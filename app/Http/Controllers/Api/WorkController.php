<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\WorkRequest;
use App\Http\Resources\WorkResource;
use App\Models\Work;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class WorkController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return WorkResource::collection(Auth::user()->works);
    }

    /**
     * @param WorkRequest $request
     * @return WorkResource
     * @throws AuthorizationException
     */
    public function store(WorkRequest $request): WorkResource
    {
        $this->authorize('create', Work::class);
        $work = new Work();
        $work->name = $request->input('name');
        $work->user_id = Auth::user()->id;
        $work->description = $request->input('description');
        $work->start_date = $request->input('start_date');
        $work->finish_date = $request->input('finish_date');
        $work->save();
        return new WorkResource($work);
    }


    /**
     * @param WorkRequest $request
     * @param Work $work
     * @return WorkResource
     * @throws AuthorizationException
     */
    public function update(WorkRequest $request, Work $work): WorkResource
    {
        $this->authorize('update', $work);
        $work->update($request->all());
        return new WorkResource($work);
    }

    /**
     * @param Work $work
     * @return Application|ResponseFactory|Response
     * @throws AuthorizationException
     */
    public function destroy(Work $work)
    {
        $this->authorize('delete', $work);
        $work->delete();
        return response('', 204);
    }

}
