<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubcommentRequest;
use App\Http\Resources\SubcommentResource;
use App\Models\Comment;
use App\Models\Subcomment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class SubcommentController extends Controller
{
    /**
     * @param Comment $comment
     * @return AnonymousResourceCollection
     */
    public function index(Comment $comment): AnonymousResourceCollection
    {
        return SubcommentResource::collection($comment->subcomments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SubcommentRequest $request
     * @return SubcommentResource
     * @throws AuthorizationException
     */
    public function store(SubcommentRequest $request): SubcommentResource
    {
        $this->authorize('create', Subcomment::class);
        $comment = new Subcomment($request->all());
        $comment->user_id = Auth::id();
        $comment->save();
        return new SubcommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subcomment $subcomment
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Subcomment $subcomment): Response
    {
        $this->authorize('delete', $subcomment);
        $subcomment->delete();
        return response([], 204);
    }
}
