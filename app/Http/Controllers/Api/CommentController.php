<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * @param CommentRequest $request
     * @return CommentResource
     * @throws AuthorizationException
     */
    public function store(CommentRequest $request): CommentResource
    {
        $this->authorize('create', Comment::class);
        $comment = new Comment($request->all());
        $comment->author_id = Auth::id();
        $comment->save();
        return new CommentResource($comment);
    }

    /**
     * @param Comment $comment
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Comment $comment): Response
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return response([], 204);
    }
}
