<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use function auth;

class PassportAuthController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $user = User::create([
            'email' => $request->input('email'),
            'name' => $request->input('name'),
            'password' => Hash::make($request->input('password'))
        ]);

        $token = $user->createToken('authPassportToken')->accessToken;
        return response()->json([
            'token' => $token,
            'name' => $user->name,
            'approved' => $user->approved,
            'id' => $user->id,
            'moderator' => $user->roles()->where('name', 'Moderator')->first()->name ?? false,
            'admin' => $user->roles()->where('name', 'Admin')->first()->name ?? false,
        ]);
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('authPassportToken')->accessToken;
            return response()->json([
                'token' => $token,
                'name' => auth()->user()->name,
                'approved' => auth()->user()->approved,
                'id' => auth()->user()->id,
                'moderator' => auth()->user()->roles()->where('name', 'Moderator')->first()->name ?? false,
                'admin' => auth()->user()->roles()->where('name', 'Admin')->first()->name ?? false,
            ]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
}
