<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UsersResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $users = [];
        $data = User::all()->where('approved', true);
        foreach ($data as $d){
            if ($d->questionnaire){
                if ($d->questionnaire->country['code'] === "996"){
                    $users[] = $d;
                }
            }

        }
        return UsersResource::collection($users);
    }

    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }


    /**
     * @return AnonymousResourceCollection
     */
    public function international(): AnonymousResourceCollection
    {
        $users = [];
        $data = User::all()->where('approved', true);
        foreach ($data as $d){
            if ($d->questionnaire){
                if ($d->questionnaire->country['code'] !== "996"){
                    $users[] = $d;
                }
            }
        }
        return UsersResource::collection($users);
    }



    /**
     * @return AnonymousResourceCollection
     */
    public function topUser(): AnonymousResourceCollection
    {
       $users = User::withCount('likes')
           ->orderBy('likes_count', 'desc')
           ->take(3)
           ->get();
        return UsersResource::collection($users);
    }
}
