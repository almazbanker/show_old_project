<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LicenseRequest;
use App\Http\Resources\LicenseResource;
use App\Http\Resources\LicenseUpdateResource;
use App\Models\License;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LicenseController extends Controller
{
    /**
     * @return LicenseResource|Application|ResponseFactory|Response
     */
    public function index()
    {
        if (Auth::user()->license){
            $license = Auth::user()->license;
            return new LicenseResource($license);
        }else{
            return response([]);
        }

    }

    /**
     * @param LicenseRequest $request
     * @return LicenseResource
     * @throws AuthorizationException
     */
    public function store(LicenseRequest $request): LicenseResource
    {
        $this->authorize('create', License::class);
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $data['picture'] = $this->saveImage($request, 'picture');
        }
        $license = new License($data);
        $license->user_id = $request->user()->id;
        $license->save();
        Auth::user()->save();
        return new LicenseResource($license);
    }


    /**
     * @param LicenseRequest $request
     * @param License $license
     * @return LicenseResource|LicenseUpdateResource
     * @throws AuthorizationException
     */
    public function update(LicenseRequest $request, License $license)
    {
        $this->authorize('update', $license);
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $data['picture'] = $this->saveImage($request, 'picture');
            if ($license->picture) {
                Storage::disk('public')->delete($license->picture);
            }
        }
        $license->update($data);
        if ($request->hasFile('picture')){
            return new LicenseResource($license);
        }else{
            return new LicenseUpdateResource($license);
        }
    }
}
