<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryShowResource;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $categories = Category::paginate(6);
        return CategoryResource::collection($categories);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function getAuthCategories(): AnonymousResourceCollection
    {
        $categories = Auth::user()->categories;
        return CategoryResource::collection($categories);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->all();
        $categories = [];
        foreach ($data as $key => $value) {
            $categories[] = $value;
        }
        Auth::user()->categories()->sync($categories);
        return response()->json('', 201);
    }

    /**
     * @param Category $category
     * @return CategoryShowResource
     */
    public function show(Category $category): CategoryShowResource
    {
        return new CategoryShowResource($category);
    }

    /**
     * @param Category $category
     * @return Application|ResponseFactory|Response
     */
    public function destroy(Category $category)
    {
        Auth::user()->categories()->detach($category->id);
        return response('', 204);
    }
}
