<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\Http\Resources\NewsShowResource;
use App\Models\News;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class NewsController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $news = News::all();
        return NewsResource::collection($news);
    }

    /**
     * @param News $news
     * @return NewsShowResource
     */
    public function show(News $news): NewsShowResource
    {
        return new NewsShowResource($news);
    }
}
