<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UsersResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TopUserController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $top_users = User::select(['id','name', 'email'])
            ->withCount('likes')
            ->orderByDesc('likes_count')
            ->limit(3)
            ->get();
        return UsersResource::collection($top_users);
    }
}
