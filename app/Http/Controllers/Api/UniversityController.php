<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UniversityRequest;
use App\Http\Resources\UniversityResource;
use App\Models\University;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UniversityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $universities = Auth::user()->universities;
        return UniversityResource::collection($universities);
    }

    /**
     * @param UniversityRequest $request
     * @return UniversityResource
     * @throws AuthorizationException
     */
    public function store(UniversityRequest $request): UniversityResource
    {
        $this->authorize('create', University::class);
        $university = new University($request->all());
        $university->user_id = Auth::user()->id;
        $university->save();
        return new UniversityResource($university);
    }

    /**
     * @param UniversityRequest $request
     * @param University $university
     * @return UniversityResource
     * @throws AuthorizationException
     */
    public function update(UniversityRequest $request, University $university): UniversityResource
    {
        $this->authorize('update', $university);
        $university->update($request->all());
        return new UniversityResource($university);
    }

    /**
     * @param University $university
     * @return Application|ResponseFactory|Response
     * @throws AuthorizationException
     */
    public function destroy(University $university)
    {
        $this->authorize('delete', $university);
        $university->delete();
        return response([], 204);
    }
}
