<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vote;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function store(Request $request)
    {
        $vote = new Vote($request->all());
        $vote->user_id = Auth::id();
        $vote->save();
        return response([],201);
    }


    /**
     * @param Vote $vote
     * @return Application|ResponseFactory|Response
     */
    public function destroy(Vote $vote)
    {
        $vote->delete();
        return response([],204);
    }
}
