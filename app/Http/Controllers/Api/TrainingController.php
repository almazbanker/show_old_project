<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TrainingResource;
use App\Models\Category;
use App\Models\Permission;
use App\Models\Training;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return TrainingResource
     */
    public function index(Request $request): TrainingResource
    {
        $category = Category::findOrFail($request->get('category_id'));
        $permission = $this->createOrFindPermission($category);
        $training = Training::findOrFail($permission->training_id);
        return new TrainingResource($training);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return TrainingResource|JsonResponse
     */
    public function next(Request $request)
    {
        $category = Category::findOrFail($request->get('category_id'));
        $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();
        if ($category->trainings->last()->id == $request->input('training_id')) {
            $permission->approved = true;
            $permission->save();
            return response()->json(['data' => 'last']);
        }
        $training = $category->trainings->where('id', '>', $request->input('training_id'))->first();
        $permission->training_id = $training->id;
        $permission->save();
        return new TrainingResource($training);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return TrainingResource
     */
    public function previous(Request $request): TrainingResource
    {
        $category = Category::findOrFail($request->get('category_id'));
        $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();
        if ($category->trainings->first()->id === $request->input('training_id')) {
            $training = $category->trainings->first();
            return new TrainingResource($training);
          }
        $training = $category->trainings->where('id', '<', $request->input('training_id'))->first();
        $permission->training_id = $training->id;
        $permission->save();
        return new TrainingResource($training);
    }

    /**
     * @param $category
     * @return Permission
     */
    private function createOrFindPermission($category): Permission
    {
        if (Auth::user()->permissions()->where('category_id', $category->id)->first()) {
            $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();
        } else {
            $permission = new Permission();
            $permission->category_id = $category->id;
            $permission->training_id = $category->trainings->first()->id;
            $permission->user_id = Auth::id();
            $permission->save();
        }

        return $permission;
    }
}
