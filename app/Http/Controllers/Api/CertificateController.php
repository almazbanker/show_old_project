<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CertificateRequest;
use App\Http\Resources\CertificateResource;
use App\Models\Certificate;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CertificateController extends Controller
{

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $certificates = Auth::user()->certificates;
        return CertificateResource::collection($certificates);
    }


    /**
     * @param CertificateRequest $request
     * @return CertificateResource
     * @throws AuthorizationException
     */
    public function store(CertificateRequest $request): CertificateResource
    {
        $this->authorize('create', Certificate::class);
        $certificate = new Certificate($request->all());
        $certificate->user_id = Auth::user()->id;
        $certificate->save();
        return new CertificateResource($certificate);
    }


    /**
     * @param CertificateRequest $request
     * @param Certificate $certificate
     * @return CertificateResource
     * @throws AuthorizationException
     */
    public function update(CertificateRequest $request, Certificate $certificate): CertificateResource
    {
        $this->authorize('update', $certificate);
        $certificate->update($request->all());
        return new CertificateResource($certificate);
    }


    /**
     * @param Certificate $certificate
     * @return Application|ResponseFactory|Response
     * @throws AuthorizationException
     */
    public function destroy(Certificate $certificate)
    {
        $this->authorize('delete', $certificate);
        $certificate->delete();
        return response([], 204);
    }
}
