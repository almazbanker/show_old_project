<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PartnerResource;
use App\Models\Partner;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PartnerController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index():AnonymousResourceCollection
    {
        $partners = Partner::all();
        return PartnerResource::collection($partners);
    }

    /**
     * @param Partner $partner
     * @return PartnerResource
     */
    public function show(Partner $partner):PartnerResource
    {
        return new PartnerResource($partner);
    }
}
