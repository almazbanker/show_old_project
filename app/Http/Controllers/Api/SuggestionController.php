<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VoteRequest;
use App\Http\Resources\SuggestionResource;
use App\Models\Suggestion;
use App\Models\User;
use App\Models\Vote;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class SuggestionController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index(): AnonymousResourceCollection
    {
        $this->authorize('viewAny', User::class);
        $suggestion = Suggestion::all();
        return SuggestionResource::collection($suggestion);
    }

    /**
     * @param VoteRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function store(VoteRequest $request)
    {
        $vote = new Vote($request->all());
        $vote->user_id = Auth::id();
        $vote->save();
        return response(['id' => $vote->suggestion_id],201);
    }

    /**
     * @param Suggestion $suggestion
     * @return Application|ResponseFactory|Response
     * @throws AuthorizationException
     */
    public function destroy(Suggestion $suggestion)
    {
        $this->authorize('viewAny', User::class);
        $vote = Auth::user()->votes->where('suggestion_id', $suggestion->id)->first();
        $vote->delete();
        return response([],204);
    }

    /**
     * @return JsonResponse
     */
    public function getUser(): JsonResponse
    {
        $votes = Auth::user()->votes;
        $new_votes = [];
        foreach ($votes as $vote) {
            $new_votes[] = $vote->suggestion_id;
        }
        return response()->json([
            'votes' => $new_votes,
        ]);
    }
}
