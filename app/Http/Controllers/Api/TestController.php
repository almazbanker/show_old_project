<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Category;
use App\Models\Question;
use App\Models\Test;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function first(Request $request): JsonResponse
    {
        $level = $request->input('level');
        $category = Category::findOrFail($request->input('category_id'));

        if (Auth::user()->tests()->where('grade', '!=', null)->where('category_id', $category->id)->count() == 1) {
            return response()->json(
                [
                   'data' => 'error'
                ]);
        }
        $test = $this->createOrFindUsersTest($category, $level);

        $question = $category->questions->where('level', $level)->first();
        $variants = [$question->variant_1, $question->variant_2, $question->variant_3, $question->variant_true];
        shuffle($variants);
        return response()->json(
            [
                'variants' => $variants,
                'question_id' => $question->id,
                'question' => $question->question,
                'test_id' => $test->id,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $level = $request->input('level');
        $category = Category::findOrFail($request->input('category_id'));
        $test = Test::findOrFail($request->input('test_id'));
        $question = Question::findOrFail($request->input('question_id'));

        $this->saveAnswer($request->input('answer'), $test, $question);

        $last_question = $category->questions->where('level', $level)->last()->id;
        if (strtotime(now()) > (strtotime($test->start_time) + 110) || $last_question == $request->input('question_id')) {
            $questions = $category->questions->where('level', $level)->count();
            $grade = ($test->true / $questions) * 100;
            $test->grade = $grade;
            $test->finish_time = now('UTC');
            $test->update();
            return response()->json(['data' => 'finish']);
        }

        $next_question = Question::findOrFail($category->questions
            ->where('level', $level)
            ->where('id', '>', $question->id)
            ->first()->id);

        $new_variants = [
            $next_question->variant_1,
            $next_question->variant_2,
            $next_question->variant_3,
            $next_question->variant_true
        ];
        shuffle($new_variants);

        return response()->json(
            [
                'variants' => $new_variants,
                'question_id' => $next_question->id,
                'question' => $next_question->question,
                'test_id' => $test->id
            ]);

    }

    /**
     * @param $users_answer
     * @param $test
     * @param $question
     * @return void
     */
    private function saveAnswer($users_answer, $test, $question)
    {
        $answer = new Answer();
        $answer->user_id = Auth::id();
        $answer->question_id = $question->id;
        $answer->test_id = $test->id;
        $answer->answer = $users_answer;
        $answer->correct_answer = $question->variant_true;
        if ($users_answer == $question->variant_true) {
            $answer->correct = true;
            $test->true += 1;
            $test->save();
        }
        $answer->save();
    }


    /**
     * @param $category
     * @param $level
     * @return Test
     */
    private function createOrFindUsersTest($category, $level): Test
    {
        if (Auth::user()->tests()->where('category_id', $category->id)->where('level', $level)->first()) {
            $test = Auth::user()->tests()->where('category_id', $category->id)->where('level', $level)->first();
        } else {
            $test = new Test();
            $test->user_id = Auth::id();
            $test->category_id = $category->id;
            $test->start_time = now('UTC');
            $test->level = $level;
            $test->save();
        }
        return $test;
    }
}
