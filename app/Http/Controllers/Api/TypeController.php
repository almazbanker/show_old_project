<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TypeShowResource;
use App\Models\Type;

class TypeController extends Controller
{
    /**
     * @param Type $type
     * @return TypeShowResource
     */
    public function show(Type $type): TypeShowResource
    {
        return new TypeShowResource($type);
    }
}
