<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MeetingResource;
use App\Http\Traits\Zoom;
use App\Models\Meeting;
use App\Models\User;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MeetingController extends Controller
{
    use Zoom;

    /**
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index(): AnonymousResourceCollection
    {
        $this->authorize('viewAny', User::class);
        $meetings = Meeting::all();
        return MeetingResource::collection($meetings);
    }


    /**
     * @param Request $request
     * @return MeetingResource
     * @throws AuthorizationException
     * @throws GuzzleException
     */
    public function store(Request $request):MeetingResource
    {
        $this->authorize('create', Meeting::class );
        $data = $this->create($request->all());
        $meeting = new Meeting();
        $meeting->start_time = $data['data']['start_time'];
        $meeting->start_url = $data['data']['start_url'];
        $meeting->join_url = $data['data']['join_url'];
        $meeting->topic = $data['data']['topic'];
        $meeting->user_id = Auth::id();
        $meeting->password = rand(10000, 200000);
        $meeting->category_id = $request->input('category_id');
        $meeting->meeting_id = $data['data']['id'];
        $meeting->save();
        return new MeetingResource($meeting);
    }


    /**
     * @param Meeting $meeting
     * @return Application|ResponseFactory|Response
     * @throws AuthorizationException
     */
    public function destroy(Meeting $meeting)
    {
        $this->authorize('delete', Meeting::class);
        $this->delete($meeting->meeting_id);
        $meeting->delete();
        return response([], 204);
    }
}
