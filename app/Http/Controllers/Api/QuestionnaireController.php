<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionnaireRequest;
use App\Http\Resources\QuestionnaireResource;
use App\Models\Questionnaire;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class QuestionnaireController extends Controller
{
    /**
     * @return QuestionnaireResource
     */
    public function index(): QuestionnaireResource
    {
        $questionnaire = Auth::user()->questionnaire;
        return new QuestionnaireResource($questionnaire);
    }

    /**
     * @param QuestionnaireRequest $request
     * @return QuestionnaireResource
     */
    public function store(QuestionnaireRequest $request): QuestionnaireResource
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $data['picture'] = self::saveImage($request, 'picture');
        }
        $questionnaire = new Questionnaire($data);
        $questionnaire->user_id = $request->user()->id;
        $questionnaire->save();
        return new QuestionnaireResource($questionnaire);
    }

    /**
     * @param QuestionnaireRequest $request
     * @param Questionnaire $questionnaire
     * @return QuestionnaireResource
     */
    public function update(QuestionnaireRequest $request, Questionnaire $questionnaire): QuestionnaireResource
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $data['picture'] = self::saveImage($request, 'picture');
            if ($questionnaire->picture) {
                Storage::disk('public')->delete($questionnaire->picture);
            }
        }
        $questionnaire->update($data);
        return new QuestionnaireResource($questionnaire);
    }
}
