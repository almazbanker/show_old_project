<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Callback;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CallbackController extends Controller
{
    /**
     * @param Request $request
     * @param User|null $user
     * @return JsonResponse
     */
    public function store(Request $request, ?User $user): JsonResponse
    {
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'phone_number' => ['required'],
            'description' => ['min:3', 'max:255']
        ]);
        $callback = new Callback($request->all());
        if($user) {
            $callback->user_id = $user->id;
        }
        $callback->save();
        return response()->json([
            'success' => 'successfully',
        ]);
    }

}
