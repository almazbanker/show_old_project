<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Like;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|numeric'
        ]);
        $this->authorize('create', Like::class);
        $exist_like = Auth::user()->subscribers->where('user_id', $request->get('user_id'))->first();
        if($exist_like) {
            $exist_like->delete();
            return response([], 200);
        }
        $like = new Like($request->all());
        $like->author_id = Auth::id();
        $like->save();
        return response(['data' => 'success'], 201);
    }
}
