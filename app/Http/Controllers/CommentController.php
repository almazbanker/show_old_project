<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth')->only('store');
    }

    /**
     * @param CommentRequest $request
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(CommentRequest $request, User $user): JsonResponse
    {
        $this->authorize('restore', $user);
        $data = $request->all();
        $data['author_id'] = Auth::id();
        $data['user_id'] = $user->id;
        $comment = Comment::create($data);

        return response()->json(
            [
                'comment' => view('comments.comment', compact('comment'))->render()
            ]
        );
    }

    /**
     * @param Comment $comment
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Comment $comment): JsonResponse
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return response()->json(['message' => 'Comment successfully deleted!'], 204);
    }
}
