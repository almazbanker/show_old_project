<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $exist_like = Auth::user()->subscribers->where('user_id', $request->input('user_id'))->first();
        if($exist_like) {
            return redirect()->back()->with('error', 'You already liked it');
        }
        $like = new Like($request->all());
        $like->author_id = Auth::id();
        $like->save();
        return redirect()->back()->with('success', 'You successfully liked');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request): RedirectResponse
    {
        $like = Auth::user()->subscribers->where('user_id', $request->input('user_id'))->first();
        if($like) {
            $like->delete();
            return redirect()->back()->with('success', 'Your like deleted');
        }
        return redirect()->back()->with('error', 'Your didnt like it');
    }
}
