<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class TypeController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param Type $type
     * @return Application|Factory|View
     */
    public function show(Type $type)
    {
        return view('types.show', compact('type'));
    }
}
