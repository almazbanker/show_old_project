<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @param $value
     * @return string
     */
    public function saveImage(Request $request, $value): string
    {
        $generator = config('platform.attachment.generator');
        $engine = new $generator($request->file($value));
        $storage = Storage::disk(config('platform.attachment.disk'));
        if (!$storage->has($engine->path())){
            $storage->makeDirectory($engine->path());
        }
        return '/storage/' . $request->file($value)->store($engine->path(), 'public');
    }
}
