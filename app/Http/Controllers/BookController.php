<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class BookController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $books = Book::all();
        return view('books.index', compact('books'));
    }
}
