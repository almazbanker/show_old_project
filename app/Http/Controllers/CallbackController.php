<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallbackRequest;
use App\Models\Callback;
use App\Models\User;
use Illuminate\Http\RedirectResponse;

class CallbackController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param CallbackRequest $request
     * @param User|null $user
     * @return RedirectResponse
     */
    public function store(CallbackRequest $request, ?User $user): RedirectResponse
    {
        $callback = new Callback($request->all());
        if($user) {
            $callback->user_id = $user->id;
        }
        $callback->save();
        return redirect()->back()->with('success', __('Successfully created'));
    }

}
