<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Subcomment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubcommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth')->only('store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $comment = Comment::find($request->input('comment_id'));
        $subcomment = new Subcomment($request->all());
        $subcomment->user_id = Auth::id();
        $subcomment->save();
        return response()->json(
            [
                'subcomment' => view('subcomments.subcomment', ['subcomment' => $subcomment, 'comment' => $comment])->render()
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subcomment $subcomment
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Subcomment $subcomment): JsonResponse
    {
        $this->authorize('delete', $subcomment);
        $subcomment->delete();
        return response()->json([], 204);
    }
}
