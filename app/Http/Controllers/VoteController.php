<?php

namespace App\Http\Controllers;

use App\Models\Vote;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $vote = new Vote($request->all());
        $vote->user_id = Auth::id();
        $vote->save();
        return redirect()->back()->with('success', __('Successfully created'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vote $vote
     * @return RedirectResponse
     */
    public function destroy(Vote $vote): RedirectResponse
    {
       $vote->delete();
       return redirect()->back()->with('success', __('Successfully deleted'));
    }
}
