<?php

namespace App\Http\Controllers;

use App\Models\Meeting;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory as FactoryAlias;
use Illuminate\Contracts\View\View;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|FactoryAlias|View
     */
    public function index()
    {
        $meetings = Meeting::all();
        return view('meetings.index', compact('meetings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|FactoryAlias|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', User::class);
        $users = User::all();
        return view('meetings.create', compact('users'));
    }
}
