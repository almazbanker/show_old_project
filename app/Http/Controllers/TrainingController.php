<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Permission;
use App\Models\Training;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrainingController extends Controller
{
    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', User::class);
        $categories = Category::all();
        return view('trainings.index', compact('categories'));
    }

    /**
     * @param Category $category
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Category $category)
    {
        $this->authorize('view', Auth::user());

        $permission = self::createOrFindPermission($category);

        $training = Training::findOrFail($permission->training_id);

        return view('trainings.show', compact('training', 'category'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     * @throws AuthorizationException
     */
    public function showNext(Request $request)
    {
        $this->authorize('view', Auth::user());
        $category = Category::find($request->input('category_id'));

        $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();

        if ($category->trainings->last()->id == $request->input('training_id')) {
            $permission->approved = true;
            $permission->save();
            return redirect()->route('tests.create', ['category' => $category]);
        }
        $training = $category->trainings->where('id', '>', $request->input('training_id'))->first();
        $permission->training_id = $training->id;
        $permission->save();
        return view('trainings.show', compact('training', 'category'));
    }


    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     * @throws AuthorizationException
     */
    public function showPrevious(Request $request)
    {
        $this->authorize('view', Auth::user());
        $category = Category::find($request->input('category_id'));
        $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();
        if ($category->trainings->first()->id == $request->input('training_id')) {
            $training = $category->trainings->first();
            return redirect()->route('trainings.show', compact('training', 'category'))->with('error', 'You are on the first page');
        }
        $training = $category->trainings->where('id', '<', $request->input('training_id'))->first();
        $permission->training_id = $training->id;
        $permission->save();
        return view('trainings.show', compact('training', 'category'));
    }

    /**
     * @param $category
     * @return Permission
     */
    private function createOrFindPermission($category): Permission
    {
        if (Auth::user()->permissions()->where('category_id', $category->id)->first()) {
            $permission = Auth::user()->permissions()->where('category_id', $category->id)->first();
        } else {
            $permission = new Permission();
            $permission->category_id = $category->id;
            $permission->training_id = $category->trainings->first()->id;
            $permission->user_id = Auth::id();
            $permission->save();
        }

        return $permission;
    }
}
