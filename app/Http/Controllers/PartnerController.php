<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class PartnerController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $partners = Partner::all();
        return view('partners.index', compact('partners'));
    }
}
