<?php

namespace App\Http\Controllers;

use App\Http\Requests\UniversityRequest;
use App\Models\University;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UniversityController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $universities = Auth::user()->universities;
        return view('universities.create', compact('universities'));
    }

    /**
     * @param UniversityRequest $request
     * @return JsonResponse
     */
    public function store(UniversityRequest $request): JsonResponse
    {
        $university = new University($request->all());
        $university->user_id = Auth::id();
        $university->save();
        return response()->json(
            [
                'university' => view('universities.university', ['university' => $university])->render(),
            ]
        );
    }


    /**
     * @param University $university
     * @param UniversityRequest $request
     * @return JsonResponse
     */
    public function edit(University $university, UniversityRequest $request): JsonResponse
    {
        $university->update($request->all());
        return response()->json(
            [
                'university' => view('universities.university', ['university' => $university])->render(),
            ],
            201
        );
    }


    /**
     * @param UniversityRequest $request
     * @param University $university
     * @return JsonResponse
     */
    public function update(UniversityRequest $request, University $university): JsonResponse
    {
        $university->update($request->all());
        return response()->json(
            [
                'university' => view('universities.university', ['university' => $university])->render(),
            ]
        );
    }

    /**
     * @param University $university
     * @return JsonResponse
     */
    public function destroy(University $university): JsonResponse
    {
        $university->delete();
        return response()->json([], 204);
    }
}
