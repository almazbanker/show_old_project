<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionnaireRequest;
use App\Models\Country;
use App\Models\Questionnaire;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class QuestionnaireController extends Controller
{
    public function __construct()
    {
        return $this->middleware('verified');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $countries = Country::all();
        $user = Auth::user();

        if ($user->questionnaire()->count() == 1) {
            $questionnaire = Auth::user()->questionnaire;
            return view('questionnaires.edit', compact('countries', 'questionnaire', 'user'));
        } else {
            return view('questionnaires.create', compact('countries', 'user'));
        }
    }

    /**
     * @param QuestionnaireRequest $request
     * @return RedirectResponse
     */
    public function store(QuestionnaireRequest $request): RedirectResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $questionnaire = new Questionnaire($data);
        $questionnaire->user_id = Auth::id();
        $questionnaire->save();
        return redirect()->route('categories.create');
    }

    /**
     * @param QuestionnaireRequest $request
     * @param Questionnaire $questionnaire
     * @return RedirectResponse
     */
    public function update(QuestionnaireRequest $request, Questionnaire $questionnaire): RedirectResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $questionnaire->update($data);
        return redirect()->route('categories.create');
    }

}
