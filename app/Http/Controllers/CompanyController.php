<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CompanyController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $company = Company::first();
        return view('companies.index', compact('company'));
    }

}
