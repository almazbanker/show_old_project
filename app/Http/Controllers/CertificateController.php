<?php

namespace App\Http\Controllers;

use App\Http\Requests\CertificateRequest;
use App\Models\Certificate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CertificateController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $certificates = Auth::user()->certificates;
        return view('certificates.create', compact('certificates'));
    }

    /**
     * @param CertificateRequest $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $data['user_id'] = Auth::id();
        $certificate = Certificate::create($data);
        return response()->json(
            [
                'certificate' => view('certificates.certificate', ['certificate' => $certificate])->render(),
            ],
            201
        );
    }

    /**
     * @param Request $request
     * @param Certificate $certificate
     * @return JsonResponse
     */
    public function update(Request $request, Certificate $certificate): JsonResponse
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $path = $request->file('picture')->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $certificate->update($data);

        return response()->json(
            [
                'certificate' => view('certificates.certificate', ['certificate' => $certificate])->render(),
            ],
            201
        );

    }

    /**
     * @param Certificate $certificate
     * @return JsonResponse
     */
    public function destroy(Certificate $certificate): JsonResponse
    {
        $certificate->delete();
        return response()->json([], 204);
    }
}
