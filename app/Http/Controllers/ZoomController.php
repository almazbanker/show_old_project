<?php

namespace App\Http\Controllers;

use App\Http\Traits\Zoom;
use App\Models\Meeting;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ZoomController extends Controller
{
    use Zoom;

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function store(Request $request): RedirectResponse
    {
        $data = $this->create($request->all());
        $meeting = new Meeting();
        $meeting->start_time = $data['data']['start_time'];
        $meeting->start_url = $data['data']['start_url'];
        $meeting->join_url = $data['data']['join_url'];
        $meeting->user_id = Auth::id();
        $meeting->password = rand(10000, 200000);
        $meeting->category_id = $request->input('category_id');
        $meeting->meeting_id = $data['data']['id'];
        $meeting->save();
        return redirect()->route('meetings.index');
    }

    /**
     * @param Meeting $meeting
     * @return RedirectResponse
     */
    public function destroy(Meeting $meeting): RedirectResponse
    {
        $this->delete($meeting->meeting_id);
        $meeting->delete();
        return redirect()->route('meetings.index');
    }
}
