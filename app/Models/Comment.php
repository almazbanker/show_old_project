<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static create(array $all)
 * @method static findOrFail($get)
 * @method static orderBy(string $string)
 * @method static find(mixed $input)
 */
class Comment extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['user_id', 'content', 'author_id'];


    /**
     * @return BelongsTo
     */
    public function  user():BelongsTo
    {
        return $this->belongsTo(User:: class);
    }

    /**
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function subcomments(): HasMany
    {
        return $this->hasMany(Subcomment::class);
    }
}
