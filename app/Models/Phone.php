<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 * @method static paginate()
 * @method static findOrFail(mixed $get)
 */
class Phone extends Model
{
    use HasFactory;
    use AsSource;

    protected $fillable = ['number', 'user_id'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
