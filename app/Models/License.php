<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class License extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $fillable = ['user_id', 'start_date', 'finish_date', 'number', 'description', 'picture'];


    /**
     * @return BelongsTo
     */
    public function  user():BelongsTo
    {
        return $this->belongsTo(User:: class);
    }

}
