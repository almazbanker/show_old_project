<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 * @method static findOrFail(mixed $get)
 * @method static orderByDesc(string $string)
 */
class Type extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */

    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['category_id', 'picture'];


    /**
     * @return BelongsTo
     */
    public function category():BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
