<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 * @method static paginate()
 * @method static findOrFail(mixed $get)
 */
class Book extends Model
{
    use HasFactory;
    use AsSource;

    protected $fillable = ['name', 'author', 'link'];
}
