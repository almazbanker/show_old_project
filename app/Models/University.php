<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 * @method static orderBy(string $string)
 * @method static findOrFail(mixed $get)
 */
class University extends Model
{
    use HasFactory;
    use AsSource;

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'start_date', 'finish_date', 'faculty', 'description', 'user_id'];
    protected $casts = [
        'start_date' => 'datetime',
        'finish_date' => 'datetime',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
