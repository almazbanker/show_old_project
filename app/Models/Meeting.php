<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static findOrFail(mixed $input)
 */
class Meeting extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'start_time', 'start_url', 'join_url', 'meeting_id'];

    protected $casts = [
        'start_time' => 'datetime',
    ];
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
