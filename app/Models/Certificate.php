<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 */
class Certificate extends Model
{
    use HasFactory;
    use AsSource;

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'start_date', 'finish_date', 'picture', 'description', 'user_id'];
    protected $casts = [
        'start_date' => 'datetime:Y-m-d',
        'finish_date' => 'datetime:Y-m-d',
    ];

    /**
     * @return BelongsTo
     */
    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
