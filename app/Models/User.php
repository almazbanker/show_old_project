<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Orchid\Platform\Models\User as Authenticatable;

/**
 * @method static withCount()
 * @method static create(array $array)
 * @method static select(string[] $array)
 * @method static where(string $string, $id)
 * @method static paginate(int $int)
 */
class User extends Authenticatable implements MustVerifyEmail
{

    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'permissions',
        'phone_number'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'permissions',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'permissions' => 'array',
        'email_verified_at' => 'datetime',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)->latest();
    }

    /**
     * @return HasMany
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(Comment::class)->latest();
    }


    /**
     * @return HasOne
     */
    public function link(): HasOne
    {
        return $this->hasOne(Link::class);
    }

    public function callbacks()
    {
        return $this->hasMany(Callback::class);
    }


    /**
     * @return HasMany
     */
    public function certificates(): HasMany
    {
        return $this->hasMany(Certificate::class);
    }


    /**
     * @return HasMany
     */
    public function works(): HasMany
    {
        return $this->hasMany(Work::class);
    }

    /**
     * @return HasMany
     */
    public function universities(): HasMany
    {
        return $this->hasMany(University::class);
    }


    /**
     * @return HasOne
     */
    public function license(): HasOne
    {
        return $this->hasOne(License::class);
    }


    /**
     * @return HasMany
     */
    public function tests(): HasMany
    {
        return $this->hasMany(Test::class);
    }

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class)->using(CategoryUser::class);
    }

    /**
     * @return HasOne
     */
    public function questionnaire(): HasOne
    {
        return $this->hasOne(Questionnaire::class);
    }

    /**
     * @return HasMany
     */
    public function subcomments(): HasMany
    {
        return $this->hasMany(Subcomment::class);
    }

    /**
     * @return HasMany
     */
    public function meetings(): HasMany
    {
        return $this->hasMany(Meeting::class);
    }

    /**
     * @return HasMany
     */
    public function likes()
    {
        return $this->hasMany(Like::class,'user_id');
    }

    /**
     * @return HasMany
     */
    public function subscribers()
    {
        return $this->hasMany(Like::class,'author_id');
    }

    /**
     * @return HasMany
     */
    public function votes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Vote::class);
    }

    /**
     * @return HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class);
    }
//    /**
//     * @param Notification $notification
//     * @return mixed
//     */
//    public function routeNotificationForNexmo(Notification $notification)
//    {
//        return $this->phone_number;
//    }
}
