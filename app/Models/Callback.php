<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Orchid\Screen\AsSource;

/**
 * @method static orderBy(string $string)
 * @method static findOrFail(mixed $get)
 */
class Callback extends Model
{
    use HasFactory;
    use AsSource;
    protected $fillable = ['name', 'description', 'phone_number', 'user_id'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
