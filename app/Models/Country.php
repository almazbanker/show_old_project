<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Orchid\Screen\AsSource;

class Country extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use AsSource;

    /**
     * @var array|string[]
     */
    public $translatedAttributes = ['name'];

    /**
     * @var string[]
     */
    protected $fillable = ['code'];


    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];



    /**
     * @return HasMany
     */
    public function questionnaires(): HasMany
    {
        return $this->hasMany(Questionnaire::class);
    }
}
