<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 * @method static findOrFail(mixed $input)
 * @method static paginate(int $int)
 * @method static find(mixed $input)
 */
class Category extends Model implements TranslatableContract
{
    use HasFactory, Translatable;
    use AsSource;
    use Filterable;

    /**
     * @var array|string[]
     */
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['picture'];


    /**
     * @return HasMany
     */
    public function types():HasMany
    {
        return $this->hasMany(Type::class);
    }

    /**
     * @return HasMany
     */
    public function tests(): HasMany
    {
        return $this->hasMany(Test::class);
    }


    /**
     * @return BelongsToMany
     */
    public function users():BelongsToMany
    {
        return  $this->belongsToMany(User::class)->using(CategoryUser::class);
    }

    /**
     * @return HasMany
     */
    public function trainings(): HasMany
    {
        return $this->hasMany(Training::class);
    }

    /**
     * @return HasMany
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class);
    }


    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class);
    }

    /**
     * @return HasMany
     */
    public function meetings(): HasMany
    {
        return $this->hasMany(Meeting::class);
    }
}
