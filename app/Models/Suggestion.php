<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static orderBy(string $string)
 * @method static findOrFail(mixed $get)
 */
class Suggestion extends Model
{
    use HasFactory;

    protected $fillable = ['suggestion', 'start_date', 'finish_date'];

    public function votes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Vote::class);
    }
}
