<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionTranslation extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['question', 'variant_1', 'variant_2', 'variant_3', 'variant_true'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
