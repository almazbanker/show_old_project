<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * @method static orderBy(string $string)
 * @method static findOrFail(mixed $get)
 */
class News extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use AsSource;
    use Filterable;

    public array $translatedAttributes = ['title', 'description'];

    protected $fillable = ['picture'];
}
