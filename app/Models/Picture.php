<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    use HasFactory;

    protected $fillable = ['picture', 'company_id'];


    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
