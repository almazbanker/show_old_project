<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Orchid\Screen\AsSource;

/**
 * @method static create(array $all)
 * @method static paginate()
 */
class Work extends Model
{
    use HasFactory;
    use AsSource;

    /**
     * @var string[]
     */
    protected $fillable = ['user_id', 'start_date', 'finish_date', 'name', 'description'];

    protected $casts = [
        'start_date' => 'datetime:Y-m-d',
        'finish_date' => 'datetime:Y-m-d',
    ];
    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
