<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Orchid\Screen\AsSource;

/**
 * @method static paginate()
 * @method static findOrFail(mixed $question)
 */
class Question extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use AsSource;

    /**
     * @var array|string[]
     */
    public array $translatedAttributes = ['question', 'variant_1', 'variant_2', 'variant_3', 'variant_true'];

    /**
     * @var string[]
     */
    protected $fillable = ['level', 'category_id'];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function scopeCategoryQuestion($query, $category)
    {
        return $query->where('category_id', $category->id);
    }
}
