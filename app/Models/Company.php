<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Orchid\Screen\AsSource;

/**
 * @method static first()
 */
class Company extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use AsSource;

    /**
     * @var array|string[]
     */
    public array $translatedAttributes = ['name', 'description'];

    /**
     * @var string[]
     */
    protected $fillable = ['logo'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    /**
     * @return HasMany
     */
    public function pictures(): HasMany
    {
        return $this->hasMany(Picture::class);
    }
}
