<?php

namespace App\Policies;

use App\Models\User;
use App\Models\University;
use Illuminate\Auth\Access\HandlesAuthorization;

class UniversityPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param University $university
     * @return bool
     */
    public function delete(User $user, University $university): bool
    {
        return $user->id == $university->user->id;
    }

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user)
    {
        return $user->id;
    }
    public function update(User $user, University $university)
    {
        return $user->id == $university->user->id;
    }
}
